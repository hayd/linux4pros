# Wikis

## Entstehung

- Wiki hawaiisch für **„schnell“** - Lesen **und** Schreiben im Browser => jeder kann schnell etwas schreiben, ändern
- Die ersten Wikis wurden Mitte der 1990er Jahre von Software-Designern zur Produktverwaltung in IT-Projekten entwickelt.
- Wichtiges Tool für die Zusammenarbeit von weltweit verstreuten Entwicklern.
- GitHub, GitLab enthalten Wikis


## Aufbau

- LAMP (= **L**inux, **A**pache, **M**ySQL, **P**HP/**P**erl/**P**ython)
- **versionsverwaltete** Seiten
  - Jeder darf mitmachen, da alles reversibel ist.
- **einfache Syntax**, z.B. [TWiki/foswiki](https://twiki.org/cgi-bin/view/TWiki/TextFormattingRules)
- Seiten sehr leicht zu **vernetzen** - CamelCase
- leistungsfähige **Suche** ([Apache Lucene](https://de.wikipedia.org/wiki/Apache_Lucene))
- heute favorisiert: Markdown, git, Python


## Gründe für ein Wiki

- Man muss unbedingt das **one-webmaster-syndrome** (nur der eine Web-Guru kann Inhalte verändern, wenn er dann mal Zeit hat) vermeiden.
- Die Technik ist so **einfach**, dass der Wissende, die Inhalte selbst einstellen kann.
- **Rechte** pro Web (Gruppe von Webseiten) definierbar
- **Workflow** (Schreiben - Kontrollieren - Freigeben) realisierbar durch schrittweise Rechteausdehnung
- durchsuchbare, zentrale Dokumenten- und Informationssammlung-**Sammlung**
- **Versionierung** mit Aufzeichnung der Änderungen (wer, was, wann), daher praktisch kein Missbrauch
- kein Ping-Pong mit (Word-) Dokumenten
- ideal für **Collaboration**, **Wissenstransfer**


## Wiki-Software

### Enterprise-Wikis

mit Enterprise-Features:

- erweiterbar (durch Plug-Ins)
- multiuserfähig
- LDAP-Anbindung (zentrale User-Verwaltung), [SSO](https://de.wikipedia.org/wiki/Single_Sign-on)


#### foswiki

- [home](http://foswiki.org/), [in WP](https://de.wikipedia.org/wiki/Foswiki)
- ein Fork von TWiki
- [handliche Syntax](https://www.wikimatrix.org/compare/mediawiki+foswiki)
- [viele (>200) Erweiterungen](https://foswiki.org/Extensions/WebHome)


#### TWiki

- [home](https://twiki.org/) , [in WP](https://de.wikipedia.org/wiki/TWiki)
- Perl, RCS


#### DokuWiki

- [home](https://www.dokuwiki.org/), [bei WP](https://www.dokuwiki.org/dokuwiki)
- PHP - kein Entscheidungskriterium mehr
- Entwickler in Berlin
- mit Erweiterungen für Unternehmen: [ICKEwiki](https://www.cosmocode.de/de/leistungen/wiki/), [evaluiert](https://subs.emis.de/LNI/Proceedings/Proceedings182/57.pdf)  durch das Fraunhofer IFF


#### Notion

- [home](https://www.notion.so/de)
- eher komplexer **Wissensmanager** als Notizprogramm, eher ein **Produktivitätswerkzeug** mit Notizen, Aufgabenmanagement, Projektverwaltung und Lesezeichen als ein Wiki
- Editor arbeitet nicht zeilenweise, sondern mit meist untereinander stehenden **Blöcken**
- viele Inhaltstypen und -formate (z.B. Videos)
- sehr **komplex**
- proprietäre **Lizenz**, nur für Einzelpersonen kostenlos
- Alles wird in **DBs** gespeichert.
- Desktop- und mobile Apps, aber nicht für  **Linux**
- **Vendor-Lock-in**, weil keine lokalen Versionen der Inhalte, sondern Speicherung in in der Herstellercloud (AWS)
- **Überlebensfähigkeit** und Ausdauer des [Herstellers](https://www.notion.so/de/about) nicht einschätzbar
- **Export**: Markdown, HTML, PDF
- Fazit: eher nicht

#### BookStack

- [home](https://www.bookstackapp.com/), [Code](https://github.com/BookStackApp/BookStack)
- Free & Open Source (MIT-Lizenz)
- Contributors: 18
- Releases:  195
- last commit: vor wenigen Tagen
- Komponenten: [Laravel PHP-Webframework](https://de.wikipedia.org/wiki/Laravel), MySQL to store data
- einige Enterprise-Features: (Multi-Factor) Authentication (Okta, SAML2 and LDAP)
- A markdown editor is provided.
- inspired by DokuWiki (s.o.)
- Fazit: kein KO-Kriterium, ich bevorzuge andere Komponenten, ein Enterprise und kein personal Wiki.


#### Git??b-Wiki

- in Git??b integriert z.B. für Dokumentation der Software und deren Entwicklung
- Ansammlung von Markdown-Files
- besitzt Markdown-Syntax-Erweiterungen und Verlinkungs-Optionen in andere GitHub-Bereiche, z.B. zu den Quellcodes, dem Bug-Tracker, ...


#### MediaWiki

- [bei WP](https://de.wikipedia.org/wiki/MediaWiki)
- für die  Wikipedia entwickelt
- etwas mühsam
- legt Wert auf freie Lizenzen der Beiträge

#### XWiki

- [home](https://xwiki.com/en/), [Wikipedia](https://de.wikipedia.org/wiki/Xwiki), [Vergleich mit Foswiki](https://www.wikimatrix.org/compare/foswiki+xwiki)
- FLOSS, in Java, benötigt Datenbank (MySQL, PostgreSQL)
- zahlreiche Erweiterungen
- von Frankreich staatlich gefördert


### Personal Wikis

#### Zim

siehe [Zim](../Prgs.d/Zim.md)


## siehe auch

- [WikiMatrix](https://www.wikimatrix.org/) -  compare the features of different Wikis
- [Wikis in Organisationen: Software](https://de.wikibooks.org/wiki/Wikis_in_Organisationen:_Software) - WikiBook
- [Liste von Wiki-Software](https://de.wikipedia.org/wiki/Liste_von_Wiki-Software) - fast 100



---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Informationsmanagement Empfehlungen<br />
letzte Änderung: 2024-11-19<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
