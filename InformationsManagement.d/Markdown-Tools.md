# Markdown-Tools

## TL;DR

Man kann jeden Editor verwenden, für den Einstieg ist ReText gut geeignet.


## Editoren

### Reviews

- [The Best Markdown Editor for Linux](https://www.sitepoint.com/the-best-markdown-editor-for-linux/)
- [Best Markdown Editors for Linux: Top 20 Reviewed For Linux Nerds](https://www.ubuntupit.com/best-markdown-editors-for-linux/) -  best markdown editor for Linux: Typora
- Vorsicht: [Atom](https://de.wikipedia.org/wiki/Atom_(Texteditor)) ist tot, es lebe VS Code (s.u.)


### ReText

- [Source](https://github.com/retext-project/retext), [Doc](https://github.com/retext-project/retext/wiki), [bei UU](https://wiki.ubuntuusers.de/ReText/)
- in Python
- im Repo
- 2 Spalten: Markdown | Vorschau
- einfach: in der Bedienung und in den Features


### formiko

- [home](https://github.com/ondratu/formiko)
- in Python
- im Repo
- Syntax-Kenntnisse muss man selbst mitbringen
- preview mode with auto scroll
- klick-optimierter Spell Checker


### VS Code

- [Visual Studio Code - home](https://code.visualstudio.com/)
- FOSS, auch wenn von MicroSoft
- Ersatz für Atom (tot), besser, monatliche Updates, gleicher Unterbau ([Electron](https://de.wikipedia.org/wiki/Electron_(Framework)))
- code_1.84.2-amd64.deb (Nov. 2023): 92M
- mehr geht nicht, Einarbeitung dauert, zahllose [Extensions for Visual Studio, Marketplace](https://marketplace.visualstudio.com/vscode)
  - multilingualer Spellchecker, [Markdown-Lint](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint) , sehr guter Previewer, viele Erweiterungen, Snippets, Prettier, ToC-Generator, integrierter File-Manager, endlos konfigurierbar, ...
- lohnt erst bei vielen, komplexen  Markdown-Files
- [Markdown and Visual Studio Code](https://code.visualstudio.com/docs/languages/markdown)
- [Feature-Liste](https://www.markdownguide.org/tools/vscode/)
- [Visual Studio Code für Markdown nutzen](https://bodo-schoenfeld.de/visual-studio-code-fuer-markdown-nutzen/)
- unterstützt [hunderte von Sprachen](https://code.visualstudio.com/docs/languages/overview)
- **Fazit**: lohnt sich, wenn man ein Tool für mehrere Sprachen sucht und die Einarbeitung nicht scheut


## Konvertierung

mit [pandoc](../Prgs.d/pandoc.md)

- [Convert files at the command line with Pandoc](https://opensource.com/article/18/9/intro-pandoc)
- [Write once, publish twice using Markdown and Pandoc.](https://opensource.com/article/18/10/book-to-website-epub-using-pandoc)

## Kommando-Zeilen Tools

- [mdless](https://github.com/ttscoff/mdless) = less für .md
  - einfach zu installieren: `$ sudo gem install mdless`
  - und zu nutzen: `$ mdless xyz.md`
  - farbige Darstellung
- [Markdown lint tool](https://github.com/markdownlint/markdownlint) ([Lint](https://de.wikipedia.org/wiki/Lint_(Programmierwerkzeug)) = statische Code-Analyse)
  - einfach zu installieren: `$ sudo gem install mdl`
  - und zu nutzen: `$ mdl xyz.md`
  - zeigt Abweichungen von den [Markdown Rules](https://github.com/markdownlint/markdownlint/blob/main/docs/RULES.md) an
- [Some great tools to create a static site from your markdown documentation](https://dev.to/mxglt/some-great-tools-to-create-a-static-site-from-your-markdown-documentation-mke) - z.B. [Hugo](https://gohugo.io/) (one of the most popular open-source static site generators)
- [Markdown im Terminal anzeigen](https://gnulinux.ch/markdown-im-terminal-anzeigen)
- [4 Markdown tools for the Linux command line](https://opensource.com/article/20/3/markdown-apps-linux-command-line)

## siehe auch

- [Applications and components that support Markdown](https://www.markdownguide.org/tools/) > 64 Stück

--

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Tools<br />
letzte Änderung: 2024-11-13<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
