# Markdown

**We love Markdown, because it’s just plain text.  
 It’s simple and powerful at the same time.  
 Pure beauty. It’s this one standard that works everywhere.**  
 [Quelle](https://alldocs.app/convert-markdown)

## Basics

- 2004 von John Gruber "erfunden".
- Syntax: [12-15 Grund-Regeln](https://www.markdownguide.org/cheat-sheet/) erfüllen bereits 90% aller Bedürfnisse.

## Vorteile

- **einfach**
- **überall** nutzbar, hochgradig portabel
- **vielseitig** einsetzbar: vom [Schreiben wissenschaftlicher Arbeiten](https://gnulinux.ch/wissenschaftliche-dokumente-in-markdown-schreiben) bis [Code-Kommentierung](https://docs.github.com/de/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax)
- weit **verbreitet** ([GitHub](https://github.github.com/gfm/), Python-Doku, ...)
- Lesen, Editieren auch ohne dedizierte **Tools** machbar
- Auch auf **Smartphones** formatiert lesbar, PDFs eher nicht.
- die üblichen Text-Tools nutzbar: Editor, Suche (grep, Recoll, find,  ...), Spell Checker,  Versionskontrolle, tar für gezielte Sicherung, ...
- viele dedizierte **Editoren** verfügbar
- leicht **transformierbar** in unterschiedlichste Formate von .rtf bis .pdf ([pandoc](../Prgs.d/pandoc.md))
- in **Collaborationsplattformen** weit verbreitet, z.B. [HedgeDoc](https://hedgedoc.org/)


## Nachteile

- Da es keine eindeutige **Spezifikation** gab, haben sich die Implementierungen in den letzten 10 Jahren  auseinanderentwickelt. Erschwerend kommt hinzu, dass in Markdown nichts als Syntaxfehler gilt und die Abweichung oft nicht sofort entdeckt wird.
- [CommonMark](https://commonmark.org/) will diesen Mangel beheben: "A strongly defined, highly compatible [specification](https://spec.commonmark.org/0.30/) of Markdown"
- meist keine [semantische](https://de.wikipedia.org/wiki/Semantik) **Auszeichnung**, sondern nur eine Formatierungsanweisung:  # Überschrift,  aber **fett**
- keine **anspruchsvolle Formatierung** möglich, z.B. mehrere Leerzeichen hintereinander, Bildgröße (z.B. 80%)


## Syntax

- [Basic Syntax](https://www.markdownguide.org/basic-syntax/)
- [Syntax](https://commonmark.org/help/)
- [Markdown Cheat Sheet](https://www.markdownguide.org/cheat-sheet/)


## siehe auch

- [Warum Markdown?](https://www.arminhanisch.de/2018/05/warum-markdown/)
- [Customizing pandoc to generate beautiful pdf and epub from markdown](https://learnbyexample.github.io/customizing-pandoc/)
- [Einführung in Markdown und Git](https://data-se.netlify.app/slides/ws-md-git-2019/ws-md-git-2019.html#1) - eine Präsentation

Erweiterungen von Git??b:

- [GitHub Flavored Markdown Spec](https://github.github.com/gfm/)
- [GitLab Flavored Markdown (GLFM)](https://docs.gitlab.com/ee/user/markdown.html#differences-between-gitlab-flavored-markdown-and-standard-markdown)


Schreiben mit Markdown:

- [Writing Blog with Markdown, Atom Editor, Pandoc and Github](https://lifelongprogrammer.blogspot.com/2019/06/writing-blog-with-markdown-atom-editor-pandoc-and-github.html)
- [wissenschaftliche Texte schreiben – mit Markdown und Pandoc](https://vijual.de/2019/03/11/artikel-mit-markdown-und-pandoc-schreiben/)
- [Scientific Writing with Markdown](https://jaantollander.com/post/scientific-writing-with-markdown/)



---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Wissen<br />
letzte Änderung: 2024-11-13<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
