# Texte vergleichen

- Die folgenden Programme vergleichen Texte, Quellcodes zeilenweise und zeigen die Unterschiede an.
- meld und kompare können auch beim Mergen, Auflösen von Konflikten helfen.


## diff

- **Standardprogramm** zum Vergleichen von 2 Dateien auf der Kommandozeile seit den 1970er Jahren
- die Basis von vielen anderen Programmen (mit GUI)
- kann [Patchdatei](https://wiki.ubuntuusers.de/patch/) erstellen
- kann auch Binärdateien vergleichen: Bilder, ausführbare Programme, ...
- Optionen:
  - `-iEZbwB` ignore Unterschiede bez. Blanks, Tabs, Leerzeilen
  - `-r`  rekursiv
  - `-y`  Ausgabe der Unterschiede in 2 Spalten, übersichtlich
- kann auch Binary files vergleichen:

```sh
$ diff bild1.jpg bild2.jpg
Binary files bild1.jpg and bild2.jpg differ

```

- [colordiff](https://www.colordiff.org/) (.deb) zeigt unterschiedliche Zeilen (nicht Zeichen) in verschiedenen Farben an.
- siehe auch: [GNU Diffutils](https://www.gnu.org/software/diffutils/)


## meld

- [home](https://meldmerge.org/),  [bei UU](https://wiki.ubuntuusers.de/Meld/)
- aus dem GNOME-Kosmos
- in Python
- auch Vergleich zweier Verzeichnisse möglich
- Gebrauch: meld text1.txt  text2.txt
- kann auch verschiedene Texte mergen, z.B. bei Konflikten in Versionsverwaltungen
- wegen bester Hervorhebung 1. Wahl, wenn sich Texte nur in wenigen Zeichen unterscheiden
- [Meld vergleicht und vereint Dateien und Verzeichnisse](https://www.linux-community.de/ausgaben/linuxuser/2017/07/vive-la-difference/), aus LinuxUser 07/2017


## kompare (+)

- [home](https://apps.kde.org/de/kompare/), [bei WP](https://de.wikipedia.org/wiki/Kompare)
- aus dem KDE-Kosmos
- in C++ geschrieben
- auch Vergleich zweier Verzeichnisse möglich
- Im Vergleich zu meld werden weniger Formatierungsunterschiede als echte Differenzen markiert.


## Aufgaben

- Finden Sie die Unterschiede zwischen den beiden Adobe-EULAs in ihrer Text-Form aus dem PDF tarball mit:
  - 2 Editoren
  - diff
  - kompare
  - meld


## siehe auch

- [Textdateien vergleichen](https://wiki.ubuntuusers.de/Textdateien_vergleichen/)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Informationsmanagement Software<br />
letzte Änderung: 2025-02-05<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
