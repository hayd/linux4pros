# Versionsverwaltung

## TL;DR

- RCS oder git - alles andere sinnlos


## git

- von Linus Torvalds übers Wochenende 2005 für Entwicklung des den Linux-Kernels programmiert
  - Linux-Kernel: seit 2005 haben sich ca. 14.000 Entwickler mitgemacht
  - branching, merging, kein zentraler Server
  - für kleine Projekte überdimensioniert, aber eine  [Zwischenlösung](https://de.wikipedia.org/wiki/Versionsverwaltung) (CVS, Bazaar, Mercurial, Subversion, ...) ist sinnlos
  - geschrieben in C, Unix-Shell, Perl, Tcl, Python, C++
  - aktuelle Version vom Okt. 2024
- Quasi-Standard bei Kooperationen mit anderen
- Server nötig - GitLab
  - mit [kostenlos](https://about.gitlab.com/pricing/)  verfügbarem Angebot beginnen
  - Zugriffsrechte: zwangsweise frei, manches geheim ?
  - auch für Texte
  - große Bilder auslagern
  - integriert: Wiki, bug tracker, ticket system (DevOp-Tools)


## RCS

- xyz,v bedeutet wichtig
- nur für lokale Entwicklungen durch eine Person empfehlenswert
- [RCS](../Prgs.d/RCS.md) - die persönliche Versionsverwaltung


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Informationsmanagement<br />
letzte Änderung: 2024-11-13<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
