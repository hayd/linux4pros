# Informationsmanagement

## TL;DR

- Nur Suchbares ist auf Dauer hilfreich.
- Auffindwahrscheinlichkeit erhöhen.
- Formatierung wird stark überbewertet.

## Creative Commons Lizenz

- Wenn man Dokumente im Netz anbietet, sollten sie eine Lizenz haben.

[Die Rechtemodule](https://de.wikipedia.org/wiki/Creative_Commons#Die_Rechtemodule):

- **by** Der Name des Urhebers muss genannt werden.
- **nc** (Non-Commercial) Das Werk darf nicht für kommerzielle Zwecke verwendet werden.
- **nd** (No Derivatives) Das Werk darf nicht verändert werden.
- **sa** (Share Alike) Das Werk muss nach Veränderungen unter der gleichen Lizenz weitergegeben werden.

Anwendung:

- Die Module kann man kombinieren.
- Empfehlung: **by, sa** => Man kann berühmt und das Werk weiterentwickelt aber nicht vereinnahmt werden.
- Bachelor-Arbeit? Was sagt Ihr Praxis-Partner, die DHSN?



## Eigene Texte

Sie schreiben Erkenntnisse, Dokumentationen, Studien-Arbeiten auf, sammeln Material, URLs aus dem Netz und speichern das schließlich in Dateien ab.

- die **erste Idee**: Die Begriffe, Bezeichnungen (z.B. für Dateinamen) verwenden, die einem zuerst einfallen. Das fällt einem später auch wieder am ehesten ein (Unter welchem Namen habe ich das Dokument vor 2 Wochen abgespeichert?).
- **Hypertext**, kein Buch: klickbare Verweise, URLs
- **DRY**: don't repeat yourself (sonst Korrekturen an 17 Stellen)
  - SSoT: single source of truth
  -  **atomare Seiten** anlegen = nicht weiter zerlegbar/unterteilbar
- Schaffen Sie sich eine **zentrale Anlaufstelle** (z.B. Zim, personal wiki), von dem aus Sie ggfs. weiterverweisen, sonst fällt der Einstieg schwer.
- Alles, was nicht **suchbar** ist, ist irgendwann verschwunden.
- Daher das **Finden erleichtern**:  
  - durch **prägnante Begriffe**: z.B. File-Server oder FileServer statt File Server.
  - durch eine **einheitliche Schreibweise** (ohne Rechtschreibfehler) und einheitliche Bezeichnungen (Laptop oder Notebook)
  - ggfs. [grep/tre-grep](../Prgs.d/grep.md) ausreizen:  
   `$ egrep -ir "laptop|notebook" Texthaufen.d`  
   `$ tre-agrep  -r -1 "Mueller" Texthaufen.d`


## Organisation einer Dokumenten-Sammlung

Sie haben >50 PDFs, .docx-, .txt-, Markdown-Dokumente, Powerpoints, Bilder mit Diagrammen, fotografierte Texte ... und wollen den Überblick behalten.

- Es wird der Zeitpunkt kommen, an dem man die Struktur seines Dokumenten-Haufens **ändern** will. Erst wenn die Sammlung eine **gewisse Größe** hat, weiss man genug, um eine geeignete Struktur anzulegen.
- eine **universelle Struktur**:
  - **flache Struktur**, d.h. nur 1 Verzeichnis. Wird dieses zu unübersichtlich, dann alphabetisch unterteilen.
  - Zusammenbinden über **Portal**-Seiten
    - Ein Thema kann auf beliebig vielen Portal-Seiten erscheinen.
    - Man kann Portal-Seiten unter verschiedenen Sortierkriterien zusammenstellen. Z.B: für Personengruppen  (User, Entwickler), nach Know-How-Level, nach Preisklasse, ...
    - z.B. [Editoren](https://wiki.ubuntuusers.de/Editoren/), [bei Wikipedia](https://de.wikipedia.org/wiki/Portal:Wikipedia_nach_Themen)
  - **Erschließen** mit tags, keywords
- Eine Gliederung nach **Zugriffsrechten**, beteiligen Personen kann möglicherweise (mehr) Sinn machen.
- Wie legt Google Mails ab?
- Was tun, wenn ein Dokument (z.B. über einen Passwort-Generator) in min. 2 Ordner **einzusortieren** wäre (Software, Security)?


## Lösch-Strategie

- Von vielen Dateien weiß man, dass sie nach einer bestimmten **Zeit** **wertlos** werden. Z.B: Bewerbungen, Angebote, Projektunterlagen, Manuals von sich rasant entwickelnder Software, ...
- 4 Jahre und hunderte von Files später ist es schwerer und (zu) zeitaufwändig zu entscheiden, ob ein bestimmter File jetzt gelöscht werden kann. Also jetzt hier und heute das **Verfallsdatum** gleich mitdefinieren.
- [Trash-CLI – die sicherere Alternative zu rm](https://linuxnews.de/2021/09/trash-cli-die-sicherere-alternative-zu-rm/)
- siehe [Bash-Aufgaben: Abräumer](/Bash.d/Aufgaben.md#Abräumer)


## Metadaten

- Ohne Metadaten entstehen nur **Datenfriedhöfe**.
  - Workaround: Suche, bei maschinenlesbaren Dokumenten
- Diese **inhaltliche Erschließung** wird meist unterlassen und durch die Hoffnung ersetzt, dass eine Suchmaschine das gewünschte schon finden wird.
- tags/keywords vergeben
  - nur **vordefiniertes Vokabular** (controlled vocabularies) verwenden, bei Bedarf erweitern
- **Beispiele** Metadaten: eigenes Abstract, tags, Löschdatum, bibliographische Daten für Zitierung, Verweise/URLs, ...
- Passende **Metadaten-Schemata** bzw. die Einigung auf eines sind eher selten. Wirklich berühmt wurde nur der  [Dublin Core](https://de.wikipedia.org/wiki/Dublin_Core).
- Metadaten in [Sidecar file](https://en.wikipedia.org/wiki/Sidecar_file): "kernel.pdf" dazu "kernel.pdf.sc.md"
  - "*.sc.md" so einzigartig, dass `find -type f -name "*.sc.md"` gut funktioniert
- **Auffind-Möglichkeiten** schaffen, z.B. aussagekräftigen Subjects für eMails, ggfs. nachhelfen mit [EditEmailSubject-MX](https://github.com/cleidigh/EditEmailSubject-MX)
- [Semantic Web](https://de.wikipedia.org/wiki/Semantic_Web)


## Exit-Strategie - aus der Lieblingssoftware

- **Exit-Strategie muss existieren.**
- Sind die Inhalte auch ohne eine **spezielle Software** lesbar, durchsuchbar?
- Ist diese Software in **5-10 Jahren** noch verfügbar bzw. erschwinglich, beim neuen Arbeitgeber lizenziert?
- Kann man die Inhalte verlustfrei von einer Software zu einer anderen **migrieren**? Auch ohne **Verlust von Metadaten** wie Zugriffsrechte, modify times, ...?
- Beispiel: [Confluence](https://de.wikipedia.org/wiki/Confluence_(Atlassian)) ändert  [Lizenzbedingungen](https://www.heise.de/news/Atlassian-stellt-Verkauf-von-Serverlizenzen-ein-Die-Zukunft-gehoert-der-Cloud-4931264.html), Preise und Hosting  und die Inhalte liegen in binärer Form in einer  [Datenbank](https://en.wikipedia.org/wiki/Confluence_(software)) strukturiert in [Spaces](https://it.cornell.edu/confluence/how-space-different-page-confluence).


## Formatierung

- [Das 11. Gebot: Du sollst nicht Formatieren](https://gnulinux.ch/das-11-gebot-du-sollst-nicht-formatieren) - Vergleich von Lesbarkeit und Overhead verschiedener Formate
- bedeutet **Aufwand** und kostet Zeit
- ist in den wenigsten Fällen so aufwändig **nötig**
- ist möglicherweise an eine **spezielle Software**, **Lizenz** gebunden
- erschwert die **Weiterverarbeitung** und die Erschließung für **Suchmaschinen**
- schränkt die **Nutzung** ein. Beispiel: Einen ASCII-Text kann jeder lesen; ein Word-Dokument kann nur mit einer speziellen Anwendung gelesen werden. Zum Editieren benötigt man möglicherweise eine teure Lizenz.
- erhöht nur selten den  **Informationsgehalt**
  - semantische Formatierung (H1) vs. formaler(16pt)
- **Probleme bei Team-Arbeit**: Beitragende Co-Autoren betreiben oft einen großen Aufwand, um den Artikel möglichst schön aufzubereiten. Die Redaktion hat dann einen Zusatzaufwand, um die Formatierungen zu entfernen. Dabei wäre ein einfacher ASCII-Text oder ein Markdown-Dokument viel einfacher zu erstellen.
- unübersichtliche **Seiteneffekte**: versteckte History (damit wurden schon Ausschreibungen verloren)  
- Fazit: Das Formatieren von Inhalten stört meistens mehr, als dass es hilft. **Eine minimalistische Formatieren ist oft besser und ausreichend.**


## Auswahl eines geeigneten Tools

Folgende Anforderungen sollte ein Programm für eigene Aufzeichnungen erfüllen:

- **must have**
  - Inhalte in Markdown-Files (oder zumindest als diese exportierbar)
  - Suche, fehlertolerant, nach Schlüsselworten
  - einfaches Ergänzen von Metadaten, kontrollierte Schlüsselworte
- **nice to have**
  - Exit ohne Verlust an Meta-Informationen wie ctime, owner
  - Versionierung

- *Do not to slip from the shoulders of giants.* D.h. Einsatz bewährter Standard-Tools via find, egrep, agrep,  plocate, Recoll, VS Code, ...


## siehe auch

- [Das 11. Gebot: Du sollst nicht Formatieren](https://gnulinux.ch/das-11-gebot-du-sollst-nicht-formatieren) - Größenvergleiche
- [Getting Things Done (GTD)](https://de.wikipedia.org/wiki/Getting_Things_Done)
- [Wie man Git-Verzeichnisse sinnvoll strukturiert – und welche Regeln helfen](https://www.linux-magazin.de/ausgaben/2019/04/regeln-fuer-git/) von Martin Loschwitz



---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Informationsmanagement<br />
letzte Änderung: 2025-02-03<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
