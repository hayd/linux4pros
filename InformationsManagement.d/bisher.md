# aktuelles InformationsManagement

## Notizen in ...

## Umfrage

Bitte beantworten Sie die Fragen im Umfrage-Pad [Notizen](https://pad.gwdg.de/KnL5JXhqRCuhQWQcHsz-kw?both#).


## Bisherige Ergebnisse

- Microsoft OneNote und [Citavi](https://en.wikipedia.org/wiki/Citavi)
- minimale Notizen auf der PowerPoint
- GoodNotes auf dem iPad
- Google Docs
- Screenshots
- Ordnerstruktur mit .odf Dateien
- lokale Textdateien
- Markdown zum Mitschreiben
- [Obsidian](https://en.wikipedia.org/wiki/Obsidian_(software))  mit verschiedenen Plugins

## Ablage, Sicherung

- Google
- Ablage auf Heim-NAS
- Discord Server
