# Interaktive Bash

## TL;DR

Die interaktive Bash ist die Mensch-Maschine-Schnittstelle, mit der man im Terminal arbeitet.  
Sie sollte durch Eintragungen in ~/.bashrc ungefährlicher und effektiver gestaltet werden.


## Environment

- Die Gesamtheit der Environment-Variablen bestimmt die Bash im Terminal maßgeblich mit.
- Auflistung aller gesetzten Environment-Variablen (typischerweise > 60):  
  `$ env`


## Konfiguration

- Pro-File wird gelesen bei Start der Shell und nimmt Einstellungen vor.
- /etc/bash.bashrc = globaler (d.h. für alle User auf dem System) Profile für die Bash
- ~/.bashrc = eigener Profile
- Kontrolle von [$PATH](./Variable.md)


## Die Konfigurationsdateien

### ~/.bashrc

Das sollte unbedingt im ~/.bashrc stehen:


```bash
umask 0077  # no rights for group and other , first 0 for Set-U/GID-, Sticky-Bit

# make destroying commands safe
alias cp='cp -ip'
alias mv='mv -i'
alias rm='rm -i'

# some usefull aliases
alias .='echo "cd $PWD"'
alias ..='cd ..'
alias h='history  | tail -$LINES'

# good promt
PS1="\[\033[34m\]\u@\h(\#)->\[\033[0m\]"
```

## Fortgeschrittene Optimierung der Konfiguration

- Optimierung  von ~/.bashrc vor allem durch: aliases, functions, Tastenbelegungen
- Bei umfangreichen Anpassungen sollte man diese strukturieren: Anlegen eines Verzeichnisses für eigene Erweiterungen:
   `$ mkdir ~/.bash.d`
- Dort 3 Files anlegen:  
   `$ touch aliases.bash functions.bash xmodmap.bash`
- Diese 3 Files in ~/.bashrc einbinden, d.h. deren Inhalt wird praktisch Teil davon, indem man am Ende einfügt:

```bash
source ~/.bash.d/aliases.bash
source ~/.bash.d/functions.bash
source ~/.bash.d/xmodmap.bash
```

### Die Erweiterungen

#### aliases.bash

- aliases erlauben eine Befehl (mit Ergänzungen) unter einem Zweitnamen aufzurufen.
- `rm -i`, `mv -i`, `cp -i` müssen bereits im ~/.bash.d gesetzt sein.
- Beispiele:
  
```bash
alias .='echo "cd $PWD"'
alias ..='cd ..'
alias grep="grep  --color=always -s -i "
alias h='history  | tail -50 | more'
alias hh='history | tail -100 | more'
alias  tmp='cd /tmp'
alias minipdf="$OCP/Scripts.d/minipdf"  # call  a own script
aliases='kwrite ~/.bashrc.d/aliases.bash &' # to make more aliases easier
```

#### functions.bash

- Problem: Man kann an aliases keine Parameter übergeben.
- Ein 1-Zeiler muss mit & }  oder  ; }   enden

```bash
function lsgrep  {  ls -alhF | grep -i $* ;}
function kw   { kwrite  $1 --geometry 700x800  2> /dev/null  & }
function cd  {   builtin cd "$@" && pwd ; }
function mkcd  { mkdir "$@" && cd "$@"    ;  }

# show all PDFs sequentialley
function mmupdf {
for i in *.{pdf,PDF}
do
  /usr/bin/mupdf $i ;
done
}
```

#### xmodmap.bash

```bash
xmodmap -e "keycode 134 = underscore    underscore"
xmodmap -e "keycode 135 = slash          "
xmodmap -e "keycode 115 = bar           bar"
```


## Aufgaben

- interessante Einträge suchen und sammeln
- ein Satz bash Profiles bereitstellen
- URLs zu guten Mustern
- Erklärungen zu komplexeren Konfigurationen
- Reihenfolge, in der die Profiles durchlaufen werden bei interaktiven Gebrauch, beim Aufruf von Scripten, in cron-Jobs
- Antworten in:  [bash snippets](https://pad.gwdg.de/KnL5JXhqRCuhQWQcHsz-kw#)



---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Shell<br />
letzte Änderung: 2025-01-26<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
