# while

Schleife wird durchlaufen bis `[[  ]]` nicht mehr "true", sondern "false" liefert.

```bash
N=0
while [[ $N -lt 100 ]]
do
  echo $N
  N=$(($N+14))
  sleep 1
done
```

## endless loop

```bash
while  true;  # loop forever
do
   echo "läuft"
   sleep 1
   echo " Enter something to continue or Ctrl-C to stop. "
   read answer   #     to stop the loop here
done
```

## zeilenweise Einlesen

Wenn keine Zeile mehr eingelesen werden kann, liefert read ein false.

```bash
file=/etc/passwd

while  read  line; do
  echo $line
done < "$file"
```

- `< "$file"` -  eingelesen wird aus dem File, der in die Schleife umgelenkt wird
- ggf. können dabei die read-Optionen `IFS` und `-r` hilfreich sein, siehe [dort](read.md)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Programmierung  Shell<br />
letzte Änderung: 2023-11-13<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>