# Aufgaben zum Bash-Scripting


## Testbed einrichten

[Hilfen](../Hilfen.d/Testbeds.md) zum Einrichten und Befüllen  einer Testumgebung

## kleine Scripte

1. Geben Sie dem jüngsten File das **Entstehungsdatum** als Zweitnamen.
   - Tipps: ln -s,  ls -t, sed um Unerwünschtes zu entfernen
1. Benennen Sie alle Files mit [**kritischem Filenamen**](./bash-globbing.md) um (wie [detox](https://linux.die.net/man/1/detox) es macht). Dabei dürfen keine bereits existierenden überschrieben werden.
1. Ersetzen Sie alle Filenamen durch **fortlaufende Nummern**, erhalten aber das Suffix.
1. Setzen Sie in jeden Filenamen zwischen Namen und Suffix die **Zahl seiner Zeilen** ein.
1. Setzen Sie in jeden Filenamen eines Bildes (.jpg, .png, ...) die **Auflösung**  zwischen Namen und Suffix ein.
   - Tipp: `file xyz.jpg`
1. Finden Sie Files, mit **gleichem Inhalt**. Die Namen können unterschiedlich sein. Verwenden Sie dafür 80% der vorhandenen Cores.  
   - Tipp: `md5sum` verwenden
2. Berechnen Sie **brutto, netto**, zu einem eingegeben Preis in deutschem Format (1.234,78€). [bc](../Prgs.d/Rechnen.md) beherrscht nur das amerikanische Format (1,234.78$).
    - Tipp: `bc -l`
3. Hängen Sie an den File einen Footer an, in dem steht, wer wann die **letzte Änderung** gemacht hat.
   - Tipp: `cat a b > c`
4. Schieben Sie glxgears in den **Hintergrund**, sobald es mehr als 30 Sekunden CPU-Zeit verbraucht hat.
   - Tipp: ps, kill
5. Erzeugen Sie einen [sidecar file](https://en.wikipedia.org/wiki/Sidecar_file) zu einem PDF, in das automatisch wichtige **Metadaten** des PDFs geschrieben werden und Sie eigene Notizen ergänzen können.
   - Tipp: `pdfinfo`, `stat` liefern Metadaten, die man ausdünnen muss
6. Testen Sie, ob alle **URLs** dieser Webseite  [Sonstige Software](https://wiki.ubuntuusers.de/Sonstige_Software/) noch zum gewünschten Ziel führen. Die, die ins Leere zeigen, sollen alle in eine Liste geschrieben werden.
   - Tipps: Test mit [w3m](https://wiki.ubuntuusers.de/w3m/) oder curl
7. dd-Benchmark: Schreiben Sie ein Script, das mit [dd](https://www.mankier.com/1/dd) und Zufallszahlen einen **Platten-Benchmark** durchführt. Der Wert für bs (blocksize, dd-Parameter) wird als Parameter abgefragt. Der Aufwand für die Erzeugung der Zufallszahlen soll rausgerechnet werden. Alle Zeiten sollen  in eine Datei geschrieben werden.
   - Tipp: dd in [Everything_is_a_file](../Filesystem.d/Everything_is_a_file.md)
8. Schreiben Sie ein Script, das sich nach 1 Minute wieder **beendet**, indem es ein beobachtendes Satelliten-Script startet.
9. Schreiben Sie einen **Zufallszahlen-Generator**, der Zufallszahlen von 1-n ausgibt. Jede Zahl darf nur 1x vorkommen. n soll abgefragt werden.
    - Tipp: `n=5 ; echo $(($RANDOM %n))`
10. Ein Reihe von eingegebenen Worten, die auch kritische Zeichen wie {* Umlaute, Leerzeichen enthalten, soll zu einem **Dateinamen** verarbeitet werden. Der Name muss unkritisch sein. Das letzte Wort  soll zum Dateityp werden (z.B. .txt). Mit diesem Namen soll dann im aktuellen Verzeichnis ein Editor geöffnet werden.
11. Schreiben Sie eine **generische Schleife** (ein Template) über alle Dateien eines bestimmten Typs.
In der Schleife sollen die Dateien einem Befehl übergeben werden:  [md5sum](../Prgs.d/Pruefsummen.md) oder [file](../Prgs.d/file.md) oder wc oder ...
Die Dateien sollen zusammengesucht werden mit [find](../Prgs.d/find.md), [locate](../Prgs.d/mlocate.md) oder Globbing.
1. **Vereinheitlichen** Sie in /tmp/Pics.d die Suffixe ohne Dateien zu überschreiben oder seltsame (per Zufallsgenerator erzeugt) Filenamen einzuführen:

```bash
D=/tmp/Pics.d  &&  mkdir $D  && cd $D 
touch a.jpg  a.jpeg  a.JPG  a.JPEG  a.webp  b.png b.PNG  c.tiff
```

18. Erstellen Sie mit `locate *.png` eine Liste aller PNG-Files auf Ihrem Rechner. Kopieren Sie alle Files dieser Liste nach /tmp/PNGs.d/ und suchen sie dort nach Dubletten.


## Handling von .md Files

- last change anhängen
- Metadaten als Kommentar
- Metadaten ändern
- Liste aller geänderten .md's
- ci nach Änderung
- alle zusammensuchen, in einen verschlüsselten .tar.bz2 packen, nach extern kopieren (Backup)


## Misc

- Bilder auf eine vorgegebene Größe eindampfen


## command line Freimacher

- Langlauferde Prozesse  (real > 1 Min.), die auf der Kommandozeile gestartet wurden, sollen in den Hintergrund geschoben, so dass die Kommandozeile wieder benutzbar wird - vergleichbar dem interaktiven `Ctrl-z , bg`
- Dazu muss die Prozessliste ausgewertet werden: Laufzeit, interaktiv oder nicht.


## Passphrase-Generator für Verschlüsselung

- Erzeugen Sie ein gutes Passwort aus dem Filenamen oder der Prüfsumme, so dass man bei Kenntnis des Algorithmus und des Filenamens jederzeit das PW berechnen kann.
- Die Passwortlänge soll einstellbar sein. Ebenso der Zeichenvorrat, z.B. mit/ohne Sonderzeichen, Zahlen
- Hilfsmittel: tr, sed, awk, cut, sha512sum, md5sum


## Abräumer

- Verfallsdatum interaktiv in Namen einkodieren.
  - für den User möglichst einfach
- In den Filenamen ist dann also ein Datum "einkodiert", z.B. Ankuendigung.10d.txt oder Ankuendigung.1.1.23.txt
  - Änderung des Filenamens minimal aber leicht interpretierbar für User
- Das Script sucht diese Files und verschiebt sie in ein Zwischenlager, wenn Fälligkeitsdatum erreicht ist.
- cron startet dieses Script.
- Hilfen: stat, [Rechnen mit Zeiten](../Prgs.d/Zeiten_rechnen.md)
- Oder alternativ einen [sidecar file](https://de.wikipedia.org/wiki/Filialdatei) verwenden: xyz.md  mit sicecar .xyz.md.sc
  - Vorteile: keine Eingriff in den Namen, auch für weitere Metadaten benutzbar
  - Nachteile: File und sein sidecar können leicht getrennt werden. Man muss daher dafür sorgen, dass beide ggfs. wieder zueinander finden.


## Change Management

- ci
- Suche nach allen ,v und Sicherung


## Ergebnisse vorführen

- Script in das Pad [Bash Scripte](https://pad.gwdg.de/wsshJWblS-mGB1HawdBVlA#) kopieren.
- Schreiben sie zu Beginn den Namen des Teams hinter die Script-Nummer.


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Aufgaben Programmierung Shell<br />
letzte Änderung: 2025-01-31<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
