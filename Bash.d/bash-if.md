# if

## Syntax

```bash
if [[ <some test> ]]
then
   <commands>
elif [[ <some test> ]]
then
   <different commands>
else
   <other commands>
fi
```


Blanks müssen sein:  `[[<blank> ... <blank>]]` !


## Tests


Operator              | Description
----------------------|----------------------------------------------
! EXPRESSION          | The EXPRESSION is false.
-n STRING             | The length of STRING is greater than zero.
-z STRING             | The lengh of STRING is zero (ie it is empty).
STRING1 = STRING2     | STRING1 is equal to STRING2
STRING1 != STRING2    | STRING1 is not equal to STRING2
_____________         | ________________
INTEGER1 -eq INTEGER2 | INTEGER1 is numerically equal to INTEGER2
INTEGER1 -ne INTEGER2 | INTEGER1 is numerically not equal to INTEGER2
INTEGER1 -gt INTEGER2 | INTEGER1 is numerically greater than INTEGER2
INTEGER1 -lt INTEGER2 | INTEGER1 is numerically less than INTEGER2


[d.h:](https://unix.stackexchange.com/questions/16109/bash-double-equals-vs-eq)  
**=**      string (lexical) comparison  
**==**    is a bash-specific alias for =  
**-eq**   numeric comparison  


## Operatoren

Wahr wenn, Test positiv ausfällt.

``-d directoryname`` - check for directory existence  
``-e filename`` - check for file existence, regardless of type (node, directory, socket, etc.)  
``-f filename`` - check for regular file existence not a directory  
``-L filename`` - Symbolic **l**ink  
``-r filename`` - check if file is a **r**eadable  
``-s filename`` - check if file is nonzero **s**ize  
``-w filename`` - check if file is **w**ritable  
``-x filename`` - check if file is e**x**ecutable  
``-z VAR s.u.`` - Länge von VAR ist 0 (**z**ero)

- [mehr Operatoren](https://tldp.org/LDP/Bash-Beginners-Guide/html/sect_07_01.html), [noch mehr](https://openbook.rheinwerk-verlag.de/shell_programmierung/shell_006_004.htm#RxxKap00600404004E261F04B172)


## Logische Operatoren

- `&&` AND operator
- `||`  OR operator

`if [ -d $dir ] && [ -w $dir ]`  
`if [[ -f $VAR1 && -f $VAR2 && -f $VAR3 ]]`  
`if [ $first -eq 0 ] || [ $second -eq 0 ]`  

komplexeres Beipiel:

```bash
if [[ 8 -eq 11 && "hello" == "hello" || 1 -eq 3 || 1 -eq 1 ]];
then
    echo "if 1"
fi
```

## `[[ ]]` vs. `[ ]`

`[[ ]]` bietet (fast) nur Vorteile:

- `[[ ]]` birgt weniger Überraschungen und ist im Allgemeinen sicherer, aber nicht [POSIX](https://de.wikipedia.org/wiki/POSIX)-kompatibel
- `[[ ]]` ist eine Bash-Erweiterung.
- `[  ]` ist wie "`test`"
- `[[ ]]` bietet Shell-Globbing. Globbing funktioniert nicht, wenn Sie die entsprechende Zeichenfolge in Anführungszeichen setzen
- kein Quoting mehr nötig:  
`if [[ -f $file ]]`   statt `if [ -f "$file" ]`
- In `[[ ]]`  kann man mit &&, ||, <,  > arbeiten.
- Man kann mit regular expression matching ( =~) in `[[ ]]` arbeiten.
- **Fazit: Immer `[[ ]]`verwenden.**
- siehe auch:
  - [What is the difference between ...](http://mywiki.wooledge.org/BashFAQ/031)
  - [Conditions in bash scripting (if statements)](https://acloudguru.com/blog/engineering/conditions-in-bash-scripting-if-statements)


## Beispiele

### Strings

```bash
if [[ $stringvar == "tux" ]]; then
if [[ -z $emptystring ]]; then
```


### File-Typ

```bash
if [[ -L symboliclink ]]; then
if [[ ! -f regularfile ]]; then
```


### Integers

```bash
if [[ $foo -ge 3 ]]; then
if [[ $foo -ge 3 -a $foo -lt 10 ]]; then
```


### File-Existenz

- [How to Check if a File or Directory Exists in Bash](https://linuxize.com/post/bash-check-if-file-exists/)

```sh
FILE=/etc/resolv.conf
if [[ -f $FILE ]]; then
    echo "$FILE exists."
else
    echo "$FILE does not exist."
fi
```

- [Check](https://devconnected.com/how-to-check-if-file-or-directory-exists-in-bash/), ob File existiert oder nicht

```bash
if [[ ! -f  /etc/resolv.conf ]]
then  
   usw.
```

 `!` ist der logische "NOT" Operator.


### Kurzform

`[[  ]]` liefert TRUE oder FALSE und darauf reagieren `&&` bzw. `||` wie auf den exit code eines Kommandos

```bash
[[ -f /etc/resolv.conf ]] && echo "$FILE exists." || echo " No file there!"
```

Zum Vergleich der exit code: `$ grep root /etc/passwd && echo "root exists"`

### 2 Tests

Check, ob 2 Files existieren:

```bash
if [[ -f /etc/resolv.conf && -f /etc/hosts ]]; then
    echo "Both files exist."
fi
```

oder in Kurzform:

```bash
[[ -f /etc/resolv.conf && -f /etc/hosts ]] && echo "Both files exist."
```

2 Vergleiche

```bash
if [[ $num -eq 3 && "$stringvar" == foo ]]; then
  echo "Both files exist."
fi
```



### exit code

- 0 = erfolgreich = TRUE
- 1 bis 255 = Nummer einer Fehlermeldung = FALSE


 Auswertung des exit status am Beispiel: if mit grep

- `grep -q` = quiet, d.h. kein Output, nur exit status
- grep findet etwas =>  0 = TRUE
- grep findet nichts =>  non-zero = FALSE


```bash
if grep -q root /etc/passwd
then
    echo "root found"
else
    echo "no root"
fi
```

- siehe auch [Advanced Bash-Scripting Guide](https://www.linuxtopia.org/online_books/advanced_bash_scripting_guide/testconstructs.html)


### Variable gesetzt oder nicht ?

Test, ob ein Variablen-Inhalt ein nicht-leerer String ist oder nicht:

```bash
if [[ -n $1 ]]
then
  echo "You supplied the first parameter!"
else
  echo "First parameter not supplied."
fi
```

- kompakter: `if [[ -z $i  ]]; then ...`  
- noch kompakter: `[[ -z $1  ]] && { echo  'no $1'; xeyes; }`
  - `{  }` fasst mehrere Befehle syntaktisch zu einem zusammen.

- siehe auch: [How to check if a variable is set in Bash](https://stackoverflow.com/questions/3601515/how-to-check-if-a-variable-is-set-in-bash)



## siehe auch

- [If Statements - Bash Scripting Tutorial](https://ryanstutorials.net/bash-scripting-tutorial/bash-if-statements.php)
- [Conditions in bash scripting (if statements) | A Cloud Guru](https://acloudguru.com/blog/engineering/conditions-in-bash-scripting-if-statements)
- [How To Check If File or Directory Exists in Bash – devconnected](https://devconnected.com/how-to-check-if-file-or-directory-exists-in-bash/)
- [How To Script Error Free Bash If Statement?](https://www.shell-tips.com/bash/if-statement/#gsc.tab=0)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Programmierung  Shell<br />
letzte Änderung: 2024-11-13<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
