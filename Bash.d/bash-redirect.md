# Umlenkungen von Datenströmen in der Shell, redirects

![stdin, stdout, stderr](../Pics.d/std-streams.png)

- 3 Datenströme: stdin, stdout, stderr
- stdout, stderr kann man getrennt verarbeiten
- stdout in den File output_file.txt umlenken. Dieser wird dabei neu  angelegt, d.h. der bisher vorhandene Inhalt gelöscht:  
`$ cmd > output_file.txt`  
- stdout nach output_file.txt umlenken ohne output_file.txt vorher zu löschen:  
`$ cmd1 >> output_file.txt ; cmd2 >> output_file.txt`  
- Beispiele:  
  
```shell
  $ echo "1" > out.txt
  $ echo "2" > out.txt
  $ cat out.txt
      # aber
  $ echo "1" >> out.txt
  $ echo "2" >> out.txt
  $ cat out.txt
```

- redirect stderr to output_file.txt  
`$ cmd 2> output_file.txt`
  - Beispiel:  Verwerfen vieler Warnungen beim Programm-Start  
`$ kwrite --geometry 700x800 2> /dev/null &  V`

- pipe cmd1 stdout to cmd2's stdin  
`$ cmd1   | cmd2`  
z.B: `$ ls -1 | wc -l`

- stderr umlenken:
  - redirect both stderr and stdout to output_file.txt  
`$ cmd >& outpout_file.txt`
  - pipe cmd1 stdout and stderr to cmd2's stdin  
`$ cmd1 2>&1 | cmd2`


## tee

- [tee](https://de.wikipedia.org/wiki/Tee_(Unix)) = T-Stück, ein Splitter für Datenströme
- hilfreich, wenn man einen Datenstrom für mehrere Zwecke gleichzeitig nutzen möchte, z.B. aufzeichnen und sofort auswerten:  
  
```bash
$ ping -c 5 google.com | tee /tmp/log.txt | wc -l  # etwas warten
$ cat /tmp/log.txt
```

- print stdout,stderr to screen while writing to result.txt  
`$ cmd 2>&1 | tee result.txt`

## siehe auch

- [Standard-Datenströme – Wikipedia](https://de.wikipedia.org/wiki/Standard-Datenstr%C3%B6me)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Shell<br />
letzte Änderung: 2024-11-06<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
