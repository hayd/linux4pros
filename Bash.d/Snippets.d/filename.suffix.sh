# divide filename in basename and suffix

file=$1

suffix="${file##*.}"                # bash parameter expansion              
filename="${file%.*}" 


# ${variable##pattern} = Remove the longest match from the beginning of the 
# variable where the pattern matches.

# ${variable%pattern} 	= Remove the shortest match from the end of the 
# variable where the pattern matches.

# allowed wildcards:  * ?  [ ] 
