# Shell-Scripte

## Allgemeines

- Ein gutes (= mit guten Kommentaren) Skript ist auch eine gute Doku.
- einfaches Debugging
- Davon lebt die Anpassung des Linux-Arbeitsplatzes.
- [Platz 8 der Top Programming Languages](https://www.heise.de/news/GitHub-Report-Python-ueberholt-JavaScript-waehrend-TypeScript-Java-schlaegt-10003572.html)


## Bash oder Python?

Bash- oder Python-Script ?

### pro Bash

- nichts verknüpft Linux-Tools einfacher
- nur einfache Abfolge von Linux-Befehlen mit schlichten Datenstrukturen (max. kleines, lineares Array; temporäre Datei) und einfachen  [Kontrollstrukturen](https://de.wikipedia.org/wiki/Kontrollstruktur).

### pro Python

- objekt-orientierte Programmierung
- Verbreitung in der jeweiligen Community (z.B. AI)
- komplexe Datenstrukturen
- Nutzung mächtiger Module, Libraries (z.B. [pandas](https://de.wikipedia.org/wiki/Pandas_(Software)), [NumPy](https://de.wikipedia.org/wiki/NumPy), [Matplotlib](https://matplotlib.org/))
  - aber: Programme wie sed, [Gnuplot](https://de.wikipedia.org/wiki/Gnuplot), [QPDF](https://qpdf.sourceforge.io/), usw., die man über ein Bash-Script bedient, fungieren wie mächtige Libaries.


### aus [Google Shell Style Guide](https://google.github.io/styleguide/shellguide.html#s7-naming-conventions)

*If you are writing a script that is more than **100 lines** long, or that uses non-straightforward control flow logic, you should rewrite it in a more structured language now. Bear in mind that **scripts grow**. Rewrite your script **early** to avoid a more time-consuming rewrite at a later date.*


## Programmierung

- schwierige Zeilen auf Kommandozeile testen
- Debugging:  `set -x` (man page: *Print commands and their arguments as they are executed.*)
  - Kann man an jeder Stelle im Script einbauen und mit einem 2. `set -x` auch wieder deaktivieren.
- `exit` und danach Tests, URLs, Befehle aus der Entwicklungsphase, ...

## Script-Anfang

- die ersten Zeilen:
  
```bash
#! /bin/bash
# the shebang line
# set -x
```

- `#!` sagt, dass die folgenden Kommandos vom angegebenen Interpretern ausgeführt werden.
- [set -x](https://linuxhint.com/set-x-command-bash/) listet jede Zeile mit expandierten Variablen vor der Ausführung auf. Gut für Debugging
- Script ausführbar machen: `$ chmod 755 script-file.sh`


## Empfehlungen

- Scripte in einem eigenen Verzeichnis sammeln und ggfs. mit alias verfügbar machen.
- **Notizen** aus der Entwicklungsphase nach exit-Kommando
- **Versionskontrolle** (incl. Backup) nutzen: RCS oder git



### creating and accessing libraries

- [Shell Scripting, Steve Parker](https://doc.lagout.org/operating%20system%20/linux/Commands%20and%20Shell%20Programming/Shell%20Scripting.pdf)  S.181ff
- Aufbau einer Bibliothek: Module mit source aufrufen
  - [Verteilte Systeme in der Bash schreiben – Teil 1](https://www.linux-magazin.de/ausgaben/2022/06/shell-serie-teil-1/)
  - [Verteilte Systeme in der Bash schreiben – Teil 2](https://www.linux-magazin.de/ausgaben/2022/07/shell-serie-teil-2/)


- Nur eine sehr ausgereifte Bibliothek ist sinnvoll. Wenn man die Bibliothek modifiziert, hat das unübersichtliche Nebenwirkungen auf zahlreiche Scripte. Alle Scripte müssten erneut getestet werden.
- Besser: Eine Sammlung von **Musterlösungen** (in Form von **Snippets**). Arbeitserleichterung, Dazulernen ohne alte Scripte zu gefährden. Z.B. Test, ob ein File bereits vorhanden ist und ggfs. Wahl eines anderen Namens.



---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Programmierung Shell<br />
letzte Änderung: 2024-11-12<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
