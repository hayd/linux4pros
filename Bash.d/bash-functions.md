# functions

Nutzen Sie Unterprogramme, functions, um den Code übersichtlicher zu machen und um [DRY](../Vorlesung.d/DRY.md).


## Syntax

- Die Funktionsdefinition muss vor jedem Funktionsaufruf stehen.

```bash
function function_name  {
  commands
}
```

oder als 1-Zeiler:

```bash
function  function_name { commands; }
```


Statt des reserved words `function`, kann man auch () verwenden (= schlechter):

```sh
function_name () {
  commands
}
```

- Nach `{` dürfen keine Blanks stehen !!! Überprüfen mit:  
`$ cat -vte myScript.sh`


## Übergabe von Parametern

- siehe [Positional Parameters](./Variable.md),  Scope auf der Seite [Shell Variable](./Variable.md)
- Um eine beliebige Anzahl von Argumenten an die Bash-Funktion zu übergeben, fügen Sie diese einfach durch ein Leerzeichen getrennt direkt hinter den Funktionsnamen ein.
- Es ist eine gute Praxis, die Argumente in **Anführungszeichen** zu setzen, um zu vermeiden, dass ein Argument mit Leerzeichen falsch interpretiert wird:  
 `function_name "$arg1" "$arg2"`
- Die übergebenen Parameter sind in der Funktion als  `$1, $2, $3 … $n` verfügbar,
- alle Parameter als ein einziger String in `$*`  oder
- als ein 1-dimensionales Feld von Strings in  `$@`.
- Oder man nutzt die **Globalität** der Variablen.


## return

- Damit kann man dem Verlassen der function einen return code mitgeben, der in Variable `$?` zu finden ist. Z.B: `return 55`
- return beendet eine function.


## function statt alias

Einem Alias kann man keinen Parameter übergeben. Ausweg: function verwenden. Beispiele (zum Einsetzen in ~/.bashrc):

```bash
# kwrite with certain size and without error messages
function kw   { kwrite  $1 --geometry 700x800  2> /dev/null  & }

# find file by substring from here till bottom
function ff  { find . -xdev -type f -name \*$1\* 2> /dev/null  ; }

## find file by substring in $PWD
function lsgrep  { ls -alhF | grep -i $* ; }


# show all PDFs sequentially
function mmupdf {
for i in *.{pdf,PDF}
do
  /usr/bin/mupdf $i ; 
done
}

```

## siehe auch

- [The Complete How To Guide of Bash Functions](https://www.shell-tips.com/bash/functions/)
- [Bash Functions](https://linuxize.com/post/bash-functions/)
- [Bash Functions](https://linuxize.com/post/bash-functions/)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Programmierung  Shell<br />
letzte Änderung: 2023-11-13<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
