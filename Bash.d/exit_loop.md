# aus Schleife vorzeitig herausspringen



## break (raus-springen)

[break](https://linuxize.com/post/bash-break-continue/) beendet die aktuelle Schleife und übergibt die Programmkontrolle an den Befehl, der auf die beendete Schleife folgt.

## continue (eine Runde auslassen)

[continue](https://linuxize.com/post/bash-break-continue/) überspringt die verbleibenden Befehle innerhalb des Schleifenkörpers  für die aktuelle Iteration und übergibt die Programmkontrolle an die nächste Iteration der Schleife.


## siehe auch

- [What is the Right Way to do Bash Loops?](https://www.shell-tips.com/bash/loops/#gsc.tab=0)



---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Programmierung  Shell<br />
letzte Änderung: 2023-11-13<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>