
# echo

echo kann Strings und Variablen-Inhalte ausgeben:

`$ echo "Ich bin gerade in Ordner: $PWD"`

Achtung:

- `' '` maskiert auch \$:  
  `$ echo 'Ich bin gerade in Ordner: $PWD'`
- PWD ist der Name der Variablen, nicht ihr Inhalt:  
  `$ echo "Ich bin gerade in Ordner: PWD"`

## Steuerzeichen

Diese Steuerzeichen "versteht" der echo-Befehl nur mit der  Option `-e`:

- `\a`  Alarm-Ton (Beep)
- `\b`  Backspace; ein Zeichen zurück
- `\c`  continue; das Newline-Zeichen unterdrücken
- `\f`  Form Feed; einige Zeilen weiterspringen
- `\n`  Newline; Zeilenumbruch
- `\r` return; zurück zum Anfang der Zeile
- `\t`  Tabulator (horizontal)
- `\v`  Tabulator (vertikal); meistens eine Zeile vorwärts

`$ echo -e " 1 2 3 \n 4 5 6"`  
  aber  
`$ echo -e " 1 2 3 \c" ; echo -e " 4 5 6"`

[Beeper](https://www.amazon.de/gp/product/B017LB6CJS/ref=ppx_yo_dt_b_search_asin_title) vorhanden/aktiv?  `$ echo -e  "\a"`


## Farbe

- Farbdefinition mit [ANSI escape codes](https://de.wikipedia.org/wiki/ANSI-Escapesequenz)
  - '\033[' = jetzt kommt Farbe
  - Farben: Red: 0;31, Green:  0;32, Blue:  0;34
  - ersetzt man 0 durch 1 wird die Farbe heller
- handlichere Definition:  
`RED='\033[0;31m'`  
`NC='\033[0m'` # No Color
- Beispiel: `$ echo -e "Dieser ${RED}Alarm ${NC} ist wichtig."`
  - {} definieren die Grenzen des Variablennamens
- mehr: [terminal - List of ANSI color escape sequences - Stack Overflow](https://stackoverflow.com/questions/4842424/list-of-ansi-color-escape-sequences), [How to Change the Output Color of Echo in Linux](https://linuxhandbook.com/change-echo-output-color/)
 
---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Programmierung  Shell<br />
letzte Änderung: 2024-11-06<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
