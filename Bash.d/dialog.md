# dialog

- [ncurses](https://de.wikipedia.org/wiki/Ncurses) für auch komplexere Dialoge der Shell mit dem User
- [home](https://invisible-island.net/dialog/)
- `$ sudo apt install dialog`
- Sieht man sich die zahlreichen, unmittelbar ausführbaren Beispiele an, weiss man, was dialog leisten kann:  
`$ ls  /usr/share/doc/dialog/examples/`


## Alternativen

### gum

- [home](https://github.com/charmbracelet/gum)
- [Interaktive Shell-Skripte mit Gum](https://gnulinux.ch/interaktive-shell-skripte-mit-gum)

### KDialog

- [KDialog](https://wiki.ubuntuusers.de/KDialog/) bei UU

### Zenity

- [Zenity](https://wiki.ubuntuusers.de/Zenity/) bei UU
- für GTK-Dialoge


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Programmierung  Shell<br />
letzte Änderung: 2023-11-13<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>