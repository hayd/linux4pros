# Shell-Variable

- XYZ: die Variable (Schachtel mit Namen)
- $XYZ: Wert der Variablen (Inhalt der Schachtel)
- Variable einen Wert zuweisen: `a="ein string"`
- Inhalt der Variablen anzeigen: echo $a

## Environment

Environment: Gesamtheit der Variablen, die die Linux-Arbeitsumgebung (z.B. in  Shell, Terminal-Emulator, grep-Ausgabe, Datumsformat, Sprachwahl, ...) bestimmen.


## Environment-Variable

- Environment-Variablen bestimmen die Umgebung ganz wesentlich. Sie werden daher in **Konfigurations-Dateien** gesetzt, um ein angepasstes Environment zu gestalten:
  - **/etc/bash.bashrc**:  systemweit (=für alle User)
  - **~/.bashrc**: nur für `echo $USER`
- Syntax: `export ENV_VARIABLE="some value"`
  - zum Vergleich: `MY_SHELL_VARIABLE="some value"`
  - d.h. `export` macht aus einer normalen Variablen eine Environment-Variable.
- Die **Vererbung** unterscheidet Environment-Variable von normalen Variablen. Environment-Variablen werden bei der Prozessgenerierung vererbt, d.h. Kindprozesse bekommen eine Kopie des Environments des Vaterprozesses und vererben dies auch wieder an ihre Kinder weiter.
- **Scope**: Environment-Variablen sind lokal, d.h. Veränderungen in Terminal 1 sind in Terminal 2 unbekannt.
- Anzeige aller Environment-Variablen:  
   `$ printenv`  oder einfach nur `$ env`


### Wichtige Exemplare

Variable  | Bedeutung
----------|--------------------------------------------------
$COLUMNS  | Breite, der Ausgabe auf dem Bildschirm
$HOME     | selbsterklärend
$HOSTNAME | selbsterklärend
$LANG      | die aktuelle Sprach- und Lokalisierungseinstellungen, einschließlich der Zeichencodierung
$PWD      | aktuelles Verzeichnis
$OLDPWD   | working directory vor dem Wechsel in das aktuelle
$PS1      | main prompt
$SECONDS  | Aufenthaltsdauer/Laufzeit im aktuellen Script


### PATH

- PATH: Diese aneinandergereihten Directories (und nur diese) werden von links nach rechts nach dem eingegebenen Befehl **durchsucht**. Daher: Vorsicht bei der Reihenfolge und den Erweiterungen. Am Anfang sollte stehen:  
 `/usr/sbin:/usr/bin:/sbin:/bin`
- In [/opt, /usr/local](../Filesystem.d/FHS.md) können executables stehen, die aus nicht seriösen Quellen stammen.
- PATH erweitern:
  
```bash
LPATH=~/opt/bin:~/Scripts.d
export PATH=$PATH:$LPATH
```

- Kontrolle: `$ echo $PATH`
- Woher kommt ein Befehl?  
   `$ type bc`  
- Firefox kann beispielsweise aus einem Debian- (/usr/bin/firefox) oder Snap-Paket (/snap/bin/firefox) stammen.
- Software aus weniger seriösen Quellen:

```bash
$ type noteshrink
noteshrink is /home/helmut/.local/bin/noteshrink

$ type lsgrep
lsgrep is a function
lsgrep ()
{
    ls -alhF | grep -i $*
}
```

## Namenskonventionen

- Environment Variable oder Shell Variables des Betriebssystem: `ENV_VAR`
- normale Variablennamen: `my_variable_name`
- [Shell Style Guide](https://google.github.io/styleguide/shellguide.html#s7-naming-conventions) of many Googlers


## Positional Parameters

Variable | Bedeutung  
--|--
`$0, $1, $2`, etc.  | positional parameters, die an ein Script oder eine Funktion übergeben werden
`$@`  |  alle übergebenen Argumente als Liste von strings
`$*`  |  alle übergebenen Argumente als ein einziger string
`$#`   | Zahl der übergebenen Argumente

- `$*` ist also ein einziger String, wohingegen  `$@` ein 1-dimensionales Feld von Strings ist.


## Daten-Typen

Es gibt 3 nicht-typisierbare Daten-Typen:

1. String (default)
1. Integer
1. Array


## Scope (Sichtbarkeitsbereich)

- Die Variablen der Bash haben einen **dynamischen Scope**.
- Alle Variablen sind defaultmäßig **global**, egal wo sie im Script deklariert werden.
- `local my_local_var=“value”` setzt den Scope der Variablen my_local_var von global auf **lokal**, d.h. nur gültig in der function. Die Definition muss innerhalb einer function erfolgen.
- Funktionsvariable erben den Scope des Aufrufers, d.h. ein Unterprogramm kann den  Scope ändern, z.B. von lokal auf global.
- Was rechts von `|` (Pipe) steht, wird in einer Subshell ausgeführt. D.h. die Variableninhalte werden zwar von oben in die Subshell übernommen, können aber nicht zurückgegeben werden.


## Hilfsmittel

- `set ‑u`  Fehlermeldung, wenn eine ungesetzte Variable referenziert wird
- [Shellcheck](https://www.shellcheck.net/) "_finds bugs in your shell scripts_". Statischen Analyse im Browser, um  syntaktische und semantische Fehler zu erkennen und zu erklären.



## siehe auch

- [Shell Style Guide](https://google.github.io/styleguide/shellguide.html#s7-naming-conventions) of many Googlers
- [Lesen und Einrichten von Umgebungs- und Shell-Variablen unter Linux](https://www.digitalocean.com/community/tutorials/how-to-read-and-set-environmental-and-shell-variables-on-linux-de) - ein Tutorial
- [A Complete Guide to the Bash Environment Variables](https://www.shell-tips.com/bash/environment-variables/#gsc.tab=0)
- [Bash variables — Things that you probably don’t know about it](https://medium.com/unboxing-the-cloud/bash-variables-things-that-you-probably-dont-know-about-it-8a5470887331) - scopes and types
- [Advanced Bash-Scripting Guide](https://tldp.org/LDP/abs/html/internalvariables.html) 9.1. Internal Variables
- [typische Anwendungsfälle](https://wiki.ubuntuusers.de/Umgebungsvariable/typische_Anwendungsf%C3%A4lle/) für den Einsatz von Environment Variablen
- [Positional Parameters, Other Special Parameters](https://tldp.org/LDP/abs/html/internalvariables.html)



---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Shell<br />
letzte Änderung: 2023-11-15<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>

<!---  
## 2do

- ergänzen:
  - FOO="${VARIABLE:=default}"  # If variable not set or null, set it to default.  
-->
