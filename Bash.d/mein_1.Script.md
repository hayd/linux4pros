# Das 1. Script

1. in geeignetes Verzeichnis gehen bzw. zuvor ein solches anlegen, z.B.  
 `$ mkdir ~/Bash-Scripts.d && cd ~/Bash-Scripts.d`
2. unten stehenden Rumpf in einen Editor kopieren
3. abspeichern als xyz.sh
4. ausführbar machen: `$ chmod 755 xyz.sh`
5. ausführen: `$  ./xyz.sh`

```bash
#! /bin/bash
# the shebang line
#set -x

USAGE='
usage: xyz filename   
do something with a file
'

# no parameter => help and exit
[[ -z $1  ]] && { echo  "$USAGE"; exit;}

# requested help
if [[ "$1" == "-h" ]]; then
    echo "$USAGE"
    exit
fi

echo " doing something with file $1"
sleep 1

exit





--------------- some notes  ------------------

2do:
- more commands

usage:
https://stackoverflow.com/questions/19507902/bash-script-usage-output-formatting
https://stackoverflow.com/questions/687780/documenting-shell-scripts-parameters

```

## shellcheck

[shellcheck](https://github.com/koalaman/shellcheck) : [lint](https://de.wikipedia.org/wiki/Lint_(Programmierwerkzeug))  tool for shell scripts

- `$ sudo apt install shellcheck`
- Im [shellcheck Wiki](https://www.shellcheck.net/wiki/) sind die Meldungen ausführlich erklärt


## Selbst Programmieren lernen


[Eine Studie](https://www.heise.de/news/Schlechte-Code-Qualitaet-durch-die-KI-Assistenten-GitHub-Copilot-und-ChatGPT-9609271.html) analysiert, dass die Code-Qualität durch den zunehmenden Einsatz von KI-Assistenten sinkt und zwar insbesondere in Bezug auf das DRY-Prinzip (Don’t repeat yourself). Die technischen Schulden erhöhen sich dadurch, d.h. Pflegeaufwand nimmt zu.

KI liefert durchschnittlichen Code, keinen mit genialen Einfällen, vielleicht nicht mal mit bewährten Snippets, Templates.


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Programmierung  Shell<br />
letzte Änderung: 2024-11-06<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
