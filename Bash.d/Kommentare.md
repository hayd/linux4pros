# Kommentare

## Kommentieren

Stellen Sie sich immer diese beide Fragen:

1. Weiss ich selbst nach einem halben Jahr noch, was der Code an dieser Stelle macht?
2. Wenn mir ein Kollege dieses Script gegeben hätte, könnte ich es dann weiterentwickeln (ohne tagelanges [Reverse Engineering](https://de.wikipedia.org/wiki/Reverse_Engineering)) ?


## Konsequenzen

- **häufig**  # später werden Sie dankbar sein dafür
- **sofort**  # sonst passiert es nie mehr
- **im Quelltext** # sonst passiert es gar nicht und geht verloren
- **englisch** # Übung, weltweite Kooperation
- längere Kommentare **nach exit**-Kommando, z.B. Erklärung komplizierter Konstrukte mit URL


## Syntax

- alles nach **#** ist ein Kommentar
- **#** kann überall in einer Zeile stehen
- Die Bash ignoriert alles, was dahinter steht.


## siehe auch

[styleguide | Style guides for Google-originated open-source projects](https://google.github.io/styleguide/shellguide.html#s4-comments)

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Programmierung<br />
letzte Änderung: 2024-11-13<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>

