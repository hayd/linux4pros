# read

- Man muss ein oder mehrere Variable angeben, in die eingelesen wird.
- dauerhafte Abfrage in Endlos-Schleife:

```bash
echo " enter 0 to exit or !0 to continue !
while read N1
do
  if [[ $N1 = 0 ]] 
  then         
    exit
  fi
done
```

- Man kann auch ein Zeitlimit(`-t sec`) für die Antwort mitgeben:

```bash
echo "enter 2 numbers ASAP!"
read -t 3 N1 N2
echo "you have been fast enough or not: N1 = $N1 ; N2 = $N2 "
```

- obiges Beispiel mit `-p` (prompt) statt echo:  
  
```bash
...
read -t 3  -p "enter 2 numbers ASAP!" N1 N2`
...
```



## Ein komplexeres Beispiel

In Abhängigkeit von der eingelesenen Antwort, wird in eine function gesprungen mit case:

```bash
USAGE='
m   merge  different collections by date
(c)   initial clean up (e.g. .JPG--> .jpg, permissions 644) and merge by date
w   create next or initial working names
o   interactively re-order the pictures by entering numbers
f   final naming of the files according to the $PWD and extracting metadata
h   help
'

read SUB

case $SUB in
  m)  merging         ;;
  c)  cleanup         ;;
  w)  WorkingNaming   ;;
  f)  FinalNaming     ;;
  o)  ordering        ;;
  h)  echo -e  "\n\n usage:  $USAGE" ;;
esac
```

## siehe auch

read kennt noch mehr Optionen:

- [Bash read command](https://linuxhint.com/bash_read_command/)
- [How To Use The Bash read Command {10 Examples}](https://phoenixnap.com/kb/bash-read)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Programmierung  Shell<br />
letzte Änderung: 2024-01-10<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>