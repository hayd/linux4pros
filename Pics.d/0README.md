# Pics für Vorlesung

## Allg

- jeweils 2 Bilder: Original, Verkleinerung mit Größenangabe (x-Ausdehnung)
- gute Maße:
- Originalauflösung: -> identify xyz.jpg


## nicht SVG


convert -resize 200 in.jpg  out.200.png


##  SVG

b=bild.svg
inkscape --export-type=png --export-filename=$b.png --export-width=200 $b.svg

.svgz geht auch

Die Transparenz selber lässt sich über den Parameter 
--export-background-opacity 
mit einem Wert von 0.0 bis 1.0 oder 1 bis 255.

