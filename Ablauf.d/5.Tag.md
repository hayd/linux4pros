# 5.Tag - 4.2.2025

## Wiederholung

1. Was ist die Shell?
1. Was macht die Shell?
1. Warum Bash?
1. Was ist Globbing?
1. Was sind REs?
1. Wahl eines Filenamens?
1. Shell-Variable: scope, Environment-Variable, spezielle, mit und ohne $
1. Path-Variable
1. Was ist~/.bashrc und was sollte dort stehen?
1. Datenströmen in der Shell und deren Umlenkung?
1. grep, sed?
1. bash: if
1. bash: for
1. bash: Möglichkeiten der Strukturierung?
1. bash: echo, read
1. Wann Python, Bash, Rust oder C?



<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Übung<br />
letzte Änderung: 2025-02-03<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
