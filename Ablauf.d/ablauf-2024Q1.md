# Ablauf Q4 2023

## Vorlesung


Wenn nicht anders vermerkt: 10:00 - 17.15 im Jan. 2024

- Di., 2.1. (14:00-17:15)  ;  Do. 4.1.
- Di., 9.1 ;  Do. 11.1.
- Di., 16.1 ;  Do. 11.1.
- Di., 23.1 ;  Do. 25.1.



## Prüfung

- Feb. 2024 ?
- 30 Minuten mündliche Prüfung
- Vorstellung eines vorbereiteten bash-Scriptes als Einstieg
