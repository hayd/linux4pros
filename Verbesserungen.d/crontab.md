# crontab - ergänzen

- [UU](https://wiki.ubuntuusers.de/Cron/)
- [How To Use Cron to Automate Tasks on Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/how-to-use-cron-to-automate-tasks-ubuntu-1804)
- [How to Use Cron to Automate Linux Jobs on Ubuntu 20.04](https://www.cherryservers.com/blog/how-to-use-cron-to-automate-linux-jobs-on-ubuntu-20-04)
- [How to Automate Regular Tasks with Cron on Ubuntu 20.04](https://serverspace.io/support/help/automate-tasks-with-cron-ubuntu-20-04/)
- [Crontab Syntax und Tutorial: Cronjobs unter Linux einrichten und verstehen](https://www.stetic.com/developer/cronjob-linux-tutorial-und-crontab-syntax/)
- [How to configure Crontab on Linux](https://webdock.io/en/docs/how-guides/system-maintenance/how-configure-crontab-linux)
- [ Crontab in Linux – with Real-time Examples and Tools ](https://geekflare.com/crontab-linux-with-real-time-examples-and-tools/)
- [The 5 places cron jobs are saved ](https://cronitor.io/cron-reference/5-places-cron-jobs-live)

- [Cron Job Generator](https://www.generateit.net/cron-job/)
