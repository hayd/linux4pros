# Verbesserungen


## Verbessern im Vorlesungsskript

- qpdf Beispiel: Blanks raus aus Seitenlisten
- poppler-utils installieren in Doku
- pdfinfo findet Javascript, Beispiel
- find, tr: Zeilenumbrüche
- bash Aufgaben als Script in eigenem File
- mein pwgen mitbringen
- ein Bash-Script schreiben, das Bilder verkleinert, bis die als Parameter übergebene Größe erreicht ist.  Größe in kB übergeben
- Script: Parameter-Übergabe $1 $2 usw.


## Material

[Material zur Vorlesung](https://t1p.de/linuxmaterial)  PW: linux-mat-d0wn


## Pads

- [Linux-Vorlesung, Pad 1](https://pad.gwdg.de/KnL5JXhqRCuhQWQcHsz-kw#)
- [Linux-Vorlesung, Pad 2](https://pad.gwdg.de/JYMazhuyTZaS8RNG42gq2g)
- [Linux-Vorlesung, Pad 3](https://pad.gwdg.de/jPaQ3tiMSlWKBoffy89DBQ)


## Misc

- Verlagerung nach GitLab
- Auf gaben auf eigene Seiten und Lösungen auch
  