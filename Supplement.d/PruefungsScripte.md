

## Zu präsentierende Scripte
- Schreiben Sie je ein kurzes Bash-Script, das eine der folgenden Aufgabe löst. 
- Verwenden Sie vorzugsweise Material aus der Vorlesung.
- Benennen bzw. implementieren Sie sinnvolle Ergänzungen der Scripte. z.B. eine Ausgabe über den Fortschritt des Scriptes.
- Tauchen Fragen oder Probleme auf, schreiben Sie eine Mail an meinem BA-Account e0014...


### 1. dangling URLs
Es sollen alle URLs und Links in  Markdown-Files gefunden werden, die  inzwischen ins Leere zeigen. Die Files sind in einem hierarchischen Filesystem abgelegt.


### 2. Footer
Fügen Sie vor der dritt-letzten Zeile eines jeden Markdown-Files eine CC-Lizenz ein. D.h. nach den Lizenzzeilen folgen noch 3 weitere aus dem Original-Text. Die Files sind in einem hierarchischen Filesystem abgelegt.


### 3. Syntax konvertieren
Ein Konverter soll Seiten in Zim-Syntax nach Markdown wandeln ohne Einsatz von pandoc.


### 4. Langläufer in Hintergrund 
Programme, die auf der Kommandozeile gestartet wurden (z.B. ein Editor) und irgendwann länger als 2 Minuten laufen, sollen automatisch in den Hintergrund geschoben werden (und dort weiterlaufen), damit die Kommandozeile wieder nutzbar wird.


### 5. Übersichtseite
Die 20 Bilder mit den meisten Pixeln in einem Verzeichnis sollen auf einer ausdruckbaren Din-A4-Seite zusammengestellt werden. Unter jedem Bild soll der Filename und die Bildgröße in MB stehen.


### 6. Sanierer
Filenamen sollen sanieren werden ohne dabei unbeabsichtigt einen anderen File zu überschreiben. Sanierungsbedarf besteht, wenn der Filename Globbing-Opfer werden könnte.


### 7. Auto-Remove
Das Script soll 2 Dinge leisten:
In den Namen eines Files soll ein Löschdatum einkodieren werden und das Script soll solche Files an ihrem Fälligkeitsdatum finden und nach /tmp verschieben. 


### 8. Markdown-Backup
Alle Markdown-Files (manche mit personenbezogenen Daten), die das Schlüsselwort "Backup" enthalten, sollen verschlüsselt in eine (Pseudo-) Cloud gesichert werden.


### 9. Sortierung nach Größe
Es sollen alle Bilder in einem Ordner so umbenannt werden, dass `ls -al`  die Bild-Files nach Breite sortiert anzeigt, d.h. das Bild mit den meisten Pixeln in x-Richtung zuerst, das mit den wenigsten zuletzt.


### 10. News-Backup
Es sollen alle Files, die an einem Tag neu entstanden sind, in eine Liste eingetragen werden. Diese Files sollen dann außer den Bildern verschlüsselt abgelegt werden.

### 11. Lotto-Zahlen
Das Script soll 6 Lotto-Zahlen im [7x7 Format](https://bildagentur.panthermedia.net/m/lizenzfreie-bilder/3908087/lottoschein-/) vorschlagen. 2 Spalten müssen die gleiche Summe haben. Z.B. 2 + 30 in Spalte 2, 32 in Spalte 4.


### 12. Top-Worte
Es sollen die 30 Worte bestimmt werden, die am häufigsten in einem PDF mit min. 1000 Worten vorkommen.


### 13. Umbenennung
Es soll eine Musterlösung geschrieben werden für die kollisionsfreie Umbenennung eines Files unter möglichst weitgehender Beachtung des Umbenennungwunsches. xyz.txt soll z.B. nach a1.txt umbenannt werden, a1.txt existiert es aber bereits. bdfed496-773f-11ed-a14d-63ec6798ffae.txt o.ä. sind keine Lösung.


### 14. Dubletten
Es sollen Dubletten (Files mit gleichem Inhalt) gefunden werden, auch wenn ihre Namen verschieden sind. Eine der Dubletten ist sinnvoll zu ersetzen.


### 15. differentieller Backup
Es existiert eine ältere Sicherung aller Konfigurationsdateien in Form eines tar-Files . Die seitdem entstandenen/veränderten Konfigurationsdateien sollen in einem weiteren tar-File gesichert werden.

### 16. Reiseapotheke sortieren



