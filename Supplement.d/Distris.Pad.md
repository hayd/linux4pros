# Linux-Vorlesung, Herbst 2022

## Wahl einer Distri

### Videobearbeitung
#### Ubuntu Studio
- easy to use, for dayli use.
- variety of media editing software pre-installed

#### Fedora Design Suite
- Vorinstallierte Medien Tools
- Einfach zu benutzen da auf fedora basiert

### uralte Hardware
#### Zorion OS
- umfangreiches Kontrollzentrum "Zorion Grid"
    - Setup und Management von Netzwerken mit zahlreichen Zorion-Devices
- vorinstallierte Pakete (sehr gut für Einsteiger geeignet)
- geringe Systemanforderungen Zorion OS Lite:
    - CPU: 1 GHz Single Core
    - RAM: 1 GB
    - Speicher: 10 GB
    - Display: 800 x 600px

---
### Gaming
#### SteamOS
- von Valve (Videospiel(plattform)entwickler)
- Abgeleitet von Debian
- Leichtgewichtiger als Ubuntu (da kein Overhead)
- Release 2015
- Valve-Entwickler
- Valve
- Download: [SteamOS](https://store.steampowered.com/steamos)

#### PopOS
- OpenSource (von System76 - öffentlich einsehbar aber keine Möglichkeit mitzuentwickeln)
- Basiert auf Ubuntu - OOTB Support Für Nvidia und AMD Grafikkarten
- Abgeleitet von Ubuntu
- Seit 2017
- System76 - nicht OpenSource
- Finanzierung durch Systemverkauf von System76
- Download: [PopOS](https://pop.system76.com)

---

### MacBooks

#### [Elementary OS](https://elementary.io/)
- unterstützt Windows- und MacOS-ähnliche Oberfläche
- pay-what-you-want
- programmiert in Vala und Meson (mglw. nicht so einfach erweiterbar, weil die Programmiersprachen eher exotisch sind)
- basiert auf Ubuntu
- nutzerfreundliche, sinnige Defaulteinstellungen
- scheint [finanziell](https://en.wikipedia.org/wiki/Elementary_OS#Change_of_ownership) etwas zu hinken; kann sein, dass es daran irgendwann scheitert

#### Asahi Linux
- angepasstes Arch Linux für ARM-Prozessoren
- Plasma-Desktop oder minimales Arch-System
- keine GPU-Unterstützung

### Wissenschaft

#### Elementary OS
- durchdacht, schnell, benutzerfreundlich
- datenschutzfreundlich
- quelloffen -> jeder kann mitentwickeln
- durch spenden bzw. kostenpfl. Versionen finanziert
- stabile updates

#### Scientific Linux

- voll kompatibel mit Red Hat Enterprise
- Forschende haben eine gemeinsame Installationsbasis für Experimente
- kommt mit vielen vorinstallierten Paketen, die vor allem in der Wissenschaft häufig Verwendung finden
- auf Stabilität und lange Wartungszyklen ausgelegt
- free

### Steuerfahnder
#### [SystemRescue](https://www.system-rescue.org/)
- keine hohe Anforderung (da nicht mal grafische Oberfläche vorhanden)
- Als Live verfügbar (ausschließlich)
- Kommt mit tool aus zum Editieren der registry von Windows.
- Kann unverschlüsselte Accounts von Windows knacken
- Hat partitionierungstools
- Kann Bitlocker öffnen (Schlüssel notwendig)
- Open Source --> [Git](https://gitlab.com/systemrescue/systemrescue-sources)

#### [Kali](https://www.kali.org/)
+ Pen Testing Distro, Debian basiert
+ ISOs sehr customizable
+ für den Live-Boot Use-Case entworfen -> Zugang zu Computern verschaffen, die kein Anschluss an lokales Netz haben
+ Open-Source Community, verwaltet und finanziert von Offensive Security
