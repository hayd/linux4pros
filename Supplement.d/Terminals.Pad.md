
# Terminals
Pad, Herbst 2022

## Terminator
- *Die Roboterzukunft des Terminals*
- mehrere Terminal-Tabs gittermäßig in einem Fenster
- an erfahrene User gerichtet
- Drag and drop zum Anordnen
- Speichern der Layouts und Profile via GUI preferences editor
- Gleichzeitiges Schreiben in mehreren Tabs(Broadcasting, Multi-Cursor)
- Plugin-Unterstützung
- https://wiki.ubuntuusers.de/Terminator/
- https://gnome-terminator.org/


## Tmux
- Tilling Terminal
- Session Support
- Hotkeytaste -> Ctrl-b
- installieren mit sudo apt install tmux
- in tty nutzbar
- ermöglicht das laufen lassen von Programmen auf Servern auch mit geschlossener ssh verbindung (ähnlich screen)
- Basic Shortcuts
    - Ctrl-b % vertikales Fenster
    - Ctrl-b " horizontales Fenster
    - Ctrl-b Pfeiltaste Fensterwechseln
    - Ctrl-b d Seassion verlassen
    - Ctrl-d Fenster schließen
    - Ctrl-b c Neues Window erstellen
    - Ctrl-b n nächstes Window
    - Ctrl-b p vorheriges Window
![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fblazepress.com%2F.image%2F2020%2F06%2Fcursed-images-feat-1.jpg&f=1&nofb=1&ipt=1a3e997ff4042c51148ecd1e438710aad7c06d69ee06ced8781f697bc13b10df&ipo=images)

## Extraterm
![](https://extraterm.org/empty_terminal_450.png)
#### Links
[Homepage](https://extraterm.org/) // [Git](https://github.com/sedwards2009/extraterm)

#### Features:
- Text und Command Outputs manipulieren direkt mit der Tastatur wie in ein Text Editor
- Linux, macOS und Windows kompatibel
- Unterstützt mehrere Tabs verschiedener Terminals: z.B. basic, Powershell (siehe oben)
- Command Palette:
  - finde/suche und ausführe beliebige Commands direkt mit der Tastatur

![](https://extraterm.org/command_palette.png)
- Images direkt anzeigen: ```show logo.png```
- Shell Integration:
  - Command output ist klar markiert and Rückgabestatus is deutlich sichtbar.

![](https://extraterm.org/selection_mode2.gif)
  - kann aktuelle Downloads anzeigen
- Command-Ausgaben wiederverwenden: ```from {previous command} {this command}```
![](https://extraterm.org/from_command.gif)

## Konsole

## Tilix
- Light Mode (cringe)
- Multiple panels
- Image Support
- Drag and Drop
- Grouping of terminals can be saved and loaded from disk

## [yakuake](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjxxvjMsLD7AhVORfEDHVo0DaEQyCl6BAgUEAM&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DdQw4w9WgXcQ&usg=AOvVaw0aHtehaphMhOCAkCydRLZU)
- "drop-down-terminal", mit F12 ein-/ausfahren, relativ angenehme User Experience
- Auswahl an Tastenkürzeln und visuelle Anpassungsmöglichkeiten
- Max mag es, Nico mag es gar nicht >:O
- unterstützt Tabs und Splitscreens
- ansonsten einfach wie GNOME Shell

## Gnome Terminal
- minimal terminal emulator
- Linux only
- in C entwickelt
- [Repository](https://gitlab.gnome.org/GNOME/gnome-terminal)
### Features
- std features z.b.:
    - mehrere Tabs
    - farbiger Text
    - text-wrap
- besonders gut:
    - Anlegen versch. Profile angepasst an best. Aufgaben
    - konfigurierbare Tastenkombis
    - Mouse-Event Handling: kann mouse scrolling und links/rechtsclick abfangen
    - URL Erkennung
    - Safe quit:
![](https://upload.wikimedia.org/wikipedia/commons/b/ba/GNOME_Terminal_3.32_quit_warning_screenshot.png)

## 2do
- Distri-Wahl in eigenes .md
- 
