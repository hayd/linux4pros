# Refactoring

[Refactoring](https://de.wikipedia.org/wiki/Refactoring) bezeichnet in der Software-Entwicklung die Optimierung der Quelltexte hinsichtlich:
- Lesbarkeit
- Verständlichkeit
- Wartbarkeit
- Erweiterbarkeit
- Vermeidung von Redundanz
- Testbarkeit

Die Funktionalität darf sich dabei nicht ändern.


## Maßnahmen

- informativere Variablen-Namen, d.h. sprechenden Namen für Variablen, Konstanten, Methoden
- Erfüllen eines [Style Guides](https://google.github.io/styleguide/shellguide.html#s7-naming-conventions)
- DRY
- Umformatierung eines Quelltextes, z. B. mit einem Beautifier




## Beispiele

- Variablennamen
- function einführen

- grep zu Suchen der Problemstelle
- diff, kompare zur Kontrolle
- RCS zum Rückgängigmachen



refactoring: andere Begriffe einsetzen mit sed , um Selectivität zu testen
    Serverschrank, Serverrack, Server-Rack --> Server-Schrank
   Vereinheitlichung von Variablennamen: calculate-pi , CalculatePi, CalcPi, CalcPI, Calc_Pi, ,  --> calcPi




## see also
- [explainshell.com - sed -i -e ‘s/class/enum/’ ./billing-method.ts](https://explainshell.com/explain?cmd=sed+-i+-e+%E2%80%98s%2Fclass%2Fenum%2F%E2%80%99+.%2Fbilling-method.ts)
- [Be a Happier Programmer with sed.](https://medium.com/hackernoon/be-a-happier-programmer-with-sed-cc540e0b58d3)