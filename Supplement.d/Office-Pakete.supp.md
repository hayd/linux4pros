# Office Pakete


## LibreOffice

## OnlyOffice
[Eine Stellungnahme](https://wiki.init.mpg.de/share/BAR/Berichte/2020-05-20%2028.%20Bericht%20des%20IT-Sprecherkreises%20an%20den%20BAR?highlight=%28OnlyOffice%29) von Fachleuten der MPG zu OnlyOffice:

Eine überzeugende Alternative im Bereich kollaborativer Online-Office-Anwendungen

Diese Alternative ist aus unserer Sicht ein Produkt wie OnlyOffice. Als Alternative zu einer proprietären Lösung sollte ein solches Produkt formatkompatibel und multiplattformfähig sein. Seine wichtigsten Teile sollten quelloffen und frei nutzbar sein. Es sollte zumindest eine Firma dahinter stehen, die sinnvollen technischen Support und die gezielte Weiterentwicklung anbieten kann. Das trifft auf Only-Office auch im Detail zu:

- Es bietet bietet eine weitgehende Kompatibilität bei Formaten, Funktionalität und Bedienung zu den Office-Produkten verschiedener Hersteller8 . Dazu gehören auch die Eigenformate von LibreOffice und OpenOffice.
- Es ist verfügbar als App für Linux, Windows, macOS, iOS und Android und als serverbasierter kollaborativer Web-Editor, dessen Integration in die für die MPG wichtigsten Kollaborationsplattformen bereits verfügbar ist - Sharepoint, Nextcloud, ownCloud, PowerFolder, Confluence etc. Die Integration in Seafile ist angekündigt.
- Die Nutzung der Desktop- und Mobil-Varianten und der Community-Edition ist frei. Die Community-Edition selbst ist unter AGPL v.3 veröffentlicht. Die Enterprise-Versionen sowie die Integrationen in die genannten Kollaborationsplattformen werden vermarket. Mit der Nextcloud-Lizenzierung haben wir bereits eine Integration lizenziert und verfügbar.
- OnlyOffice ist Teil einer größeren Office-Lösung mit Mail, Kalender etc. und kann über eine Document-Builder-Schnittstelle anprogrammiert werden.
- Hinter OnlyOffice steht eine Firma, die auch Support und Weiterentwicklung anbietet.

Konkret nutzen wir bereits seit einiger Zeit und mit wachsender Begeisterung einen Only-Office-Service des FHI in mehreren File-Sharing-Lösungen mit Nextcloud und PowerFolder nicht nur im FHI sondern MPG-weit in mehreren Installationen - unter anderem auch in der InIT-Plattform des IT-Sprecherkreises.

Was für das FHI gilt, gilt auch MPG-weit: eine einheitliche Dokumentverarbeitung für alle genutzten Systeme anbieten zu können verbessert die Nutzbarkeit des Gesamtsystemes entscheidend und reduziert zudem sogar die Abhängigkeit von eingesetzten Sharing-Plattformen. Welches Werkzeug die Daten zur Verfügung stellt, ist für die Nutzer weniger entscheidend als das Tool, mit dem man die Dokumente bearbeitet.

Für einige wissenschaftliche Communitys ist MS Office auch deshalb interessant, weil es Zusatzmodule gibt, die die Literaturverwaltung unterstützen. Zudem hatten wir keinen Maßstab, weder für die Qualität des angebotenen Supports noch für die Güte gezielt beauftragter Erweiterungen. Deshalb haben wir die Entwicklung von Plugins für die Referenzmanager Zotero und Mendeley beauftragt. Bereits nach wenigen Wochen waren diese Plugins für die gesamte OpenOffice-Community verfügbar. Die Community war begeistert und hat bereits damit begonnen, Hinweise für die Verbesserung der Module zu liefern - so, wie man sich das vorstellt.

Die Finanzierung solcher Erweiterungen sollte aus den entsprechenden Mitteln der Beratenden Kommission Software übernommen werden. Das würde im Gegensatz zur verteilten Finanzierung durch einzelne Institute9 , einen wesentlich besseren Überblick über die Aufwände zur Stärkung von Software-Alternativen garantieren.


## WPS
von Kingsoft, China



## SoftMaker Office
https://www.softmaker.de/softmaker-office
- aus Nürnberg
- Die herausragenden Merkmale der Software von SoftMaker sind ... enorm hohe Kompatibilität mit Microsoft Office ...



## siehe auch
- [Alternativen zu Microsoft Office: Cloud-Office-Pakete im Vergleich](https://www.heise.de/ratgeber/Alternativen-zu-Microsoft-Office-Cloud-Office-Pakete-im-Vergleich-7339468.html) kostet,  Google Workspace, Collabora Online und Canva Visual Worksuite, Microsoft 365
  - Fazit: Die vorgestellten Pakete richten sich an klar definierte Zielgruppen. Google Workspace und Microsoft 365 sind die cloudbasierten Allround-Bundles für alltägliche Office-Aufgaben. Die lassen sich großteils auch mit Collabora Online erledigen, doch muss sich der Admin um das Hosting selbst zu kümmern. Canvas wird noch eine Weile brauchen, bis das Paket wirklich ausgereift ist und genug Optionen für den Großteil der Nutzer bietet.
