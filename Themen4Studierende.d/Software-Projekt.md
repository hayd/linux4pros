# Informationsmanagment unter Linux

## Aufgabe

Mit Hilfe von Markdown-Dateien sollen Informationen niedergeschrieben und erschlossen werden:

- Markdown-Files und ein geeigneter Editor für eigene Notizen
- Sidecar-Files im Markdown-Format für Notizen zu externen Dokumenten (PDFs, Bilder, Präsentationen, URLs, Bücher, ...)
- Anreicherung der Markdown-Files um Schlagworte zwecks Kategorisierung und inhaltlicher Erschließung
- graphische Darstellung der Beziehung der Dokumente untereinander
- Tag-Cloud
- Suche im Volltext und in Metadaten


## Komponenten des Software-Paketes

- freie Software unter Linux wie: 
    - recoll (Suchmaschine)
    - pandoc (Format-Konverter)
    - tesseract (OCR)
    - Graphviz (Visualisierung)
    - git (Versionierung)
    - KI-Programm zur Generierung eines Abstracts
    - tagcloud
    - tar, OpenSSH zur Datensicherung in der Cloud
    - uvm.
- Orchestrierung der vorhandenen Programme durch Shell-Scripte


## Lehrinhalte

- Linux
- Programmieren
- Beurteilung von Software
- kollaboratives Entwickeln


## vergleichbare Software-Pakete
und deren KO-Kriterien

- https://obsidian.md/ - kommerziell
- https://roamresearch.com/ - kommerziell
- https://logseq.com/  - in Clojure geschrieben