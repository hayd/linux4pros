# SquashFS als Backup

## Vorteile

- kompakt, da komprimiert
- ausgereift
- Deduplizierung
- direkter Zugriff


## 2do

- incrementeller Backup
- Vollständigkeit überprüfen



---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  <br />
letzte Änderung: 03. Dez. 2024<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
