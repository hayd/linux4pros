# Organize Pictures

Es soll ein möglichst effektiver Ansatz untersucht werden, um größere Mengen an Bildern zu sortieren und mit Metadaten anzureichern. Es gilt die Erschließung von großen Bildhaufen so effektiv zu realisieren, dass sie auch wirklich vorgenommen wird.

Es soll ein Konzept entworfen werden, das aus freien Software-Komponenten eine modulare Lösung aufbaut, die beliebig erweiterbar ist (z.B. um eine KI-gestützte Gesichtserkenneung oder Bildbeschreibung).
Das Konzept soll in signifikanten Teilen prototypisch realisiert werden.


## Features

- digitale Souveränität
- Freie Software
- Cloud ist eine Option keine Vorraussetzung
- möglichst wenige Abhängigkeiten
- schnell und einfach, sonst wird es nicht gemacht
- für häufigen Gebrauch ausgelegt
- einfache Ergänzung von Metadaten direkt in den Bildern
- Extraktion aller Metadaten für anderweitig Verwendung, z.B. Suche, Präsentationsscript


## Realisierung

- Einsatz eines Drehbuchs, das enthält:
  - Reihenfolge
  - Metadaten
  - aussortierte Bilder
- Es kann nacheinander mehrere Drehbücher nacheinander geben.
- Metadaten als EXIF oder IPTC ins Bild
- Aussortieren in Trash-Ordner, nicht wirklich wergwerfen, um Hemmschwelle zu seneken
- GPS-Daten in Adressen, POIs wandeln



## Randbedingungen

- aus FLOSS-Komponenten aufgebaut
- Diese werden durch ein Shell-Script verknüpft.
- gehostet auf GitHub