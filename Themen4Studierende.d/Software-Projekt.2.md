# Software-Projekt digitales Informationsmanagement



## Komponenten

- Scannen
- Optimierung des Scans
- OCR
- Sidecar-Generator
- Ontologie-Bildung
- klickbarer Graph
- Markdown-Converter: nach PDF, Präsentation, LaTeX
- 




## Randbedingungen

- ausgereifte freie Komponenten
- modular aufgebaut
- nur die Verbindung selbst schreiben
- angelegt als dauerhafte Projekt, in dem ständig Module ergänzt oder ausgetauscht werden
- auf GitLab, GitHub
- KI-gestützt
- agile Entwicklung
