# linux4pros - Linux für den professionellen Einsatz


![Tux - Linux-Logo](./Pics.d/tux.png)

![GNU / Linux](./Pics.d/gnu-linux.png)


## Wo zu finden?

- die [Vorlesung für 5. Semester](https://gitlab.gwdg.de/hayd/linux4pros/-/blob/0502b3b8afe1c7d51f374782044e657bb803b57d/Vorlesung.d/0-ToC.md): [https://t1p.de/linuxgit](https://t1p.de/linuxgit) / Vorlesung.d / 0-ToC.md
  - oder direkt zum Inhaltsverzeichnis: https://t1p.de/linuxba5
- [Linux for Beginners](https://gitlab.gwdg.de/hayd/linux4pros/-/blob/0502b3b8afe1c7d51f374782044e657bb803b57d/L4B.d/1-ToC.md), für das 3. Semester:  [https://t1p.de/linuxgit](https://t1p.de/linuxgit) / L4B.d / 1.ToC.md
  - oder direkt zm Inhaltsverzeichnis: https://t1p.de/linuxba3
- die Pads: [https://t1p.de/linuxpad](https://t1p.de/linuxpad)
  - [Verbesserungen für Vorlesung](https://pad.gwdg.de/DVR_OPQLQyadpdEbmTljzQ#)




---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  <br />
letzte Änderung: 2024-10-27<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>