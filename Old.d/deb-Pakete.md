# Debian-Pakete

## Warum Pakete ?

- einfache Installation, rückstandsfreie De-Installation
- Abhängigkeiten werden aufgelöst, z.B. fehlende Libraries 
- formale Vorgaben, so dass Tools wie dpkg, apt damit umgehen können.
- Der Paketbau beinhaltet ein Qualitätsmanagement, der [Maintainer](https://wiki.debian.org/DebianMaintainer) muss sich  qualifizieren und Vertrauen erarbeiten.
- Alle Pakete einer Version einer Distri kommen sich normalerweise nicht in die Quere und beruhen auf einer gemeinsamen Basis (Satz von Libraries), die ausführlich getestet wird (im [Feature Freeze](https://linuxnews.de/2022/10/17/debian-freeze-fuer-debian-12-bookworm-rueckt-naeher/)).

## Pakete ala Debian

- Es gibt 2 Arten von Debian-Paketen: [Binärpakete, Quellpakete](https://de.wikipedia.org/wiki/Debian-Paket)
- Ein Binärpaket muss min. bestehen aus: ausführbaren Dateien, Konfigurationsdateien, man/info-Seiten, MD5-Prüfsummen, Copyright-Informationen, andere Dokumentation und den Maintainer-Skripten namens [preinst, postinst; prerm, postrm](https://debiananwenderhandbuch.de/debianpaketeerstellen.html#debianpaketeerstellenpostinst)
- Pakete kann man mit [lintian](https://book.dpmb.org/debian-paketmanagement.chunked/ch37s01.html) prüfen.
- Datenbank: Flat file database in /var/lib/dpkg

## Suche nach Paketen

- [Ubuntu-Paketsuche](https://packages.ubuntu.com/de/) via Browser
- [Kategorien](https://packages.ubuntu.com/de/jammy/) im Browser sichten
- [How to Search for Ubuntu Packages](https://linuxhint.com/search-ubuntu-packages/)
- apt search sucht in Namen und Beschreibung:  
`$ apt search agrep`
- nur nach Namen (-n) suchen:  
`$ apt search -n agrep`

## dpkg

- Basis der höheren Installationstools apt*
- löst keine Abhängigkeiten auf
- .deb installieren:  
  `$ sudo dpkg -i ./zoom_amd64.deb`
- .deb de-installieren:  
  `$ sudo dpkg -r  agrep`
- Konfigurationsdateien auch noch entfernen:  
  `$  sudo dpkg -r --purge agrep`

dpgk kann man auch für Inventuren nutzen: 

- Anzeige aller installierten Pakete:  
  `$ dpkg -l`
- vereinfachte Anzeige aller installierten Pakete:  
  `$ dpkg --get-selections`
- Die angezeigte Liste kann man verwenden, um eine [identische Installation](https://linuxprograms.wordpress.com/2010/05/13/dpkg-set-selections-clone/), soweit es die installierten Pakete betrifft, zu erstellen:  
  `$ sudo dpkg --set-selections < ./packages_list`
- Anzeige aller Files in einem Paket:  
  `$ dpkg -L zim` #   588 Files
  - zoom_amd64.deb besteht aus 1380 Files
- siehe auch: [bei UU](https://wiki.ubuntuusers.de/dpkg/)

## apt, apt-get

- apt=  Advanced Packaging Tool
- apt* und dpkg nutzen eine Datenbank, die alle Metadaten über die Pakete und ihren Installationszustand enthält

### apt vs apt-get

- apt ist eine eigenständige Weiterentwicklung von apt-get
- apt ist gegenüber apt-get stärker für das interaktive Arbeiten auf der Kommandozeile optimiert.
- [Vergleichstabelle](https://itsfoss.com/apt-vs-apt-get-difference/) apt commands vs. apt-get commands

## apt-cache

- [apt-cache](https://wiki.ubuntuusers.de/apt/apt-cache/#apt-cache-showpkg) tut nichts, es dient nur der Verwaltung der APT-DB
- Anzeige der Metadaten eines Paketes:  
  `$ apt-cache showpkg agrep`

## Infos sammeln

- Suche in Namen und Beschreibungen:  
  `$ apt search agrep`
- Suche nur in Namen:  
  `$ apt search -n agrep`
- Abhängigkeiten anzeigen:  
  `$ apt depends agrep`
- reverse dependencies anzeigen: Welche Pakete benötigen libc6 als Vorraussetzung:  
  `$ apt rdepends libc6`

### Paket-Pflege

- Paket installieren mit Auflösung von Abhängigkeiten:  
  `$ sudo apt install agrep`
- bereits heruntergeladenes Paket installieren mit Auflösung der Abhängigkeiten, Pfadangabe zwingend erforderlich:  
  `$ sudo apt install ./zoom_amd64.deb`
- Neueinlesen der Paketlisten als Voraussetzung für Upgrades:  
  `$ sudo apt update`
- Pakete upgraden (nur bug fixes, keine neuen Features):  
  `$sudo apt upgrade`
- Paket entfernen:  
  `$ apt remove agrep`
- Paket + Konfigurationsdateien entfernen:  
  `$ apt purge agrep`
- Reste eines missglückten Installationsversuchs abräumen:  
  `$ sudo apt --fix-broken install`

## PPA ergänzen

PPA = Personal Package Archive

- Pakete von Personen, die keinen [Maintainer](https://wiki.debian.org/DebianMaintainer)-Status haben oder aktuellere Versionen anbieten, in einem privaten, unkontrollierten Repository.
- Beispiele: atom (Editor), Zim, VS Code, KeePassXC
- PPA hinzufügen:  
  `$ sudo add-apt-repository ppa:LAUNCHPAD-NUTZERNAME/PPA-NAME`
- In /etc/apt/sources.list.d/ erfolgt ein zusätzlicher Eintrag.
- [PA bei UU](https://wiki.ubuntuusers.de/Paketquellen_freischalten/PPA/#PPA-hinzufuegen)


## Files

- In /var/cache/apt/archives werden .deb beim Download abgelegt.
-  

## Sicherheit

- Pakete täglich upgraden. So sieht man, was aktuell passiert:  
  `$ sudo apt  update && sudo apt upgrade`
  - Es werden nur Bugfixes, keine neuen Features installiert.
- PPAs bergen Risiken.


## Open Build Service

- .deb, .rpm Pakete haben Vorteile
- Ihr Erstellung ist mühsam  und verlangt eine besonderen Status des Paketbauers oder ein PPA
- Der Open Build Service https://openbuildservice.org/ kann in einem Zug Pakete für alle wichtigen Distris bauen - zur privaten Verwendung, ...


## siehe auch

- [Grundlagen des Debian-Paketverwaltungssystems](https://www.debian.org/doc/manuals/debian-faq/pkg-basics.de.html)
- [APT Übersichtsseite](https://wiki.ubuntuusers.de/APT/)
- [apt bei UU](https://wiki.ubuntuusers.de/apt/apt/)
- [Linux Packages Under The Hood](https://www.juliensobczak.com/inspect/2021/05/15/linux-packages-under-the-hood.html) 
- [Nala: APT on Steroids](https://linuxnews.de/2022/02/24/nala-apt-on-steroids/) - ein Front-End für libapt-pkg in Python: schneller, informativer, hübscher

## Aufgaben

- apt automatisieren
- 
