# Schleifen


## for

Iteriert wird über alles, was nach `in` steht.

```sh
for i in $( ls )
do
    echo item: $i
done
```

- `$( cmd )`  setzet an diese Stelle das Ergebnis des cmd

```bash
for animal in dog cat 'fruit bat' elephant ostrich
do ...
```

```bash
for thing in $collection_of_things
do  ...
```

```bash
for i in $(seq 1 100)
do
  echo $i
done
```

- `seq`
  - normaler Linux-Befehl: seq - print a sequence of numbers (s. man seq
  - `$ seq  0 3 30`    # INCREMENT ist optional


### Hochzählen in der Schleife

```sh
N=100
for FN in $(ls -1)
do
    ln -s $FN $N
    ((N++))          # hier wird hochgezählt                   
done
```

## while


```bash
N=0
while [ $N -lt 100 ]
do
  echo $N
  N=$(($N+14))
  sleep 1
done
```

### endless loop

```bash
while  true;  # loop forever
do
   echo "läuft"
   sleep 1
   echo " Enter something to continue or Ctrl-C to stop. "
   read answer   #     to stop the loop here
done
```

## vorzeitig beenden

### break (raus-springen)

[break](https://linuxize.com/post/bash-break-continue/) beendet die aktuelle Schleife und übergibt die Programmkontrolle an den Befehl, der auf die beendete Schleife folgt.

### continue (eine Runde auslassen)

[continue](https://linuxize.com/post/bash-break-continue/) überspringt die verbleibenden Befehle innerhalb des Schleifenkörpers  für die aktuelle Iteration und übergibt die Programmkontrolle an die nächste Iteration der Schleife.


## siehe auch

- [What is the Right Way to do Bash Loops?](https://www.shell-tips.com/bash/loops/#gsc.tab=0)
