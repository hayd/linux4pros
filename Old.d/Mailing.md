

Da ich gerade auch in anderen Zusammenhang einiges zum Thema Footer und Gestaltung einer e-Mail zusammengesucht habe, möchte ich auch Sie nicht damit verschonen:


HTML E-Mail ist gefährlich und schädlich.
 https://www.heise.de/security/dienste/HTML-E-Mails-472898.html
 https://mac-service.koeln/htmlemail.html

Wir haben leider ganz direkte Erfahrungen mit diesen Gefahren machen müssen, so dass sich mein (ich bin auch Sicherheitsbeauftragter des Instituts) Verständnis für diese Dekorationen in engen Grenzen hält.

Was wollen Sie also mit HTML-E-Mails vermitteln?
- Ihre Sicherheit ist mir egal.
- Sie sind so unbedarft, dass Sie die Gefahren von HTML-E-Mails ohnehin nicht kennen.
- Wenn das alle machen, können wir auch nicht gegen den Strom schwimmen.


Sinnlose Footer
 https://angstklauseln.wordpress.com/hintergrund/


Mir ist durchaus bewusst, dass Sie sicherlich nicht der "Hauptschuldige" sind, aber diese erreiche ich nicht.

