# Filesysteme


## lokale Filesysteme

Anzeige der auf dem Host gerade unterstützen Filesysteme:  
 `$ more /proc/filesystems`


### Wie heisst mein Filesystem?

Auflisten, was lokal gemountet (dem Betriebssystem verfügbar gemacht) ist und Zeilen herausfiltern, die mit "/dev/" beginnen:

`$ mount -l | grep ^/dev/`

Hinter "type" steht das Filesystem.


## spezielle Features

Bei der Auswahl eines Filesystems überlege man sich, ob man neben Stabilität und einer gewissen Performance eines dieses verbreiteten Features benötigt:

- Redundanz - [RAID](https://de.wikipedia.org/wiki/RAID), [erasure code](https://docs.ceph.com/en/reef/rados/operations/erasure-code/)
- [Snapshots](https://de.wikipedia.org/wiki/Schnappschuss_(Informationstechnik)#Massenspeicher)
- Verschlüsselung
- Komprimierung
- Deduplizierung


### ext4

- [home](https://www.kernel.org/doc/html/latest/filesystems/ext4/)
- Wenn keine besonderen Anforderungen, dann ext4.
- ext = extended filesystem
- ext3:  journaling eingeführt
- ext4: bessere Performance, größere Files möglich
- ext4 seit Linux Kernel 2.6.28 (=Oktober 2008),  rock solid
- **Obergrenzen** (z.T. bei Anlegen veränderbar):
  - File-Größe: 16 TB
  - Partition-Größe: 1 Exabyte (1 Mio. Terabyte)
  - Anzahl Files: 4 Milliarden
  - Länge des Dateinamens: 255 bytes
- mit [Journal](https://www.maketecheasier.com/journaling-in-file-system/), erleichtert Reparatur
  - Alle Änderungen werden vor dem eigentlichen Schreiben in einem dafür reservierten Speicherbereich, dem Journal, aufzeichnet. Damit ist es zu jedem Zeitpunkt möglich, einen konsistenten Zustand der Daten zu rekonstruieren, auch wenn ein Schreibvorgang an beliebiger Stelle abgebrochen wurde. Gilt meist nur für Metadaten, d.h. FS immer konsistent, aber Datenverlust möglich.
  - In SSD-Zeiten nicht mehr so wichtig.
  - [Journaling-Dateisysteme](https://de.wikipedia.org/wiki/Journaling-Dateisystem#Auswahl_von_Journaling-Dateisystemen)
- **ext5** wird es nicht geben.

### sonstige

- [exFAT](https://wiki.ubuntuusers.de/exFAT/) für überall nutzbare USB-Sticks, Datenaustausch mit anderen Welten
  - FAT32, vFAT
- XFS
  - [XFS](https://de.wikipedia.org/wiki/XFS_(Dateisystem)) ist auf hohe Skalierbarkeit ausgelegt und bietet nahezu native I/O-Leistung, selbst wenn das Dateisystem mehrere Speichergeräte umfasst.
  - Obergrenzen (z.T. bei Anlegen veränderbar):
    - File-Größe: 8 Exabyte
    - Partition-Größe: 16 Exabyte (16x 1 Mio. Terabyte)
    - Anzahl Files: 2^63
    - Länge des Dateinamens: 255 bytes
- ZFS
  - [ZFS-Vortrag](./ZFS.vortrag.handout2.pdf)
  - [ZFS auf Linux](https://de.wikibooks.org/wiki/ZFS_auf_Linux) ein Wikibook, [bei WP](https://de.wikipedia.org/wiki/ZFS_(Dateisystem))
  - **Designziele, Features**: einfache Administration (nur 2 Befehle), hohe Geschwindigkeit, Kapazität wird nur durch Hardware begrenzt
  - mehr als ein Next Generation Filesystem. Folgende Komponenten bilden eine funktionale Einheit, die Informationen austauschen und daher mehr leisten als die Summe der Einzelteile:
    - Filesystem mit Komprimierung, Snapshots, Replication, Prüfsummen, Verschlüsselung
    - Logical Volume Manager - erlaubt dynamisch veränderbare Partitionen auch über mehrere Festplatten hinweg
    - [RAID](https://de.wikipedia.org/wiki/RAID) -Controller
    - Monitoring-Tool
    - (via iSCSI als) Block-Device
- Btrfs
  - zahlreiche Gemeinsamkeiten mit ZFS
  - Bei Facebook laufen Millionen Server mit einfachen Consumer-SSDs auf Btrfs.  
- ReiserFS
  - [Autor](https://de.wikipedia.org/wiki/Hans_Reiser_(Informatiker)) sitzt wegen Mord im Gefängnis
  - interessante Features
  - mal im Kernel, mal nicht, ab 2025 nicht mehr

- [SquashFS](https://de.wikipedia.org/wiki/SquashFS)
  - komprimiertes read-only Dateisystem in einen File gepackt (think mountable tar.gz.)
  - SquashFS wird häufig zusammen mit [UnionFS](https://de.wikipedia.org/wiki/UnionFS)  verwendet, um temporär auch Schreibzugriff auf Dateien zu erhalten.
  - Wird u.a. in Containern (Singularity), Live-CDs, Snap genutzt.
  - dedupliziert
  - Es gibt zahlreiche [Tools](https://wiki.ubuntuusers.de/SquashFS/) dafür.
  - besser für Archivierung oder [Backup](https://wiki.archlinux.org/title/Full_system_backup_with_SquashFS) als tar.gz
- loopback
  - Mounten einer einzelnen Datei (SquashFS, ISO-Image einer CD) als Dateisystem


## netzwerkweite Filesysteme

![NFS](../Pics.d/NFS.png)

- [NFSv3](https://wiki.ubuntuusers.de/NFS/) - Mutter der netzwerkweiten Filesysteme unter Unix
  - alle nötigen Pakete im Repo verfügbar
  - Der Server exportiert Verzeichnisse/Partitionen, die ein  Client mounten kann.
  - Der Client kann hierfür den Automounter [Autofs](https://wiki.ubuntuusers.de/Autofs/) nutzen, der bei Bedarf (z.B. cd) mountet. Via [LDAP](https://de.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol) kann man dem Automounter die [Infos](https://www.selflinux.org/selflinux/html/autofs02.html) zur Verfügung stellen, welches Verzeichnis auf welchem Rechner liegt und mit welchen Optionen gemountet werden kann. Auf diese Art kann man riesige netzwerkweite Filesysteme erstellen.
  LDAP kann man in erster Näherung mit Active Directory vergleichen.
- NFSv4
  - Sicherheit durch [Kerberos](https://de.wikipedia.org/wiki/Kerberos_(Protokoll)) möglich
  - [sehr kompliziert](https://wiki.init.mpg.de/share/NfsWorkshop), keine einheitlichen Tools
- [Samba](https://de.wikipedia.org/wiki/Samba_(Software))
  - verbindet die Microsoft-Filesysteme mit Linux
- [Ceph](./ceph.md) - skaliert vertikal **und**  horizontal
- [sshFS](https://github.com/libfuse/sshfs) (verwaist)
  - kann das Filesystem entfernter Rechner via SSH in das eigene Dateisystem sicher einbinden kann.
- [Quobyte](https://www.quobyte.com/) - netzwerkweit, schnell, einfach, aus Berlin


## Tools

[VFS](../Filesystem.d/VFS.md) erleichtert das Schreiben von Tools für Filesysteme

- [gparted](https://wiki.ubuntuusers.de/GParted/) - Partitionierung eines Laufwerks
  - GPT-Schema verwenden
  - [Platte labeln](https://wiki.ubuntuusers.de/Labels/#Label-in-der-Praxis-verwenden)
- [tunefs](https://wiki.ubuntuusers.de/ext/#tune2fs) - Änderungen der  Parameters eines Dateisystems (z.B. Deaktivierung des Journalings, Intervall zwischen Filesystem-Checks)
- [bonnie++](https://wiki.ubuntuusers.de/bonnie%2B%2B/) - einfaches Tool für I/O-Benchmark, hier die schnelle Version:
  - `$ time bonnie++  -d  ./Test -n 0  -f -b`
    - -d: in diesem Directory die Testdaten ablegen
    - -n 0: Zahl der Files für creation test
    - -f: fast, keine per-char IO tests
    - -b: no write buffering


## siehe auch

- [Dateisystem](https://wiki.ubuntuusers.de/Dateisystem/) - gute Auflistung mit wichtigsten Merkmalen bei UU
- [Dateisysteme für Linux](https://apfelböck.de/dateisysteme-fuer-linux/)
- [Filesystems in the Linux kernel](https://docs.kernel.org/filesystems/index.html)
- [Archlinux-Wiki](https://wiki.archlinux.org/title/file_systems)
- [Comparison of file systems](https://en.wikipedia.org/wiki/Comparison_of_file_systems) - lange Liste
- [Introduction to the Linux File System](https://phoenixnap.com/kb/linux-file-system)
- [Comparison of kernel and user space
file systems](https://edoc.sub.uni-hamburg.de/informatik/volltexte/2015/210/pdf/bac_duwe.pdf)


<!---
Verweis auf FUSE.md einfügen
-->



---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Admin Empfehlungen<br />
letzte Änderung: 2024-10-21<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
