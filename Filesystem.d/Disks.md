# Disks

## TL;DR

- Kaufempfehlung: M.2-NVMe
- drehende Festplatte, nur wenn viel Speicherplatz benötigt wird


## Mount Point

Der Mount Point ist ein Verzeichnis, an dem ein Device (HDD, SSD, ...) angebunden wir.

`$ df -hl`

- Die 1. Spalte zeigt die Devices.
- Die letzte Spalte zeigt die Mount Points.


## HDD oder SSD?

### Kapazität

#### HDD

- 3 TB SATA, HGST Deskstar: 25€/TB
- HGST Ultrastar 7K6000 6TB, SAS 12Gb/s (HUS726060AL5210 / 0F22791): 39€/Tb
- Obergrenze: 22 TB

#### SSD

- OWC Mercury Electra 6G 1TB, SATA: 104€/TB


## HDD-Auswahl

- [MTBF](https://de.wikipedia.org/wiki/Mean_Time_Between_Failures) als Kriterium
  - Mean Time Between Failures: Der Erwartungswert der Betriebsdauer zwischen zwei aufeinanderfolgenden Ausfällen.
- geeignet für Dauerbetrieb, z.B. [HGST Ultrastar](https://geizhals.de/?cat=hde7s&xf=10287_Ultrastar+He%7E957_HGST%7E960_5) Serie
- Aufnahmeverfahren: Conventional Magnetic Recording (CMR), nicht (!) [Shingled Magnetic Recording (SMR)](https://de.wikipedia.org/wiki/Shingled_Magnetic_Recording),
- [Workshop mit Seagate](https://wiki.init.mpg.de/IT4Science/FragenAnSeagate?highlight=%28seagate%29), s. Fragen 3., 4., 5., 7., 9., **14.**, **15.**


## SSDs

- Problem:  Zelle nach 3.000 bis 100.000 Löschzyklen kaputt. Lösch-Zyklen zerstören die [Oxidschicht](https://de.wikipedia.org/wiki/Flash-Speicher#Anzahl_der_L%C3%B6schzyklen) in den Transistoren Stück für Stück.
- [diverse  Maßnahmen](https://www.thomas-krenn.com/de/wiki/Solid-State_Drive#Schreibzugriffe) sorgen für gleichmäßige Abnutzung ([Wear Leveling](https://de.wikipedia.org/wiki/Solid-State-Drive#Wear-leveling)) und Redundanz
- NVMe (nonvolatile memory PCI Express),
  - über PCI Express angebunden wie Steckkarte (#lanes, PCIe-Generation?)
  - ermöglicht ein sehr hohes Maß an Parallelität
  - Schreibgeschwindigkeit: >3300 MByte/s


### TBW

= TeraBytes Written

- Menge an Daten, die ohne Ausfall garantiert (?) auf eine SSD geschrieben werden kann
- zuverlässiger Wert, siehe [Seagate-Workshp](https://wiki.init.mpg.de/IT4Science/FragenAnSeagate?highlight=%28TBW%29)


## Loopback Device

- Mit einem Loopback-Device kann man ein Block Device **simulieren**, das aber nicht wirklich ein Hardware-Gerät ist, sondern eine **Datei** mit einem eigenen Filesystem.
- Mithilfe spezieller Treiber können auch Loopback Devices transparent **komprimiert** oder **verschlüsselt** werden.
- Wird eingesetzt für Troubleshooting, oder wenn ein physisches Geräte zu teuer, zu umständlich, zu langsam, zu groß ... ist.
- Alle **Snaps** sind als Loopback-Device eingebunden:  
`$ mount -l | grep squashfs`


## siehe auch

- [HDD, SSD oder NVMe: Festplatte nur für große Datenmengen, sonst stets SSD kaufen](https://www.techstage.de/ratgeber/hdd-ssd-oder-nvme-festplatte-nur-fuer-grosse-datenmengen-sonst-stets-ssd-kaufen/1r5wf2t#nav-ring-53)
- [Artikelserie zu SSDs](https://wiki.ubuntuusers.de/SSD/)
- [33 HDD Auswahlkriterien](https://geizhals.de/?cat=hde7s)
- [38 SSD Auswahlkriterien](https://geizhals.de/?cat=hdssd)
- [SSD-Ausfälle: Western Digital verspielt alles Vertrauen | heise online](https://www.heise.de/meinung/SSD-Ausfaelle-Western-Digital-verspielt-alles-Vertrauen-9352972.html)



---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Hardware<br />
letzte Änderung: 2025-02-12<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>



<!--- Artikel nachtragen HDD vs SSD  -->
