head	1.1;
access;
symbols;
locks
	helmut:1.1; strict;
comment	@# @;


1.1
date	2023.01.11.15.02.40;	author helmut;	state Exp;
branches;
next	;


desc
@initial
@


1.1
log
@Initial revision
@
text
@# Filesystem Hierarchy Standard (FHS)

Hierarchisches Filesystem: hierarchisch = hat Unterverzeichnis, die eine baumartige Struktur bilden können

## Unterteilung

- static oder variable
- shareable oder unshareable

|              | shareable       | unshareable |
|--------------|-----------------|-------------|
| **static**   | /usr            | /etc        |
|              | /opt            | /boot       |
| **variable** | /var/mail       | /var/run    |
|              | /var/spool/news | /var/lock   |


Wichtig für integrity checker,  [Einbruchserkennung](https://www.selflinux.org/selflinux/html/ids.html) (= hostintrusion detection systems), Security Information and Event Management-System (SIEM)  wie:
- [OSSEC](https://en.wikipedia.org/wiki/OSSEC)
- [Splunk](https://de.wikipedia.org/wiki/Splunk)
- [Tripwire](https://de.wikipedia.org/wiki/Open_Source_Tripwire)
- [Samhain](https://en.wikipedia.org/wiki/Samhain_(software)), [bei SelfLinux](https://www.selflinux.org/selflinux/html/samhain.html)
- [AIDE](https://de.wikipedia.org/wiki)


## Die wichtigsten Verzeichnisse

###  /sbin
- Essential system binaries
- s wie singel user mode
- nur für root nötig, meist für device management
- grub, mkfs.ext4,fsck,  mkswap, mount, modprobe, adduser


###  /usr
- /usr (engl.: Unix System Resources oder user mit /usr/home). Dieser Bereich kann von mehreren Rechnern gemeinsam verwendet werden (shareable) und enthält dementsprechend keine vom lokalen Rechner abhängigen oder zeitlich variable Inhalte.
- Als UNIX zum ersten Mal (197x) geschrieben wurde, lagen  /bin und sich /usr/bin physisch auf 2 verschiedenen Datenträgern: /bin auf einem kleineren, schnelleren (teureren) Datenträger und /usr/bin auf einem größeren, langsameren Datenträger. Nun /bin ist ein symbolischer Link zu /usr/bin: Sie sind im Wesentlichen das gleiche Verzeichnis.
- /bin enthält nur das nötigste an Systemprogrammen, /bin muss auf der /-Partition liegen, da der mount-Befehl in /bin liegt. /usr/bin enthält fast alle anderen Programme, /usr darf auf anderen Platten oder Partitionen liegen.
- /bin -> /usr/bin/

###  /bin ,  /usr/bin
- command binaries
- wichtig Befehle für alle (root und user)
- 3k files
- Sub-Dirs verboten (Wie überprüfen?)

###  /usr/local
- Verzeichnisstruktur wie /usr
- für Programme, die man an der Paketverwaltung vorbei installieren werden, aber sonst FHS erfüllen

### /opt
- für Software, die nicht in Paketen daherkommt, sondern eher proprietär, monolithisch, in the "Windows" style

###  /lib
- essential shared libraries und Kernelmodule
- /lib/modules: Kernelmodule

### /dev
- device files
- enthält alle Gerätedateien, über die die Hardware im Betrieb angesprochen wird
- /dev/zero, /dev/null, /dev/random, /dev/ecryptfs
- Treiber liegen z.B. in /lib/modules, /boot, /usr/lib/xorg/modules/drivers

### /proc (processes)
- Schnittstellen zum aktuell geladenen Kernel und seinen Prozeduren
- /proc/cpuinfo  /proc/filesystems  /proc/version
- /proc/123/: Infos zum Prozess 123

### /boot
- alle zum Systemstart erforderlichen Dateien

### /etc
- alle systemweiten config files aller Programme  = registry
- keine Executables
- 18 MB groß, d.h.  ganze Systemkonfiguration einfach zu sichern

### /var
- variable Dateien
- Log-Dateien, zu druckendes, caches

### /media
- mount point für Wechselmedien

### /home
- Home-Verzeichnis der User außer root (/root)

### /tmp
- temporäre Dateien
- Wird bei Booten geleert: ein Feature und eine Gefahr

### /lost+found  
- nicht bei allen Filesystemen (z.B. xfs)
- Dateien und Dateifragmente, die beim Versuch, ein defektes Dateisystem zu reparieren, übrig geblieben sind.
- Sollte leer sein.


## see also
- https://wiki.ubuntuusers.de/Verzeichnisstruktur/
- https://www.linuxfordevices.com/tutorials/linux/linux-filesystem-hierarchy
- [FHS](https://de.wikipedia.org/wiki/Filesystem_Hierarchy_Standard) Hierarchy Standard – Wikipedia
- https://refspecs.linuxfoundation.org/FHS_3.0/fhs-3.0.pdf
- https://refspecs.linuxfoundation.org/FHS_3.0/fhs/index.html
- https://geek-university.com/fhs-filesystem-hierarchy-standard/
https://refspecs.linuxfoundation.org/FHS_3.0/fhs/index.html


## ergänzen
- .files  hidden files
@
