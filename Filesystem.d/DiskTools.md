# DiskTools

## Standard-Befehle

### df (disk free)

- listet Nutzung des Datenträgers
- wichtigste Optionen: -h -l
- ohne snap Pakete:  
   `$ df -hl | grep -v snap`
- Wo bin ich und wie viel Platz habe ich  hier noch?  
   `$ df -h .`


#### moderne Alternativen

- duf
  - https://github.com/muesli/duf


### du (disk usage)

- zeigt Größe von Ordnern
- wichtigste Optionen: -h -s
  - `-B` = --block-size=SIZE, SIZE:  K,M,G,T,P,E,Z,Y
- Auflistung alle Dateien und Ordner in \$PWD in MB, sortiert nach Größe, größte zuerst:  
   `$ du -s -BM  * | sort -nr`
- ohne Bilder:  
   `$ du -hs --exclude="*.jpg"  *`


#### moderne Alternativen

- dust
  - = du + rust, aber intuitiver
  - https://github.com/bootandy/dust
- gdu
  - in Go geschrieben, parallelisierbar
  - https://github.com/dundee/gdu
- ncdu
  - NCurses Disk Usage
  - https://dev.yorhel.nl/ncdu
- dutree
  - in Rust, zeigt Bäume
  - https://github.com/nachoparker/dutree
- pdu
  - parallelisiert, schnell, liefert Graphik
  - https://github.com/KSXGitHub/parallel-disk-usage


## Tools

### filelight

- visualize the disk usage, KDE
- https://apps.kde.org/de/filelight/

### baobab

- GNOME disk usage analyzer
- http://www.marzocca.net/linux/baobab/


### ext4magic

- [ext4magic](https://manpages.ubuntu.com/manpages/bionic/man8/ext4magic.8.html) - recover deleted files on ext3/4 filesystems


### siehe auch

[Festplattenbelegung](https://wiki.ubuntuusers.de/Festplattenbelegung/)


## Aufgaben

- Finden Sie mit `filelight` oder `baobab`  die größten Plattenfüller im System und in Ihrem `~` .
- Für welche Zwecke ist das "Treemap Chart" in baobab besser geeignet als das "Rings Chart"?


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Tools<br />
letzte Änderung: 2025-01-20<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
