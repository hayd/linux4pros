# VFS - Virtual File System

## Basics

- VFS: eine  **Schicht** zwischen Betriebssystem und Dateisystem. Sie dient dazu, den Zugriff (z.B. Datei anlegen, Daten schreiben, Ordner löschen) auf die verschiedenen Dateisysteme zu **vereinheitlichen** und so die Integration neuer Filesysteme zu erleichtern.
- Das VFS bildet also die **Abstraktion** und die einheitliche Schnittstelle, in die alle Dateisystem-Treiber (weit mehr als 40) integriert werden können.

![VFS](../Pics.d/vfs.gif)

- Nebenprodukt: Co-Existenz beliebig vieler File-Systeme in einem **einzigen Namensraum**. Z.B:
  - /home1/user1 auf lokaler Platte mit **ext4**,
  - /home2/user2  auf Fileserver im Netz via **NFS**.
- VFS kann keine Dateinamen länger als **255 Bytes** verwalten, auch wenn das Dateisystem mehr ermöglichen würde (z.B: [ReiserFS](https://de.wikipedia.org/wiki/Reiser_File_System)).
  - Man sollte den **Namen** nicht als Abstract verwenden (z.B: Linus_Torvalds_ueber_Neues_im_Kernel_5.5.txt), sondern [sidecar files](https://en.wikipedia.org/wiki/Sidecar_file) anlegen.


## Page Cache

- Linux-Prinzip: freier Speicher ist verschwendeter Speicher
- beschleunigt Schreib-, Lese-Zugriffe
- Beim erstmaligen Lesen von oder Schreiben auf Datenträgern  werden Daten in ungenutzten Bereichen des Arbeitsspeichers gecacht.
- Anzeige (Spalte buff/cache) wie viel gerade gecacht wird: `$ free -h`


## Inode

- Inode (index node) ist eine Datenstruktur, die alle **Metadaten** für eine Datei enthält. z.B: Art, Größe, owner, Zugriffsrechte, [atime, ctime, mtime](https://wiki.magenbrot.net/books/dateisysteme/page/mtime-ctime-atime-was-sind-die-unterschiede)
- besitzt eine eindeutige Nummer
- Metadaten in Inode anzeigen: `$ ls -alFh`
- Ausgabe:
  - dir or not, file permissions
  - number of (hard) links, bei File: 1, bei Ordnern: >1
  - owner name
  - owner group
  - file size in bytes, **-h** macht es übersichtlicher
  - time of last modification
  - file/directory name, **-F** macht Ordner kenntlich
- Inode-Nummer anzeigen: `$ ls -i`
- [mehr](https://www.routech.ro/de/wie-funktionieren-linux-inodes/)


### stat

[stat](https://wiki.ubuntuusers.de/stat/)  zeigt noch wesentlich mehr Metadaten an:  
`$ stat --help`  
Z.B. time of last access, modification, change, time of file birth


## siehe auch

- [Introduction to the Linux Virtual Filesystem](https://www.starlab.io/blog/introduction-to-the-linux-virtual-filesystem-vfs-part-i-a-high-level-tour)
- [WP](https://en.wikipedia.org/wiki/Virtual_file_system)

## Zusätzliche Infos

- Allgemeine Objekte im VFS:
  - Superblock: speichert Informationen über ein Dateisystem (Größe, Zeiger auf Liste der freien Blöcke, Anzahl freier Inodes, div. Zeiger)
  - Inode: s.o.
  - Fileobject: speichert Information zur Interaktion zwischen einer geöffneten Datei und einem Prozess, existiert nur zur Laufzeit im Kernelspeicher
  - Dentry: (=directory entry) ordnet einer inode number einen file name zu.


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Wissen<br />
letzte Änderung: 2025-01-20<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
