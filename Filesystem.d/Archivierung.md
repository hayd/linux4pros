# Archivierung


<!--- 2do ## Unterschied Archiv - Backup, als Tabelle   -->


## Archiv vs. Backup

Backup:

- **automatisch** jede Nacht
- Wiederherstellung der Funktionsfähigkeit
- Schutz vor Datenverlust durch Hardware-Defekte, Software-Probleme, Layer 8 Problemen, ...
- reicht typischerweise **3 Monate** zurück

Archiv:

- langfristigen Datenaufbewahrung (typischerweise **10 Jahre** und mehr)
- meist zur Erfüllung von [Datenspeicherungsanforderungen](https://www.verbraucherzentrale.de/wissen/digitale-welt/datenschutz/aufbewahrungspflichten-welche-unterlagen-muss-ich-wie-lange-behalten-84296)  (z.B. Patientendaten, Steuerunterlagen, Essensmarkenabrechnungen, ...)
    - Aufzeichnungen über Röntgenbehandlungen müssen [30 Jahre](https://www.bgbl.de/xaver/bgbl/start.xav?startbk=Bundesanzeiger_BGBl&jumpTo=bgbl103s0604.pdf#__bgbl__%2F%2F*%5B%40attr_id%3D%27bgbl103s0604.pdf%27%5D__1738136387141) aufbewahrt werden
- **aktive Entscheidung**, was archiv-würdig, archiv-pflichtig ist


## Sinn und Zweck

- Archiv != Backup
- User (nicht IT) müssen archiv-würdiges definieren
- inaktive Daten oft noch auf aktiven (rotierenden) Datenträgern.  Sie gehören auf LTO-Bänder.  
  ![Alt text](../Pics.d/inaktive_Daten.png)
- bei aktuellen Stromkosten besonders interessant. Einsparung min. 80%.
- Manches muss dank gesetzlicher Vorgaben archiviert werden, z.B. Unterlagen, die zu Beschaffungen führen, [steuerlich relevantes](https://www.ihk-muenchen.de/de/Service/Recht-und-Steuern/Steuerrecht/Finanzverwaltung/Aufbewahrungsfristen/) (bis zu 10a), [ärztliche Unterlagen](https://www.blaek.de/arzt-und-recht/aufbewahrung) bis zu 30a.


## DSGVO

- Besitz personenbezogener Daten ist strafbar, es sei denn, man hat einen triftigen Grund dafür.
- Dauer der Archivierung festlegen und Löschroutine implementieren
- [Verstoß gegen DSGVO: Deutsche Wohnen soll 14,5 Millionen Euro zahlen | heise online](https://www.heise.de/newsticker/meldung/Verstoss-gegen-DSGVO-Deutsche-Wohnen-soll-14-5-Millionen-Euro-zahlen-4578269.html)
  - [Löschung von Daten im XtreemStore gemäß DSGVO](https://pad.gwdg.de/spsLHgzzQMWkv9LchwdElA#)


## HSM

![HSM](../Pics.d/HSM.png)

- HSM = **H**ierarchical **S**torage **M**anagement
- Tier: Ebene, Stufe
- Für den User sieht die ganze **Pyramide** aus wie ein großes File-System (mit schnelleren und langsameren Teilen)
- Eine **Policy** definiert Bedingungen für die  Verschiebung auf eine langsamere Ebene. Z.B:
  - Wenn 3 Wochen unverändert, eine Ebene tiefer.
  - Besser: Wenn vom User markiert (z.B. durch Existenz eines [sidecar files](https://de.wikipedia.org/wiki/Filialdatei)  mit Metadaten), auf Band schreiben.



### XtreemStore

- ein HSM
- [XtreemStore](https://konferenz.dlr.de/hsm-and-cluster-fs-konferenz/2023/praesentationen/2023-06-15_07%20-%20Stier%20exteme%20Store%20-20230321_XTS_DE.pdf)
  

## siehe auch

- [Internet Archive, Wayback Machine](https://archive.org/)



---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Storage<br />
letzte Änderung: 2025-01-20<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
