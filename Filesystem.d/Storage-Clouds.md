# Storage-Clouds

![Alt text](../Pics.d/seafile-logo.200.png)  ,   ![Alt text](../Pics.d/GitLab-logo.200.png)

![Alt text](../Pics.d/ownCloud-logo.200.png)  ,  ![Alt text](../Pics.d/nextcloud-logo.200.png),
![Google One](../Pics.d/Google_One_logo.png)

Wo werden Linux-Clients gut bedient?  
Obige Clouds bieten lokale Clients für Linux an:

```sh
seafile-daemon - Seafile daemon
seafile-cli - Seafile command line interface.

nextcloud-desktop - Nextcloud folder synchronization tool
nextcloud-desktop-cmd - folder synchronization with an Nextcloud server - cmd client

owncloud-client - ownCloud Desktop Client
owncloud-client-cmd - folder synchronization with an ownCloud server - cmd client
```

## Basics

- Ein lokaler Client synchronisiert (in einstellbaren Intervallen) Daten eines Ordners in die Cloud.
- Es begann mit [Dropbox](https://de.wikipedia.org/wiki/Dropbox)  und den kostenlosen 4GB.

## DSGVO

- Erfüllt der Hoster die DSGVO, wenn nötig? Ggf. ein anderer Hoster?

## Seafile bei luckycloud.de

### Seafile

- FOSS
- 2012 von chinesischen Studenten gegründet
- konzentriert sich auf das Wesentliche: Datei-Synchronisation, daher schnell, rel. fehlerfrei
- Ende-zu-Ende-Verschlüsselung mit AES 256/CBC
- Wiki mit git auf Basis von Markdown-Files

### luckycloud

- [luckycloud.de](https://luckycloud.de/de/) - ein Seafile-Hoster
- DSGVO-konform
- in Berlin
- kompetent, erreichbar
- [preiswert](https://luckycloud.de/de/preise-cloud-speicher-und-funktionen) zu testen


## Google One

- [100 GB für 1,99 €/Monat](https://one.google.com/plans?i=m&g1_landing_page=7#upgrade) dürfte das günstigste Angebot sein
- Synchronisierung mit [rclone](https://wiki.ubuntuusers.de/rclone/)
- bei Bedarf Daten verschlüsselt dort ablegen: [Datei-Verschluesselung](../Vorlesung.d/Datei-Verschluesselung.md) mit OpenSSL, [Ordner-Verschlüsselung](../Vorlesung.d/Ordner-Verschluesselung.md) mit GoCryptFS


## Clouds mit Zubehör

### Nextcloud

- [Nextcloud – Wikipedia](https://de.wikipedia.org/wiki/Nextcloud)
- [Fork](https://chemnitzer.linux-tage.de/2018/en/programm/beitrag/101)  von [ownCloud](https://de.wikipedia.org/wiki/OwnCloud)
- [FOSS](https://github.com/nextcloud)
- eierlegende Wollmilchsau:
  - Collabora Online = Online-LibreOffice
  - Kalender
  - OnlyOffice
  - Videokonferenz
  - [Plugins](https://apps.nextcloud.com/): WYSIWYG-Editor für Textdateien inkl. Markdown, Fotogalerie, Musik- und Videowiedergabe, Kanban-Projektmanagement, Hören von Internetradio, Verwaltung von Kochrezepten
  - und und und
- [42361 issues](https://github.com/search?q=org%3Anextcloud+bug&type=issues)
- Finanzierung über [Enterprise-Support-Verträge](https://nextcloud.com/de/enterprise/)  (software optimized and tested for mission critical environments)


### GitLab

- [GitLab – Wikipedia](https://de.wikipedia.org/wiki/GitLab)
- FOSS
- an Unis verbreitet
- [kostenlos nutzbar](https://about.gitlab.com/pricing/)
- Wiki aus versionsverwalteten .md-Seiten
- viele Tools, die die Software-Entwicklung im Team unterstützen


## S3

- [S3](https://en.wikipedia.org/wiki/Amazon_S3) = **S**imple **S**torage **S**ervice
- **Amazon** S3 ist ein Cloud-basierter Objektspeicher, in dem sich (auch) Dateien in Buckets speichern lassen.
- Zugriff erfolgt über **HTTP/HTTPS**.
- Diese Amazon-Schnittstelle ist **Industrie-Standard**.
- Das S3-Protokoll wird von einigen Filemanagern, [Backup](https://www.ubackup.com/author/zelia.html?_di_c=ZGV2X2lkXzg3ZGU0ODEyLWEwYzktNDUwNC1hNjlmLWVmYmFjMzFkZGM5MQ==)- und Archivierungs-Programmen (z.B. [XtreemStore, S. 9f](https://konferenz.dlr.de/hsm-and-cluster-fs-konferenz/2023/praesentationen/2023-06-15_07%20-%20Stier%20exteme%20Store%20-20230321_XTS_DE.pdf)) unterstützt.


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Storage<br />
letzte Änderung: 2025-01-20<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
