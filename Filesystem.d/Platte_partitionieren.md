# Platte partitionieren

## Partitionen

- Partitionsgrenzen verschieben kann zu **Datenverlusten** führen
- übliches **Partitionierungsschema**:
  - / für Linux-OS: 20 - 40 GB
  - /swap  2,5x RAM
  - /home
- bei SSDs ist **swap on file** meist auch schnell genug
- **Trennung**: OS-Daten, eigene Daten
  - Man sollte Verzeichnisse, die von einem Upgrade betroffen sind (alles, was die Linux-Installation mitbringt außer /home), auf eine andere Partition legen, als die eigenen Daten, die bei einem dist-upgrade (Upgrade auf neuere Ubuntu-Version) unverändert bleiben sollen.


## Wahl eines Filesystems

- Wenn keine besonderen/extremen Ansprüche bestehen: **ext4**
- für File-Server: **ZFS**
- PC mit vielen großen Dateien: **XFS**


## Partitionieren

- [Label](https://wiki.ubuntuusers.de/Labels/#Label-in-der-Praxis-verwenden): GPT-Partition-Label
- [gparted](https://wiki.ubuntuusers.de/GParted/)  als graphisches Tool verwenden


## siehe auch

- [How to partition USB drive in Linux - Linux Tutorials - Learn Linux Configuration](https://linuxconfig.org/how-to-partition-usb-drive-in-linux)
- [Grundlagen › Partitionierung › Wiki › ubuntuusers.de](https://wiki.ubuntuusers.de/Partitionierung/Grundlagen/#Wofuer-ist-die-Bezeichnung)



---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Admin Empfehlungen Storage<br />
letzte Änderung: 2025-01-13<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
