# SMART

## Grundlagen

- SMART, S.M.A.R.T. =  **S**elf-**M**onitoring, **A**nalysis and **R**eporting **T**echnology = Selbstüberwachungstechnik für HDDs und SSDs zur Vorhersage eines möglichen Ausfalls des Speichermediums an Hand von Grenzwerten
- Grenzwerte stammen vom jeweiligen Hersteller
- [Liste der Parameter](https://de.wikipedia.org/wiki/Self-Monitoring,_Analysis_and_Reporting_Technology#Auswertung)


## Datensammler

- 3 Arten:
  1. permanent laufende Protokollierung, keine Störung
  2. Kurztest:  Prüfung aller Parameter, gefolgt von Lese-Stichproben, geringe Störung
  3. Langer Test:  Komplettüberprüfung statt Stichprobe, dauert bei HDDs Stunden
- Tests können im Betrieb laufen, zerstören keine Daten


## smartmontools

- [home](https://www.smartmontools.org/), [bei UU](https://wiki.ubuntuusers.de/Festplattenstatus/)
- Freie Software
- Tests starten, Werte abfragen und melden lassen
- Installation:  `$ sudo apt install smartmontools`


### DB-Update

- DB der SMART-Werte "aller" Festplatten. Es kommen ständig neue (SSD-) Werte dazu.
- DB: /var/lib/smartmontools/drivedb/drivedb.h
- Update: `$ sudo update-smart-drivedb`


### Tests starten

- Kurztest:  
`$ sudo smartctl -t short /dev/sda`
  - 1TB, SATA-SSD: dauert 2 Min.
- Langtest, kann bei großen HDDs 1-2h dauern:  
`$ sudo smartctl  -t long  /dev/sda`
- nach Test, Gesundheitsstatus abfragen

### Gesundheitsstatus

- Auflisten aller Platten:  
`$ df -hl | egrep -v "loop|tmp|udev"`
- Abfrage des Gesundheitszustandes:  
`$ sudo smartctl -H /dev/nvme0`
  - "PASSED" = gesund
  - "No Errors Logged" sollte auch sein


### Werte abfragen

- Ausgabe aller Platten-Infos und Parameter:  
`$ sudo smartctl -a  /dev/nvme0`

```console

=== START OF INFORMATION SECTION ===
Model Number:                       Samsung SSD 970 EVO 500GB
Serial Number:                      S5H7NG0N306132A
Firmware Version:                   2B2QEXE7
Total NVM Capacity:                 500.107.862.016 [500 GB]
.
.
.
=== START OF SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED

SMART/Health Information (NVMe Log 0x02)
Critical Warning:                   0x00
Temperature:                        32 Celsius  <----
Available Spare:                    100%        <----
Available Spare Threshold:          10%
Percentage Used:                    0%
Data Units Read:                    12.067.532 [6,17 TB]
Data Units Written:                 25.377.106 [12,9 TB]  <----
Host Read Commands:                 191.360.743
Host Write Commands:                449.385.337
Controller Busy Time:               1.915
Power Cycles:                       1.062
Power On Hours:                     2.560
Unsafe Shutdowns:                   20
Media and Data Integrity Errors:    0
Error Information Log Entries:      1.088
Warning  Comp. Temperature Time:    0
Critical Comp. Temperature Time:    0
Temperature Sensor 1:               32 Celsius
Temperature Sensor 2:               36 Celsius

Error Information (NVMe Log 0x01, max 64 entries)
No Errors Logged
```

- [Achtung Betrug: Gebrauchte Festplatten als neu verkauft](https://www.heise.de/news/Gebrauchte-Seagate-Festplatten-als-Neuware-im-Umlauf-10254276.html): smartctl -a /dev/sd×


### Interpretation der Werte, SSDs

- Temperature, Temperature Sensor x
  - siehe Datenblatt, üblich: , 0°C - 70° C,  
  - 30ºC - 50 ºC sind gut
- Spare: "Percentage Used:"
  - 0% = keine Probleme
- Data Units Written:
  - Vergleich mit TBW-Wert des Modells bei [Geizhals](https://geizhals.de/samsung-ssd-970-evo-500gb-mz-v7e500bw-a1809116.html)
  - Einheit: 512Byte-Blöcke


### GSmartControl

- GUI für smartctl
- min. Version 1.1.3:  `$ gsmartcontrol -V`
- [home](https://gsmartcontrol.shaduri.dev/)


### smartd

- Daemon, der Probleme per Mail meldet
- [Anleitung bei UU](https://wiki.ubuntuusers.de/Festplattenstatus/#smartd)



## siehe auch

- [Festplattenstatus bei UU](https://wiki.ubuntuusers.de/Festplattenstatus/) - smartd
- [Festplattendiagnostik- und Überwachung](https://wiki.debianforum.de/Festplattendiagnostik-_und_%C3%9Cberwachung)
- [Wie man den Zustand der SSD/HDD unter Linux überprüft](https://www.howtoforge.de/anleitung/wie-man-den-zustand-der-ssd-hdd-unter-linux-uberpruft/)
- SMART im [wiki.archlinux](https://wiki.archlinux.org/title/S.M.A.R.T.)
- [Smartmontools NVMe support](https://www.smartmontools.org/wiki/NVMe_Support)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Hardware Storage<br />
letzte Änderung: 2025-01-26<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
