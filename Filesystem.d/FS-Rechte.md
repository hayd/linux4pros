# Rechte im Filesystem

**Falsch gesetzte Rechte können beliebig großen Schaden verursachen.**


## Basics

- In den [Inodes](https://de.wikipedia.org/wiki/Inode) (grundlegende Metadaten-Struktur zur Verwaltung von Datei-Systemen) werden die Rechte festgehalten.
- Jedes Objekt (Datei, Ordner, Link, ...) hat 3 Besitzer:
  - ugo = user, group, other
  - o = other = Rest der Welt
- Jedes Objekt kennt 3 Rechte für seine 3 Besitzer:
  - rwx = **r**ead, **w**rite, e**x**ecute
    - x bei Files: nur bei Programmen sinnvoll, Voraussetzung für deren Start
    - x bei Ordner: Erlaubnis in diesen zu wechseln
- Die Rechte von Windows-Filesystemen können nicht 1:1 auf Unix-Filesysteme abgebildet werden, u.U. wichtig z.B. bei USB-Sticks
- Anzeige der Rechte: `$ ls -al`  
    Interpretation von links nach rechts:
    Daten-Typ u-Rechte (rwx) g-Rechte(rwx) o-Rechte (rwx)

```shell
drwxr-xr-x 12 linuxer users 4.2K Apr  9 20:51 Ordner/
|[-][-][-]    [-----] [---]
| |  |  |        |      |       
| |  |  |        |      +------------> Group
| |  |  |        +-------------------> Owner
| |  |  +----------------------------> Others Permissions
| |  +-------------------------------> Group Permissions
| +----------------------------------> Owner Permissions
+------------------------------------> File Type
```

- user auf dem System ermitteln:  
  `$ egrep ^.*:x:[0-9]{4}:  /etc/passwd`
- groups auf dem System ermitteln:  
  `$ more /etc/group`



## Ändern der Besitzer

### owner ändern

- chown = change owner
- Syntax: `chown [OPTIONEN] [BENUTZER][:[GRUPPE]] Datei/Ordner`
- Optionen:
  - `-R`   recursiv (-r gibt es nicht)
    - allgemeine, oft ignorierte Notation: -r: reverse ,  -R: recursive
  - `-v`   verbose
- Dem user "ba" wird ein Verzeichnis mit allen Inhalten übergeben. Das [darf](https://elearning.wsldp.com/pcmagazine/chown-operation-not-permitted/) nur root, da das nicht ganz unproblematisch ist: Quota können platzen, Inhalte können untergeschoben werden, ...  
  `$ sudo chown -Rv ba /tmp/Test`

### group ändern

- group erlaubt es, an eine Gruppe von Nutzern Rechte zu vergeben.
- `$ more /etc/group` zeigt die Gruppen und ihre Mitglieder auf dem System
- mehr zum [Anlegen und Bevölkern](https://wiki.ubuntuusers.de/Benutzer_und_Gruppen/) einer Gruppe
- funktioniert wie chown
- Syntax: `chgrp [Option] Gruppe Datei/Ordner`:
- `$ sudo chgrp -R ba /tmp/Test ; ls -al /tmp/Test`


## Ändern der Rechte

- Die Rechte kann man absolut setzen oder relativ ändern.


### absolut

- wenn eine bestimmte Rechte-Konstellation erzwungen werden soll, unabhängig vom Ausgangszustand
- Syntax: `chmod [Optionen] Rechte Datei/Ordner1 Datei/Ordner2 ...`
  - Optionen (wie chown): `-R`, `-v`
- geschieht im Oktalmodus für u, g, o getrennt
- r = 4, w = 2,x = 1
- Werte werden addiert: rw = 4 + 2 = 6
- user bekommt alle Rechte an einem File (4+2+1), group darf lesen + schreiben (4+2), Rest darf nur ausführen:  
  `$  chmod  761 file.txt ; ls -al file.txt`


### relativ

- wenn Rechte ergänzt werden sollen
- Syntax: `chmod -R ugo+/-rwx  Datei/Ordner2`
- der ganzen Gruppe Schreibrechte an Text-Files geben:  
  `$ chmod g+w *.txt ; ls -al *.txt`
- Ausführungs- und Schreib-Rechte auf den owner beschränken:  
  `$ chmod -R go-rw /tmp/Xyz ; ls -al *.txt`


## Voreinstellung umask

- Mit umask definiert man die Rechte, die automatisch bei Erzeugung neuer Files, Ordner vom System vergeben werden.
- umask + Rechte (octal) = 777
- Abfrage des aktuellen Wertes (Die allererste 0 der 4 Ziffern kann vorerst ignorieren werden.): `$ umask`  
  - bzw. mit Darstellung in symbolischer Form: `$ umask -S`
- Empfehlung: `umask 077`
- zur dauerhaften Änderung in ~/.bashrc eintragen
- System denkt mit bei umask 077 bezüglich "x". Siehe Aufgaben

## Aufgaben

- `umask 077 ; cd /tmp ; mkdir -p /tmp/Rechte/Test1 ; cd /tmp/Rechte ; touch test1 ; ls -al`
  - Was fällt auf? Warum ist das so?
- Mache /tmp/Rechte/Test1 auch für die group zugänglich.
- Welche umask erzeugt einen File mit den Rechten rwxr-xr-x ?
- Legen Sie eine Programmdatei an und machen Sie ausführbar für alle.
- Setzen Sie die Rechte auch mit Ihrem Lieblingsdateimanager (thunar).

## siehe auch

- [Rechte](https://wiki.ubuntuusers.de/Rechte/)
- [Was ist umask unter Linux?](https://www.howtoforge.de/anleitung/was-ist-umask-unter-linux/)
- [Linux umask command](https://www.computerhope.com/unix/uumask.htm)
- [Rechte an Dateien und Verzeichnissen unter Linux](https://www.linux-bibel-oesterreich.at/viewtopic.php?f=10&t=1186&sid=9071b8d920690f36c75a0d3119ec26af&p=4190#p4190)
- [Rechte mit PolicyKit unter Linux vergeben](https://www.linux-bibel-oesterreich.at/viewtopic.php?f=10&t=1192&sid=9071b8d920690f36c75a0d3119ec26af&p=4214#p4214) - wenn chmod nicht mehr ausreicht
  


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  HowTo Security<br />
letzte Änderung: 2024-10-21<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
