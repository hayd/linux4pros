# Filesystem Hierarchy Standard (FHS)

Hierarchisches Filesystem: hierarchisch = hat Unterverzeichnis, die eine baumartige Struktur bilden können

## Unterteilung

- static oder variable
- shareable oder unshareable

|              | shareable       | unshareable |
|--------------|-----------------|-------------|
| **static**   | /usr            | /etc        |
|              | /opt            | /boot       |
| **variable** | /var/mail       | /var/run    |
|              | /var/spool/news | /var/lock   |


Wichtig für integrity checker,  [Einbruchserkennung](https://www.selflinux.org/selflinux/html/ids.html) (= hostintrusion detection systems), Security Information and Event Management-System (SIEM)  wie:

- [OSSEC](https://en.wikipedia.org/wiki/OSSEC)
- [Splunk](https://de.wikipedia.org/wiki/Splunk)
- [Tripwire](https://de.wikipedia.org/wiki/Open_Source_Tripwire)
- [Samhain](https://en.wikipedia.org/wiki/Samhain_(software)), [bei SelfLinux](https://www.selflinux.org/selflinux/html/samhain.html)
- [AIDE](https://de.wikipedia.org/wiki)


## Die wichtigsten Verzeichnisse


### /usr

- Als UNIX in den 1970-er Jahren geschrieben wurde, waren schnelle Datenträger sehr teuer. Nur die wichtigsten Befehle zum Booten und für den normalen Gebrauch lagen auf /sbin und /bin.
- Alle anderen Programm wurden auf das langsamere /usr gelegt.
- Heute liegt alles in **/usr**, aber die historische Einteilung in je 2 Ordner wurde erhalten.
- `$ ls -al  /sbin /bin`
  - /bin -> /usr/bin/ , /sbin -> /usr/sbin/


### /sbin, /usr/sbin/

- essential system/admin binaries wie: [mount](https://www.mankier.com/8/mount) , [mkfs](https://www.mankier.com/8/mkfs) , [fsck](https://www.mankier.com/8/fsck) , [nvme](https://www.mankier.com/1/nvme), [route](https://www.mankier.com/8/route), [grub](https://www.mankier.com/8/grub2-install) , [modprobe](https://www.mankier.com/8/modprobe) , [adduser](https://www.mankier.com/8/useradd)
- **s** wie **s**ingle user mode
- nur für root nötig, meist für device management


### /bin ,  /usr/bin

- command binaries
- Befehle, Programme für alle (root und user)
- Tausende von Files: `$ find /usr/bin  -type f | wc -l`
- Sub-Dirs verboten
  - überprüfen: `$ ls -al /usr/bin/ |  egrep "^d"`


### /etc

- alle systemweiten config files aller Programme  = **registry**
- keine Executables
- typischerweise 18 MB groß, d.h.  ganze Systemkonfiguration einfach zu sichern


### /home

- Home-Verzeichnis der User außer root (/root)
- in der Shell abgekürzt mit ~


### /tmp

- temporäre Dateien
- wird bei Booten geleert: ein Feature und eine Gefahr


### /dev

- device files, **keine Gerätetreiber**
  - Treiber liegen z.B. in /lib/modules, /boot, /usr/lib/xorg/modules/drivers
- enthält alle Gerätedateien, über welche die Hardware/Pseudo-Hardware im Betrieb angesprochen wird, z.B: /dev/zero, /dev/null, /dev/random, /dev/ecryptfs
- Details: [Everythin is a file.](./Everything_is_a_file.md)


### /lib

- essential **shared libraries** und Kernelmodule
- /lib/modules: Kernelmodule
- Libs von Programmen in Sub-Directories, z.B. /lib/firefox


### /proc (processes)

- virtuelles Dateisystemen/Pseudodateisysteme, das Informationen über den Systemzustand und Prozesse hierarchisch strukturiert verfügbar macht
- `$ ls -l /proc/cpuinfo`    aber   `$ more  /proc/cpuinfo`
- /proc/123/: Infos zum Prozess 123
  - `$ tree /proc/1`
  - daher sehr umfangreich: `$ find /proc -type f  | wc -l`


### /var

- **var**iable Dateien
- Log-Dateien, zu druckendes, caches


### /media

- mount point für Wechselmedien


### /usr/local

- Verzeichnisstruktur wie /usr
- für Programme, die man **an der Paketverwaltung vorbei** installieren werden, aber sonst FHS erfüllen
- z.B: singularity, mdless


### /opt

- für Software, die **nicht in Paketen** daherkommt, sondern eher proprietär, monolithisch, in the "Windows" style
- z.B: Zoom, Google Chrome, OnlyOffice, Sublime Text


### /boot

- alle zum Systemstart erforderlichen Dateien


### /lost+found  

- nicht bei allen Filesystemen (z.B. xfs)
- Dateien und Dateifragmente, die beim Versuch, ein defektes Dateisystem zu reparieren, übrig geblieben sind.
- Sollte leer sein.


## Jenseits von FHS üblich

- **~/.config** - dort legen viele Programme die persönlichen config files ab, z.B. XFCE, VS Code, Editoren, ...
- ein . vor dem Namen macht den File, Ordner bei vielen Auflistungen unsichtbar (**hidden files**), z.B. ~/.mozilla/. Damit werden Listen kürzer und konzentrieren sich auf selbst generierte Daten.


## see also

- [UU](https://wiki.ubuntuusers.de/Verzeichnisstruktur/)
- [A Detailed guide to Linux Filesystem Hierarchy Standard](https://www.linuxfordevices.com/tutorials/linux/linux-filesystem-hierarchy)
- [FHS](https://de.wikipedia.org/wiki/Filesystem_Hierarchy_Standard) Hierarchy Standard – Wikipedia
- [Filesystem Hierarchy Standard](https://refspecs.linuxfoundation.org/FHS_3.0/fhs-3.0.pdf), PDF, 50 S.


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Wissen<br />
letzte Änderung: 2025-01-20<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
