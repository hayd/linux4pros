# Filer, File-Server

für Linux-Hosts

- Filer = [Network-attached storage](https://en.wikipedia.org/wiki/Network-attached_storage)

- Files und File-Server verwenden oft folgende Komponenten:
  - ZFS - bes. geeignet für lokale File-Server
  - Ceph - wenn Skalierbarkeit und Redundanz eine große Rolle spielt
  - NFS - der Klassiker für netzwerkweite Filesysteme unter Linux

## kommerzielle Produkte

- [Fritzbox NAS für Ubuntu](https://avm.de/service/wissensdatenbank/dok/FRITZ-Box-7430/543_Speicher-NAS-am-Computer-als-Netzlaufwerk-einrichten/)
- [QNAP](https://www.qnap.com/de-de)
  - [nutzt ZFS](https://www.qnap.com/de-de/operating-system/quts-hero/5.1.0)
- [Synology](https://event.synology.com/de-de/build-your-private-cloud)
  - [ZFS muss nicht sein](https://www.synology.com/ru-ru/releaseNote/ArchiwarePure)
  

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Storage<br />
letzte Änderung: 2025-01-20<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
