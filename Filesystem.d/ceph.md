# Ceph

![ceph logo](../Pics.d/ceph.png)

## TL;DR

- skaliert horizontal und vertikal
- Jeder Client berechnet (beeinflussbar) die Lage seines Datenblocks in einem globalen Adressraum selbst.


## Killer-Feature: Skalierbarkeit

- vertikale Skalierbarkeit (Scale-Up):  Ressourcen bereits vorhandener Geräte  erweitern = mehr Platten ins RAID
- horizontale Skalierbarkeit (Scale-Out):  neue Geräte hinzuzufügen = noch ein [RAID](https://de.wikipedia.org/wiki/RAID), noch ein Schrank, noch ein Rechenzentrum
- big picture:
  - alle Platten (lokale SSD, Platte in einem Schrank im RZ in [Island](https://www.heise.de/newsticker/meldung/Island-Insel-der-gruenen-Rechenzentren-1619591.html)) in einem einzigen Namensraum (RADOS)
  - jeder Client rechnet ganz allein aus, auf welcher Platte sein Object liegt (CRUSH)


## Object Stores

- RADOS => globaler Adressraum
- Auf sämtlichen Servern, die zum Teil eines Object Stores werden sollen, läuft eine Software, die den lokalen Plattenplatz dieses Servers verwaltet und exportiert. Sämtliche Instanzen dieser Software arbeiten im Rechnerverbund zusammen und sorgen so dafür, dass für den Betrachter von außen das Bild eines einzelnen, großen Storages entsteht.
- RADOS = reliable autonomic distributed object store
- Alle Platten, SSDs, egal wo, sind Teil eines einzigen Adressraums
- Es gibt 3 Zugriffsmöglichkeiten:
  - CephFS-Dateisystem,
  - RADOS-Block-Device,
  - RADOS-Gateway - via App und API

## CRUSH

- Problem: tausende Clients suchen ihre Daten
- Lösung: [CRUSH-Algorithmus](https://docs.ceph.com/en/latest/architecture/#crush-introduction) (=Controlled, Scalable, Decentralized Placement of Replicated Data)
-  berechnet, wo Daten liegen für Lesen und Schreiben
- dadurch kein Single-Point-of-Failure, kein Leistungsengpass, keine  physische Beschränkung der Skalierbarkeit
- Man kann CRUSH durch Regeln beeinflussen: #Kopien, (geographische) Verteilung der Daten, Nutzung unterschiedlicher Datenträger, Stromverbrauch, ...


## Misc

- Redundanz für Datensicherheit
  - beliebig viele Kopien mit steuerbaren Ablageorten (1. Kopie in Iskland, 2. in Europa)
  - [Erasure Coding](https://storage-base.de/knowledgebase/106/Was-ist-Erasure-Coding-und-warum-sollte-man-es-nutzen.html): vergleichbar mit [RAID 5/6](https://de.wikipedia.org/wiki/RAID#RAID_5:_Leistung_+_Parit%C3%A4t,_Block-Level_Striping_mit_verteilter_Parit%C3%A4tsinformation), effektiver als einfache Kopien
- Entwicklung von Providern getrieben
- aus Doktorarbeit (2007)  von Sage Weil entstanden
- Lizenz:	LGPL (= GPL minus Copyleft)
- Ceph: Abkürzung von  Cephalopoda = Kopffüßer = Zeichen für hohe Parallelität


## Siehe auch

- [Ceph-Cluster durchbricht die Marke von 1 TiB/s](https://www.heise.de/news/Ceph-Cluster-durchbricht-die-Marke-von-1-TiB-s-9603852.html)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Storage<br />
letzte Änderung: 2024-01-21<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>