# Filesystem-Befehle

## TL;DR

- **pwd** **cd** **ls** **cp** **mv** **rm** **mkdir** muss man kennen.
- **cp** **mv** **rm** muss man per alias entschärfen.



## Pfade

- absolute
  - beginnend bei /
  - `./my_script`  ist ein absoluter Pfad.
- relative
  - beginnend bei $PWD
- geräteunabhängige Einstiegspunkte in ein Filesystem als Environment-Variable definieren und diese dann verwenden. Das macht unabhängig von unterschiedlichen lokalen Installationen, Umzügen, Updates, ... z.B.  
   `export OCP="/data/helmut/ownCloud"`  
   `alias hf="$OCP/Scripts.d/handleNewTextFile.sh"`  


## Standard-Befehle

### pwd (print working directory)

- `$ echo $PWD`
- `alias .='pwd'`
- in Fenster-Oberkante anzeigen, nicht im Prompt


### cd (change directory)

- `alias ..='cd ..'`


#### (bessere) Alternativen

- [Broot](https://github.com/Canop/broot) -  a better way to navigate directories, [doc](https://dystroy.org/broot/)
  - [Dateimanager für die Kommandozeile](https://www.linux-community.de/ausgaben/linuxuser/2021/07/broot-dateimanager-fuer-die-kommandozeile/)
  - [Easy way to see and navigate directory trees in Linux](https://computingforgeeks.com/broot-easy-directory-trees-navigation-in-linux/)
  - in Rust geschrieben
- [zoxide](https://github.com/ajeetdsouza/zoxide) - a smarter cd command
  - ab Ubuntu 21.04 im Repo


### ls (list)

- die wichtigsten Optionen: -a -l -h -S -F -1
- aliases definieren
- `$ ls -1 | wc -l`

#### (bessere) Alternativen

- [exa](https://the.exa.website/) - a modern replacement for ls
  - ab Ubuntu 20.10 (Groovy Gorilla) im Repo
  - [bei UU](https://wiki.ubuntuusers.de/exa/)
- [lsd](https://github.com/Peltoche/lsd) - ls-deluxe
  - .deb auf [GitHub](https://github.com/Peltoche/lsd/releases)


### tree

- `$ tree  -L 2`  # L = Level, wie weit im Baum
- die wichtigste Optionen: -L  -F

### cp

- Datenverlust möglich
- die wichtigsten Optionen: -i -r -p
- [xcp](https://crates.io/crates/xcp) - an extended cp


### mv

- Datenverlust möglich  
- die wichtigste Optionen: -i


### rm

- Kann der Anfang einer wunderbaren Neuinstallation werden. Getreu der Unix-Leitlinie: Der User weiß, was er tut.  
- die wichtigsten Optionen: -i -r -f


### touch

- `$ touch -t 201303051310 /tmp/timestamp`
  - Format: CCYYMMDDhhmm
  - Einsatz: Suche nach Files, die neuer/älter als /tmp/timestamp sind.


### mkdir (make dir)

- die wichtigste Option: -p


### ln (link)

- Syntax: `$ ln -s alt neu`
- Einsatz:
  - Auswahl einer Konfigurationsdatei aus n  möglichen:  
       `$ ln -s xfce4-22Mai2020 xfce4`
  - Markierung des zu bearbeitenden Textes:  
      `ln -s kapitel-8.txt hier-weitermachen`

- die wichtigste Option: -s


### locate

- [locate, mlocate, plocate](../Prgs.d/mlocate.md)


## Aufgaben

- Erzeugen Sie ein kleines Filesystem (z.B. als [Testbed](https://en.wikipedia.org/wiki/Testbed) für Versuche in einem Filesystem) in /tmp/Test/ mit min. 3 Directories, davon 2 Sub-Directories. In jedem Directory sollen sich min. 2 Files befinden (s.u.). Wie macht man das mit minimalem Aufwand? siehe [Linux-Befehl, Arbeitserleichterungen](../Vorlesung.d/Befehl_allg.md)

``` bash
.
├── a
├── b
└── D/
    ├── D1/
    │   ├── e
    │   └── f
    └── D2/
        ├── l
        └── m
```


- sinnvolle aliases schreiben, um Fehler zu vermeiden, um sinnvolle Zusatz-Infos immer zu sehen
  - z.B: `alias rm='rm -i'`

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  <br />
letzte Änderung: 2025-01-20<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
