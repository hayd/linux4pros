# Everything is a file

Eines der grundlegenden Prinzipien in Linux ist:  

**"In Linux and UNIX, everything is a file"**  
   oder genauer:  
“everything is a stream of bytes”,  Linus Torvalds

Das bedeutet:

- **einheitlicher Zugriff** auf alles, was Bits enthält oder liefert
- UNIX war das erste Betriebssystem, das den gesamten I/O unter einem solchen einheitlichen Konzept und einer kleinen Anzahl von Bestandteilen **abstrahierte**.
- **Vorteil**: Werkzeuge und Programmierschnittstellen für Dateien können für den Zugriff auf all diese Ressourcen genutzt werden.
- Ein-/Ausgabe-Ressourcen wie Dateien, Verzeichnisse, Geräte  und sogar Interprozess- und Netzwerk-Verbindungen sind als **einfache Byteströme** via Dateisystem verfügbar.


## Arten von Files, Streams

Es gibt 8 Arten von Files, die einheitlich behandelt werden können mit den gleichen Tools:

1. Files
2. Directories
3. Links (= Zweit-Namen)
4. Mass storage devices (z.B. HDDs, SSDs, CD-ROM, Tape, USB-Sticks, ...)
5. Inter-process communication (z.B. Pipes, Shared Memory, UNIX Sockets)
6. Network connections
7. interactive terminals (z.B. /dev/console)
8. andere Geräte (z.B. Printer, Grafik-Karten)

Es gibt 3 Standard-Datenströme:

![Alt text](../Pics.d/std-streams.png)

1. Standardeingabe (stdin),  file descriptor: 0
2. Standardausgabe (stdout),  file descriptor: 1
3. Standardfehlerausgabe (stderr),  file descriptor: 2


## Redirect

### Umlenkung an ein Gerät

Ein Fenster schreibt in eine anderes Fenster:

- Öffnen Sie 2 Terminal-Fenster.
- Bestimmen sie im 1. den Gerätenamen:  
`$ tty`
  - pts = **p**seudo **t**erminal **s**lave, wird normalerweise für Terminal-Emulatoren verwendet
- Schicken Sie einen String vom 2. Fenster auf das 1. Fenster:  
`$ echo "umgelenkter String" > /dev/pts/1`
- Funktioniert auch mit anderen Geräten, z.B. Druckern, Platten, Bändern, ...


### Umlenken von Datenströmen, Datei-Inhalten

- Output in File umlenken:  
  `$ ls -la /home > /tmp/filelist.txt`
- Outputs in einem File aggregieren mit `>>`:  
  `$ ls -al /etc >> /tmp/filelist.txt`
- Input aus File beziehen:  
  `$ wc -l < /tmp/filelist.txt`
- Output und Input über Pipe direkt verknüpfen:  
  `$ ls -la |  wc -l`
- [mehr](../Bash.d/bash-redirect.md) dazu im Shell-Kapitel

## /dev/

- /dev/ enthält **Pseudo-Files**, die (Pseudo-) Geräte verfügbar machen, keine Treiber
- Beispiele:
  - **/dev/null** - Müllschlucker
  - **/dev/zero** - bezeichnet eine virtuelle Gerätedatei , liefert 0's
  - **/dev/urandom**  liefert Zufallszahlen
  - **/dev/tty0**     Current virtual console


## Anwendungen

- Daten verschwinden lassen: `$ ls -al > /dev/null`
- (nervige) Fehlermeldungen wegwerfen: `$ kwrite file.txt 2> /dev/null &`
- Files (2GB) erzeugen mit [dd](https://wiki.ubuntuusers.de/dd/) (disk dump zum bit-genauen Kopieren von Daten):  
`$ time dd bs=1G count=2 if=/dev/zero     of=/tmp/2GBzeros`  
`$ time dd bs=1G count=2 if=/dev/urandom  of=/tmp/2GBrandom`
  - und dann komprimieren:  
`$ cd /tmp`  
`$ time lbzip2 -1  2GBrandom`  
`$ time lbzip2 -1  2GBzeros`
`$ ls -alh 2GB*`  

    - Erkläre die Unterschiede. Verwenden Sie 2 Terminals => Parallel-Verarbeitung.

- Platte löschen
  - `dd bs=1M if=/dev/zero    of=/dev/sdX`
  - `dd bs=1M if=/dev/urandom of=/dev/sdX`


- [netcat](https://linuxize.com/post/netcat-nc-command-with-examples/)
  - auf empfangendem Host Port 5555 öffnen und was dort eintrifft in File umlenken:  
`$ nc -l 5555 > from_net`
  - auf sendendem Host:  
`$ nc receiving.host.com 5555 < file_name`


## /proc/

/proc/ liefert **Schnittstellen zu Kernel-Daten** in Form eines Pseudo-Dateisystems.


### Beispiele

- Uptime:  

```bash
 $ ls -al /proc/uptime
 $ cat /proc/uptime ; sleep 3 ; cat /proc/uptime
```

- die 1. Zahl: Uptime in Sek., vergleiche:  `$ w`
- die 2. Zahl: aufsummierte idle time aller Cores

- Eine Liste aller Dateisystemtypen, die der Kernel im Augenblick kennt:  
`$ more /proc/filesystems`
  - nodev: dahinter steckt keine Hardware, Filesystem-Check sinnlos

- Wie groß sind die in /proc betrachteten Files?

- siehe auch: [Das /proc-Dateisystem](https://www.linux-praxis.de/das-proc-dateisystem)




## siehe auch

- [Wikipedia](https://de.wikipedia.org/wiki/Everything_is_a_file)
- [In UNIX Everything is a File](https://hackmd.io/@jkyang/unix-everything-is-a-file)
- [Standard-Datenströme – Wikipedia](https://de.wikipedia.org/wiki/Standard-Datenstr%C3%B6me)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Wissen<br />
letzte Änderung: 2025-01-22<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
