# FUSE

FUSE = **F**ilesystem in **USE**rspace

![FUSE in Kernel- und User-Space](../Pics.d/FUSE.png)

## Was ist das?


- FUSE (Filesystem in USErspace) ist ein Linux-Kernel-Modul, das es ermöglicht, Dateisystem-Treiber aus dem Kernel-Modus in den User-Modus zu verlagern.

## Sinn und Zweck

- User-Space-Dateisysteme sind beliebt für die **Erprobung** neuer Ideen und die **Entwicklung** komplexer Produktionsdateisysteme, die im Kernel nur schwer zu pflegen sind (warum?)
- mount, umount **als User**, nicht nur als root möglich
- Auf dem Weg vom FUSE-Daemon bis zum VFS kann man den Datenstrom einigen Operation aussetzen, z.B. Verschlüsselung.


## Leistungsfähigkeit


Werte im Vergleich zu ext4 abhängig vom Einsatz, z.B: Mail-Server oder Web-Server oder File-Server

- Performance: 5% -  83% schlechter
- CPU utilization: 3× - 18× größer =  0.2% - 31% größer
- Latenz: 0x - 4x größer


**Fazit**:

- user-space file systems kann man auch produktiv (non-“toy”) einsetzen.
- Anwendbarkeit hängt von den Workloads ab.
- Optimierung ist nötig.


## Einsatz

- gocryptfs
- Squashfs
- exFAT nicht mehr

<!---
hier 
https://de.wikipedia.org/wiki/Filesystem_in_Userspace
nachtragen 
+ https://distributedstorageblog.wordpress.com/nfs-ganesha/
+ https://en.wikipedia.org/wiki/SquashFS

https://de.wikipedia.org/wiki/Filesystem_in_Userspace
von dort Bild mitnehmen
-->



## siehe auch

- [home](https://github.com/libfuse/libfuse) auf GitHub



---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  <br />
letzte Änderung: 2025-02-12<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
