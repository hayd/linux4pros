# Backup

## TL;DR

- No Backup? No Mercy!
- jeder Backup ist besser als keiner
- beruflich: bareos, privat: rsync oder s.u.

## LTO

- = [Linear Tape Open](https://de.wikipedia.org/wiki/Linear_Tape_Open)
- LTO-9
  - Bandkapazität: 18 TB
  - max. Transferrate: 400 MB/s
  - zum Vergleich: Seagate Ultrastar HDD 8 TB: 200 MB/s, sequenziell, 500€
- [Tape Library](https://www.youtube.com/watch?v=CVN93H6EuAU) mit Offline-Bändern bietet max. Kapazität
- eigenes Filesystem für LTO-Bänder: [LTFS](https://de.wikipedia.org/wiki/Linear_Tape_Open)
- Im großen Stil geht es nicht grüner und preiswerter.
- Problem bei privater Nutzung: [Preis des Laufwerks](https://geizhals.de/?cat=datex&xf=1202_LTO-7%7E1202_LTO-8%7E1202_LTO-9) > 3k€

## Anforderungen

- Usern beibringen: Backup != Archiv
- Restore muss schneller sein als Backup
  - alle Daten eines Clients hintereinader auf Band
- Backup muss in einer Nacht fertig werden
- verlustfreier Produktwechsel
- auf >= LTO-8 Bänder


## Fallen

- Produkt-Wechsel
- Upgrade: überlebt DB?
- unterstützte OS
- Lizenzierung nur in großen Paketen (min. 10 Clients)
- Lizenzierung von Datenmenge abhängig, neues Projekt = Problem


## Bareos

Bareos = Backup Archiving Recovery Open Sourced

- [home](https://www.bareos.com/de/)
- FOSS
- über 1500 Commits von insgesamt 22 Mitwirkenden in Bareos 20
- Fork von [Bacula](https://de.wikipedia.org/wiki/Bacula) in 2011
- modulares Design
- zahlreiche Plugins für das Sichern und Wiederherstellen verschiedener Datenbankserver (PostgreSQL, MySQL, MSSQL), LDAP-Verzeichnisdienste, Cloud-Speicher und virtueller Maschinen
- Unterstützte Backup-Konzepte:
  - Vollsicherung
  - differenzielles Backup: Delta zum letzten full backup
  - inkrementelles Backup:  Delta zum letzten Backup
- Always Incremental: 1 full backup + n incremental backups
  - für riesige Datenmengen, bei denen ein full backup zu lange dauert
- [Workshop: Aufbau und Inbetriebnahme von Bareos » ADMIN-Magazin](https://www.admin-magazin.de/Das-Heft/2015/06/Workshop-Aufbau-und-Inbetriebnahme-von-Bareos)
  

## privater Backup

- Was will, muss ich wirklich sichern? Betriebssystem auch?
- Muss der Backup auf der heimischen USB-Platte verschlüsselt (eine Komplikation mehr) sein?
- Genügt eine Kopie der "Kronjuwelen" (Bachelorarbeit) in der Cloud?
- Programme:
  - rsync auf USB-SSD
  - tar-File (verschlüsselt und komprimiert) in die Cloud
  - [Liste bei UU](https://wiki.ubuntuusers.de/Datensicherung/#Programme)

*Backup-Programme für den Linux-Desktop*, c’t 2022, Heft 13, S. 126 ff

- Fazit: Jedes Backup ist besser als keines. Irgendwie empfehlenswert:
  - [Back In Time](https://wiki.ubuntuusers.de/Back_In_Time/) - unkompliziert, keine Integritätsprüfung
  - [Déjà Dup](https://wiki.ubuntuusers.de/D%C3%A9j%C3%A0_Dup/) - unkompliziert, keine Integritätsprüfung
  - [Duplicati](https://www.linux-community.de/ausgaben/linuxuser/2023/08/backup-software-duplicati-im-test/) -   einfach und vielseitig, einfachen Bedienung, in die Cloud
  - [Kup](https://www.linux-community.de/ausgaben/linuxuser/2020/12/backups-mit-kup-erstellen/) -  einfach und vielseitig, keine Verschlüsselung
  

## Projekt-Arbeit

- Backup via [SquashFS](https://wiki.ubuntuusers.de/SquashFS/): Komprimierung, Deduplizierung, einfacher Zugriff, bewährtes Tool

<br> <br> <br> <br> <br> <br> <br> <br>

![Alt text](../Pics.d/no_backup.jpg)



---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Empfehlungen Storage<br />
letzte Änderung: 2024-11-06<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
