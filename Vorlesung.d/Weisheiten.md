# IT-Weisheiten

## dutzendweise+

1. Erst mal **booten** vor der Fehlersuche.
1. [80-20-Regel](https://karrierebibel.de/pareto-prinzip/)
   - In 20% der Entwicklungszeit, hat man 80% der Features des Scripts programmiert. Für die restlichen 20% benötigt man dann 80% der Zeit.
   - Wenn man alleine damit arbeitet (und nur dann), muss das Script nicht perfekt sein. Den Rest ggfs. in Handarbeit erledigen. Z.B. Konvertierung in eine andere Wiki-Syntax.
2. Die **Halbwertszeit** des IT-Wissens liegt bei 2 Jahren.
3. Nichts ist also beständiger als der **Wandel**. Also lerne man mit dem Wandel umzugehen und nicht im status quo verharren zu wollen.
4. Man sollte nur Infos aus **Quellen** nutzen, die Qualitätssicherung ([Peer Review](https://de.wikipedia.org/wiki/Peer_Review)), Widerspruch, Korrekturen, Verbesserungen, Updates institutionell organisiert haben. Z.B wissenschaftliche Journals, Wikipedia, ... ; Facebook, Telegram u.ä. gehört nicht dazu, sie dienen vor allem der Erregungsbewirtschaftung.
5. **modular**, man muss es sowieso bald wegwerfen, neu schreiben oder austauschen. Das gilt für Hardware (gesteckt statt verlötet), Software und Doku
6. 1 Tool , 1 Zweck - keine  [eierlegende Wollmilchsau](https://de.wikipedia.org/wiki/Eierlegende_Wollmilchsau)
7. **KISS** - keep it small and simple
1. Die Kunst ist nicht **Komplexität** irgendwie in den Griff zu bekommen, sondern durch innovative Ideen zu vermeiden bzw. zu reduzieren.
   - Der Mensch neigt dazu (laut Studie) disfunktionale, komplexe Lösungen durch hinzufügen weiterer Bausteine sanieren zu wollen, was die Komplexität vergrößert. Die Lösung komplett neu und radikal vereinfacht aufzubauen, wird meist vermieden. Die Veränderung wäre zu groß.
   - ein Ausweg: unabhängige Module, nicht voneinander abhängige Schichten
8. How not to slip from the shoulders of giants
   - means "using the understanding gained by major thinkers who have gone before in order to make intellectual progress".
   - **Bewährte Tools** (grep, sed, GNUplot, Pandas, Kerberos, ...) integrieren statt selbst etwas schreiben, bzw. darauf achten, dass man diese nutzen kann
9.  **DRY**: don't repeat yourself - sonst Korrekturen, Updates mühsam bzw. werden unterlassen => Fehler
    - SSoT: single source of truth (Doku im source code und aus diesem extrahieren)
10. **GUI**: gute GUI: wenige Klicks, wenige Überraschungen
11. Was man nicht **messen** kann, kann man nicht **managen**.
    z.B. mit [Grafana](https://grafana.com/grafana/dashboards/)
12. Ohne [Baselining](https://en.wikipedia.org/wiki/Baselining) keine Beurteilung von Messwerten.
    z.B. mit [Grafana](https://grafana.com/grafana/dashboards/)
13. Was man nicht **suchen** (grep, agrep, pdfgrep, recoll) kann, gibt es irgendwann nicht mehr.
14. **Commodity Hardware** plus Super-Software - nicht umgekehrt
    z.B. ZFS statt [Adaptec Serial ATA II RAID](https://www.amazon.de/Adaptec-AAR-2820SA-Controller-S-ATAII-Devices/dp/B000BPRGKQ/ref=sr_1_1?c=ts&keywords=RAID-Controller&qid=1703930398&s=computers&sr=1-1&ts_id=430184031) zum Preis eines Servers
15. Schon zu Beginn eine **Exit-Strategie** entwickeln, damit Entscheidungen für ein Produkt, eine Firma leichter zu korrigieren sind. Sonst ein anderes Produkt.
    - Man muss nicht nur Daten, sondern auch Metadaten (z.B. Zugriffsrechte) mitnehmen können.
    - Offene Standards sind hier hilfreich.


## effektive  Arbeitsweise

1. Terminal nutzen
1. TAB - zur Vervollständigung
1. aliases bzw. functions - Arbeitserleichterung und Doku
1. kleine Scripte - Arbeitserleichterung und Doku
1. Editor mit Befehlssammlung für copy 'n paste
1. ein virtueller Desktop pro Aufgabe, Namen vergeben


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  <br />
letzte Änderung: 2025-02-14<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
