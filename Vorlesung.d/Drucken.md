# Drucken

## TL;DR

Erst informieren, dann kaufen.


## Allgemeines

- ein eher trauriges Kapitel
- [CUPS](https://wiki.ubuntuusers.de/CUPS/) (Common Unix Printing System) oft die Basis. Ein CUPS-Server kann ein ganzes Netz an Drückern versorgen.
- Wenn Drucker PostScript/PDF unterstützt, wird es einfacher.
- [lange Kompatibilitätsliste für Drucker unter Linux](https://openprinting.org/printers)
- einige Bewegung, z.B. CUPS 3.0, [OpenPrinting](https://openprinting.github.io/)

## Probleme

- **Tinte** trocknet ein
- Drucker entscheidet eigenmächtig über Nachfüllen (Abschaltung) an Hand eines **Zählers**
- Abhilfe: [Toner-Reset](https://www.superpatronen.de/ratgeber/toner-reset)


## Druckerauswahl

- Farbe oder Schwarz-Weiss
- Tinte (bessere Qualität) oder Toner (verträgt lange Pausen)
- Tinte/Toner: vom Hersteller oder kompatibel oder aus Flasche nachfüllen
- Nachfüllen von Tinte oder Toner an Hand der Druck-Qualität, nicht wegen Zählerstand mit Zwangsabschaltung


## Siehe auch

- [Printer Applications - A new way to print in Linux](https://openprinting.github.io/documentation/01-printer-application/)
- [OpenPrinting](https://openprinting.github.io/)
  

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Hardware<br />
letzte Änderung: 2025-02-05<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
