# Frage-Runde, Vorstellung

## Studenten

- Praxispartner?
  - dort Linux?  
- Linux, privat
  - Vorkenntnisse?
  - welche Distribution?
  - wozu? Audio, Video, Grafik, Spielen, ... ?
- Erwartungen
  - Welche Themen, Aufgaben werden gewünscht?
  - LaTeX für (Bachelorarbeit) ?
  - Markdown? Mit Markdown-Editor?
- Linux auf Laptop
  - bereits vorhanden?
  - Distri wechseln?
  - Linux  installieren?
- git-Kenntnisse?
- Videokonferenz ggf.
  - Zoom ein Problem?

## Dozent

- Helmut Hayd
- 30a UNIX
    - high performance computing, Admin, Grafik, Sicherheitsfragen, ...
- 25a Linux
- 25a IT-Leiter an einem MPI
- IT-Sprecherkreis, Berater des Präsidenten, ...
- Linux-Tage der MPIs, Workshops zu Freier Software, ...
- ab 1997 Betreuung des 1. BA-Studenten, danach noch ca. 60 Studenten aller Hochschulen
- Script mit EI nicht KI erstellt
- Motivation:
  - FOSS ist wichtig
  - Meckern genügt nicht
  - Alternativen aufzeigen
  - Geld, Beschäftigung ... wohl kaum
- Erwartung: mitmachen, mal den Dozenten wichtiger als das Handy nehmen


---

<sub>
letzte Änderung: 2025-01-05
