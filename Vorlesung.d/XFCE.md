
# XFCE

## Allgemeines

- [home](https://www.xfce.org/)
- [Documentation](https://docs.xfce.org/), [Developer Wiki](https://wiki.xfce.org/), [ubuntuusers](https://wiki.ubuntuusers.de/Xfce/)
- Erscheinungsjahr: 1996
- eine offizielle Variante von Ubuntu: [Xubuntu](https://wiki.ubuntuusers.de/Xubuntu/)
- Aktuelle Version: [4.20](https://alexxcons.github.io/blogpost_14.html) vom   Dez. 2024
- rel. [wenige Entwickler](https://www.xfce.org/about/credits): 9 + 12, vergleiche [GNOME](https://www.gnome.org/about-us/)
- Basis:  GUI-Toolkit [GTK](https://de.wikipedia.org/wiki/GTK_(Programmbibliothek)) (GIMP-Toolkit, C)
- **gut konfigurierbar** (s.u.)
- **geringe Systemanforderungen**
- einfach mit der Maus bedienen, daher das Logo für das [Projekt](https://de.wikipedia.org/wiki/Xfce#/media/Datei:Xfce_logo.svg) und den [Desktop](https://www.xfce-look.org/p/1301156)
- übernimmt alle GNOME- und KDE-Programme in sein Menü, orientieren sich an den Standards von freedesktop.org


## Installation

- Installation von XFCE + XFCE-Programmen:  
`$ sudo apt install xubuntu-desktop`
- Installation von reinem XFCE, ohne Zubehör:  
`$ sudo apt install xfce4`


## Konfiguration allgemein

- in [flat file DB](https://en.wikipedia.org/wiki/Flat-file_database) gespeichert, d.h. alles einsehbar, leicht zu sichern, Änderungen einfach zu finden
- alles in ~/.config/xfce4/ , ca. 400k, ca. 70 Files
- Bewährtes festhalten:  
  `cp -pr xfce4/ xfce4.1Feb2023`
- gewünschte Konfiguration auswählen:  
  `ln -s xfce4.15Jan2023 xfce4`


## siehe auch

- [How to customize Xfce desktop](https://linuxhint.com/customize-xfce-desktop/)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: HowTo Software<br />
letzte Änderung:2024-01-06 <br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
