# XFCE einrichten

## TL;DR

Effektives Arbeiten benötigt einen gut konfigurierten Desktop. Dafür ist bei XFCE nicht viel nötig. Es muss nicht KDE oder GNOME sein.


## Whisker Menu

- [Whisker Menu](https://docs.xfce.org/panel-plugins/xfce4-whiskermenu-plugin/start) ist ein alternatives/besseres Startmenü für Xfce,  ersetzt Dock, listet Favoriten
- [Whisker Menu bei UU](https://wiki.ubuntuusers.de/Whisker_Menu/)
- Installation: `$ sudo apt install xfce4-whiskermenu-plugin`


## Programme ins Panel

- Die wichtigsten Programme (Terminal, Editor, ...) sollte man griffbereit im Panel (= Randleiste) ablegen:
  - `Whiskers-Menü > Programm suchen > Kontext-Menü des Treffers > Add to Favorites`
  - `Whiskers-Menü > Kontext-Menü des Programs > Add to Panel`
  - Icon an gewünschte Position schieben mittels seines Kontext-Menüs


## Panel konfigurieren

### Panel Preferences

- **senkrecht**: Da Monitore eher zu breit als zu hoch sind, sollte man das Panel an den linken oder rechten Rand stellen: `Kontext-Menü im Panel > Panel Preferences > General Mode: Vertical`
- **über ganze Höhe**: `Kontext-Menü im Panel > Panel Preferences > Measurements Length (%): 100`
- **einspaltig**: `Kontext-Menü im Panel > Panel Preferences > Measurements Number of rows: 1`


### Schriften an Icons im Panel entfernen

- mit Schriften wird es unübersichtlich
- `Panel Context Menu: Panel > Panel Preferences > Items (Reiter) > Windows Button (Menü-Eintrag, Doppel-Klick) > Uncheck 'Show button labels'`


### gleichartige Fenster zusammenfassen

- verschafft besseren Überblick
- `Panel Context Menu: Panel > Panel Preferences > Items (Reiter) > Windows Button (Menü-Eintrag) > "Window grouping:" Always`


### Panel Plugins

- Es gibt viele hilfreiche Panel-Plugins, komplette Liste der Plugins im Repository:  
  `$ apt-cache search xfce | grep -i plugin | grep -i panel`
- [Clipboard](../Prgs.d/Clipboard.md)
- [Notizen](../Prgs.d/Notizen.md) - digitale Post-its
- [Notes](https://docs.xfce.org/panel-plugins/xfce4-notes-plugin/start) - kennt "Recent Documents"
- [Clock](https://docs.xfce.org/xfce/xfce4-panel/clock): diverse Anzeigeformate, Anzeige eines Monatskalenders
- [mehr](https://wiki.ubuntuusers.de/Xfce_Panel_Plugins/) Xfce Panel Plugins bei UU


### Workspace Switcher

- Der [Workspace Switcher](https://docs.xfce.org/xfce/xfce4-panel/pager) erlaubt es, für jede Aufgabe einen dedizierten  virtuellen Desktop einzurichten.
- installieren: `Panel-Kontext-Menu: Add New Items , Workspace Switcher: [Add]`
- 4 Zeilen, 1 Spalte einstellen, sonst nur ein kleiner Streifen


## Xfdashboard

- zeigt alle Fenster auf allen Desktops im Überblick verkleinert auf einem Bildschirm, auch als [Exposé](https://www.maceinsteiger.de/was-ist/mac-expose/) bekannt
- Xfdashboard: [home](https://docs.xfce.org/apps/xfdashboard/start), [bei UU](https://wiki.ubuntuusers.de/Xfdashboard/)
- Installation: `$ sudo apt install xfdashboard xfdashboard-plugins`
- Konfiguration:  `Whiskers > Settings Manager (Icon, lu) > Xfdashboard settings (Icon, ro)`

## Themes

- zu einem Theme gehören z.B: Fensterrahmen, Fensterinhalte, Symbole, Mauszeiger
- im Repository gibt es: `$ apt-cache search xfwm | grep theme`
- mehr Themes: [Eyecandy for your XFCE-Desktop](https://www.xfce-look.org/browse/)
- [mehr](https://wiki.ubuntuusers.de/Xfce_Themes/)


## Misc

- [Maximierung](https://askubuntu.com/questions/891859/xubuntu-snap-to-window-switching-workspaces) verhindern, wenn Fenster am Rand anstößt
- ungewollten Desktopwechsel verhindern: `Settings > Window Manager >  Advanced (Reiter): "wrap workspaces when reaching the screen edges"` : 2x keine Häkchen


## Fortgeschrittenes

- Man kann auch ganze Kommando-Sequenzen in [Launcher](https://docs.xfce.org/xfce/xfce4-panel/launcher) packen und im Panel platzieren.
- Man kann auch Aktionen an [Tasten](https://wiki.ubuntuusers.de/Xfce-Einstellungen/#Tastatur) binden. Die aktuellen Tastenkürzel auflisten: `$ xfconf-query -c xfce4-keyboard-shortcuts -l -v`
- Viele Einstellungen kann man im  [Settings Manager](https://wiki.ubuntuusers.de/Xfce-Einstellungen/) von Xfce vornehmen: `Whiskers Menü > Icon links unten`


## Autostart

- Via Autostart kann man definieren, welche Programme nach dem Einloggen gestartet werden sollen:
  `Whiskers > Settings > Session and Startup > Application Autostart (Reiter)`


## siehe auch

- [4 Ways You Can Make Xfce Look Modern and Beautiful](https://itsfoss.com/customize-xfce/)
- [7 Great XFCE Themes for Linux](https://www.maketecheasier.com/xfce4-desktop-themes-linux/)
- [How to customize Xfce desktop](https://linuxhint.com/customize-xfce-desktop/)
- [Eyecandy for your XFCE-Desktop - xfce-look.org](https://www.opendesktop.org/s/XFCE/browse/)
- [How to Customize Your Xfce Desktop Look Like macOS Big Sur](https://linuxhint.com/customize-xfce-desktop/) - 40:40


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Desktop Konfiguration<br />
letzte Änderung:2024-01-13<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
