# Seiten-Template


## tl;dr

## Übersicht


## Installation


## Nutzung
### Optionen
### Beispiele




## Alternativen

## Weblinks

## ergänzendes Material
Supplement, Abkürzung: spl

## Übungen
- [Übungen zu Paketen](./Snap,Flatpak.ex.md) - Größe, Startzeit, memory usage, ...


## Beiträge
.ex = exercise
.from.md  = from audience
.for.md
## 2do

---

<sub>
Autor: Helmut Hayd  
Schlagwörter:    
letzte Änderung:   
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.  
![cc-by-sa](cc-by-sa.120.png)
</sub>


Lizenz 	LGPLv3+ und GPLv3+
Code GPLv3

In und für die Vorlesung entstandener Code ist Free and Open Source Software licensed under the GPLv3. 
https://www.gnu.org/licenses/gpl-3.0.html
