# Die Vorlesung

- [t1p.de/linuxgit](https://t1p.de/linuxgit)
- oder direkt zum ToC: https://t1p.de/linuxba5
- Mitmachen: [t1p.de/linuxpad](https://t1p.de/linuxpad)


## Ziele

- IT-Anforderungen im Beruf mit Linux bearbeiten
- dafür kompetente Auswahl der Hilfsmittel, (freier) Software, Services
- Alternativen zu kommerziellen Angeboten
- ein Einstieg, von dem man ggf. weitergehen kann
- auf dem Desktop übt man, qualifiziert sich für den Server
- Erweiterung der beruflichen Perspektiven - wird in KI-Zeiten  wichtiger


## Inhalte

- siehe [ToC](./0-ToC.md)
- bewährte Tools
- Informationsmanagement
- Arbeitsumgebung, Desktop
  - Auswahl eines Desktops
  - Einrichtung der Arbeitsumgebung
  - Shell
- Vorstellung von Programmen zum Aufbau einer Linux-Server-Landschaft (Filer, Backup, Archiv, ...)
- Services im Netz
  - Server als Einstieg sehr/zu ambitioniert => Einstieg, nicht Administration
  - Linux-Eignung untersuchen
- auch Umfeld: Hardware, Arbeitsumgebung


## Stil der Vorlesung

- Konkretes: **Beispiele** statt [Metasprache](https://de.wikipedia.org/wiki/Backus-Naur-Form) zur Syntax-Erklärung
- Ausgangspunkt ist die **Aufgabe**, nicht das Programm  ("Wir nehmen immer was vom Marktführer.")
- Vermittlung von **Erfahrungen**, googeln/AI nutzen kann jeder
- keine Feature-Listen, sondern HowTos, erste Eindrücke
- **mitmachen**, Vorlesungsscript ergänzen (in Übungen), cut 'n paste-fähige Cheat Sheets beginnen
- **Skript** zur Vorlesung: Markdown, Hypertext
  - viele URLs
    - statt cut 'n paste
    - die Auswahl ist Empfehlung
- Die **Halbwertszeit** des IT-Wissens liegt bei 2a, daher lebendes Dokument mit Gedächtnis.
  - Erfahrung hilft Entwicklungen zu beurteilen, aber nicht konkret.
- Anregungen, in welche Richtungen man sein **Wissen vertiefen** könnte


## Fazit

- Linux bietet einen komplexen Baukasten, mit dessen Bausteinen man sich maßgeschneiderte Lösung zusammenstellen kann.
- Bausteine kann man selbst herstellen.

## siehe auch

![QR-Code](../Pics.d/t1p-de-linuxgit.QR.png)


---

<sub>
Autor: Helmut Hayd<br />  
letzte Änderung: 2025-01-05
</sub>







