# Übungen zu REs

mit [grep](../Prgs.d/grep.md)  und [sed](../Prgs.d/sed.md)

Die Übungsbeispiele sind zwar nicht immer die sinnvollsten, aber mit Bordmitteln leicht zu realisieren und sinnvollen Anwendungen sehr ähnlich.


## So geht es

- gerne auch in 2-er Gruppen. Wer fertig ist, hilft anderen[.](RE-Uebungen.mitL.md)
- Testen Sie die **REs** auf der Kommandozeile oder mit Hilfe von (selbst erstellten) Beispieldateien.
- Verwenden Sie egrep --color  (als alias), um die Treffer erkennbar zu machen.
- Schreiben Sie die gesuchten REs mit ihrer Nummer in dieses [Pad](https://pad.gwdg.de/_5QW5o3XSjiOw_zBDshyGA#), so dass sie diese leicht erklären und vorführen können.
- Die REs sollten möglichst einfach und verständlich sein, damit sie diese auch 6 Monate später noch schnell verstehen.


## verschieden Schreibweisen (v)

- Extrahieren Sie mit einem möglichst kompakten RE nur "Meyer" und "Maier":  
`$ echo "Maier Mayr Meier Doppelmeier Meyr Meyer Mey Meier-Schuh Mäyer" | egrep RE-v`  

## Datum (d)

- Schreiben Sie ein egrep, das nur dieses Datumsformat findet: T.M.JJJJ  
`$ echo "10.12.1972  1.2.2022 3.4.22  1.02.23" | egrep RE-d`
  

## Suche in Listen (l)

Die Suche darf nicht zu viele Treffer liefern, z.B. trusted

- Wie viele Rust-Pakete bietet ihre Distribution an? **RE-l1**
  - Tipp: `apt-cache search ...`
- Wie viele libraries sind auf ihrem Rechner installiert?  **RE-l2**
  - Tipp: `dpkg -l ...`


## Suche in URL-haltigen Files (u)

Suchen Sie in dem File /etc/apt/sources.list

1. Extrahieren Sie alle Worte, die komplett groß geschrieben sind. **RE-u1**
1. Extrahieren Sie alle Zeilen, die nicht auskommentiert sind. **RE-u2**
1. Extrahieren Sie alle Zeilen, die eine URL enthalten. **RE-u3**
1. Extrahieren Sie alle Zeilen, die eine URL enthalten und auskommentiert sind. **RE-u4**
1. Extrahieren Sie alle Worte, die 2 Vokale hintereinander enthalten. **RE-u5**


## Prozesse analysieren (p)

1. Extrahieren Sie aus der Prozessliste (ps -ef) alle Prozesse, die nicht root gehören. **RE-p1**
1. Extrahieren Sie aus der Prozessliste (ps -ef) alle Prozesse, die nicht root und nicht user gehören. **RE-p2**
1. Extrahieren Sie aus der Prozessliste (ps -ef) alle Prozesse, die mehr als 10 Sek. CPU-Zeit verbraucht haben (Spalte TIME). **RE-p3**


## Mail (m)

1. Schreiben Sie einen RE, der eine Mailadresse auf formale Korrektheit überprüft und deren Provider in einer der großen Top-Level-Domains beheimatet ist. **RE-m1**
2. Entfernen Sie ">" am Zeilenanfang eines Zitats in einer Mail.  
`$ echo -e "> Hallo Max,  \n> viel Text  \n> Viele Grüße Otto" | sed RE-m2`



## Dateinamen mit Endungen (e)

Schreiben Sie einen RE, der all die [Dateiendung](https://de.wikipedia.org/wiki/Liste_von_Dateinamenserweiterungen) ".txt .txt,v  .md .odt" (und nicht mehr)  matcht?  
`$ echo "abc.def.txt abc.txt.def lmn.txt,v  lmn,txt,v lm,n.txt,v opq.rst.md  O.123.md  a/brief.odt bild.jpg"  | egrep RE-e`


## Dubletten (du)

1. Finden Sie Worte mit mind. 2 gleichen aufeinanderfolgenden Zeichen bzw. Buchstaben:  
`$ echo "Boot Dampfer Schiff brief..text Kahn Tanker Schifffahrt" | egrep --color RE-du-1`
1. Finden Sie doppelte Worte, die direkt hintereinander stehen.  
`$ echo "eins zwei drei drei vier fünf fünf fünf sechs" | egrep  RE-du2`  
1. Ersetzen Sie eine von beiden Dubletten.  
`$ echo "eins zwei drei drei vier fünf fünf fünf sechs" | sed RE-du3`


## Geldbeträge (g)

Extrahieren Sie Geldbeträge in deutscher Schreibweise.  
`$ echo "10  12.345,00  7,777.00  9,99  7,9  999,99  95.00" | egrep RE-g`


## Einspalter (1)

Verwandeln Sie die man page von dpkg in eine einspaltige, lange Textdatei. **RE-1**  
Tipp: `man dpkg > /tmp/dkg.txt`


## Telefonnummer (n)

- Extrahieren Sie die Telefonnummern aus dem [Telefonbuch](https://www.dasoertliche.de/?kw=meier&ci=&form_name=search_nat) zu einer Telefonnummernliste. **RE-n1**
  - Tipp: als HTML-Seite abspeichern und dann mit grep, sed die Liste erstellen
- Anonymisieren Sie die Tel.Nr. so, dass die falsche Nr. nicht auffällt. **RE-n2**


## Refactoring (r)

Auch für Bachelorarbeiten nützlich, um z.B. eine einheitliche Begrifflichkeit (Laptop, Notebook) oder Schreibweise (Server-Schrank, Serverschrank) sicherzustellen.

1. Vereinheitlichen Sie diese Variablennamen: calculate-pi , CalculatePi, CalcPi, CalcPI, Calc_Pi zu calcPi ohne die Namen "Pi, RasPi , 5Pi" zu verändern.  
`$ echo "calculate-pi , RasPi CalculatePi, Pi CalcPi, CalcPI, Calc_Pi 5Pi" | sed  RE-r1`
1. Um die Les- und Suchbarkeit zu verbessern, führen Sie bitte in Bash-Scripten das Schlüsselwort "function" ein, d.h. ersetzen Sie "function_name()" oder "function_name ()" durch "function function_name ()"  
`$ echo -e "function_name()  \nfunction_name () \nfunction function_name ()" | sed  RE-r2`


## Gendern (ge)

Verwandeln Sie diese gegenderten Substantive in eine korrekte Schreibweise z.B. Benutzer:innen / Benutzer*innen -> Benutzer und Benutzerinnen  
`$ echo -e "Student*innen  \nAdministrator:innen" | sed RE-ge`
  
<!---
`$ echo -e 'Student*innen \nAdministrator:innen' | sed -E 's/([[:alnum:]]*)[*:]innen/\1en und \1innen/g' `
-->

## /etc/passwd (pw)

1. Ersetzen Sie in einer Kopie des /etc/passwd Files das x im 2. Feld durch ein y, außer wenn im 3. Feld eine min. 4-stellige Zahl steht. **RE-pw1**
1. Listen Sie alle nummerischen user IDs in /etc/passwd (man 5 passwd) auf und nur diese (Rest weg-substituieren). **RE-pw2**


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Uebungen<br />
letzte Änderung: 2025-01-27<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
