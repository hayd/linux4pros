# Phishing

Angeln nach Vertraulichem

- Phishing basiert auf **Emotionen**: Gier, Neugier, Mitleid, Angst
- Phishing ist die [dominierende Bedrohung](https://thriveagency.com/safe-browsing/)
- Aktivitäten in sozialen Meiden liefern Munition für [Spear-Phishing](https://de.wikipedia.org/wiki/Phishing#Spear-Phishing)


## Gefahren

- Mitunter reicht eine gut gemachte Phishing-Mail, um Systeme zu infiltrieren, die eigentliche Schadsoftware nachzuladen und damit Dateien und Laufwerke zu verschlüsseln, wenn möglich auch die Sicherungskopien.
- [Delayed Phishing](https://www.ikomm.de/was-ist-eigentlich-delayed-phishing/): lange Inkubationszeit



## Erkennung

### Irreführende URLs

Vorlesen: vvetter.com, arnazon.de, faecbook.com

Irreführende URLs arbeiten z.B mit:

- vv statt w (z.B: vvetter.com), rn statt m (z.B: arnazon.de)
- Buchstabendreher im Wortinneren (z.B: faecbook.com)
- hinter/unter ausgeschriebener URL liegt eine falsche

Es werden bekannte URLs (z.B: https://cbs.mpg.de) eingebaut:

- https://cbs.mpg.de.forms.com/...
- https://forms.google.com/cbs.mpg.de
- Die URL des Servers muss mit genau der richtigen Domain enden.


### Andere Kennzeichen

- Druck
- von KI übersetztes Deutsch (früher auffällig schlecht, heute eher auffällig gut)
- Inhalt: Passwort, Geld, Mail-Adresse
- angehängtes PDF mit Javascript (`pdfinfo` nutzen)


## Abwehr

- **URLs** müssen komplett **sichtbar** werden. Mail-Client auf "text only" umstellen.
- Nie, niemals, nicht: Geben Sie ihr **Passwort** [hier](https://xyz.somecompany.com/) ein.
  - Seriöse Aufforderungen: *"Bitte ändern Sie ihr Passwort. Sie wissen schon wo."*
- Die **Mail-Adressen** haben des öfteren einen seltsamen,  skalierbaren Anteil. Nicht alle stellen sich so doof an:  

`Tᴇʟᴇᴋᴏᴍ Pʀᴀ̈ᴍɪᴇɴ 8594469946@ctluprw.ctluprw.ctluprw.nl um  01:03`  
`Tᴇʟᴇᴋᴏᴍ Pʀᴀ̈ᴍɪᴇɴ 6018380810@gjgqhqu.gjgqhqu.gjgqhqu.nl um  01:06`

- **Landing pages** können optisch täuschend echt sein. Nicht irritieren lassen.
- "[Awareness-Maßnahmen](https://getgophish.com/) ohne jeden praktischen relevanten Effekt."  von Linus Neumann in internationalen Studien nachgewiesen: [Menschliche Faktoren der IT-Sicherheit](https://media.ccc.de/v/36c3-11175-hirne_hacken), Versuchsaufbau:30:30  Ergebnis: 34:00


## HTML-Mail

- [Eingebettete Objekte](https://www.heise.de/news/Checkliste-Mails-so-verschicken-dass-man-Ihnen-vertraut-7238158.html): HTML-Mails bergen unnötige **Risiken**: Der Empfänger sieht nicht auf den ersten Blick, auf welche Webadresse ein Link wirklich zeigt und von externen Servern eingebettete Inhalte sind entweder ein Datenschutzrisiko oder werden vom Empfängerclient nicht angezeigt.
- [The Only Safe E-Mail Is Text-Only E-Mail](https://www.scientificamerican.com/article/the-only-safe-e-mail-is-text-only-e-mail/)
- [Schilderung der Sinnlosigkeit des Versands von HTML E-Mail](https://mac-service.koeln/htmlemail.html)
- **Fazit**: Schalten Sie HTML in E-Mail unbedingt ab.

Was will man also mit HTML-E-Mails vermitteln?

- Ich bin mir der durch HTML-E-Mails verursachten Gefahren nicht bewusst.
- Ihre Sicherheit ist mir egal.
- (Altbackene) Werbung ist wichtiger als eine sichere, technologieoffene Kommunikation.
- Wenn das alle machen, können wir auch nicht gegen den Strom schwimmen.


## Sinnlose Footer

- [Angstklauseln](https://angstklauseln.wordpress.com/hintergrund/) am  Ende einer E-Mail.
  - **Fazit**: Wer vertrauliche Informationen per E-Mail versendet, der sollte diese elektronisch Verschlüsseln und nicht auf Disclaimer vertrauen.


## siehe auch

- [Wie Sie Phishing-Mails erkennen und abwehren](https://www.heise.de/ratgeber/Wie-Sie-Phishing-Mails-erkennen-und-abwehren-7240007.html?wt_mc=intern.red.plus.topteaser.startseite.teaser.teaser) 26.08.2022, c't Magazin
- [NoPhish Konzept](https://secuso.aifb.kit.edu/betruegerische_nachrichten_erkennen.php) von der Forschungsuniversität in der Helmholtz-Gemeinschaft
- [Phishing & Smishing auf dem Vormarsch](https://www.bsi.bund.de/DE/Themen/Verbraucherinnen-und-Verbraucher/Cyber-Sicherheitslage/Methoden-der-Cyber-Kriminalitaet/Spam-Phishing-Co/Passwortdiebstahl-durch-Phishing/passwortdiebstahl-durch-phishing_node.html) - BSI
- [Social Engineering – der Mensch als Schwachstelle](https://www.bsi.bund.de/DE/Themen/Verbraucherinnen-und-Verbraucher/Cyber-Sicherheitslage/Methoden-der-Cyber-Kriminalitaet/Social-Engineering/social-engineering_node.html)- BSI


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Security<br />
letzte Änderung: 2025-02-12<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
