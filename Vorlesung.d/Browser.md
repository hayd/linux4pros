# Browser

## TL;DR

- aus Sicherheitsgründen **nur** Browser mit aktuellem Support einsetzen
- Firefox gut konfiguriert für das Tagesgeschäft
- Chromium mit Werkseinstellung, wenn Firefox versagt (wegen diverser Blocker)


## Firefox

- [Firefox](https://www.mozilla.org/de/firefox/new/) wird von der  Non-Profit-Organisation Mozilla Foundation entwickelt
- Rendering-Engine: [Gecko](https://de.wikipedia.org/wiki/Gecko_(Software)), in C++ geschrieben
- alle 6 Wochen eine neue Version
- Verbreitung: [weltweit](https://de.statista.com/statistik/daten/studie/157944/umfrage/marktanteile-der-browser-bei-der-internetnutzung-weltweit-seit-2009/), [in Deutschland](https://gs.statcounter.com/browser-market-share/desktop/germany), [mit Versionsangabe](https://www.stetic.com/de/market-share/browser/)
- Firefox hat vor allem [hausgemachte Probleme](https://linuxnews.de/hat-mozilla-noch-eine-zukunft/).
- Die Suche kennt diverse **Operatoren** wie:
  - `AJD/x`: x = max. Abstand in same order
  - `NEAR/x`: x = max. Abstand in any order
    - "Reiter München" => ungefähr 10.100.000 Ergebnisse
    - "(Reiter NEAR/5 München)" => ungefähr 944.000 Ergebnisse  (/ 11)
    - "(Reiter ADJ/5 München)"  => ungefähr 16.100 Ergebnisse  (/ 627)


### Erweiterungen, AddOns

- [Tree Style Tab](https://addons.mozilla.org/en-US/firefox/addon/tree-style-tab/) - stellt Tabs senkrecht, hierarchisch dar
  - [home](https://github.com/piroor/treestyletab)
  - hat zahlreiche [eigene Erweiterungen](https://github.com/piroor/treestyletab/wiki/API-for-other-addons)
- [Copy as Markdown](https://github.com/yorkxin/copy-as-markdown)
- [LanguageTool](https://languagetool.org/de) - beste Grammatik-, Stil- und Rechtschreibprüfung


### Abkömmlinge

- [ESR-Version](https://www.mozilla.org/en-US/firefox/enterprise/)  = Extended Service Release,  etwa alle 54 Wochen ein Feature-Update, nicht alle Einrichtungen wollen ihren Usern alle 6 Wochen neue Features erklären

- [Pale Moon](http://www.palemoon.org/) - fokussiert auf Anpassbarkeit und Effizienz
- [LibreWolf](https://librewolf.net/) - fokussiert auf Privatsphäre, Sicherheit, Freiheit


### siehe auch

- [soeren-hentzschel.at](https://www.soeren-hentzschel.at/projekt-unterstuetzen/) - umfangreichste Berichterstattung über Mozilla im deutschsprachigen Raum
- [bei UU](https://wiki.ubuntuusers.de/Firefox/)


## Chromium

- [home](https://www.chromium.org/Home/), [UU](https://wiki.ubuntuusers.de/Chromium/)
- FOSS
- Rendering-Engine: Blink in  C++
- technische Basis von Google Chrome, Edge, Opera, Vivaldi, Brave
- saubere Implementierung der Web-Standards, z.B. WebRTC
- gegenüber Google Chrome fehlen: Digitale Rechteverwaltung (= Art von Kopierschutz), automatische Update-Funktion, einige patentierte Codecs, Senden von Fehlerberichten an Google



## Schlanke Browser

oft veraltet oder ohne Mehrwert

- [Min](https://minbrowser.org/) - letzte Version vom Dez. 2022, .deb zum Download
  - technische Basis ist das von GitHub entwickelte Electron-Framework, somit im Endeffekt Chromium
- [Dillo](https://wiki.ubuntuusers.de/Dillo/) - letzte Version vom Juni 2015
- [Lariza](https://wiki.ubuntuusers.de/Lariza/) - kein .deb, tote Links
- [MyBrowse](https://wiki.ubuntuusers.de/MyBrowse/) - nur via PPA
- [NetSurf](https://wiki.ubuntuusers.de/NetSurf/) - letzte Version vom Mai 2020
- [Viper](https://github.com/LeFroid/Viper-Browser/releases) - AppImage vom Oct 18, 2021
- [surf](https://en.wikipedia.org/wiki/Surf_(web_browser))  - letzte Version vom Mai 2021


## Default-Browser definieren

- Liste der Möglichkeiten:  
  `$ update-alternatives --list x-www-browser`
- Default-Browser systemweit festlegen:  
$ `sudo update-alternatives --config x-www-browser`
- Default-Browser für User festlegen in XFCE:  
`Einstellungen > Standardanwendungen`


## Siehe auch

- [Liste](https://wiki.ubuntuusers.de/Internetanwendungen/#Webbrowser) der Webbrowser bei UU


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Empfehlungen Software<br />
letzte Änderung: 2025-02-05<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
