# Office Pakete

... die man lokal installieren kann.

## LibreOffice

- freie Alternative zu Word und seinen Modulen
- [home](https://de.libreoffice.org/), [bei UU](https://wiki.ubuntuusers.de/LibreOffice/), [bei WP](https://de.wikipedia.org/wiki/LibreOffice)
- ein Fork von OpenOffice
- echte Unterstützung des Dateiformatstandards OpenDocument
- libreoffice.deb mit vielen Erweiterungen, Ergänzungen (>400) in Repo
- Kann mit alten MS-Dokument oft besser umgehen als Word.
- Bildet die Basis der Cloud-Version [Collabora Online](https://de.wikipedia.org/wiki/Collabora_Online). OnlyOffice ist hier im Vorteil, da es im Browser läuft.
- [Kommentar: LibreOffice gibt auf – Office ist angeblich ausentwickelt](https://curius.de/2023/08/kommentar-libreoffice-gibt-auf-office-ist-angeblich-ausentwickelt/), August 2023


## OnlyOffice

- [home](https://www.onlyoffice.com/de/)
- in JavaScript geschrieben, läuft daher im Browser
- entwickelt von einer freundlichen Firma in Lettland
- Kann man in der Cloud [kostenlos testen](https://www.onlyoffice.com/de/registration.aspx).
- formatkompatibel und multiplattformfähig
- verfügbar als App für Linux, Windows, macOS, iOS und Android und als serverbasierter kollaborativer Web-Editor. Die Enterprise-Versionen sowie die Integrationen in die genannten Kollaborationsplattformen muss man kaufen.
- [integriert](https://de.wikipedia.org/wiki/OnlyOffice#Integrationen_von_OnlyOffice_Docs) z.B.  in OwnCloud, Nextcloud, [Seafile](https://wiki.ubuntuusers.de/Seafile/#Office-Integration-mit-Seafile), Google Drive
- Desktop- und Mobil-Varianten und die Community-Edition sind kostenlos und frei.
- Hinter OnlyOffice steht eine Firma, die auch Support und Weiterentwicklung (Aufträge) anbietet.
- als onlyoffice-desktopeditors_amd64.deb beim Hersteller zum [Download](https://www.onlyoffice.com/de/download-desktop.aspx#desktop) und im Repository

## WPS Office

- [home](https://www.wps.com/office/linux/), [bei UU](https://wiki.ubuntuusers.de/WPS_Office/)
- kostenlos nicht frei
- von [Kingsoft](https://de.wikipedia.org/wiki/Kingsoft_Corporation) , China
- [In](https://de.wikipedia.org/wiki/WPS_Office)  staatlichen Organen und Betrieben in China ist WPS Office relativ weit verbreitet.
- .deb vom Hersteller
- Einsatz, wenn die anderen am einem MS-Dokument scheitern


## SoftMaker Office

- [home](https://www.softmaker.de/softmaker-office), [bei WP](https://de.wikipedia.org/wiki/Softmaker#SoftMaker_Office), [bei UU](https://wiki.ubuntuusers.de/Softmaker_Office/)
- kleine Firma in Nürnberg
- Man kann .debs kaufen beim Hersteller, eine reduzierte Version ist kostenlos.
- "Die herausragenden Merkmale der Software von SoftMaker sind ... enorm hohe Kompatibilität mit Microsoft Office ..." [Eigenwerbung](https://www.softmaker.de/ueber-uns)
- Einsatz, wenn die anderen am einem MS-Dokument scheitern




## siehe auch

- [Alternativen zu Microsoft Office: Cloud-Office-Pakete im Vergleich](https://www.heise.de/ratgeber/Alternativen-zu-Microsoft-Office-Cloud-Office-Pakete-im-Vergleich-7339468.html) kostet,  Google Workspace, Collabora Online und Canva Visual Worksuite, Microsoft 365
  - Fazit: Die vorgestellten Pakete richten sich an klar definierte Zielgruppen. Google Workspace und Microsoft 365 sind die cloudbasierten Allround-Bundles für alltägliche Office-Aufgaben. Die lassen sich großteils auch mit Collabora Online erledigen, doch muss sich der Admin um das Hosting selbst zu kümmern. Canvas wird noch eine Weile brauchen, bis das Paket wirklich ausgereift ist und genug Optionen für den Großteil der Nutzer bietet.


## Aufgaben

- [onlyoffice-desktopeditors](https://www.onlyoffice.com/de/download-desktop.aspx#desktop) installieren und mit einem komplexen Word-Dokument, PowerPoint-Präsentation die Kompatibilität testen.


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Empfehlungen Software<br />
letzte Änderung: 2024-01-22<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>


<!---  OnlyOffice: Was ist frei? PDFs editieren  -->

