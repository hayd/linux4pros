# Scannen

## TL;DR

- SANE
- OCR: tesseract

## SANE

- [SANE - Scanner Access Now Easy](https://sane-project.gitlab.io/website/)Scanner Access Now Easy
- [XSane](https://wiki.ubuntuusers.de/XSane/) - GUI um Scannen zu beobachten, steuern, optimieren
- Linux verwendet eine eigene offene Schnittstelle für Scanner, welche von den Herstellern unabhängig ist: die SANE Schnittstelle
- [List of Supported Devices](https://sane-project.gitlab.io/website/sane-supported-devices.html)


## noteshrink

- Verstärkung des Kontrastes und Entfernen von "Dreck" durch Transformation in anderen Farbraum (RGB --> HSV) und Clusteranlyse
- [Doku](https://mzucker.github.io/2016/09/20/noteshrink.html) mit eindrucksvollen Beispielen
- [GitHub](https://github.com/mzucker/noteshrink)
- Installation: `$ pip install noteshrink`


## OCR

- [tesseract-ocr](https://wiki.ubuntuusers.de/tesseract-ocr/): OCR auf Kommandozeile
  - bei Google im [Einsatz](https://de.wikipedia.org/wiki/Tesseract_(Software)#Geschichte) : Books, Mail, Android
  -  Das [Internet Archive](https://archive.org/developers/ocr.html)  verarbeitet damit mehr als 2 Millionen Seiten pro Tag.
- [OCRmyPDF](https://wiki.ubuntuusers.de/OCRmyPDF/): Scan + OCR-Text --> PDF/A mit durchsuchbarer Textebene
- [gscan2pdf](https://wiki.ubuntuusers.de/gscan2pdf/): a GUI to produce PDFs or DjVus from scanned documents
- [OCRFeeder](https://wiki.ubuntuusers.de/OCRFeeder/) vereinfacht das Importieren, Zuschneiden und Konvertieren von gescannten bzw. abfotografierten Dokumenten durch Kombination mehrerer Tools
- [Paperwork](https://wiki.ubuntuusers.de/Paperwork/): Programm zur Digitalisierung, Indexierung und Archivierung von Dokumenten aller Art
- alle diese Programme sind im Ubuntu Repository


## siehe auch

- [OpenPaper.works Projekte](https://openpaper.work/de/projects)
- [SANE bei UU](https://wiki.ubuntuusers.de/SANE/)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Informationsmanagement Software<br />
letzte Änderung: 2023-11-25<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>