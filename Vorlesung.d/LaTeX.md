# LaTeX

## Was ist LaTeX? Warum LaTeX?

- gesprochen la-tech, nicht latex (kein Gummianzug)
  - La: Lamport, τέχνη (Handwerk, Kunst)
- ein **Textsatzsystem**, d.h. ein riesiges Software-Paket, mit eigenen Schriften, zahllosen Tools um Dokumente jeden Umfangs in **Profi-Qualität** zu erstellen
- FOSS
- τέχ wurde 1978 von [Donald E. Knuth](https://www.amazon.de/Art-Computer-Programming-Seminumerical-Combinatorial/dp/0137935102/ref=sr_1_1) entwickelt (300 Befehle), von Leslie **La**mport  1984 als LaTeX nutzbar gemacht
- Mit LaTeX kann man die gute Layout-Qualität von Texten **programmieren**.


## Vorteile

- FLOSS, die man für den Rest des Lebens lernt
- nicht WYSIWYG, sondern **WYSIWYAF** (What you see is what you asked for.)
  - oft **logisches Markup**, nicht Layout-Vorgaben: `\subsubsection`{Experiment 3} statt fett,Arial,14pt
- Es werden **Kapitel** (z.B. Platzierung von Bildern und Tabellen), Absätze und Seiten **optimiert**, nicht nur Zeilen wie bei Word.
- basiert auf Erfahrungen von **Generationen von** Buchdruckern, Setzern, Typographen, die z.B. Schusterjungen (erste Zeile eines Absatzes ist die letzte Zeile auf einer Seite) und Hurenkinder (letzte Zeile eines Absatzes auf einer neuen Seite) berücksichtigen
- beherrscht [hochkomplexe Formeln](https://ei.hs-duesseldorf.de/personen/braun/lehre/Documents/LaTeX%20SS17/Latex%2008%20-%20Formeln.pdf) dank [AMS-LaTeX](https://de.wikipedia.org/wiki/AMS-LaTeX)
- Alle Formatierungen werden über **Kommandos** im Text erreicht.
- **portabel** von OS zu OS, von Editor zu Editor, bei jeder Kompilierung das gleiche Ergebnis
- von **Versions-Verwaltungen** gut zu handeln
- Bücher-Reihen, Grafiken und Verzeichnisse für Literatur, Abbildungen, Abkürzungen, Glossar, ... - kein Problem (für LaTeX)
- zahllose **Ergänzungen**: biblatex, KOMA-Script, [MusiXTeX](https://de.wikipedia.org/wiki/MusiXTeX), viele Pakete (s.u.) ...
- (floatende) **Umgebungen** für Aufzählungen, Bilder, Tabellen, Code-Beispiele, Beschreibungen, ...
  - `\begin{itemize} \item Montag \item Dienstag \end{itemize}`



## Nachteile

- LaTeX schreiben ist das **Programmieren** eines optimierten PDFs.
- **steile Lernkurve**, die mit einem Muster deutlich abgeflacht werden kann
- sehr **viele Erweiterungen**, Pakete
- produziert **kein Word**-Format (aber z.B. PDF)
- funktioniert **ganz anders** als herkömmliche Office-Programme


## Installation

- `$ sudo apt install texlive texlive-lang-german texlive-latex-extra`
- [mehr](https://wiki.ubuntuusers.de/TeX_Live/) zur Installation
- Bei entsprechender Wahl werden mehrere hundert Megabyte  installiert wegen der zahlreichen Fonts und Sprachen. Daher klein anfangen und bei Bedarf mit Hilfe von  [Durchsuchen des Inhalts von Paketen](https://packages.ubuntu.com/) nachlegen.


## Dokumentklassen, Packages in LaTeX

Mit **Dokumentklassen** wird das grundsätzliche Layout des Textes festgelegt (zweiseitig oder einseitig, Formatierung der Überschriften ...). Der dafür verantwortliche LaTeX-Code  wird in Form von [class files](https://www.overleaf.com/learn/latex/Understanding_packages_and_class_files) installiert (z.B: article.cls, book.cls, letter.cls, ...). Eine solche Angabe ist in jedem LaTeX Dokument zwingend erforderlich.

```latex
\documentclass[Optionen]{Dokumentklasse}

\begin{document}
...
\end{document}
```

- Dokumentklassen:
  - **article**: für Hausarbeiten, Ausarbeitungen von Vorträgen, Artikel in wissenschaftlichen Zeitschriften
  - **report**: längere Berichte, die aus mehreren Kapiteln bestehen, einseitig
  - **book**: für Bücher, zweiseitig, Kolumnentitel (headings)
  - **letter**: für Briefe (siehe auch cbsletter)
- z.B: `\documentclass[12pt, a4paper, portrait]{article}`

- **Packages** (LaTeX Makros) implementieren zusätzliche Objekte wie  Index, Glossar, Abkürzungensverzeichnis, Listings, ...:  
`\usepackage{listings}`


## Pakete

Pakete enthalten TeX- oder LaTeX-Code in Form von *.sty Files. Sie bringen neue Features mit.

Überblick:

- [Standardpakete für LaTeX](https://www.namsu.de/Extra/pakete/latex-packages.html)
- [Browse Packages](https://ctan.org/pkg/)

einige wichtige Pakete:

- [inputenc](https://www.namsu.de/Extra/pakete/German.html#1) für die direkte Eingabe von Umlauten  
- [fontenc](https://www.namsu.de/Extra/pakete/German.html#1) für Handling von Umlauten
- [hyperref](https://de.wikibooks.org/wiki/LaTeX-W%C3%B6rterbuch:_hyperref) - wandelt alle internen Verlinkungen in klickbare Verweise
- [babel](https://www.namsu.de/Extra/pakete/Babel_V2017.html) für mehrsprachige Dokumente
- [lineno](https://ctan.org/pkg/lineno) - Zeilennummerierung (als Besprechungsgrundlage)
- [graphicx](https://ctan.org/pkg/graphicx) - Enhanced support for graphics
- [lgrind](https://ctan.org/pkg/lgrind) - Produce beautiful listings of source code
- [fancyhdr](https://ctan.org/pkg/fancyhdr?lang=en) – extensive control of page headers and footers in LaTeX2ε
- [microtype](https://ctan.org/pkg/microtype?lang=en) - Subliminal refinements towards typographical perfection
  - [Unterschied](https://www.khirevich.com/latex/microtype/) durch [tracking, expansion, protrusion, ligatures](https://latexkurs.github.io/lecture/01_formatierung_pakete.pdf)


## KOMA-Script

- Die LaTeX-Standardklassen richten sich nach US-amerikanischen typografischen Konventionen und Papierformaten. Aus diesem Grund wurden zusätzliche Pakete und Klassen entwickelt, die es erlauben, auf **europäische** typografische Konventionen und **DIN-Papierformate** umzuschalten.
- Paketnamen beginnen mit **scr** (script)
- [KOMA-Script Documentation Project](https://www.komascript.de/)
- [KOMA-Script, Die Anleitung](https://komascript.de/~mkohm/scrguide.pdf) - 2022-10-12, PDF, 594 S.


## LaTeX-Engines

- xyz.tex --LaTeX-Engine--> xyz.pdf
- Empfehlung: [LuaTeX](https://de.wikipedia.org/wiki/LuaTeX), sonst XeTeX
  - Vorteile bei Einbindung von Schriften, Speicherverwaltung
- LuaTeX = Engine, lualatex = deren Aufruf


## minimales Beispiel

```latex
\documentclass{article}
\begin{document}
Das ist der Inhalt.
\end{document}
```

## Zerlegung größerer Dokumente

- Gründe:
  - Überblick behalten in großen Dokumenten
  - Fehlersuche einfacher
- Ein Master-Dokument zieht sich mit Hilfe von `\input` und `\include` die Kaptel einzeln rein.
- \input{Kapitel-1.tex} verwenden, ist wie Eintippen
- \include{Kapitel-2.tex} =  \clearpage \input{ } \clearpage
- [Details](https://tex.stackexchange.com/questions/246/when-should-i-use-input-vs-include) zu input vs. include

![input include](../Pics.d/LaTeX-zerlegt.png)


## do it

- `$ lualatex xyz.tex`
- viele Warnings
- ein PDF
- min. 2x teXen, damit Nummerierungen erzeugt und eingesetzt werden können


## Debugging

- Auskommentieren bis Fehler weg:

```latex
\iffalse
 Zeilen, die
  ignoriert werden sollen
\fi
```

- veraltete Vormerkungen, Zählungen entfernen:  
 `$ rm xyz.l* xyz.aux`


## beste IDE: TeXstudio

- [home](https://www.texstudio.org/), [WP](https://de.wikipedia.org/wiki/TeXstudio) (Bild der GUI)
- Pfade dürfen keine Umlaute enthalten
- vielseitig anpassbar, z.B. zusätzliche Knöpfe, Macros im  CentralToolBar
- [Download](https://www.texstudio.org/) vom Hersteller oder aus [Repo](https://packages.ubuntu.com/search?keywords=texstudio&searchon=names&suite=all&section=all)
- Springen von PDF in Quellen möglich
- [TEXstudio-Anleitung](http://www.mi.uni-koeln.de/wp-MIEDV/wp-content/uploads/2016/05/dokumentNeuYP.pdf)



## Verwaltung der Referenzen: biblatex, BibTeX

- Sobald man den Verdacht hat, die Referenzen in mehr als einem LaTeX-Dokument, in verschiedenen Zitierstilen verwenden zu müssen, lohnt sich der Einsatz von biblatex. Es sammelt die im Text referenzierten Einträge, holt sich weitere Informationen (Titel, Author, ...), formatiert diese Daten und bindet sie ein.
- frei verfügbare Referenzsammlungen, z.B [Collection of
Computer Science Bibliographies](https://liinwww.ira.uka.de/bibliography/index.html)
- [JabRef](https://wiki.ubuntuusers.de/JabRef/) - Programm zu Erstellung Referenzlisten für biblatex, .deb von Jan. 2023, "intuitiv", "Platzhirsch"
- [Literaturverwaltung fur LATEX-Neulinge](https://mediatum.ub.tum.de/doc/1315979/1315979.pdf)
- [Literaturverwaltung mit Zotero](https://www.linux-community.de/ausgaben/linuxuser/2021/04/literaturverwaltung-mit-zotero) statt mit EndNote, Mendeley, Citavi, ...
- [Zotero with BibTeX/LaTeX, Princeton University](https://libguides.princeton.edu/c.php?g=148292&p=991756)


## DANTE

- [DANTE e.V.](https://www.dante.de/) =  Deutschsprachige Anwendervereinigung TeX e.V.
- Treffen der Gruppe Leipzig/Halle [ca. zweimal im Jahr](https://www.dante.de/dante-e-v/stammtische/)


## Aufgaben

- [LaTeX-Files herunterladen](https://gitlab.gwdg.de/hayd/linux4pros/-/raw/c73e5926d8e79243d5e89da6ea4ec0c30bdc4c3d/Materialien.d/LaTeX.tar.bz2?inline=false) nach /tmp
- Spielfeld schaffen:
  
```bash
cd /tmp 
tar -xvjf LaTeX.tar.bz2
cd LaTeX.d/
tree
kwrite commands.txt 2>/dev/null &
```

- Compilieren Sie "bachelorarbeit.tex".
- Sehen Sie sich das Ergebnis an: `$ mupdf bachelorarbeit.pdf &`
  - besonders das Inhaltsverzeichnis auf Seite 2
- dann der 2. LaTeX-Lauf
- Geben Sie mupdf-Fenster `r` (reload) ein. Hat sich das Inhaltsverzeichnis verändert?
- Verstehen der .tex-File-Inhalte



## siehe auch

- [CTAN](https://ctan.org/) = Comprehensive TeX Archive Network, zentrale Anlaufstelle für LaTeX-Material wie Pakete, Doku, ...
- [Erstellung wissenschaftlicher Texte mit LaTeX](https://www.fz-juelich.de/en/ias/jsc/news/events/training-courses/2022/latex-2022/latex/@@download/file), Forschungszentrum Jülich, 331 Folien als PDF, April 2022
- [Einführung in das Textsatz-System LaTeX - LinuxCommunity](https://www.linux-community.de/ausgaben/easylinux/2017/01/schoenschreiber/)
- [Einführung in LaTeX](http://www.nagel-net.de/Latex/DOKU/Latexkurs_Skript.pdf) - Universität Tübingen, 234 Seiten
- [Die wissenschaftliche Arbeit mit LaTeX: unter Verwendung von LuaTeX, KOMA-Script und Biber/BibLaTeX](https://www.amazon.de/Die-wissenschaftliche-Arbeit-mit-LaTeX/dp/3965432176/ref=sr_1_2), 448 Seiten
- [LaTeX FAQs](https://texfragen.de/)

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  HowTo LaTeX<br />
letzte Änderung: `2025-02-12`<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>





