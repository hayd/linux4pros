# Videokonferenz-Lösungen

## Jitsi

- freie Software
- nicht im Repo
- basiert auf [WebRTC](https://de.wikipedia.org/wiki/WebRTC)
- nur für kleine Gruppen geeignet (<20)
- Man braucht auch noch einen Hoster.
- [bei UU](https://wiki.ubuntuusers.de/Howto/Jitsi_Meet_Videokonferenzserver/)


## BigBlueButton

- die leistungsfähigste, freie Lösung (OpenTalk?)
- Community-Prozess ist nicht organisiert, d.h. an zahlreichen Unis werden Erweiterungen, Verbesserungen programmiert aber nicht integriert
- bei größeren Gruppen (>200) ist mit Probleme zu rechnen
- Man braucht auch noch einen Hoster.
- [Doku und FAQs aus der Praxis](https://docs.gwdg.de/doku.php?id=de:services:mobile_working:elearning_tools:start) bei der GWDG


## Zoom

- Zoom: _"Not everyone uses Linux, but everyone certainly depends on it. The engineers that work in Linux need a frictionless way to communicate without having to leave the OS they spend most of their day in. Our founding engineers relate to this need. That’s why we have always supported Linux at close parity with our Mac and PC clients."_
- kein [WebRTC](https://de.wikipedia.org/wiki/WebRTC)
- **Durchreichen** der Audio-Devices unter Windows, Mac => Musiker mit gutem Mikro nutzen es zum Zusammenspiel
- gutes **Echo-Cancelling**
- gute [Doku](https://support.zoom.us/hc/de)
- **Aufzeichnung**: Nach Ende der Zoom-Session wird in .mp4 umkodiert und automatisch heruntergeladen, 30 Min. Zoom: 110 MB, DSGVO beachten (Alle, die ihre Nase ins Bild halten, müssen irgendwie zugestimmt haben.)
- **Datenschutz**
  - einen Hoster suchen, der die DSGVO beachtet, z.B. [Connect4Video](https://www.connect4video.com/). "C4V pflegt ein [Datenschutzmanagementsystem](https://www.connect4video.com/index.php/ds) und erfüllt die Vorgaben der DSGVO und des BDSG."
  - [Zoom X ­­– eine einzigartige Verbindung von Zoom und der Telekom](https://geschaeftskunden.telekom.de/magenta-business-collaboration/video-konferenzen/zoom-x)
  - Nur die Mail-Adresse des Veranstalters und der via Zoom Eingeladenen geht in die USA.
- Es gibt einen [Rahmenvertrag mit dem DFN](https://www.dfn.de/dienste/cloud/rahmenvertraege-fuer-cloud-videodienste/).
- Bereitet auch bei **vielen** Teilnehmern (bis zu 10k passive) die wenigsten Probleme.

### Installation

- Download von der [Zoom-Seite](https://zoom.us/download?os=linux):   Linux Type Ubuntu  
`$ sudo apt install /tmp/zoom_amd64.deb`
- Debian-Paket der Version 6.3.6: 204M, 1341 Files, installiert wird in /opt/zoom mit einem Link nach /usr/bin/zoom


## OpenTalk

- [OpenTalk](https://opentalk.eu/de/produkt) bei [Heinlein Support](https://www.heinlein-support.de/news/mailboxorg-setzt-auf-opentalk-als-sichere-videokonferenz)
  - [Präsentation](https://chemnitzer.linux-tage.de/2022/de/programm/beitrag/238) auf den Chemnitzer Linux-Tagen 2022
  - Die digital souveräne Videokonferenzlösung OpenTalk ist **Open Source**, in der als besonders sicher geltenden Programmiersprache RUST geschrieben und basiert auf einer von Grund auf **neu entwickelten Architektur**
  - modern aus dem Jahr 2021, vollends unter dem Gedanken von Scale-Out, Sicherheit, Datenverschlüsselung, Datenschutz.
- nette Features wie: Wer hat sich schon lange nicht mehr beteiligt/gemeldet?
- [alle Features und Funktionen](https://opentalk.eu/de/produkt/features)


## der Rest

Die anderen Anbieter (z.B. Adobe Connect, Cisco Webex, Microsoft Teams, ...) wollen meist noch andere Dienste, Software verkaufen, so dass Linux oft benachteiligt ist.  
Lizenzierung, document sharing, Kollaborations-Tools?


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Empfehlungen Video<br />
letzte Änderung: 2025-02-13<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
