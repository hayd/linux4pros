# Editoren

## emacs
- „Emacs begann als Texteditor, der zu einer Lebensweise für viele Benutzer wurde, weil man die ganze Arbeit damit erledigen kann, ohne Emacs zu verlassen, ...“ Richard Stallman
- auch dank [Org-mode](https://de.wikipedia.org/wiki/Org-mode)
- Start 1976, aktuelle Version 28.2 vom 12. September 2022
- in Lisp
- Fork: Xemacs - letzte stabile Version: 30.1.2009

## vi, vim
- vi = visual
- überall verfügbar
- ausgereift, Start 1976
- einfache Typo-Korrektur bis Enwicklungsumgebung für fast alle Sprachen

3 Modi:
1. Befehlsmodus <- Esc
2. Einfügemodus   <- i,a,o
3. Kommandozeilenmodus <- :

### siehe auch
- [VIM bei ubuntuusers](https://wiki.ubuntuusers.de/VIM/)
- [vi Modi](https://wiki.ubuntuusers.de/VIM/Modi/)
- beste Kurzanleitung, bestes Cheat Sheet ??


## kwrite
- guter Kompromiss
- erweiterbar
- block selection mode
- Problem: spell checker: zu umständlich

## atom
- abgekündigt





## Aufgaben
- vi - Anleitungen: möglichst effektiv die immer gleiche Ersetzung in einem Text vornehmen, 2 Buchstaben vertauschen, 1 Macro definieren
