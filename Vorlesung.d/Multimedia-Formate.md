# Multimedia-Formate

## Komprimierbarkeit


## Bilder

- PNG

- JPG

- WebP




## Audio

- MP3


## Video

- MP4



## 2do

Ergebnisse aus PAD einarbeiten

  
---

<sub>
Autor: Helmut Hayd<br>  
Schlagwörter:  Admin  Software<br>
letzte Änderung: 2024-11-13<br>  
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
