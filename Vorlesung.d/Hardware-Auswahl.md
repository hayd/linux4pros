# Hardware 4 Linux

## Empfehlungen
- Zuerst auf Linux-Tauglichkeit prüfen.
- modular aufbauen

## Allgemeines
- keine allg., aktuelle HW-DB
- Einige Projekte führen HW-Listen


## Laptop
- Lenovo Thinkpad
- [Warum ich jetzt wieder ein Thinkpad T430 von 2012 nutze](https://www.golem.de/news/notebook-warum-ich-jetzt-wieder-ein-thinkpad-von-2012-nutze-2209-166195.html)
  - https://www.pcbilliger.de/
- [Business-Laptops: Das beste Notebook für Homeoffice und Büro finden](https://www.techstage.de/ratgeber/business-laptops-das-beste-notebook-fuer-homeoffice-und-buero-finden/jrzm8p0)
-



## Mainboard

## CPU


## Grafikkarte
- AMD mit integrierter GraKa
-  Accelerated Processing Unit (APU)
- APU = CPU +  GPU
- Grafikkarte kann mann immer noch nachrüsten

## Grafikkarte


## Netzwerk-Interface
### LAN
- Intel
- Realtek (Vorsicht)


### WLAN
- auch als USB-Device möglich


## WebCams
- USB
- Logitech
- auf Chip achten, nicht auf Hersteller
