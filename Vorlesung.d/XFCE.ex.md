# Übungen zu XFCE

## Die Aufgabe

- Personalisieren Sie den XFCE-Desktop nach folgenden Hinweisen.
- Alles nötige steht auf diesen beiden XFCE-Seiten:
  - [XFCE](./XFCE.md) - ein Desktop Environment
  - [XFCE konfigurieren](./XFCE-config.md)
- Tragen Sie Ergebnisse, Erkenntnisse, Verbesserungsvorschläge im [XFCE-Pad](https://pad.gwdg.de/izbozrcMRt6k6C-3mPHOWQ#) zusammen.



## Panel

- Panel konfigurieren
  - Panel: Icons ohne Texte
  - Panel: gleichartige Fenster zusammenfassen = "Window grouping"
  - nützliche Hilfs-Programme (Panel Plugins) auswählen und aufs Panel legen
  - Workspace Switcher mit 4 Desktops konfigurieren

  
## Favorisierte Programme

- favorisierte Programme auswählen, installieren
- zum schnelleren Start als Favorit in das Whiskers-Menu oder auf das Panel legen


## Misc

- Auswahl eines ansprechenden und ergonomischen [Themes](https://www.maketecheasier.com/xfce4-desktop-themes-linux/), [Liste](https://www.opendesktop.org/s/XFCE/browse/)
- [Xfdashboard](https://docs.xfce.org/apps/xfdashboard/start)  einrichten
- [4 Ways You Can Make Xfce Look Modern and Beautiful](https://itsfoss.com/customize-xfce/)


## flat file DB

- Sichern Sie die Konfiguration nach den ersten Erfolgen und spielen Sie den Backup bei Bedarf zurück oder nutzen Sie ihn für Vergleiche:
  
```bash
cd ~/.config/
cp -pr  xfce4/ xfce4.$(date "+%s")  # sichern
cp -pr  xfce4.17294*  xfce4/  # zurückspielen
```

- Welche Dateien ändern sich bei den Konfigurationen? Nutzen Sie [find](../Prgs.d/find.md) dafür.
- Was ändert sich? Nutzen Sie [diff](https://www.mankier.com/1/diff) dafür.


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Desktop Übung <br />
letzte Änderung: 2025-01-13<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
