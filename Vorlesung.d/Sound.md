# Sound

## pavucontrol

- = **P**ulse**A**udio **V**ol**u**me Control
- das wichtigste Werkzeug für die Soundkontrolle: Pegel von Eingabe-, Ausgabe-Geräten, Abschalten von Geräten (z.B. Mikrofon in WebCam), Regelung von Sound-Applikationen
- [bei UU](https://wiki.ubuntuusers.de/pavucontrol/)
- Configuration (Reiter): alles überflüssige "off" schalten
- Debugging: `pulseaudio --log-level=4 --log-target=stderr`

- ALSA: eine Ebene tiefer; [JACK](https://wiki.ubuntuusers.de/JACK/) für Echtzeit, meist überflüssig


## Audacity

![Audacity Logo](../Pics.d/Audacity_Logo.200.png)

- leicht zu bedienender grafischer Audio-Editor/-Recorder
- sehr populär
- [home](https://www.audacityteam.org/), [bei UU](https://wiki.ubuntuusers.de/Audacity/) , [wichtige Features](https://de.wikipedia.org/wiki/Audacity#Eigenschaften)

- Von Audacity gibt es auch eine Online-Version für die kleine Audiobearbeitung zwischendurch ohne Installation: [Wavacity](https://wavacity.com/).
  - [mehr](https://www.heise.de/news/Web-Tipps-Schneidig-9312347.html)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Empfehlungen Sound Software<br />
letzte Änderung: 2025-02-05<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
