# Passworte

## TL;DR

- für jede Authentifizierung (Log-in) ein anderes Passwort
- Passwort-Qualität: Länge schlägt Raffinesse
- Länge: min. 12 Zeichen
- [2FA](https://de.wikipedia.org/wiki/Zwei-Faktor-Authentisierung) (2-Faktor-Authentisierung: Besitz, Wissen, Biometrie, [TOTP](https://de.wikipedia.org/wiki/Time-based_one-time_password)) oder [Passkeys](https://www.bsi.bund.de/DE/Themen/Verbraucherinnen-und-Verbraucher/Informationen-und-Empfehlungen/Cyber-Sicherheitsempfehlungen/Accountschutz/Passkeys/passkeys-anmelden-ohne-passwort_node.html#doc1107470bodyText1) nutzen


## geleaked?

- [Data breach, Datenleck](https://en.wikipedia.org/wiki/Data_breach): Sicherheitsvorfall, bei dem Unberechtigte sensible Daten (Passworte, Passwort-Hashes, Kontonummern, Telefonnummern, Fotos, ...) erbeuten.
- [Liste von relevanten Datendiebstählen](https://en.wikipedia.org/wiki/List_of_data_breaches)
- Suche nach eigenen kompromittierten Daten:
  - [Identity Leak Checker](https://sec.hpi.de/ilc/) des Hasso-Plattner-Instituts
  - [Have I Been Pwned](https://haveibeenpwned.com/)
    - [über Have I Been Pwned](https://en.wikipedia.org/wiki/Have_I_Been_Pwned%3F)


## Misc

- Passworte dürfen beim Dienstleister nicht im **Klartext** (Wenn sich das herausstellt, hilft nur dann Flucht.), sondern nur als Hash gespeichert werden.
- Browser: **primäres Passwort** setzen !!! Das ist nicht der Default!
- **Passwort-Manager** nutzen, z.B. [KeePassXC](../Prgs.d/KeepassXC.md)  
- [NIST](https://de.wikipedia.org/wiki/National_Institute_of_Standards_and_Technology) rät von häufigen **PW-Wechseln** ab => unsichere, leicht zu merkende PW, Klebezettel o.ä.
- [BSI-Basisschutz: Sichere Passwörter](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Checklisten/sichere_passwoerter_faktenblatt.pdf?__blob=publicationFile&v=4)
- **Brute-Force-Attacken**:
  - zuerst eine lange Liste gängiger Passworte
  - dann Begriffe aus Wörterbüchern
  - abschließend probieren Algorithmen systematisch beliebige Zeichenkombinationen
  - [59 Prozent aller Passwörter in unter 60 Minuten knackbar](https://www.golem.de/news/forscher-machen-den-test-59-prozent-aller-passwoerter-in-unter-60-minuten-knackbar-2406-186329.html) mit einer Nvidia Geforce RTX 4090 (ca. [4k€](https://geizhals.de/nvidia-geforce-rtx-4090-founders-edition-a2815453.html?hloc=at&hloc=de))
- das [eigentliche Problem](https://www.heise.de/newsticker/meldung/Kommentar-Steckt-Euch-Euren-Aendere-dein-Passwort-Tag-sonstwohin-4291584.html): Online-Dienste sichern die Passworte schlecht
- [Qualität des Passwort prüfen](https://www.passwortcheck.ch/) - mit einem ähnlichen, nicht dem geplantem PW  testen
- [TOTP](https://de.wikipedia.org/wiki/Time-based_One-time_Password_Algorithmus) (Time-based One-time Password) mit [Google Authenticator](https://de.wikipedia.org/wiki/Google_Authenticator) verwenden
- [Top Ten der deutschen Passwörter 2021](https://www.heise.de/news/123456-Deutschlands-haeufigste-Passwoerter-im-Jahr-2021-6297181.html):
  
```text
123456 passwort 12345 hallo 123456789 qwertz schatz  basteln berlin 12345678
```

- [Top Ten der deutschen Passwörter 2023](https://hpi.de/artikel/123456789-das-beliebteste-passwort-2023/):

```text
123456789 12345678 hallo 1234567890 1234567 password password1  target123 iloveyou gwerty123
```


- Haben die Leute etwas dazugelernt? Länger, mit Zahl, sonst **nix**!


## Passwort-Generatoren

### pwgen

erzeugt zufällige, aussprechbare (=leichter erinnerbare) Passwörter

- [home](https://sourceforge.net/projects/pwgen/) , [bei UU](https://sourceforge.net/projects/pwgen/)
- im Repo
- erzeugt 3 Passworte mit je 12 Zeichen:
  `$ pwgen 12 3`
- Optionen:
  - `-B` nur unverwechselbare Zeichen, kein 0O, 1l
  - weitere Optionen um Zeichenvorrat zu wählen: `-c -n -y`


### weitere Programme, Methoden

Bauanleitungen, wie man mit Bordmitteln (sha256, urand, tr, ...) sichere Passworte erzeugen oder aus File-Namen ableiten kann.

- [8 Ways to Generate a Random Password on Linux Shell](https://vitux.com/generation-of-a-random-password-on-linux-shell/)
- [10 Ways to Generate a Random Password from the Linux Command Line](https://www.howtogeek.com/howto/30184/10-ways-to-generate-a-random-password-from-the-command-line/)
- [Different Ways To Generate A Strong Password In Linux](https://ostechnix.com/4-easy-ways-to-generate-a-strong-password-in-linux/)
- [Zufällige Passwörter über das Terminal erstellen](https://phlow.de/magazin/terminal/passwoerter-erstellen/)
- eine Idee von [Wietse Viennema](https://de.wikipedia.org/wiki/Wietse_Zweitze_Venema):  
 `$ (ps axl & ps -el & netstat -na  & netstat -s & ls -lRt /dev & w)  | md5sum`
  - Aufgabe: Viennemas Idee erklären. Ist das PW reproduzierbar?


## siehe auch

- [KeePassXC](../Prgs.d/KeepassXC.md)  
- [Das sind die fünf größten Passwort-Mythen](https://www.sueddeutsche.de/digital/it-sicherheit-passwort-kennwort-tipps-passwoerter-1.3587661)
- [Tag der unsinnigen Passwort-Ratschläge](https://www.golem.de/news/imho-tag-der-unsinnigen-passwort-ratschlaege-1802-132533.html)
- [Passwortsicherheit – Alles, was Sie wissen müssen | heise online](https://www.heise.de/ratgeber/Passwortsicherheit-Alles-was-Sie-wissen-muessen-7074549.html)
- [BSI - Sichere Passwörter erstellen](https://www.bsi.bund.de/DE/Themen/Verbraucherinnen-und-Verbraucher/Informationen-und-Empfehlungen/Cyber-Sicherheitsempfehlungen/Accountschutz/Sichere-Passwoerter-erstellen/sichere-passwoerter-erstellen.html?nn=131366)


## Aufgabe

### Passphrase-Generator für Verschlüsselung

- Erzeugen Sie ein gutes Passwort aus einem Filenamen, so dass man bei Kenntnis des Algorithmus und des Filenamens jederzeit das PW berechnen kann.
- Die Passwortlänge soll einstellbar sein. Ebenso der Zeichenvorrat, z.B. mit/ohne Sonderzeichen, Zahlen
- Hilfsmittel: [tr](../Prgs.d/tr.md), [sed](../Prgs.d/sed.md) , [awk, cut](../Prgs.d/awk,cut.md), [sha512sum, md5sum](../Prgs.d/Pruefsummen.md)
- Skripte in [dieses Pad](https://pad.gwdg.de/PT3ISzuWQUKXftN-L77iBA?both)

Die Idee dahinter: Obiger Algorithmus + [Datei-Verschlüsselung](./Datei-Verschluesselung.md) + tar/rclone erlauben die Ablage vertraulicher Daten in jeder beliebigen Cloud. Und man muss sich nicht jedes Mal ein Passwort notieren.

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Security<br />
letzte Änderung: 2025-02-15<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>

<!--- 2do   Passkeys-->



