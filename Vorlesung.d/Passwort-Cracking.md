
# Password Cracking

Das folgende soll kein Einstieg in eine kriminelle Karriere werden, sondern zeigen, wie einfach es sein kann und wie bedroht Passworte sind.

## TL;DR

Gute (=lange) Passworte sind nicht zu knacken, schlechte sehr einfach.

## Vorgehen

- Probe-Passworte hashen bis Hashkollision
- Quellen für Probe-Passworte: bewährte/personalisierte Sammlungen, Wörterbücher, an Hand beeinflussbarer Regeln erzeugte Zeichen-Kombinationen
  - personalisierte Sammlungen: persönliches wie Name der Kinder, der Katze, ... aus social media
- [Rainbow Tables](https://de.wikipedia.org/wiki/Rainbow_Table) beschleunigen Brute-Force-Angriffe erheblich.

## Die wichtigsten Programme

### John the Ripper

- John the Ripper, kurz John oder JtR
- Software zum Testen von Authentifizierungseinrichtungen und Passwörtern
- [home](https://www.openwall.com/john/)


### Hashcat

- Hashcat is a password recovery tool.
- [hashcat](https://hashcat.net/hashcat/)
- nutzt GPU effektiv
- [home](https://hashcat.net/hashcat/)
- [Hashcat vs John the Ripper: A comparative benchmarking](https://dev.to/bhavikgoplani/hashcat-vs-john-the-ripper-a-comparative-benchmarking-of-password-cracking-tools-26a4)


### und noch mehr

- im offiziellen Repository
- **medusa** - fast, parallel, modular, login brute-forcer for network services
- **ophcrack** - Microsoft Windows password cracker using rainbow tables (gui)
- **ophcrack-cli** - Microsoft Windows password cracker using rainbow tables (cmdline)



## Hackerparagraf

- Man kann sich nach dem [Hackerparagraf](https://de.wikipedia.org/wiki/Vorbereiten_des_Aussp%C3%A4hens_und_Abfangens_von_Daten) strafbar machen.
- User und Vorgesetzte fragen, bevor man die Passworte zu knacken versucht
- Betroffene nur vom Erfolg des Crackings informieren, die Passworte nicht ansehen und nicht missbrauchen
- password cracking: Besser es machen die Guten als die Bösen. Die Versuche gibt es auf jeden Fall.


## ein erster Eindruck

- John installieren:  
  `$ sudo apt install john john-data`
- einfaches Passwort (hier 123) erzeugen mit passendem Hash-Verfahren: -1: MD5 , -5 SHA256 ,  -6 SHA512  
`$ openssl passwd -1 123  > pws.txt`
- Cracken:  
`$  time john --format=md5crypt  pws.txt`
- Passwort komplizierter machen: verlängern, Sonderzeichen einsetzen, Reichenfolge verändern, ...
  - Wie ändert sich der Zeitaufwand?
- in ~/.john steht alles Wissenswerte über den Versuch:
  - **john.pot**: Sammlung bisher geknackter Passworte
  - **john.log**: welche Regeln wurden wie lange angewandt
  


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Security<br />
letzte Änderung: 2025-02-12<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>

  
  
  
  
