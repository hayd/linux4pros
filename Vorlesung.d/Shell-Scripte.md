# Shell-Scripte

## Allgemeines
- bash oder sh oder zsh oder ...
  - Die Z-Shell kann als Quintessenz aller Verbesserungen und Features der bash, der csh und der tcsh betrachtet werden. 
- Ein Skript ist auch eine gute Doku.
- einfaches Debugging
- Davon lebt die Anpassung des Linux-Arbeitsplatzes.
- bash oder Python-Script ?
  - Bash: einfache Abfolger von Linux-Befehlen mit schlichten Datenstrukturen (max. kleines, lineares Array) - andernfalls: Python
- [beliebteste Programmiersprachen im 3. Quartal 2022](https://www.heise.de/news/Programmiersprachen-Ranking-JavaScript-gewinnt-das-Rennen-7317740.html): 14. Platz: Shell









## Aufgaben
### Abräumer
- In den Filenamen ist ein Datum "einkodiert" (z.B. Ankuendigung.10d.txt).
- Das Script sucht diese Files und löscht sie, wenn Fälligkeitsdatum erreicht ist.
- cron startet dieses Script.
- Hilfen: stat,  dateutils (dateutils.ddiff), `date -d "$date +2 week" +%j`


### Mini-Pics
- Bilder auf eine vorgegebene Größe eindampfen

### poor mans queueing system
- kill sleep, kill continue



### Handling von .md Files
- last change anhängen
- Metadaten als Kommentar
- Metadaten ändern
- Liste aller geänderten .md's
- ci nach Änderung
- alle zusammensuchen, in einen verschlüsselten .tar.bz2 packen, nach extern kopieren (Backup)
-


## Change Management
- ci
- Suche nach allen ,v und Sicherung


## Library
- Sammlung wichtiger functions
- Variablen-Namen: Kollisionsbereinigung
