# SSH-Login mit Schlüsselpaar

- [Telnet](https://de.wikipedia.org/wiki/Telnet) ist eine sicherheitstechnische Katastrophe, unbedingt durch SSH ersetzen

## Vorteile

- Passwort wird durch Schlüssel-Paar ersetzt.
- Angreifer muss wissen, wo der andere Schlüssel liegt.
- Bei Schlüssel-Paar kann man eine bestimmte Qualität erzwingen.

## Prinzip

[Challenge-Response-Authentifizierung](https://de.wikipedia.org/wiki/Challenge-Response-Authentifizierung)

1. (Client: private key)  -- will mich anmelden --> (Server: public key)
2. (Client, priv.) <-- verschlüsselte Nachricht -- (Server, pub.)
3. (Client, priv.) -- entschlüsselte Nachricht --> (Server, pub.)
4. Server verifiziert durch Vergleich die Nachricht, bei Erfolg ist der Client authentifiziert


## How-To

### Schlüssel-Paar erzeugen

- `$ ssh-keygen -t ed25519  -f ~/.ssh/XY.ed25519  -C "Schluessel für Server-XY"`
  - `-keygen` erzeugt 2 Dateien in ~/.ssh/:
    - id\_ed25519 mit private key  
    - id\_ed25519.pub  
  - `-t ed25519` Schlüsselpaar mit der elliptische Kurve [Curve25519](https://de.wikipedia.org/wiki/Curve25519) erstellt
  - `-f` Files für Schlüsselpaar
  - `-C` comment


### Schlüssel-Transfer

- Der private key muss **privat** bleiben:
  - `$ chmod 400 private_key_file`
  - Er darf nicht auf dem Server liegen und möglichst auch nicht vom Server kommen (Bleibt eine Kopie auf dem Server zurück?).
- Der **public key** muss jetzt auf den Server übertragen werden.
- Einloggen noch per Passwort und Transfer per ssh-Befehl:  
`$ ssh-copy-id   -i ~/.ssh/XY.ed25519.pub  ich@xyz.com`
- oder: **Cloud-Init-Verfahren**: Bei einigen Clouds kann man  seinen öffentlichen Schlüssel einfach in der Weboberfläche der Cloud eintragen.
- [SSH keys to communicate with GitLab](https://gitlab.gwdg.de/help/user/ssh.md)


### Testen

- `$ ssh -i ~/.ssh/XY.ed25519 ich@xyz.com`
  - Es wird nach Passphrase gefragt werden, die bei der Generierung des Schlüsselpaars festgelegt wurde, und nicht mehr nach dem Passwort auf dem Server.

### Host-Liste anlegen

- In ~/.ssh/config kann an eintragen, welche UID und welche Keys zu welchem Host gehören. Sinnvoll, sobald man dieses Verfahren für mehr als einen Server nutzt. Z.B:
  
```text
Host test
   HostName test.xyz.com
   User ich
   IdentityFile ~/.ssh/test.ed25519

```

- Details: `$ man ssh_config`


## Aufgaben

- Schlüsselpaar erzeugen (s.o.)
- Inhalte und Rechte der mit ssh-keygen erzeugten Dateien ansehen


## siehe auch

- [Ubuntu: SSH - Wie und Warum?](https://www.heise.de/tipps-tricks/Ubuntu-SSH-Wie-und-Warum-4203356.html)
- [Basisinstallation auf dem Server](https://mkurtz.de/openssh-auf-ubuntu-20-04-lts-einrichten/)
- [Verwenden von SSH zum Herstellen einer Verbindung mit einem Remoteserver](https://www.digitalocean.com/community/tutorials/how-to-use-ssh-to-connect-to-a-remote-server-de)
- [OpenSSH Public Key Authentifizierung unter Ubuntu](https://www.thomas-krenn.com/de/wiki/OpenSSH_Public_Key_Authentifizierung_unter_Ubuntu)
- [a user interface that helps organizing remote terminal sessions](https://www.asbru-cm.net/)
- [SSH (Secure Shell) Key Management](https://www.manageengine.com/key-manager/information-center/what-is-ssh-key-management.html)



---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Security<br />
letzte Änderung: 2024-12-03<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
