# Desktop-Environment (DE)

## TL;DR

- Programme sind viel wichtiger als und (fast) unabhängig von DE
- DE ist auch nur ein Grafik-Programm.


## Allgemeines

- [freedesktop.org](https://de.wikipedia.org/wiki/Freedesktop.org): (vormals X Desktop Group, XDG) Projekt um die **Interoperabilität**  von Desktop-Umgebungen für das X-Window-System durch desktop-übergreifende Standards zu verbessern.
- Problem: **Kommunikation** der Komponenten untereinander, z.B. via [D-Bus](https://de.wikipedia.org/wiki/D-Bus)
  - `$ pgrep -lf dbus`
  - Beispiel: File-Manager wird über Einstecken eines USB-Sticks informiert.
- Ein DE besteht aus einer **grafischen Oberfläche** (was man sieht, wenn keine Fenster geöffnet sind) und typischen **Anwendungen** mit passendem Look & Feel (z.B. Panel, File-Manager, Start-Menü).

- Ein DE ist auch nur eine **Ansammlung von Grafik-Programmen**.
  - Eine DE besteht auch nur aus **ganz normalen** Paketen zu einem Meta-Paket zusammengefasst, z.B:  ubuntu-desktop, kubuntu-desktop, xubuntu-desktop, xfce4
  - Entscheidung **leicht zu ändern** und daher ohne große Konsequenzen
  - Man kann mehrere Desktop-Umgebungen **parallel** nutzen ohne die Stabilität zu gefährden.
  - **Ein User pro DE** anlegen, damit sich benutzerbezogene  Konfigurationen nicht in die Quere kommen.


## Übersichten

- [Vergleichstabelle](https://www.reddit.com/r/kde/comments/k9ro88/comparison_of_15_desktop_environments_for_linux/) für 15 DEs
- [The 8 Best Desktop Environments](https://itsfoss.com/best-linux-desktop-environments/) mit Pros und Cons
- [Linux-Desktops 2020](https://www.pcwelt.de/article/1140962/der-optimale-linux-desktop.html) mit Steckbriefen
- [Best Linux desktops of 2023](https://www.techradar.com/best/best-linux-desktop), sortiert nach  Einsatzgebiet


## Die wichtigsten DEs

### GNOME Shell

- aktuelle, 3. Generation des GNOME-Desktops
- **Konzept**:  schick, einfach, überzeugend, elegant
- **viel vorkonfiguriert**: Entwickler wollen User nicht überfordern vs. bevormunden User. Abhilfe: [GNOME Tweaks](https://www.linuxfordevices.com/tutorials/linux/gnome-tweaks)
- **optimiertes Look and Feel**: [GNOME Human Interface Guidelines](https://developer.gnome.org/hig/) mit [Guidelines](https://developer.gnome.org/hig/guidelines.html)
- bei vielen Distributionen Default


### MATE

- [MATE](https://de.wikipedia.org/wiki/MATE_Desktop_Environment) entstand 2011 als Abspaltung von Gnome 2 mit Retro-Anspruch
- sehr anpassungsfähig ohne Spezialwerkzeuge
- leichtgewichtig und schnell


### KDE

- [KDE](https://kde.org/de/) 1996 starteten 1996 mit Qt (Oberflächenbibliothek in C++) von Trolltech
- graphisch am anspruchsvollsten
- konfigurieren bis der Arzt kommt
- \>200 [KDE-Anwendungsprogramme](https://apps.kde.org/de/), deren Name normalerweise mit K beginnen


### Budgie

- [home](https://ubuntubudgie.org/), [bei UU](https://wiki.ubuntuusers.de/Ubuntu_Budgie/), [Code](https://github.com/BuddiesOfBudgie/budgie-desktop), [Review](https://www.maketecheasier.com/solus-os-review/)
- Hauptaugenmerk der erfahrenen Entwickler liegt auf Einfachheit und Eleganz
- ab 2013 komplett neu geschrieben, kein Fork von ...
- Hardware-Anforderungen zwischen GNOME/KDE und XFCE
- geschrieben in C und [Vala](https://de.wikipedia.org/wiki/Vala_(Programmiersprache)) (ähnlich C#)



### XFCE

- [XFCE](./XFCE.md)


### COSMIC

- neues DE from scratch und von einem American computer manufacturer: [COSMIC](https://system76.com/cosmic/), in Rust, im August 2024 ein alpha release. *"There are no unexpected surprises."*


### Semantic desktop

- Digitale Informationen zu einem Projekt werden in einem Graphen abgespeichert. Jeder Datentyp wird mit einem zugehörigen Programm verknüpft. Ähnelt [Mind-Map](https://thesius.de/blog/articles/mind-map-erstellen/).
- [DeepaMehta](https://www.deepamehta.de/) stellt Projekte als Netzwerk dar.
- [KDE's semantic desktop: Nepomuk vs. Baloo](https://www.xmodulo.com/kde-semantic-desktop-nepomuk-baloo.html)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Software<br />
letzte Änderung: 2025-01-12<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
