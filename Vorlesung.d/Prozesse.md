# Prozesse

- Prozess: Programm, das auf dem System (im Hintergrund) aktiv ist
- Zahl der gerade laufenden Prozesse:  
 `$ ps -ax | wc -l`


## Scheduler

- verteilt **Ressourcen** (z.B. Zeitscheiben der CPUs) an Prozesse
- **Aufgaben**: Bedienung unterschiedlichster Hardware (z.B. CPU mit 1 oder 64 cores); Bevorzugung bestimmter Prozesse, Prozess-Gruppen (z.B. für I/O, für  interaktives Arbeiten); Einhaltung der Systemregeln, Fairness gegenüber allen Usern
- **Optimierung**: Gesamt-Durchsatz vs. reaktivem Desktop, Kontext-Wechsel minimieren.
- **Kontext-Wechsel** sind teuer wegen Speichern und Laden von Register-Inhalten und internen Tabellen der Prozesse.
- Kernel in **unterschiedlichsten Umgebungen**: Server für Web-Services, Ampelsteuerung, Telefonanlagen, DNS-Anfragen; Spiele-PC, Office-Laptop benötigen unterschiedliche Scheduler
- Es werden **verschiedene Scheduler** eingesetzt. Z.B:
  - [Completely Fair Scheduler](https://de.wikipedia.org/wiki/Completely_Fair_Scheduler), CFS
  - [Fair-Share-Scheduling](https://de.wikipedia.org/wiki/Fair-Share-Scheduling)
  - [Brain Fuck Scheduler](https://de.wikipedia.org/wiki/Brain_Fuck_Scheduler) - von einem australischen Anästhesist entwickelt. [Why "Brain Fuck"?](http://ck.kolivas.org/patches/bfs/bfs-faq.txt)
- [Kernel 6.6](https://www.heise.de/news/Linux-6-6-bringt-neuen-Scheduler-9350044.html) tauscht nach 15 Jahren den Scheduler CFS aus
  - CFS meist gut, ignoriert aber:
    - CPU-Wechsel teuer, da Cache-Inhalte neu angelegt werden müssen
    - [hybride Systeme](https://www.heise.de/news/Ryzen-AMD-stellt-seine-ersten-Hybrid-Prozessoren-vor-9352226.html) (stromsparende Cores, high performance cores)
    - Power-Management, bei Laptops, IoT devices wichtig
    - Latenz (nice Wert garantiert keine schnelle Zuteilung einer Zeitscheibe, sondern viele)
  - daher neuer Scheduler: "Earliest Eligible Virtual Deadline First" (**EEVDF**) Scheduler
    - #Zeitscheiben in der Vergangenheit beeinflusst #Zeitscheiben in der Zukunft
    - auch latency bekommt nice-Wert
- Anzeige aller Schedular, den aktiven in [ ]:  
`$ cat /sys/block/sda/queue/scheduler`
- [How and When to Change I/O Scheduler in Linux](https://linuxhint.com/change-i-o-scheduler-linux/)


### Echtzeit

- Echtzeit: Reaktion auf Signale oder Daten bis zu einem definierten Zeitpunkt, z.B. für Robotersteuerung, Ampeln, ... Nicht schnell, sondern vorhersagbar.
- [Kernel 6.12](https://www.heise.de/news/Echtzeitfaehiges-Linux-6-12-erschienen-10059625.html) wird echtzeitfähig durch den Realtime-Scheduler PREEMPT_RT.
- Entwicklung begann 2005.


## Prozesssteuerung auf CLI

- Prozesse kann man über [Signale](https://www.linux-community.de/ausgaben/linuxuser/2014/11/klar-signalisiert/) steuern, die man ihnen schickt.
- Alle Eingaben im gleichen Terminalfenster machen.
- [xeyes](../Prgs.d/xeyes.md) kann man mit `Alt+lM` verschieben
- Programm starten und beenden:  
`$ xeyes `  
`$ Ctrl-c`  sendet Signal SIGINT (Interrupt) an Prozess
- Programm starten und in Hintergrund schicken:  
`$ xeyes `
  - in Kommandozeile, wo gestartet eingeben:  
       `$ Ctrl-z`  sendet Signal SIGTSTP (Terminal Stop) an Prozess  
      `$ bg` belebt Prozess im **b**ack**g**round wieder
- Programm im Hintergrund starten:  
  `$ xeyes &`


## Prozesspriorität

- Auch auf einem Linux-System sind nicht alle Prozesse gleich.
- **Systemkritische Prozesse** müssen trotz aller Fairness beim Verteilen der Taktzyklen bevorzugt werden. Das lässt sich Unix-typisch über den nice-Wert steuern, der Prozesse höher und niedriger priorisiert.
- Je kleiner die **Priorität**, desto mehr Ressourcen erhält Prozess vom Scheduler. Meist vergibt der Scheduler die Priorität deutlich besser als der User. Ausnahme: [anspruchsvoller Job](https://boinc.berkeley.edu/), der aber nur idle time nutzen soll.
- Priorität liegen im Bereich von -20 (höchste Priorität) bis +19 (niedrigste Priorität).
- Priorität mitgeben:  
`$ nice -n 100 glxgears &  glxgears &`
- und kontrollieren:  
`$ ps -o pid,user,pri,args`
- Priorität nachjustieren. PID mit ps ermitteln:  
`$ renice 100 -p 58650`



## Programme

### ps

- berichtet über alle gerade laufenden Prozesse
- unterstützt 3 Arten von Optionen: UNIX, BSG, GNU. Das ist eher ein Problem als ein tolles Feature.
- Liste aller Prozesse Rechner:
   `$ ps -ef`
- Ein Prozess kommt selten allein, Darstellung als Baum:
  `$ pstree`  (in psmisc.deb) , `$ htop , F5`
- Optionen:
  - -e: every
  - -f: full
  - -F: overfull


### htop

- interactive processes viewer
- aufgebohrtes [top](https://wiki.ubuntuusers.de/top/)  mit ncurses GUI
- `Tree` (F5) ersetzt pstree
- mit F3 Prozess suchen und mit F9 killen
- Spalte NI zeigt nice-Wert
- [bei UU](https://wiki.ubuntuusers.de/htop/)


## Prozesse killen

### Killen mit der Maus

- Ctrl-Alt-Esc => (Totenkopf)-Cursor
- zu killendes Fenster mit Totenkopf anklicken


### kill, killall

- killt nicht wirklich, sondern schickt Signal an Prozess
- [Signale](https://de.wikipedia.org/wiki/Signal_(Unix)):
  - -9: kill, unblockable (Signal kann vom Program nicht abgefangen werden)
- killen:
  - mit `$ ps -ef | grep xyz` PID (process ID, 2. Spalte) bestimmen,
  - mit kill ein Signal an diese PID schicken: `$ kill 12794` 
  - oder in hartnäckigen Fällen `$ kill -9 12794` (nächste Steigerung: reboot)
- [killall](https://wiki.ubuntuusers.de/killall/) - kill processes by name, not by substring


## siehe auch

- [The Linux Scheduler: a Decade of Wasted Cores](https://people.ece.ubc.ca/sasha/papers/eurosys16-final29.pdf)
- [Linux process priorities demystified](https://blog.sigma-star.at/post/2022/02/linux-proc-prios/)
- `$ man ionice` (set or get process I/O scheduling class and priority)
- `$ man 7 signal` (overview of signals)


### Aufgaben

- Welche Prozesse haben die besten nice-Werte?
  - htop nutzen
- Geben Sie diese Befehle ein: `$ for i in  1 $(seq 1 30) ; do  P=$(($RANDOM%500)); xeyes -geometry $Px$P+$P+$P  &   done`
  - Schließen Sie alle Augen wieder.
- Ermitteln Sie mit den htop, wie viel Speicher Firefox mit allen Tochter-Prozessen belegt
- Starten Sie in 2 Terminals:

```bash
  glmark2 &
  nice -n 100 glmark2 &
```

und vergleichen Sie deren Performance und kontrollieren Sie deren CPU-Gebrauch mit htop.

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  <br />
letzte Änderung: 2025-01-13<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>


<!--- 2do:  EEVDF Schedular besser erklären  -->
