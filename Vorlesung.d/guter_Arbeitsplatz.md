# Der ideale Arbeitsplatz

![Alt text](../Pics.d/PC-Arbeitsplatz.jpg)

## TL;DR

Abwechslung für den Körper


## Arbeitsplatz

- Bildschirminhalte konfigurieren: [RandR](https://wiki.ubuntuusers.de/RandR/)
- [Augenwellness](https://www.spiegel.de/gesundheit/diagnose/bildschirmstress-entspannung-fuer-die-augen-a-1032693.html): Blick aus Fenster
- Mikro, WebCam
- Maus links ![Home Office](../Pics.d/HomeOffice.jpg)
- höhenverstellbarer Tisch ![höhenverstellbarer Tisch](../Pics.d/hv-tisch.jpg)


## Wenn es doch schmerzt ...

- [Schluss mit Mausarm](https://www.techstage.de/ratgeber/ergonomische-maus-gegen-den-mausarm-ab-15-euro/x6s169s) - vertikale Mäuse, Trackballs, [Contour RollerMouse](https://www.amazon.de/Contour-Design-RM-RED-WL-RollerMouse-Wireless/dp/B0797QT87Y?th=1)  und mehr
  - Was wirklich hilft: Maus mit der anderen Hand bedienen
- [3 Übungen bei Nackenschmerzen](https://www.youtube.com/watch?v=fPABt1qBYwA)
- Übungen für Büro & Homeoffice als Poster
  - [Nacken und Kopf](https://3953618.fs1.hubspotusercontent-na1.net/hubfs/3953618/L%26B_Uebungsposter_Nacken.pdf)
  - [Unterer Rücken und Gesäß](https://3953618.fs1.hubspotusercontent-na1.net/hubfs/3953618/L%26B_Uebungsposter_Ruecken_Gesaess.pdf)

## Siehe auch

Die Bilder auf diesen Seiten können vielleicht noch ein paar Anregungen liefern. Nicht alles ist sinnvoll.

- ![alt text](../Pics.d/Laufband-PC_von_Linus_Torvalds.jpg)
- [Explore the workspaces of creative individuals](https://www.workspaces.xyz/)
- [r/Workspaces](https://www.reddit.com/r/Workspaces/?rdt=51478)



---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Empfehlungen Hardware<br />
letzte Änderung: 2025-02-20<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>


<!--- 

[Ampera Power  Bürofahrrad ](https://www.lifespaneurope.com/de-de/collections/ampere)

[Drei Laufbänder für den Stehschreibtisch im Test | heise online](https://www.heise.de/tests/Drei-Laufbaender-fuer-den-Stehschreibtisch-im-Test-10029176.html)

-->



