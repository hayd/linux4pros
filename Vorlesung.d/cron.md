# cron

Der Cron-Daemon startet automatisch Skripte zu definierten Zeiten um regelmäßig wiederkehrende Aufgabe zu erledigen: Backup,  DBs aktualisieren, Updates suchen, ...


## Voraussetzungen

- `$ sudo apt install cron anacron`
- Was bereits gegeben ist: `$ crontab -l`
- In  /etc/cron.allow, /etc/cron.deny steht, wer mitspielen darf und wer nicht. Wenn beide nicht vorhanden: alle
- Lieblingseditor festlegen: `$ export EDITOR=vi`

## die einfache Variante

Man lege das auszuführende Script in einem dieser /etc-Ordner ab, dessen  Name die Ausführungs-Frequenz enthält:

- cron.daily
- cron.hourly
- cron.monthly
- cron.weekly


## crontab

- Crontab = Tabelle
- Format: Zeit(en), Befehl
- Zeitangaben erlauben Wildcards (*) und Bereiche (9-12)
- [Erklärung des Formats](https://de.wikipedia.org/wiki/Cron#Crontab)
- [Online-Hilfe](https://crontab.guru/) von crontab guru

## Cronjobs  einrichten

- `$ crontab -e`
  - Beim Abspeichern wird Syntax kontrolliert.
  - Kontrolle mit: `$ crontab -l`
- Definition eingeben
- Am Ende der Tabelle muss ein Kommentar oder eine Leerzeile stehen.
- In /etc/crontab kann man zusätzlich eintragen, mit wessen Rechten, das Script ausgeführt werden soll.


## anacron

- holt cron-Jobs nach, die während der Rechner-Abschaltung verpasst wurden
- Script-Name darf dort keinen . enthalten


## Debugging

### Häufige Fehler

- $PATH unpassend: Es wird nicht der PATH der interaktiven Shell genutzt. Abhilfe: PATH im Script explizit definieren oder Script mit vollem Pfadnamen aufrufen.
 .sh im Namen. Abhilfe: siehe anacron
- Bildschirmausgabe muss man umlenken, [Abhilfe](https://wiki.ubuntuusers.de/Cron/#Fenster-einer-Anwendung-oeffnet-sich-nicht)


### Fehlermeldungen

- Hinter jeden Befehl  `2>> /tmp/cron.err` setzen, um dort Fehlermeldungen nachzulesen
- Man lasse den Job jede Minute laufen.
- Man starte eine neue Shell, definiere sich den Pfad-Inhalt weg und starte dann das Script: `export PATH=""`
- Suche in den [Log-Files](https://wiki.ubuntuusers.de/Logdateien/):  

```sh
$ grep CRON /var/log/syslog
$ grep crontab /var/log/syslog
```

## siehe auch

- [CronHowto - Community Help Wiki](https://help.ubuntu.com/community/CronHowto)
- [bei UU](https://wiki.ubuntuusers.de/Cron)
- [How to Use Cron to Automate Linux Jobs on Ubuntu 20.04](https://www.cherryservers.com/blog/how-to-use-cron-to-automate-linux-jobs-on-ubuntu-20-04)
- [Incron](https://wiki.ubuntuusers.de/Incron/) - überwacht  Änderungen am Dateisystem


## Aufgaben

- Starten Sie jede Minute in der aktuellen Viertelstunde `xeyes`  und kontrollieren Sie die Fehlermeldungen.
  - Hilfe: Man muss den Augen sagen, wo sie erscheinen sollen:  
  `export DISPLAY=:0.0 && /usr/bin/xeyes &`

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  <br />
letzte Änderung: 2023-12-03<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
