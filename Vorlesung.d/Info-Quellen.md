# Informationsquellen

## TL;DR

- alles im Netz - gewusst wo
- Bücher eher überflüssig

## Allgemeines

- Oft ist Suchen einfacher.
- KI lieferte die wahrscheinlichste, etwas veraltete Information - nicht die beste
- Auf das Alter der Doku achten - sonst wertlos.


## kurz, konkret

- `$ befehl -h` oder `$ befehl --help`
- `$ man befehl`


## man pages

- sections, normalerweise 1. für User am wichtigsten:
    1. **Executable programs or shell commands** (`man ls`)
    2. System calls (functions provided by the kernel)
    3. Library calls (functions within program libraries)
    4. Special files (usually found in /dev)
    5. File formats and conventions, e.g. /etc/passwd (`man fstab`)
    6. Games
    7. Miscellaneous (including macro packages and conventions), e.g. man(7), groff(7)
    8. **System administration commands (usually only for root)** (`man mount`)

- Suche nach Thema (keyword):  
    `$ man -k editor`

- [ManKier](https://www.mankier.com/about) - *tries to make reading and man pages as convenient as possible.*
  - Gliederung am Rand, TL;DR
  - [siehe](https://www.mankier.com/): `less` und `tar`  


## Webseiten

die wichtigsten:

- [ubuntuusers Wiki](https://wiki.ubuntuusers.de/Wiki/Neue_Artikel/) - guter Einstieg, oft hilfreich
- [ubuntuusers Howtos](https://wiki.ubuntuusers.de/Wiki/Neue_Howtos/) - mitmachen und als Praxisarbeit anerkennen lassen an der DHSN
- [Wikipedia](https://en.wikipedia.org/wiki/Xubuntu) - Angaben zur Lizenz, Aktualität uvm.

auch nicht schlecht:

- [Read the Docs](https://readthedocs.org/)
  - free software documentation platform
  - “docs like code” workflows, keeping your code & documentation as close as possible
  - [Quellen auf GitHub](https://github.com/readthedocs/readthedocs.org)
  - 2021: 700 Million page views
- [LinuxWiki.org](https://linuxwiki.de/)- hier wird Wissen über Linux gesammelt
- [ArchWiki](https://wiki.archlinux.org/) =  Arch Linux documentation
- [Linux Bibel Oesterreich](https://linux-bibel.at/) - interessante Hinweise auf FOSS
- home page des Projektes, z.B.
  - [Documentation of the GNU Project](https://www.gnu.org/doc/doc.html)
- Suche nach: `Linux/Ubuntu Thema`


## User Support

- IRC, Mailing Lists, ...


## (Online-) Bücher

- [Debian GNU/Linux Anwenderhandbuch](https://debiananwenderhandbuch.de/) von Frank Ronneburg
- [Das Debian Administrationshandbuch](https://debian-handbook.info/browse/de-DE/stable/) - von der Erstinstallation bis zur Dienste-Konfiguration
- [Debian-Referenz](https://www.debian.org/doc/manuals/debian-reference/index.de.html) -  behandelt viele Aspekte der Systemadministration mittels Shell-Befehlsbeispielen für einfache Benutzer


## Zeitschriften

- [LinuxUser](https://www.linux-community.de/magazine/linuxuser/) - für User
- [Linux-Magazin](https://www.linux-magazin.de/) für Admins, Profis


## News

- [Heise: Linux und Open Source](https://www.heise.de/thema/Linux-und-Open-Source)
- [Open-Source-Blog-Netzwerk](https://osbn.de/ticker/?p=1&view=list)
- [golem: Open Source](https://www.golem.de/specials/open-source/)


## Veranstaltungen

- [Chemnitzer Linux-Tage](https://de.wikipedia.org/wiki/Chemnitzer_Linux-Tage) - ein Wochenende im März, Aufzeichnungen
  - [23. und 24. März 2025](https://chemnitzer.linux-tage.de/2025/de)


## Aufgaben

- Suche nach Thema in Section 1, z.B. file, editor,
- Warum ManKier konkret besser als man? Z.B. tar, rsync, gcc
- Auf welche Journals hat die DHSN Zugriff?
- anmelden, mitmachen, von DHSN als Praxisarbeit, ... anerkennen lassen (z.B. bei [ubuntuusers](https://wiki.ubuntuusers.de/Howto/Vorbereitung_auf_Videokonferenzen/))


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Admin<br />
letzte Änderung: 2024-01-05<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>








