# Organisatorisches


## Tage der Vorlesung

Wenn nicht anders vermerkt: 10:00 - 17.15, meist Di.



## VM

- Jeder hat eine VM mit Xubuntu (= Ubuntu mit XFCE-Desktop) und root-Rechten. Kann man kaputtspielen.
- Feste Plätze (in den vorderen Reihen!) bedeuten immer gleiche VM.

## Vorlesung

- [t1p.de/linuxba5](https://t1p.de/linuxba5)
- ![QR-Code](../Pics.d/t1p-de-linuxgit.QR.png)
- Materialien (für Übungen) in git.


## Übungen

- geforderte Ergebnisse mailen an:  e0014625@ba-sachsen.de
- oder via  [HedgeDoc Pad](https://hedgedoc.org/): [t1p.de/linuxpad](https://t1p.de/linuxpad)


## "Hausaufgaben"

- Wiederholung: Fragen in Prüfungsqualität zu Beginn jeder Vorlesung als Wiederholung
- Übungsaufgabe statt letzter Stunde


## Prüfung

- 10.03.2025, Mo.
- mündlich, einzeln
- ca. 20 Min.
- Einstieg: Erläuterung eines vorbereiteten Scriptes

---

<sub>
letzte Änderung: 2025-01-06
</sub>
