# Terminals

![VT100](../Pics.d/Terminal-dec-vt100.jpg)

- eigentlich Terminal-Emulation, ein [VT100](https://en.wikipedia.org/wiki/VT100) ist/war ein echtes Terminal

## Anforderungen

- Lesbarkeit der Texte
  - jiIl|1 unterscheidbar? Auch ganz klein geschrieben?
  - Empfehlung: Tahoma, Verdana, ...
- $PWD oben im Rahmen
- Farben (fg, bg) leicht einstellbar
  - Ergonomie: dunkle Schrift auf hellgrauem Grund
  - Akku-Laufzeit: dark theme
- Font-Größe leicht zu verändern (Ctrl +/-)
- mehrere Tabs
- Toolbars anpassbar
- angenehmes Look & Feel
- einfach zu bedienen
- noch gepflegt
- Spezialitäten
  - parallele Eingabe in mehrere Tabs/Tiles (auf verschiedenen Hosts, für  Admins)
  - gleichzeitig sichtbare Kacheln


## Programme

### Reviews

- [Top 7 open source terminal emulators for Linux](https://opensource.com/life/17/10/top-terminal-emulators)
- [Best Linux terminal emulators of 2022](https://www.techradar.com/news/best-linux-terminal-emulator)
- [22 Useful Terminal Emulators for Linux](https://www.tecmint.com/linux-terminal-emulators/)
- [24 Best Free Linux Terminal Emulators](https://www.linuxlinks.com/terminalemulators/)
- [The 30 Best Linux Terminal Emulators and Bash Editors](https://www.ubuntupit.com/best-linux-terminal-emulators/)


### xfce4-terminal

- reicht
- `$ man xfce4-terminal`
- [How to use Xfce Terminal](https://linuxhint.com/xfce-terminal/)


### Terminator

- [home](https://terminator-gtk3.readthedocs.io/en/latest/plugins.html#plugins), [Plugins](https://terminator-gtk3.readthedocs.io/en/latest/plugins.html#plugins)
- a useful tool for arranging terminals



### eterm

- https://wiki.ubuntuusers.de/Eterm/
- aus dem DE [Enlightenment](https://wiki.ubuntuusers.de/Enlightenment/)

### Extraterm

- http://extraterm.org/
- Schweizer Messer unter den Terminal-Emulatoren


### guake

- https://wiki.ubuntuusers.de/Guake/
- rollt sich vom oberen Bildschirmrand herunter und wieder ein

### Konsole

- https://wiki.ubuntuusers.de/Konsole/
- Terminal-Emulator von KDE

### rxvt-unicode

- [rxvt-unicode](http://software.schmorp.de/pkg/rxvt-unicode.html)
- im Ubuntu Repository


### tilda

- https://wiki.ubuntuusers.de/Tilda/
- rollt sich vom oberen Bildschirmrand herunter und wieder ein

### Tilix

- [home](https://gnunn1.github.io/tilix-web/)
- tiling von Terminal-Views

### Upterm

- [home](https://www.omgubuntu.co.uk/2017/08/upterm-terminal-ubuntu)
- stellt Session blockweise dar

### Yakuake

- https://wiki.ubuntuusers.de/Yakuake/, aus KDE
- rollt sich vom oberen Bildschirmrand herunter und wieder ein


## Aufgabe

- Beurteilen Sie die freien Terminals nach obigen Kriterien; positive, negative Auffälligkeiten in [Pad](https://pad.gwdg.de/29Ftb4f9SMmoTSQQB74U5g#) eintragen. Untersuchen Sie nur einfache installierbare Terminals.


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Empfehlungen Software<br />
letzte Änderung: 2025-01-13<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
