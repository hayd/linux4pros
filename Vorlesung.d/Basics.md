# Linux-Grundlagen

## UNIX

- Seit 1969 von [Bell Labs](https://de.wikipedia.org/wiki/Bell_Laboratories) entwickelt.

## Linux

- 1991 wollte der Student Linus Torvalds eigentlich nur einen  Terminalemulator schreiben.
- Linux = Kernel, Rest aus [GNU-Projekt](https://de.wikipedia.org/wiki/GNU-Projekt), daher GNU/Linux
- Entwicklungsmodelle: [Die Kathedrale und der Basar](https://de.wikipedia.org/wiki/Die_Kathedrale_und_der_Basar)
  - [Release early, release often](https://de.wikipedia.org/wiki/Release_early,_release_often)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  <br />
letzte Änderung:2023-11-06<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
