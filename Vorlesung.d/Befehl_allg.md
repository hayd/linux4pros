# Der Linux-Befehl

am Beispiel ls

## Basics

![Alt text](../Pics.d/cray1.jpg)

- kurze Befehle (w, wc, ls, du, awk, ...), da ursprünglich von Spezialisten für faule Spezialisten in den 1970er Jahren geschrieben
- GUIs kamen erst viel später.
- `$ cd /bin && ls ??`


## häufige Optionen

- **-a**: all
- **-h**: human-readable, 3,7G statt 3873501184
- -**h**: help
- **-l**: long
- **-v**: verbose

Es gibt auch lange, sprechende Optionen.  
Z.B:  **--human-readable** statt **-h**.  
Aber niemand will **--dereference-command-line-symlink-to-dir** eintippen.


## mehrere Optionen

`$ ls -a -l -h -F`  oder kürzer `ls -alhF`

daher: `ls --all` zur Unterscheidung von Optionen-Anhäufung

vergleiche: `$ ls -all`   und  `$ ls --all`


## Dokumentation

Erklärungen zu den Befehlen und all ihren Optionen findet man hier:

- Online-Hilfe: `$ ls --help` , obligatorisch
- man pages:`$ man ls` , obligatorisch
- [ManKier](https://www.mankier.com/1/ls) - übersichtlicher; mit Beispielen als Einstieg, was oft genügt

- Suche nach Begriffen (keywords) in den man pages aller installierter Befehle: `-k`  , z.B: `$  man -k editor`

## Beispiele

- `$ ls -ahlF`
- `$ ls --all -l -hF`
- `$ ls -al *.png
`

## Arbeitserleichterungen

### Autovervollständigung, Tab-Vervollständigung

- [Tab-Vervollständigung](https://www.linux-community.de/ausgaben/easylinux/2017/10/shell-tipps/)  funktioniert für Befehle, Pfadnamen, Optionen (nur Langversionen)
- Wenn eingegebener Sub-String eindeutig, liefert Tab den Rest.
- Wenn eingegebener Sub-String mehrdeutig, liefert Tab nichts, aber 2. Tab alle möglichen Angebote. Dann Sub-String zur Eindeutigkeit ergänzen.
- versuche: `$ ls ---de <TAB>`



### select 'n paste

- select
  - Wort: Doppelklick
  - Zeile: 3-fach-Klick
- paste: mittlere Maustaste
- funktioniert via [primary clipboard](../Prgs.d/Clipboard.md)
  - Im primary clipboard ist etwas nur solange es im Text markiert ist.
- Dokus, cheat sheets sind besonders hilfreich, wenn man mit der Maus vom Bildschirm direkt auf die Kommandozeile kopieren kann (s.o.).

### Misc

- Mit Pfeiltasten hoch, runter kann man in History der Befehle navigieren.
- `$ history`
- `$ !!`   wiederholt den letzten Befehl, z.B: `sudo !!`
- `$ !12`   wiederholt Befehl 12
- Befehle und Optionen, Parameter in einen alias packen:  
   `$ alias ll='ls -lhF'`


## Siehe auch

- [Bash TAB completion tutorial](https://www.gnu.org/software/gnuastro/manual/html_node/Bash-TAB-completion-tutorial.html)
- [How to create a Bash completion script](https://opensource.com/article/18/3/creating-bash-completion-script)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Wissen<br />
letzte Änderung: 2025-01-13<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
