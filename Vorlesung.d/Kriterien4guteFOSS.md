# Woran erkennt man gute FOSS ?

Wie finde ich ein gutes freies Tool für .... ?

## TL;DR

1. Wahl: paketierte Software aus einer renommierten Linux-Distribution

## Finden

- `$ apt-cache search xyz | grep xyz`
- suchen: _Linux best xyz_   => Vergleiche, Reviews, ...



## Versionsnummern

- [Semantic Versioning](https://semver.org/lang/de/)
  - MAJOR.MINOR.PATCH  - z.B. 20.04.5
  - MAJOR: nicht abwärtskompatibel
  - MINOR: neue Features
  - PATCH: weniger/andere Bugs
  - Beispiel [Inkscape](https://de.wikipedia.org/wiki/Inkscape#Versionsgeschichte)
- 0.x: (jahrelang) auf dem Weg zum ersten stable release
- Linux-Kernel: Wenn Linus Torvalds die [Finger](https://linuxnews.de/2022/10/linux-6-0-mit-verbessertem-scheduler-freigegeben/) ausgehen ...


## Kriterien

### Entwicklung

Wie lebendig, sicherheitsbewusst ist das Projekt?

- Von wann letztes **stable release** ([Emacs](https://de.wikipedia.org/wiki/Emacs) vs. [XEmacs](https://de.wikipedia.org/wiki/XEmacs))?
- Wann **last commit**?
- Wie schnell werden **security patches** geliefert?
- Wo **gehostet** (github, gitlab, berlios.de)?
- **release notes** listen die Entwicklung auf: Tempo, Inhalte, bug fixes
- **\#Autoren**
- **Alter**, wie lange schon im Markt?
- **charismatischer** Anführer hilft ([BDFL](https://en.wikipedia.org/wiki/Benevolent_dictator_for_life), Benevolent Dictator for Life)
  - [Linux](https://de.wikipedia.org/wiki/Linus_Torvalds), [Python](https://de.wikipedia.org/wiki/Guido_van_Rossum), [Perl](https://de.wikipedia.org/wiki/Larry_Wall), [Apache](https://de.wikipedia.org/wiki/Brian_Behlendorf), [GNOME](https://de.wikipedia.org/wiki/Miguel_de_Icaza), [KDE](https://de.wikipedia.org/wiki/Matthias_Ettrich)
  - [Do-ocracy](https://de.wikipedia.org/wiki/Do-ocracy): Wer handelt, der entscheidet.

### Bewertungen

- [Sternchen](https://www.heise.de/news/3-1-Millionen-boesartige-Fake-Sterne-auf-GitHub-entdeckt-Tendenz-steigend-10223115.html), Likes werden in großem Stil gefälscht um Malware zu promoten und damit zu verteilen.
- Daher auf Beurteilung durch NI (natürliche Intelligenz) setzen:
  - **Aufnahme in eine Distribution**, d.h. Maintainer finden die Software so gut, dass sie Arbeit investieren und sich mit anderen koordinieren.
  - im [Ubuntu-Repository](https://packages.ubuntu.com/):  `$ apt-cache search xyz`
  - **Reviews** in Publikationen mit einer Redaktion/Peer Review, sonst ist auch dieser von KI geschrieben.


### Finanzierung

Die Finanzierung sollte seriös und nachhaltig sein. Z.B:

- **Support**: Einrichtung, Schulung, Support, Entwicklung [zusätzlicher Features](https://opsi.org/de/pricing/). Kann teuer werden, z.B:  ([MINIO, eine freie S3-Storage-Software](https://min.io/pricing))
- **Enterprise-Features** (z.B. AD-, LDAP-Anbindung) gegen Geld
- vertrauenerweckend? [Geldprobleme haben wir wirklich nicht. Viel Freude mit der Software.](https://help.pdf24.org/de/fragen/frage/wie-kann-ich-an-pdf24-spenden-zahlen/)

Auch zu beachten:

- Wer finanziert Entwickler (z.B. Kernel: RH, IBM, Microsoft, ...)?
- Was will mit die Firma sonst noch verkaufen (z.B. Hardware, Werbung)?
- Spenden (Blender, krita): Was passiert, wenn Spendenbüchse leer?
- siehe auch: [Open-Source-Software 2.0](https://www.bitkom.org/sites/default/files/file/import/FirstSpirit-1498131485664160229-OSS-Open-Source-Software.pdf), Kap. 3 Geschäftsmodelle

### Misc

- Unterstützung [offener Schnittstellen](https://www.se-trends.de/offene-schnittstellen-erzwingen/) - unterstützt Aufbau einer Community, Plugins/Add-Ons/Zusatz-Module führen zu mehr Usern (z.B: [Gimp](https://www.google.com/search?client=ubuntu&channel=fs&q=gimp+plugin), [Firefox](https://www.google.com/search?client=ubuntu&channel=fs&q=gimp+plugin), [Zim](https://zim-wiki.org/manual/Plugins.html), [Nautilus](https://de.ubunlog.com/die-besten-Erweiterungen-f%C3%BCr-Nautilus/))
- Hits bei Suche - auch Pannen generieren viele Hits


### Funktionalität

Wenn bisher kein KO-Kriterium aufgetreten ist, ...

- Anforderungen zusammenstellen
- Erfüllung der Anforderungen in der Praxis testen
  - Anlernzeit, ähnlich vertrauten Programmen
  - Doku
  - Usability (z.B. Zahl der nötigen Klicks)
  - usw.



## Übungen

Suche nach FLOSS gemäß obiger Kriterien:

1. bester Filemanager
2. bestes Snapshot-Tool (Bildschirmfotos)
3. bester Browser
4. bester Text-Editor
5. bester Terminal-Emulator
6. bester Audioplayer
7. bester Videoplayer
8. bester PDF-Reader / PDF-Viewer
9. bester Bildbetrachter
10. bestes Office-Paket


Erkenntnisse in dieses Pad eintragen:
https://pad.gwdg.de/Ptv7t3wdRLyIDysySFwqUg?both

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Admin  Software<br />
letzte Änderung: 2025-01-05<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>




