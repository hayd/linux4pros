# freie Lizenzen

2do: einbauen


## CC

### Warum eine Lizenz
Durch eine Creative-Commons-Lizenz soll die Verbreitung von Inhalten erleichtert werden. Das Internet lebt von der aktiven Beteiligung seiner Nutzer. Dabei kann es sich um Blogeinträge, Bilder, Videos oder Reaktion in den sozialen Medien handeln.

Mit der Nutzung der Creative Commons Lizenz bleibt das Urheberrecht erhalten, da nur bestimmte Nutzungen erlaubt sind. Das bedeutet auch, dass es dem Schöpfer weiterhin möglich ist, gegen Urheberrechtsverletzungen rechtlich vorzugehen.

Der grösste Vorteil ist die grosse Verbreitung der Werke. Das bedeutet, dass der Pool an freien Werken dank der CC-Lizenzen stetig wächst

Sobald sich ein Künstler entschieden hat, sein Werk unter die CC-Lizenz in Umlauf zu bringen, kann die Lizenz nicht mehr widerrufen werden.

Wer ein Werk verwendet, das mit einer Creative Commons Lizenz verbunden ist, geniesst den Vorteil, dass er mit dem Urheber keine juristisch kniffligen Verhandlungen über die Nutzung seines Werkes führen muss. Dank der Creative Commons Lizenz besteht sogenannte Rechtssicherheit.

### Warum CC

- modular
- verbreitet
