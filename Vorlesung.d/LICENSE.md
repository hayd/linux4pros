# Lizenz
## Texte
Texte stehen unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de).  
![cc-by-sa](cc-by-sa.120.png)

## Code
In und für die Vorlesung entstandener Code ist Free and Open Source Software licensed under the [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html)
