# Dateien verschlüsseln

## Basics

- [OpenSSL](https://www.openssl.org/)  ist eine Open-Source-Version des SSL/TLS-Protokolls.
- Umgangssprachlich wird auch heutzutage **TLS** noch als SSL bezeichnet, obwohl SSL der inzwischen veraltete Vorgänger ist.
- Man sollte [TLS 1.3](https://de.wikipedia.org/wiki/Transport_Layer_Security) nutzen.
- Einsatz: SSH, OpenVPN, **HTTPS**, ...
- Gerade in sicherheitskritischen Bereichen sollte man Tools, Pakete verwenden, die verbreitet und frei sind.
- OpenSSL hat über **100 Parameter**, so dass ein Fehlgriff nicht ganz  unwahrscheinlich ist.
- Warum nicht [GPG](https://de.wikipedia.org/wiki/GNU_Privacy_Guard) ? [schlechte Codebasis](https://de.wikipedia.org/wiki/GNU_Privacy_Guard#Kritik)





OpenSSL:

- Erscheinungsjahr: 1998
- Aktuelle Version: [3.4.1](https://openssl-library.org/source/index.html) vom 11. Feb. 2025
- [home]( https://www.openssl.org/)
- Liste der verfügbaren Verschlüsselungsalgorithmen:  
  `$ openssl enc -list` -> 140 supported ciphers


## Datei verschlüsseln

`$ openssl enc -e -aes-256-cbc -pass pass:GanzGeheim -pbkdf2 -in xyz -out xyz.enc`

- `enc` (encrypt) Sub-Command von OpenSSL: encryption oder decryption
- `-e` encrypt
- `-aes-256-cbc` Verschlüsselungsalgorithmus
- `-in` Name des zu verschlüsselnden Files
- `-out` Name des verschlüsselten Files
- `-pbkdf2` [Password-Based Key Derivation Function 2](https://de.wikipedia.org/wiki/PBKDF2) = Verfahren um aus einem Passwort einen Schlüssel abzuleiten

- `-pass` Es gibt 4 Möglichkeiten das PW zu übergeben:
  1. von stdin:      `-pass stdin`
  1. auf der Kommandozeile:  `-pass pass:password`
  1. aus einem File: `-pass file:/tmp/ password.txt`
  1. aus der Env-Variable $PW: `-pass env:PW`


## Datei entschlüsseln

`$ openssl enc -d -aes-256-cbc -pbkdf2 -in xyz.txt.enc -out xyz.txt`

- `-d` – used to decrypt the files


## tarballs ver-, entschlüsseln

- z.B. für einen sicheren Backup:
  - Daten tar-ren
  - tarball verschlüsseln
  - tarball in der Cloud ablegen

Beispiele:

- Verschlüsseln mit Passwort auf der Kommando-Zeile:  
  `$ echo "password" | openssl enc -e -aes-256-cbc  -in archive.tgz -out archive.tgz.enc -pbkdf2  -pass stdin`
- Entschlüsseln mit Passwort aus der Environment-Variablen PASSWORD:  
  `$ openssl enc -d -aes-256-cbc  -in archive.tgz.enc -out archive.tgz -pbkdf2 -pass env:PASSWORD`
  - Passwort aus Variablen löschen:  
  `$ unset PASSWORD`


## Aufgabe

- Kombinieren Sie in einem Script einen filenamen-spezifischen Passwort-Generator, tar und openssl, um /etc verschlüsselt in /tmp abzulegen.

## siehe auch

- [home](https://www.openssl.org/)
- [Basics: OpenSSL Tutorial](https://www.ranta.ch/tutorials/openssl-simple/)
- [Web-Verschlüsselung: OpenSSL 3.2.0 ist da und verschlüsselt robuster | heise online](https://www.heise.de/news/Web-Verschluesselung-OpenSSL-3-2-0-ist-da-und-verschluesselt-robuster-9541736.html)
- [How to provide encryption password when using OpenSSL utility](https://sleeplessbeastie.eu/2021/05/19/how-to-provide-encryption-password-when-using-openssl-utility/)
- [SSL und TLS - was ist der Unterschied?](https://www.heise.de/tipps-tricks/SSL-und-TLS-was-ist-der-Unterschied-4884686.html)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: HowTo Security<br />
letzte Änderung: 2025-02-12<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
