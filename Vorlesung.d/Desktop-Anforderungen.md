# Desktop-Anforderungen

## TL;DR

Erst überlegen, was man von der DE, nicht von den Programmen,  wirklich benötigt.

## Die wirklich wichtigen Komponenten

- Die [Desktopkomponenten](https://wiki.ubuntuusers.de/Desktop/) können aus verschiedenen Desktopumgebungen stammen.


### Panel

- kleine Kommandozentrale an einem Bildschirmrand


### Starter, Startmenü, Dock

- Finden einer App an Hand von Kategorien (Internet, Multimedia, Office, ...)
- z.B. [Whisker Menu](https://wiki.ubuntuusers.de/Whisker_Menu/)


### Workspace-Switcher

- Umschalten zwischen virtuellen Desktops
- Anforderungen
  - Bedienung mit Maus und Tastatur
  - einfach zu benennen
  - "miniature view"


### [Expose](https://www.maceinsteiger.de/was-ist/mac-expose/), Window Picker

- Überblick über alle virtuellen Desktops, Workspaces


### Terminal

- [Terminals](./Terminals.md)


### File-Manager

- Anforderungen
  - 2-Fenster-Betrieb für copy, delete, paste
  - Vorschaubilder skalierbar
  - ggfs. Applikation an Filetyp binden und schnell starten (Test: PDF)
  - Setzen von Rechten


### Clipboard

- Anforderungen
  - zuvor selektiertes einfach einzufügen
- [diverse Beispiele](https://wiki.ubuntuusers.de/Zwischenablage/)
- [clipman](https://docs.xfce.org/panel-plugins/xfce4-clipman-plugin/start) - die XFCE-Variante


### Editor

- [Editoren](./Editoren.md)


### System-Monitor

- Ein [System-Monitor](./Prgs.d/System-Monitor.md) zeigt u.a. auffällige Aktivitäten und Überlastung eines Rechners.


## siehe auch

- [Desktop](https://wiki.ubuntuusers.de/Desktop/) bei UU
  - _Im Minimalfall bezeichnet er das, was man sieht, wenn keine Fenster geöffnet sind._ Viele URLs.
- [Eigene Desktopumgebung](https://wiki.ubuntuusers.de/Eigene_Desktopumgebung/) - Infos zu Komponenten



---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Desktop Empfehlungen<br />
letzte Änderung: 2025-01-12<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
