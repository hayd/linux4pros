# Reguläre Ausdrücke

= regular expression, RegExp, Regex, RE


## Definition

Ein regulärer Ausdruck ist eine (wilde) Zeichenfolge, die mit Hilfe syntaktischer Regeln ein Muster beschreibt, das eine Regex-Engine verwendet, um Text (oder Positionen) in einem Dokument zu finden, typischerweise zum Zwecke der Validierung, des Suchens, Ersetzens oder Aufteilens.

- Diese Regeln definieren z.B. Zeichen-Klassen z.B. [a-z], Zeichen-Gruppen z.B. (aeiou), Quantifizierungen z.B. {5,9} und Positionsbezeichnungen z.B. ^$.



## Allgemeines

- Zu REs gibt es eine [komplexe Theorie](https://de.wikipedia.org/wiki/Regul%C3%A4rer_Ausdruck#Theoretische_Grundlagen), mit der man auch [dicke Bücher](https://www.google.de/books/edition/Regul%C3%A4re_Ausdr%C3%BCcke/ZBOuAgAAQBAJ?hl=de&gbpv=1) (> 500 Seiten) füllen kann.
- **Viele** Tools (grep, sed, kwrite, vi) und Sprachen ( Perl, Python,C, C++, Java, JavaScript, PHP, ...) unterstützen REs.
- Es gibt verschiedene **Implementierungen** (basic, extended REs) mit Unterschieden in Funktionsumfang und Syntax: Perl Compatible Regular Expressions (PCRE),  POSIX.2, ...
- Wir nutzen, was  [grep](../Prgs.d/grep.md) und [sed](../Prgs.d/sed.md)  unterstützen (extended REs), weil man damit einfach testen kann.


## Zeichenauswahl

RE            | Bedeutung
--------------|-----------------------------------
`.`           | ein  beliebiges Zeichen
`[egal]`      | eines der Zeichen e, g, a oder l
`[0-7]`       | eine Ziffer von 0 bis 7
`[A-Za-z0-9]` | Buchstabe oder Ziffer
`[^a-c]`      | ein beliebiges Zeichen außer a,b,c
`ABC\|abc`   | ABC“ oder „abc“ (=alternative Ausdrücke)


### Vordefinierte Zeichenklassen

Es gibt auch vordefinierte Zeichenklassen:

RE   | Bedeutung | entspricht
-----|-----------|----------------------------
`\d` | digit     | `[0-9]`
`\D` | no digit  | `[^0-9]`
`\w` | word      | [a-zA-Z_0-9]
`\W` | no word   | `[^\w]`
`\s` | space     | Leerzeichen + Steuerzeichen
`\S` | no space  | `[^\s]`


### Positonszeiger

Zeichen | Bedeutung
--------|--------------
`^`     | Zeilenanfang
`$`     | Zeilenende
`\<`     | Wortanfang
`\>`     | Wortende
`\b`    | Wortanfang oder -ende


## Quantoren, quantifier

= Wiederholungsfaktoren

RE          | Bedeutung
------------|------------------------------------
`?`         | 0x oder 1x
`+`         | min. 1x
`*`         | beliebig oft (auch 0x)
`{n}`       | genau n-mal
`{min,}`    | mindestens min--mal
`{min,max}` | mindestens min-mal, maximal max-mal

Beispiele:

- `[ab]+` matched  „a“, „b“, „aa“, „bbaab“ etc.
- `[0-9]{2,6}` matched 2 bis 6 stellige Zahlen, nicht z.B. 1 oder 3,1415


## Spezielle Zeichen

Zeichen | Bedeutung
--------|--------------
`\`     | Maskierung
`\n`    | Zeilenumbruch


## Gier, greedyness

RE wird so ausgewertet, dass der Match möglichst groß ist.

- Beispiel: `$ echo "abcabcdabcde" | grep a.*b`
- Es gibt auch nicht-gierige Implementierungen der Quantoren.

## Rückwärtsreferenzen, back-references

= Wiederverwendung von Treffern

- Das Muster muss in `( )` stehen und diese müssen maskiert werden mit `\`.
- Der 1. Treffer wird mit `\1` aufgerufen.
- Beispiel: Vertauschen 2-er Worte:  
   `$ echo "eins zwei" | sed  's/\([a-z]*\) \([a-z]*\)/\2 \1/'`

## File für Tests

```bash
F=/tmp/testfile.txt
echo   "Max.Mueller@gmail.com" > $F
echo   "Max Müller"  >> $F
echo   "Maximilian"  >> $F
echo   "srv1.mail.google.com"  >> $F
echo   "193.1234.144.80 "  >> $F
echo   "193.99.144.80"  >> $F
echo   "-r"  >> $F
echo   ""  >> $F
cat $F
```




## siehe auch

- [Ein Einführungskurs](http://regenechsen.de/wp/regulaere-ausdruecke/01-regex-allgemein/)
- [Tutorial](https://danielfett.de/2006/03/20/regulaere-ausdruecke-tutorial/)
- [Einführung in reguläre Ausdrücke](https://www.webmasterpro.de/coding/einfuehrung-in-regular-expressions/)
- [von Stefan Trost](https://www.sttmedia.de/regulaere-ausdruecke)
- [Overview of regular expression syntax](https://www.computerhope.com/unix/used.htm#overview) in sed doc
- [Reguläre Ausdrücke](https://www-user.tu-chemnitz.de/~heha/hsn/chm/javascript.chm/05-RAU.htm) - TU Chemnitz
- [Regex: Zeichenfolgen schnell und effizient überprüfen](https://www.massiveart.com/de-de/blog/regex-zeichenfolgen-die-das-entwickler-leben-erleichtern)
- [Regular Expressions in GNU Grep 3.8 Doc](https://www.gnu.org/software/grep/manual/grep.html#Regular-Expressions)
- cheat sheets
  - [by DaveChild](https://cheatography.com/davechild/cheat-sheets/regular-expressions/), mehrspaltig, Blöcke, PDF
  - [zum Ausdrucken](https://i.stack.imgur.com/BBPrT.png) auf einer Seite
  - [RegEX cheatsheet](https://quickref.me/regex)
  - [Reguläre Ausdrücke in der Praxis](https://de.wikipedia.org/wiki/Regul%C3%A4rer_Ausdruck#Regul%C3%A4re_Ausdr%C3%BCcke_in_der_Praxis) bei Wikipedia

- Online Tools, Tester
  - [RegExr](https://regexr.com/)  provides real-time visual results, syntax highlighting, tooltips. Läuft im Browser.
  - [regex tester](https://www.regextester.com/)
  - [Reguläre Ausdrücke online testen](http://www.regexe.de/)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Programmierung<br />
letzte Änderung: 2025-01-27<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>


<!--- 2do  

https://en.wikibooks.org/wiki/Regular_Expressions/POSIX_Basic_Regular_Expressions
https://en.wikibooks.org/wiki/Regular_Expressions/POSIX-Extended_Regular_Expressions

-->














