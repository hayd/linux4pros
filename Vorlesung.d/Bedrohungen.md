# Bedrohungen

## TL;DR

**Komplexität ist der größte Feind der Sicherheit.**

## Misc

![Linux-Anteil am globalen Server-Markt](../Pics.d/linux-anteil.png)

- Oft sitzt das größte Risiko auf [Layer 8 bis 10](https://en.wikipedia.org/wiki/Layer_8#/media/File:OSI_user_layers.png).
- Ohne **permanente Pflege** sind die Daten des Rechners irgendwann verschlüsselt, ausgeleitet, Munition für Erpressung, ... Beispiel [Bitterfeld](https://www.spiegel.de/netzwelt/ransomware-cyberangriff-auf-anhalt-bitterfeld-gravierender-als-bislang-bekannt-a-d538f846-1926-484d-b8d6-19554ad8c4c5)
- [im Jahre 2023](https://www.heise.de/news/Cybercrime-Schaeden-bei-deutscher-Wirtschaft-auf-267-Milliarden-Euro-gestiegen-9850941.html) :  **267 Milliarden Euro** Schaden in deutschen Unternehmen durch Attacken auf IT-Infrastruktur (Malware, Diebstahl von Daten und IT-Geräten, Sabotage, ...)
  - **81%** der Unternehmen betroffen, weitere 10% vermuten es
  - **Cyberattacken**: **179 Milliarden** Euro Schaden, meist organisierte Kriminalität
  - Angriffe vor allem aus **China** (45%), **Russland** (39%), Iran und [Nordkorea](https://www.heise.de/news/Passwort-Folge-10-Nordkoreas-digitale-Armeen-9819831.html)
  - **Art der Cyberattacken**: Ransomware (31%, +8 dank KI), Phishing (26% - 5), Angriffe auf Passwörter (24% -5), Infizierung mit Schadsoftware (21% - 7)
  - Die Sicherheit der Lieferketten wird noch weitgehend vernachlässigt, z.B. zugekaufte Software-Komponenten
- [Chinesen kompromittieren](https://www.heise.de/news/Wegem-schwerem-Cyberangriff-auf-US-Provider-FBI-wirbt-fuer-Verschluesselung-10187110.html) Ende 2024 die Netzwerke von AT&T, Verizon, T-Mobile und anderen Providern - der **größte Telekommunikationshack** in der Geschichte der USA  und zwar mit Abstand
- Die Cyber-Kriminellen arbeiten hochgradig **professionell** und die Betroffenen oft eher dilettantisch.
- **arbeitsteilige Bedrohung**
  - Schwachstelle finden
  - Malware installieren
  - Geld eintreiben
- [Der Staat kann/will keine Cybersicherheit schaffen.](https://www.stiftung-nv.de/de/publikation/deutschlands-staatliche-cybersicherheitsarchitektur) Siehe Grafik und oben.
- [Ransomware as a Service](https://www.heise.de/news/Cybercrime-Polizei-zerschlaegt-Ransomware-Gruppe-Hive-7472192.html)
- Bekannte Probleme bekommen eine [**CVE-Nummer**](https://de.wikipedia.org/wiki/Common_Vulnerabilities_and_Exposures) um **Mehrfachbenennung** derselben Gefahren durch verschiedene Unternehmen und Institutionen zu vermeiden und somit die Bekämpfung effektiver zu gestalten. Nach CVE kommt das Jahr der Entdeckung. Beispiel: [Bluetooth-Datensicherheit](https://www.cve.org/CVERecord?id=CVE-2023-24023)
- **lange Incubationszeit:** > 6 Monate => Einspielen des Backups kann Re-Installation der Malware bedeuten
- 2 verschlüsselte Institute => Hunderte von Rechner mussten neu installiert werden (bare metal)

## siehe auch

- [Wie Künstliche Intelligenz Cyberkriminelle effizienter macht](https://www.mdr.de/nachrichten/deutschland/panorama/kuenstliche-intelligenz-ki-cyberangriffe-phishing-mails-gefaehrlicher-100.html)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Security<br />
letzte Änderung: 2025-02-16<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>


<!--- 2do: + ein paar aktuelle Bedrohungen  -->
