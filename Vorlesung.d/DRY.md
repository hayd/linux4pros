# DRY - don't repeat yourself


hier alles zusammenfassen

Bash.d/mein_1.Script.md
InformationsManagement.d/InformationsManagement.md
Prgs.d/ExifTool.md
Supplement.d/Refactoring.md
Vorlesung.d/Weisheiten.md


## Konsequenzen

DRY - sonst:

- höherer Pflegeaufwand
- Fehler werden nicht an allen Stellen korrigiert
- Optimierungen werden nicht an allen Stellen vorgenommen
- Je größer der Code, desto schwieriger wird Refactoring, Restrukturierung, Optimierung des gesamten Programms, Einführung neuer Algorithmen und Methoden.


## Einsatzgebiete

Anzuwenden auf:

- Programme
- Dokumentationen
- eigene Texte, Webseiten
  - [Hypertexte](https://de.wikipedia.org/wiki/Hypertext) (z.B. HTML, Markdown) unterstützen DRY


## KI

[Schlechte Code-Qualität durch die KI-Assistenten GitHub Copilot und ChatGPT](https://www.heise.de/news/Schlechte-Code-Qualitaet-durch-die-KI-Assistenten-GitHub-Copilot-und-ChatGPT-9609271.html) 

Eine Studie zeigt, dass die Code-Qualität im Jahr 2023 mit dem verstärkten Einsatz von KI-Coding-Assistenten abnimmt, und zwar insbesondere in Bezug auf das DRY-Prinzip.
Das bläht den Code unnötig auf.


## siehe auch

[Single Point of Truth](https://de.wikipedia.org/wiki/Single_Point_of_Truth) - bezieht sich auf vorhandene Daten