# Memory

![alt text](../Pics.d/virtual-memory.ger.png)

Quelle: [Virtuelle Speicherverwaltung – Wikipedia](https://de.wikipedia.org/wiki/Virtuelle_Speicherverwaltung)

- Jeder Prozess bekommt einen eigenen **virtuellen Adressraum** (= virtuelle Speicher).
- Der Kernel übersetzt mit Hilfe der **Memory Management** Unit (MMU, Teil der CPU) die virtuellen Adressen in reale physische Adressen.
- Die **physische Adressen** können auf RAM, Festplatten, SSDs, Graphikkarten, ... zeigen.
- **virtuelle Arbeitsspeicher** =   RAM + swap space
- **page**: aufeinanderfolgenden Speicherstellen im virtuellen Speichers
- page size in Byte: `$ getconf PAGE_SIZE`
- immer wichtiger aus **Sicherheitsgründen**: ein Prozess darf nicht unerlaubt auf den Adressraum eines anderen zugreifen => Einsatz von Rust
- **swap space**: Partition/Datei auf HDD, Datei auf SSD
- swap space sollte 2-3x RAM groß sein
- freier Speicher ist verschwendeter Speicher => RAM wird zur Beschleunigung eingesetzt:  
`$ grep ^Cached /proc/meminfo`
  - [mehr zu proc/meminfo](https://linuxwiki.de/proc/meminfo)
- **Puffer/buffer**: schnelles Zwischenlager im RAM für Daten auf dem Weg von einem Gerät zu einem anderen oder einem Programm (z.B. Bild aus dem Netz auf die Platte, Bild von Platte in Bildbetrachtungsprogramm)


## free

- `free` zeigt den eingebauten Speicher und die aktuelle Speicherbelegung
- `-h` verwenden:  `$ free -h`


## RAM

- nur RAM kann wirklich knapp werden
- zu wenig RAM bremst
- zu wenig virtueller Speicher führt zu Problemen/**Abstürzen**
  - Abhilfe: mehr swap space
- Wenn RAM knapp wird:
  - paging: Auslagern einzelner, selten genutzter pages
  - swapping: Auslagern aller pages eines Prozesses => Prozess kommt zum Stillstand


## Speicher bei der Arbeit

`$ htop`

- Bedeutung des memory bars (links oben): `$ F1`
- **VIRT**: belegter virtueller Speicher, meist keine nützliche Zahl
- **RES**: aktiv genutzter RAM eines Prozesses
- **SHR**: Speicheranteil, der mit anderen Prozessen geteilt wird
- **MEM\%**: RAM-Anteil, den ein Prozess belegt


## Swap

swap-on-file bietet mehr Flexibilität als eine HDD-Partition und sollte bei einer SSD schnell genug sein.


### Howto

swap space mit einem File vergrößern:

- swap file(s) an einer Stelle versammeln  
`$ sudo mkdir -p /var/cache/swap`
- swap file erzeugen  
`$ sudo time dd if=/dev/zero of=/var/cache/swap/swapfile0 bs=1M count=16384`
- Kontrolle  
`$ ls -alh  /var/cache/swap/swapfile0`  
- Schutz gegen unbefugtes Auslesen des Swapfiles  
`$ sudo chmod 0600 /var/cache/swap/swapfile0`
- Speicher formatieren  
`$ sudo mkswap /var/cache/swap/swapfile0`
- Kontrolle  
`$ free -h`
- swap space dem System bekannt machen  
`$ sudo swapon /var/cache/swap/swapfile0`
- Kontrollen  
`$ free -h`  
- swapon --show = more /proc/swaps  
`$ swapon --show`
- dauerhaft machen:  
`$ sudo vi /etc/fstab`  
    eintragen:  `/var/cache/swap/swapfile0    none    swap    sw      0 0`


### Performance

- Performance steigern: mehrere swap files auf unterschiedlichen devices
- Priorität für swap file definieren: `$ swapon -p`


## siehe auch

- [Swap bei UU](https://wiki.ubuntuusers.de/Swap/) - ausführlich, praxisnah
- [swappiness](https://wiki.ubuntuusers.de/Swap/)
- [Interpreting /proc/meminfo and free output](https://access.redhat.com/solutions/406773)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  <br />
letzte Änderung: 2025-01-13<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
