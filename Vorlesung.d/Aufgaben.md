# Aufgaben

## Doku
- Schreiben als Markdown-Seite.



## Themen
- Vergleich: awk vs. cut
- Vergleich: Firefox, ESR-Version,  Basilisk-Browser, Pale Moon, GNU IceCat, LibreWolf
- Rezepte für GIMP-Operationen: Bild verkleinern, Bild zurechtschneiden, Regionen verpixeln, 
