# Prompt

## TL;DR

Konfiguration unterirdisch, daher nur bei triftigem Grund von Empfehlung abweichen.


## Konvention

(in Dokus) üblich:

- `$ user` (Fehler kosten Geld.)
- `# root` (Fehler führen hinter Gitter.)

## Anpassung

- In der Variablen PS1 wird der Prompt definiert.
- Syntax max. kryptisch
- root: rot
- user: blau, ...
- in Prompt:
  - user
  - host
  - Befehlsnummer
  - $PWD in Terminal-Rahmen, nicht im Prompt

## Empfehlung

in ~/.bashrc eintragen:  
`PS1="\[\033[34m\]\u@\h(\#)->\[\033[0m\]"`


## Beispiele

- Man kann [zahlreiche Infos](https://wiki.archlinux.de/title/Bash-Prompt_anpassen) (Uhrzeit, Anzahl der Jobs der Shell, Version und Name der Shell, ...)
- Farbe: 31m=red, 34m=blue, 33m=yellow, 32m=green
- Rahmen: `\[\033[..m\]`

**blau**: `PS1="\[\033[34m\]\u@\h(\#)->\[\033[0m\]"`

**rot** (für root): `PS1="\[\033[31m\]\u@\h(\#)=>\[\033[0m\]"`


## siehe auch

So kryptisch ist die Konfiguration:

- [bei ubuntuusers](https://wiki.ubuntuusers.de/Bash/Prompt/)
- [bei archlinux](https://wiki.archlinux.de/title/Bash-Prompt_anpassen)
- [bei hauke-laging](http://www.hauke-laging.de/software/prompt/)
- [How-to: Setup Prompt Statement variables](https://ss64.com/bash/syntax-prompt.html)
- [Bash Prompt HOWTO](https://tldp.org/HOWTO/Bash-Prompt-HOWTO/index.html)
- Prompt-Generatoren, wenn man meint, einen eigenen Prompt konstruieren zu müssen:
  - [Easy Bash PS1 Generator](https://ezprompt.net/)
  - [Bash Profile Generator](https://xta.github.io/HalloweenBash/)



---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Admin Empfehlungen<br />
letzte Änderung: 2024-10-22<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
