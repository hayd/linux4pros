# Kernel

## Basics

- [Micro-Kernel](https://de.wikipedia.org/wiki/Mikrokernel)  oder [Monolithischer Kernel](https://de.wikipedia.org/wiki/Monolithischer_Kernel) ([Andrew S. Tanenbaum vs. Linus Torvalds](https://en.wikipedia.org/wiki/Tanenbaum%E2%80%93Torvalds_debate))
  - [GNU Hurd](https://de.wikipedia.org/wiki/GNU_Hurd) -  Mikrokernel als Basis, [running gag](https://de-academic.com/dic.nsf/dewiki/565610)
- Sprachen: C oder C++, ab Kernel  [6.1](https://www.golem.de/news/torvalds-rust-im-kernel-soll-mit-linux-6-1-erscheinen-2209-168396.html) (Dez. 2022) [Rust](https://www.heise.de/hintergrund/Programmiersprache-Rust-fuer-Neugierige-9345899.html) aus [Sicherheitsgründen](https://www.heise.de/news/Rust-Code-im-Linux-Kernel-Merge-steht-laut-Linus-Torvalds-ab-Linux-5-20-bevor-7154453.html) (Vermeidung von  Speicherzugriffsfehlern oder Pufferüberläufen = Angriffsvektoren)
- [kernel.org](https://www.kernel.org/) - Quelle für Kernel-Quellen
- Der Kernel unterstützt mehr als 20 [Haupt-Architekturen](https://en.wikipedia.org/wiki/List_of_Linux-supported_computer_architectures).
- Der Kernel hat 4 Funktionen:
  1. **Speicherverwaltung**: er verteilt Speicher an die Prozesse und überwacht Einhaltung der Grenzen
  2. **Prozessmanagement**: er bestimmt, welche Prozesse die CPU  wann und wie lange nutzen können
  3. **Gerätetreiber**: er verbindet Hardware mit den Prozessen
  4. **Systemaufrufe und Sicherheit**: er nimmt Serviceanfragen von den Prozessen entgegen


## Linux-Kernel 6.0

[Some 6.0 development statistics](https://lwn.net/Articles/909625/), Oct. 2022:

- 2034 Entwickler haben beigetragen
- 15.402 Non-Merge-Commits (von nur einem Autor)
- \> 1,1 Million lines of code
- 60% der Änderungen sind neue beziehungsweise aktualisierte Treiber.
- die meisten Änderungen von AMD


## User/Kernel Space/Mode

- Auf **Hardware**-Ebene kann man verschiedene **Privilegienstufen** einstellen, durch die der auf der CPU nutzbare **Befehlssatz** (Mode) und der verwendbare **Speicherbereich** (Space) dynamisch eingeschränkt wird.
- **Memory** ist in 2 Bereich geteilt: kernel space und user space
- Unprivilegierte Prozesse = Prozess im user space können nicht
  - sich aus ihren Beschränkungen **befreien**,
  - direkt auf die **Hardware** zugreifen,
  - auf **Speicherbereiche** anderer Prozesse zugreifen.
- **Mode** bezeichnet den Satz zugelassener Befehle
- Die Begriffe Space und Mode werden nicht klar unterschieden. Space ist auch der Bereich im [Stack of Services](https://www.researchgate.net/figure/Relationship-between-user-state-kernel-state-and-hardware_fig1_339679669).


### Kernel Space, Kernel Mode

- unbegrenzter Zugriff auf alle Dateien
- Kernel-Mode: jeder Befehl zugelassen
- root-Rechte nötig
- direkter (=schnellst möglicher) Kontakt zur Hardware
- Fehler können zum **Absturz des Rechners** führen.


### User Space, User Mode

- User-Mode: nur ein **eingeschränkter Befehlssatz** zugelassen
- Kontakt zur Hardware (Platten, Netzwerk, ...) nur mit Hilfe von Kernel-Routinen, sog. [system calls](https://de.wikipedia.org/wiki/Systemaufruf)
- langsamer, da kein direkter Kontakt zu **Hardware**; Beispiel Schreiben auf Platte:
  - Programm --> Kernel --> Platte
- Dort laufen die **normalen Programme** mit den Rechten eines Users.
- Fehler können zum **Absturz des Programms**, nicht des Rechners führen.
- Wenn man für einen Befehl die Zugriffsrechte von root benötigt: [sudo](../Prgs.d/sudo.md)


### Details

- [Linux fundamentals: user space, kernel space, and the syscalls API surface](https://www.form3.tech/engineering/content/linux-fundamentals-user-kernel-space)
- [A Brief Explanation of Kernel Space and User Space](https://www.fir3net.com/UNIX/Linux/a-brief-explanation-of-kernel-space-and-user-space.html)


## Kernel-Module

- **Modularisierung** des Monolithen: Größe, Neues, ....
- Kernel hat Schnittstelle für **loadable kernel modules** (LKM)
- Module ( .ko = kernel object) liegen in /usr/lib/modules/$(uname -r)
- können zur **Laufzeit** nachgeladen werden, um Funktionalität zu erweitern, z.B. Treiber, Filesysteme, Schnittstellen ...
- Laden geschieht normalerweise **automatisch**
- **kmod** - Program to manage Linux Kernel modules
- Module ( .ko = kernel object) liegen in /usr/lib/modules/  
`$ find /usr/lib/modules/ -type f -name '*.ko'  | more`
- geladene Module anzeigen: `$ kmod list`
  - Size in Byte
- **Infos** zu einem Modul anzeigen: `$ modinfo snd | more`
- Man kann Module entladen und gezielt andere laden, um z.B. einen anderen GraKa-Treiber zu verwenden. Details s. [Anleitung](https://wiki.ubuntuusers.de/Kernelmodule/).


## Linux-Kernel selbst kompilieren

- Man sollte keinen Performance-Gewinn erwarten.
- Konfiguration (welche Module werden benötigt: make menuconfig) zeitraubend, aber lehrreich
- Anleitungen: [1.](https://www.renefuerst.eu/linux-kernel-6-x-fuer-ubuntu-fedora-debian-rhel-centos-mint-installieren-lpic-2/), [2.](https://blog.desdelinux.net/de/compilar-kernel-debian/)


## /proc

/proc/ liefert Schnittstellen zum Kernel in Form eines Pseudo-Dateisystems.  
Beispiele:

- `$ ls -al /proc/meminfo`  
 `$ more  /proc/meminfo`  
 [reports](https://man7.org/linux/man-pages/man5/proc_meminfo.5.html) statistics about memory usage
- `$ more  /proc/version`  
[identifies](https://man7.org/linux/man-pages/man5/proc_version.5.html) the kernel version

- `cat /proc/sys/net/ipv4/ip_forward`  
bestimmt, ob Kernel [routet](https://wiki.ubuntuusers.de/Router/Routing-Funktion/) oder nicht
  - 0: Kernel kann nicht routen
  - 1: Kernel kann routen
  - Umstellung auf Routing: `$ echo 1 > /proc/sys/net/ipv4/ip_forward`


## siehe auch

- [Dynamic Kernel Module Support](https://en.wikipedia.org/wiki/Dynamic_Kernel_Module_Support)


## Aufgaben

- Kernel-Module suchen in /usr/lib/modules/: `$ find . -name '*.ko'`

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Wissen<br />
letzte Änderung: 2025-01-26<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>


<!--- 2do   
neues einbauen: siehe Zim / Linux / Kernel (/Module)
-->





