# LaTeX-Beispiel

Das Beispiel [qrcode](https://ctan.org/pkg/qrcode?lang=de) soll zeigen, wie mächtig LaTeX dank spezieller Pakete sein kann, und noch ein paar LaTeX-Details.

**qrcode**: *The package generates QR (Quick Response) codes in LaTeX, without the need for  any other graphical package.*

**Installation**:  

`$ sudo apt install texlive-latex-recommended texlive-latex-extra`  
`$ sudo apt install texlive-pictures`

```latex
%  
%   ci -l  antrag.tex
%   texstudio  antrag.tex
%
%===============================================================
%                            Vorspann
%===============================================================
%
%---------------------------------------------------------------
%                 allgemeine Voreinstellungen
%---------------------------------------------------------------
%
\documentclass[12pt,a4paper,ngerman]{scrartcl}

\usepackage{polyglossia}
\setmainlanguage[spelling=new,variant=german]{german}%
\setotherlanguages{english}

% größere Abstände zwischen Absätzen statt Einrückung
\usepackage{parskip}   % [skip=12pt]

% Schriftdefinitionen
\usepackage{fontspec}
\setmainfont{Trebuchet MS}

\usepackage[protrusion=true,expansion,tracking=true]{microtype}

\usepackage{qrcode}
\qrset{padding,height=3cm}

\textheight23cm

%
%
%===============================================================
%                            Textteil 
%===============================================================
%

\begin{document}
%\linenumbers

\section*{URLs}

Damit alle, die den Antrag nur in Papierform erhielten, auch die Chance haben, sich die verlinkten Web-Seiten anzusehen, seien diese hier nochmals als QR-Code  in der Reihenfolge ihres Erscheinens aufgelistet:

\begin{description}
%
\item[{\Large Connect4Video}]  \qrcode{https://www.connect4video.com/}  \hfill
\textbf{DSGVO}\qrcode{https://www.connect4video.com/produkte/easymeet24}
%
\item[{\Large Mailinglisten}] \qrcode{https://www.jpberlin.de/mailinglisten/email-verteiler/} 
\textbf{ Anbieter} \qrcode{https://www.jpberlin.de/} \\
\hspace*{11em} \textbf{Mailman}\qrcode{https://de.wikipedia.org/wiki/GNU\_Mailman}
\textbf{Kosten} \qrcode{https://www.jpberlin.de/mailinglisten/preise-mailinglisten/}
%
\item[{\Large Wiki}] \qrcode{https://de.wikipedia.org/wiki/Wiki}
\textbf{luckycloud} \qrcode{https://luckycloud.de/de/support-center/about} 
\textbf{DSGVO-konform} \qrcode{https://docs.luckycloud.de/de/data-protection-and-security}
\textbf{Open-Source-Wiki} \qrcode{https://en.wikipedia.org/wiki/Seafile}
\textbf{luckycloud Business} \qrcode{https://luckycloud.de/de/produkte/cloud-services/business} 
%
\end{description}

\end{document}
```

- `%`: Kommentar
- `\\`: Zeilenumbruch
- `\hfill`: horizontaler, unendlich dehnbarer Zwischenraum in einer Zeile
- `\textbf`: Fettschrift
- `\Large`: Großschreibung in der aktuellen Umgebung (large Large LARGE)
- `\begin{xyz} ... \end{xyz}` eine Umgebung mit speziellen Eigenschaften
- `\begin{description} ... \end{description}` Liste von Beschreibungen, bei der jeweils ein Schlagwort oder eine Phrase jeder Beschreibung fett gedruckt


## Aufgabe

- Speichern Sie obigen LaTeX-Code in der Datei "antrag.tex" ab und kompilieren diese mit:  
`$ lualatex antrag.tex`
- Was zeigt antrag.pdf jeweils nach dem 1. und 2. Durchlauf?
- Welche Pakete/ Styles wurden genutzt?  
`$ egrep "\.sty" *.log`
- Über welche [Debian-Pakete](../Distributionen.d/deb-Pakete.md)  werden diese LaTeX-Pakete installiert?

---


<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  HowTo LaTeX<br />
letzte Änderung: 2025-02-10<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
