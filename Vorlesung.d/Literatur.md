# Literatur


## Bücher
### Linux
- [Debian-Referenz](https://www.debian.org/doc/manuals/debian-reference/index.de.html)
- [Debian GNU/Linux Anwenderhandbuch](https://www.debiananwenderhandbuch.de/) von Frank Ronneburg
- [Linux, Das umfassende Handbuch](https://openbook.rheinwerk-verlag.de/linux/linux_kap00_001.html#dodtpfdccb3da-0892-4710-b808-bd81476116a3)  von Johannes Plötner, Steffen Wendzel
- [Ubuntu GNU/Linux](https://openbook.rheinwerk-verlag.de/ubuntu/)  von Marcus Fischer


### Bash
- [Advanced Bash-Scripting Guide](https://tldp.org/LDP/abs/abs-guide.pdf)
- [Bash Reference Manual](https://www.gnu.org/software/bash/manual/html_node/index.html#SEC_Contents)
- [Shell-Programmierung](https://openbook.rheinwerk-verlag.de/shell_programmierung/shell_003_000.htm#Xxx999276) von Jürgen Wolf
