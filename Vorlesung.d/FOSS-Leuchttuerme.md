# Leuchttürme Freier Software

Es gibt einige Freie Programme, die praktisch konkurrenzlos sind in deren großen Sammlung.

Sie ragen heraus durch ihren Funktionsumfang, ihre Qualität, ihre Reife und die Größe, der sie unterstützenden/entwickelnden Community.

Sie sind hier in alphabetischer Reihenfolge genannt, mit der von der Distribution vergebenen Kurzbeschreibung:


|  Program | Beschreibung |
|-------------- |----------------------- |
| [Audacity](https://www.audacityteam.org/)  | fast, cross-platform audio editor |
| [FFmpeg](https://ffmpeg.org/)  |  tools for transcoding, streaming and playing of multimedia files |
| [GIMP](https://www.gimp.org)    |   GNU Image Manipulation Program |
| [ImageMagick](https://imagemagick.org/index.php)  | image manipulation programs |
| [Inkscape](https://inkscape.org/de/)  | vector-based drawing program |
| [LaTeX](https://www.latex-project.org/)  | a document preparation system  |
| [pandoc](https://pandoc.org/)   | general markup converter |
| [RCS](https://www.gnu.org/software/rcs/rcs.html)  | The GNU Revision Control System |
| [rsync](https://de.wikipedia.org/wiki/Rsync)   |  fast, versatile, remote (and local) file-copying tool |
| [Tesseract](https://github.com/tesseract-ocr)   | command line OCR tool |
| [VLC](https://www.videolan.org/vlc/)    |    multimedia player and streamer |


![Audacity](../Pics.d/Audacity_Logo.200.png)
![FFmpeg](../Pics.d/FFmpeg_Logo_new.svg.png)
![GIMP](../Pics.d/GIMP_Icon.200.png)
![ImageMagick](../Pics.d/ImageMagick_logo.png)
![Inkscape](../Pics.d/Inkscape_Logo.png)
![pandoc](../Pics.d/pandoc-cartoon.200.png)
![rsync](../Pics.d/rsync-logo.200.png)
![Tesseract](../Pics.d/tesseract.200.png)
![VLC](../Pics.d/VLC_Icon.200.png)


## Tools der Grundausrüstung

Diese Tools sind praktisch schon Teil des Betriebssystem geworden, so dass sie meist in der Basis-Installation bereits enthalten sind:  

cat df du find grep gzip bzip less locate more sed sort sudo tar uniq


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Software<br />
letzte Änderung: 2025-03-04<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>

<!--- 2do   -->


