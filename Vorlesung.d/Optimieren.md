# Verbessern

## Raus, Kürzen
- PDF Handling
- Pakete kürzen
- Snap, Flatpack, Allgemeines zu Paketen zusammenlegen





## Rein
- crontab
- clonezilla
- ansible, Puppet, opsi
- OfficePakete



## Optimieren
- Pad-Liste


## Live-Notizen
Verbessern:
    qpdf Beispiel: Blanks raus aus Seitenlisten
    poppler-utils installieren in Doku
    pdfinfo findet Javascript, Beispiel
    find, tr: Zeilenumbrüche
    bash Aufgaben als Script in eigenem File
    mein pwgen mitbringen
    Script: Parameter-Übergabe $1 $2 usw.
