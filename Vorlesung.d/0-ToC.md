# Linux für den professionellen Einsatz

## Einstieg

- [Fragerunde](./Fragerunde.md)
- [Organisatorisches](./Organisatorisches.md)
- [Die Vorlesung](./Die_Vorlesung.md) - Zielgruppe, Stil
- [Informationsquellen](Info-Quellen.md)


## FOSS

- [Linux vs. Windows](./Linux_vs_Windows.md) - Unterschiede, Einsatzgebiete
- [Freie Software](./FOSS.md) - Was ist Freie Software?
- [Kriterien für gute Freie Software](./Kriterien4guteFOSS.md)
- [Leuchttürme Freier Software](./FOSS-Leuchttuerme.md)

## Distributionen

- [Linux-Distributionen](../Distributionen.d/Distributionen.md)
- [Auswahl einer Distribution](../Distributionen.d/Distri-Wahl.md)
- [Debian-Pakete](../Distributionen.d/deb-Pakete.md) - xyz.deb
- [APT](../Distributionen.d/APT.md) - Advanced Packaging Tool
- [Snap-, Flatpack-Pakete](../Distributionen.d/Snap,Flatpak.md)
- [Pflege der Pakete](../Distributionen.d/Pflege.md)


## Arbeitsumgebung

- [Was habe ich und wie viel?](./Orientierung.md)
- [guter Arbeitsplatz](./guter_Arbeitsplatz.md) - Empfehlungen
- [Liste der Desktop-Umgebungen](./Desktop-Liste.md)
- [Desktop-Anforderungen](./Desktop-Anforderungen.md)
- [XFCE](./XFCE.md) - ein Desktop Environment
- [XFCE konfigurieren](./XFCE-config.md)
- [Übungen zu XFCE](./XFCE.ex.md)


## Linux

### Basics

- [Basics](./Basics.md)
- [der Linux-Befehl](./Befehl_allg.md)
- [Kernel](./Kernel.md)
- [sudo](../Prgs.d/sudo.md) - für einen Befehl root werden
- [Prozesse](./Prozesse.md)
- [Memory](./Memory.md)


#### Filesysteme

- [VFS](../Filesystem.d/VFS.md) - das Virtual File System
- [Everything is a file](../Filesystem.d/Everything_is_a_file.md)
- [FHS](../Filesystem.d/FHS.md) - Filesystem Hierarchy Standard
- [Platten](../Filesystem.d/Disks.md) - etwas Hardware-KnowHow
- [FUSE](../Filesystem.d/FUSE.md) - Filesystem im User Space
- [SMART](../Filesystem.d/SMART.md) - Platten überwachen
- [Filesysteme](../Filesystem.d/Filesystem.md) - lange Listen und eine Auswahl
- [Rechte im Filesystem](../Filesystem.d/FS-Rechte.md)
- [Filesystem-Befehle](../Filesystem.d/Filesystem-Befehle.md)
- [Disk-Tools](../Filesystem.d/DiskTools.md) - Standard-Befehle und Tools
- [Platte partitionieren](../Filesystem.d/Platte_partitionieren.md) - von bare metal zur einsatzbereiten Platte


### Datenhaltung

- [Filer](../Filesystem.d/Filer.md)
- [Storage-Clouds](../Filesystem.d/Storage-Clouds.md)
- [Backup](../Filesystem.d/Backup.md)
- [Archivierung](../Filesystem.d/Archivierung.md)


## Die interaktive Arbeitsumgebung

- [CLI vs GUI](./CLI_vs_GUI.md)
- [der Linux-Befehl](Befehl_allg.md)
- [Terminals](./Terminals.md)
- [Prompt](./Prompt.md)
- [Die Shell](./shell.md)


## Bash

- [bash](../Bash.d/bash.md)
- [Variable](../Bash.d/Variable.md)
- [interaktive Bash](../Bash.d/interaktive_Bash.md)
- [redirect](../Bash.d/bash-redirect.md)
- [globbing](../Bash.d/bash-globbing.md)
- [Reguläre Ausdrücke](./REs.md)
- [grep](../Prgs.d/grep.md) und [sed](../Prgs.d/sed.md) REs im Einsatz
- [RE-Uebungen](./RE-Uebungen.md)  
- [Shell-Scripte](../Bash.d/Shell-Scripte.md)
- [Kommentare](../Bash.d/Kommentare.md)
- [if](../Bash.d/bash-if.md)
- Schleifen
  - [for](../Bash.d/for.md)
  - [while](../Bash.d/while.md)
  - [Schleife vorzeitig verlassen](../Bash.d/exit_loop.md)
- [functions](../Bash.d/bash-functions.md)
- [echo](../Bash.d/echo.md) -  Ausgabe auf Bildschirm, von Variablen
- [read](../Bash.d/read.md) - Einlesen
- [dialog](../Bash.d/dialog.md)
- [1. Script](../Bash.d/mein_1.Script.md) - Gerüst für erste Versuche
- [Aufgaben](../Bash.d/Aufgaben.md)


## Informationsmanagement

- [aktuelle Notizen](../InformationsManagement.d/bisher.md) - eine Umfrage
- [Informationsmanagement](../InformationsManagement.d/InformationsManagement.md) - eine Einführung
- [Markdown](../InformationsManagement.d/Markdown.md) - ein Allzweck-Format
- [Markdown-Tools](../InformationsManagement.d/Markdown-Tools.md)
- [Versionsverwaltung](../InformationsManagement.d/Versionsverwaltung.md)
- [RCS](../Prgs.d/RCS.md) - die persönliche Versionsverwaltung
- [Wikis](../InformationsManagement.d/Wikis.md)
- [Zim](../Prgs.d/Zim.md) - personal Wiki
- [Recoll](../Prgs.d/Recoll.md) - Desktop-Suchmaschine
- [PDF](../PDF.d/PDF.md) - PDFs ansehen, editieren, Seiten ausschneiden, ...
- [OCR](../PDF.d/OCR.md) - als Text-Layer in PDF/A
- [Text-Vergleich](../InformationsManagement.d/Text-Vergleich.md)
- [pandoc](../Prgs.d/pandoc.md) - Format-Konvertierer


## Texte

- [Browser](./Browser.md)
- [Office Pakete](Office-Pakete.md)
- [LaTeX](./LaTeX.md)
  - [spezielles LaTeX-Beispiel](./LaTeX-QR.md)
- [hunspell](../Prgs.d/hunspell.md)
- [Scannen](./Scannen.md) - Digitalisierung von Papier, OCR
- [Drucken](./Drucken.md)


## Multimedia

### Bilder

- [Viewer](../Prgs.d/Image-Viewer.md)
- [GIMP](../Prgs.d/gimp.md) - freie Photoshop-Alternative
- [Inkscape](../Prgs.d/Inkscape.md) - SVG-Editor
- [Screenshot-Programme](../Prgs.d/Screenshot.md)
- [ImageMagick](../Prgs.d/ImageMagick.md)
- [ExifTool](../Prgs.d/ExifTool.md) - Editor für Metadaten eines Fotos
- [DigiKam](../Prgs.d/DigiKam.md) - Bildverwaltungsprogramm


### Audio

- [pavucontrol](./Sound.md#pavucontrol) - Soundkontrolle
- [Audacity](./Sound.md#Audacity) -  grafischer Audio-Editor/-Recorder


### Video

- [VLC](../Prgs.d/VLC.md) - ein vollständiger Medienplayer
- [FFmpeg](../Prgs.d/FFmpeg.md) - Konverter für Video- und Audiomaterial
- [Video-Editoren](../Prgs.d/Video-Editoren.md)


## Videokonferenzen

- [Videokonferenz-Technik](./Videokonferenz-Technik.md)
- [Videokonferenz-Lösungen](./Videokonferenz-Loesungen.md)


## Komprimieren

- [Komprimierung](../Prgs.d/Komprimierung.md)
- [gzip, bzip](../Prgs.d/gzip,bzip.md)
- [7z](../Prgs.d/7z.md)


## Daten transportieren

- [Prüfsummen](../Prgs.d/Pruefsummen.md)
- [tar](../Prgs.d/tar.md) - Daten archivieren
  - [find](../Prgs.d/find.md) - suchen
- [rsync](../Prgs.d/rsync.md) - Daten spiegeln


## Security

- [Bedrohung](./Bedrohungen.md)
- [Rechte im File-System](../Filesystem.d/FS-Rechte.md)
- [Phishing](./Phishing.md)
- [Passworte](./Passworte.md)
- [Passworte erraten](Passwort-Cracking.md)
- [SSH-Login](./SSH-Login.md)
- [Datei-Verschlüsselung](./Datei-Verschluesselung.md) mit OpenSSL
- [Ordner-Verschlüsselung](./Ordner-Verschluesselung.md) mit GoCryptFS


## Programme, Tools, Bordmittel

### Text

- [awk, cut](../Prgs.d/awk,cut.md) - Worte extrahieren, ...
- [Clipbord](../Prgs.d/Clipboard.md) - Was wurde je selektiert?
- [csplit](../Prgs.d/csplit.md) - Files zerlegen gemäß Inhalt
- [grep, agrep](../Prgs.d/grep.md) - Suche nach Mustern
- [hunspell](../Prgs.d/hunspell.md) - Spell Checker
- [lorem](../Prgs.d/lorem.md) - Text-Generator (lorem ipsum)
- [more,less,cat](../Prgs.d/more,less,cat.md) - File-Inhalt anzeigen
- [pandoc](../Prgs.d/pandoc.md) - universeller Format-Konverter
- [sed](../Prgs.d/sed.md) - stream editor for filtering and transforming text
- [sort,uniq](../Prgs.d/sort,uniq.md) - Text-Zeilen sortieren
- [tr](../Prgs.d/tr.md) - translate characters

### Zahlen und Zeiten

- [Rechnen](../Prgs.d/Rechnen.md)
- [Kalender](./Kalender.md)
- [Zeitpunkte](../Prgs.d/Zeiten_rechnen.md) addieren, subtrahieren, auflisten


### Datentransport

- [Prüfsummen](../Prgs.d/Pruefsummen.md) - MD5, SHA
- [Komprimierung](../Prgs.d/Komprimierung.md)
- [rsync](../Prgs.d/rsync) - Synchronisierung von Dateibäumen
- [tar](../Prgs.d/tar) - Archiv-Programm

### Infos einholen

- [file](../Prgs.d/file.md) - File-Typ bestimmen
- [type](../Prgs.d/type.md) - Woher kommt ein Befehl?
- [locate](../Prgs.d/mlocate.md) - Wo liegen welche Daten?
- [find](../Prgs.d/find.md) - suchen

### Admin

- [sudo](../Prgs.d/sudo.md) - superuser do
- [cron](./cron.md) - regelmäßige Aufgaben
- [gkrellm](../Prgs.d/gkrellm.md) - ein alter aber guter  Systemmonitor (mit Ente)

### Internet

- [Firefox + Addons](../Prgs.d/Firefox.md)
- [Thunderbird + Addons](../Prgs.d/Thunderbird)
- [wget](../Prgs.d/wget.md) - Web-Server (-Teile) kopieren

### Misc

- [KeepassXC](../Prgs.d/KeepassXC.md) - ein Passwortmanager
- [xeyes](../Prgs.d/xeyes.md) - grafisches Testprogramm
- [Screenshot](../Prgs.d/Screenshot.md) anfertigen



## Ablauf

- [1.Tag](../Ablauf.d/1.Tag.md)
- [2.Tag](../Ablauf.d/2.Tag.md)
- [3.Tag](../Ablauf.d/3.Tag.md)
- [4.Tag](../Ablauf.d/4.Tag.md)
- [5.Tag](../Ablauf.d/5.Tag.md)
- [6.Tag](../Ablauf.d/6.Tag.md)
- [7.Tag](../Ablauf.d/7.Tag.md)
- [8.Tag](../Ablauf.d/8.Tag.md)  <---
- [IT-Weisheiten](./Weisheiten.md)

- [Pad: Verbesserungen für die Vorlesung](https://pad.gwdg.de/TJNRI4SCQcqXBOrlQeb2cA?both)
  
---

<sub>
Autor: Helmut Hayd<br>  
Schlagwörter:  Admin  Software<br>
letzte Änderung: 2025-02-10<br>  
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
