# Die Shell

## Basics

- eine Mensch-Maschine-Schnittstelle

![Mensch-Maschine-Schnittstelle](../Pics.d/shell_user_interface.svg)

- Die Shell **interpretiert** die Eingaben des Users und übermittels das Ergebnis an das Linux-System. Man kommuniziert nicht direkt mit dem OS.
  - Also Vorsicht bei Eingabe interpretierbarer Zeichen (z.B. *).
  - Filenamen aus der Windows-Welt können Probleme machen z.B. wegen Leerzeichen (*Brief an Bank.docx*), Klammern
- Die Shell ist eine Programmiersprache.
  - Man kann kleine Programme auf der Kommadozeile ausführen:  
    `$ for i in  1 $(seq 1 20); do P=$(($RANDOM%500)); xeyes -geometry $Px$P+$P+$P  & sleep 1; done`
  - Man kann 2 (oder mehr)Kommandos verknüpfen:
    - `;`   bedingungslos
    - `||`  2. Kommando nur, wenn 1. erfolglos
    - `&&`  2. Kommando nur, wenn 1. erfolgreich
    - `$ echo "ja" | grep "ja" || echo "kein Treffer" && echo "Treffer"`
  - Tests sind auf der Kommandozeile möglich:  
       `$ echo "ein Test. " | tr ' ' '_'`


## Shells

![shell-comparison](../Pics.d/shell-comparison.png)

- detaillierter Vergleich der [Interactive features](https://en.wikipedia.org/wiki/Comparison_of_command_shells)


### Bourne-Shell

- sh
- die Mutter aller Shells, von Stephen R. Bourne entwickelt, 1977/1978 veröffentlicht


### Bash

- wohl am weitesten verbreitet und überall installiert
- Bash= Bourne again shell = sh 2.0
- Erscheinungsjahr: 1989


### Z shell

- [zsh](https://en.wikipedia.org/wiki/Z_shell)
- die wohl mächtigste Shell
- Best of bash, ksh (Kornshell) und csh (C-Shell)


### xonsh

- [xonsh shell](https://xon.sh/)
- erlaubt das Mischen von Python- und Shell-Befehlen auf der Kommadozeile


## siehe auch

- [Shell](https://wiki.ubuntuusers.de/Shell/) bei UU


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Wissen<br />
letzte Änderung: 2025-01-26<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
