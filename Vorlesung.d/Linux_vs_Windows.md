# Unterschiede zwischen Linux und Windows



## TL;DR

Unix-Rechner (Cray-1: 1976)  
![Cray1](../Pics.d/cray1.jpg)

Windows-PC (MS-DOS:1981, Windows 1.0: 1985)  
![PC](../Pics.d/IBM-PC.jpg)

![windows](../Pics.d/windows-logo.30.png) Windows: ein Betriebssystem einer Firma  
![tux](../Pics.d/tux-logo.30.png) Linux-Distribution: Betriebssystem + tausende paketierte Anwendungen einer Community + Firmen

## Unix-Architektur

Aus den Erfahrungen der führenden Unix-Programmierer für obige, komplexe, verteilte Multi-User-Systeme entstanden:

1. Schreibe Computerprogramme so, dass sie nur **eine** Aufgabe erledigen und diese gut machen.
2. Schreibe Programme so, dass sie **zusammenarbeiten**.
3. Schreibe Programme so, dass sie **Textströme** verarbeiten, denn das ist eine universelle Schnittstelle.

[mehr](https://de.wikipedia.org/wiki/Unix-Philosophie)


## Anlass zum Wechsel

- **04.01.2023**: Ab 10.1.2023 [Windows-8.1 ohne Support](https://www.heise.de/news/Nur-noch-eine-Woche-Zeit-Support-Ende-von-Windows-8-1-7448516.html). D.h. fast 3 Millionen unsichere Windows-Rechner in D.
- **02.01.2025**: In Deutschland laufen etwa 32 Millionen Computer noch mit [Windows 10](https://www.heise.de/news/Keine-Eile-beim-Umstieg-32-Millionen-Rechner-in-Deutschland-noch-mit-Windows-10-10223054.html). Mit dem Support-Ende im Oktober 2025 werden sie zu potenziellen Zielen von Cyberangriffen.

## Details

### Philosophie

![windows](../Pics.d/windows-logo.30.png) Komplexer Aufbau, einfache Bedienung - $$  
![tux](../Pics.d/tux-logo.30.png) Simple is beautiful

- Beispiele:
  - registry vs. /etc
  - eierlegende Wollmilchsau vs. viele einfache Tools
  - Single-User-System vs. Multi-User-System
  - PowerShell vs. üblichem Terminal
  - Unkundiger Anwender erwartet vs. kundiger Anwender vorausgesetzt


### Standards

![windows](../Pics.d/windows-logo.30.png) Setzen von Industriestandards um Profit zu maximieren  
![tux](../Pics.d/tux-logo.30.png) Nutzung freier Standards, um Verbreitung zu erhöhen; [POSIX](https://de.wikipedia.org/wiki/Portable_Operating_System_Interface), X/Open, [RFC](https://de.wikipedia.org/wiki/Request_for_Comments), ...


### Entwicklung

![windows](../Pics.d/windows-logo.30.png) Marketing-getrieben  
![tux](../Pics.d/tux-logo.30.png) Anwender-getrieben


### Entwicklungsmodell

![windows](../Pics.d/windows-logo.30.png) nur Microsoft bestimmt  
![tux](../Pics.d/tux-logo.30.png) [200 - 300](https://www.heise.de/select/ct/2019/16/1564403599540881) Firmen tragen bei, Linus Torvalds hat das letzte Wort (Do-Ocracy)


### Programmcode

![windows](../Pics.d/windows-logo.30.png) nicht vollständig offengelegt und schlecht dokumentiert  
![tux](../Pics.d/tux-logo.30.png) (fast, z.B. kundenspezifische Patches von RH) alles frei


### Versionen

![windows](../Pics.d/windows-logo.30.png) nur eine favorisierte von Microsoft  
![tux](../Pics.d/tux-logo.30.png) nur wenige favorisierte Kernel, aber [zahllose Distributionen](https://de.wikipedia.org/wiki/Liste_von_Linux-Distributionen) = Chance und Problem


### Lizenz

![windows](../Pics.d/windows-logo.30.png) günstige, ungefährliche Lizensierung ohne [Beratung](https://www.kerstin-friedrich.com/consulting.php) nur in Sonderfällen möglich (*"Allein von Microsoft gibt es dazu 12 Vertragsarten, hinzu kommen monatlich aktualisierte Produktlisten und neue Produktbenutzungsrechte."*)
    - z.B. CALs  
![tux](../Pics.d/tux-logo.30.png) Kernel:  GPLv2, Rest: meist FOSS - d.h.  unter Freien Lizenzen


### Kosten

![windows](../Pics.d/windows-logo.30.png) sehr unterschiedlich
  \- Druck zur Cloud, Chronifizierung der Einnahmen  
![tux](../Pics.d/tux-logo.30.png) meist keine (TCO kann trotzdem hoch sein: Schulung, Support)


### Ressourcen-Bedarf

![windows](../Pics.d/windows-logo.30.png) höher  
![tux](../Pics.d/tux-logo.30.png) [minimal](https://www.heise.de/ratgeber/Alte-PCs-und-Notebooks-mit-Linux-wieder-flott-machen-9201189.html) bis hoch


### Benutzerfreundlichkeit

![windows](../Pics.d/windows-logo.30.png) für Nutzer ohne/mit geringen IT-Kenntnisse, viele Automatismen, Halbwissen genügt (?)  
![tux](../Pics.d/tux-logo.30.png) manchmal hohe Einstiegshürden

- [The Luxury of Ignorance: An Open-Source Horror Story](http://www.catb.org/~esr/writings/cups-horror.html), [Kurzform](https://www.heise.de/newsticker/meldung/Eric-Raymond-schimpft-auf-CUPS-94365.html), [dt. Übersetzung](https://kubieziel.de/computer/cups-horror.html)
- [Wenn man's weiss, ist alles ganz einfach!](https://gnulinux.ch/probleme-und-gnu-linux-wenn-man-s-weiss-ist-alles-ganz-einfach) - und umgekehrt


### GUI

![windows](../Pics.d/windows-logo.30.png) normal, Standards gesetzt, Programme ähneln sich  
![tux](../Pics.d/tux-logo.30.png) gut, schlecht, gar nicht; hohe Diversität


### Support

![windows](../Pics.d/windows-logo.30.png) durch viele Firmen, weil so verbreitet; praktisch nicht vom Hersteller  
![tux](../Pics.d/tux-logo.30.png) freundliche Communities (Kultur: man hilft sich im Netz), durch wenige Firmen, teure Linux-Abos

- Neubewertung wegen Fachkräfte-Mangel (indische Studentin im Chat vs. dt. Firma mit 2 Wochen Reaktionszeit vor Ort)


### Software

![windows](../Pics.d/windows-logo.30.png) für fast alles (Fachanwendungen)  
![tux](../Pics.d/tux-logo.30.png) weniger Anwendungen, manche Bereiche fehlen weitgehend (z.B. Fachanwendungen der Verwaltungen)


### Spiele

![windows](../Pics.d/windows-logo.30.png) optimal  
![tux](../Pics.d/tux-logo.30.png) inzwischen [praxisreife Gaming-Plattform](https://www.heise.de/hintergrund/Der-grosse-Linux-Gaming-Guide-9355598.html) (dank [Steam](https://de.wikipedia.org/wiki/Steam)), Emulation oft problematisch

### Desktop-Umgebung

![windows](../Pics.d/windows-logo.30.png) nur eine,  GUI und Windows zu einer untrennbaren Einheit verschmolzen. Nachteile: keine Auswahl, Komplexität versteckt, Fehlersuche schwieriger, Automatisierung schwieriger, Power-User lieben CLI  
![tux](../Pics.d/tux-logo.30.png) ist auch nur eine graphische Applikation, kann man austauschen, [>20](https://de.wikipedia.org/wiki/Desktop-Umgebung#Galerie)


### Installation

![windows](../Pics.d/windows-logo.30.png) muss laufen  
![tux](../Pics.d/tux-logo.30.png) bei passender Hardware-Auswahl unkritisch, Gegenteil auch richtig


### Deinstallation

![windows](../Pics.d/windows-logo.30.png) oft problematisch  
![tux](../Pics.d/tux-logo.30.png) unproblematisch dank Paket-System


### Hardware-Unterstützung

![windows](../Pics.d/windows-logo.30.png) Windows muss laufen; meist vorinstalliert  
![tux](../Pics.d/tux-logo.30.png) teilweise fehlen Treiber, daher vorher informieren; selten vorinstalliert bzw. [kein OS installiert](https://www.pro-com.org/hochschulen-firmen-notebooks/?p=1&o=1&n=12&f=3123)


### CPU-Unterstützung

![windows](../Pics.d/windows-logo.30.png) Intel, AMD  
![tux](../Pics.d/tux-logo.30.png) für etwa [30 Prozessorplattformen](https://de.wikipedia.org/wiki/Linux_(Kernel)#Architektur) verfügbar


### Stabilität

![windows](../Pics.d/windows-logo.30.png) besser geworden  
![tux](../Pics.d/tux-logo.30.png) besser


### Sicherheit

![windows](../Pics.d/windows-logo.30.png) schlechter;  schlechtere Trennung Admin - User  
![tux](../Pics.d/tux-logo.30.png) besser, auch weil kleinerer Markt;  Trennung root - User von Anfang an (Multi-User-System)


### Geschwindigkeit

![windows](../Pics.d/windows-logo.30.png) nimmt im Lauf der Zeit ab  
![tux](../Pics.d/tux-logo.30.png) bleibt hoch


### Updates

![windows](../Pics.d/windows-logo.30.png) inzwischen zwangsweise  
![tux](../Pics.d/tux-logo.30.png) eigenverantwortlich, Security-Patches oft früher


### Drucken

![windows](../Pics.d/windows-logo.30.png) kein Problem  
![tux](../Pics.d/tux-logo.30.png) nicht unkritisch, PostScript-Drucker als Druckmodell, vor Kauf informieren


## Zukünftige Entwicklungen

- [Das](https://www.heise.de/news/Microsoft-hilft-bei-Linux-Installation-9333190.html) proprietäre Betriebssystem Windows scheint nach und nach an Bedeutung zu verlieren – in der Cloud betriebene Abodienste, die man sich per Browser im Grunde auf ein beliebiges OS holen kann, dürften eher das Geschäft der Zukunft sein.
- Microsoft hat eine Anleitung veröffentlicht, wie Nutzer  Linux installieren können.
- [Microsoft konzentriert sein Geschäft auf die Cloud. In Azure laufen schon lange mehr als die Hälfte der Systeme mit Linux.](https://www.heise.de/hintergrund/Microsoft-und-Open-Source-Microsofts-Abkehr-von-der-Geheimniskraemerei-9614174.html?seite=all)



## siehe auch

- [Vergleich Windows versus Linux](https://www.ostc.de/windows.html)
- [Welches Betriebssystem für welchen Zweck?](https://www.wintotal.de/windows-vs-linux/)
- [Linux vs. Windows Betriebssysteme im Vergleich](https://www.ionos.de/digitalguide/server/knowhow/linux-vs-windows-betriebssystem/)
- [30 Jahre Linux](https://www.golem.de/news/30-jahre-linux-1-0-zwischen-kommerz-weltherrschaft-und-antihaltung-2403-183110.html) Zwischen Kommerz, Weltherrschaft und Antihaltung, März 2024


## Aufgaben

- Verbreitung/Marktanteil von Linux auf Servern, auf Desktop in den Jahren 2000, 2010, 2020
- Verbreitung/Marktanteil von Android in den Jahren 2000, 2010, 2020

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Admin<br />
letzte Änderung: 2025-01-06<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>














