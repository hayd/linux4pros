# Was habe ich und wie viel?

Befehle um seinen Rechner, VM kennenzulernen

## OS

- OS-Version numerisch: `$ more /etc/issue`
- Name der Version: `$ less /etc/apt/sources.list`
- Kernel-Version: `$ uname -a`

## Wie viel Platz?

- Memory: `$ free -h`  
- df: **d**isk **f**ree
  - Disks:  `$ df -hl`  
  - echte Disks: `$ df -hl | egrep -v "tmpfs|loop"`  
  - aktuelle Partition: `$ df -hl .`

[Mehr](https://askubuntu.com/questions/1150434/what-is-udev-and-tmpfs) zu tmpfs und udev


## Software

- Was ist installiert: `$ dpkg -l`
- Wie viel ist installiert: `$ dpkg -l | wc -l`



## Hardware

### hwinfo

- Installation: `$ sudo apt install hwinfo`
- Usage: `$ hwinfo --short`

### mehr Infos: lshw lspci lscpu lsusb

- list hardware: `$ lshw -short`
- Auflisten der PCI-Karten, Onboard Audio- und Video-Chips:  `$ lspci`
- Infos über die CPU (incl. Vulnerabilities): `$ lscpu`
- ganz ausführlichere Angaben zu Hardwarekomponenten an einer USB-Schnittstelle: `$ lsusb -vv`
  - oder strukturiert: `$ lsusb -t`

Optionen der ls???-Befehle anzeigen: `$ ls??? --help`


## siehe auch

- [Systeminformationen ermitteln](https://wiki.ubuntuusers.de/Systeminformationen_ermitteln/)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: HowTo <br />
letzte Änderung: 2025-01-12<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
