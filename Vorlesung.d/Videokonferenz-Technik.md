# Videokonferenz-Technik

## nur Kabel

- Kabel statt WLAN (shared Medium) und Bluetooth (noch eine Fehlerquelle)


## Hintergrund

- digital: Was hat man zu verbergen? Daher lieber realer Hintergrund.
- Kontrast zu Hintergrund beachten
- Wahl der Oberbekleidung: klein-gemustertes verschwimmt, [Moiré-Effekt](https://www.pixolum.com/blog/fotografie/moire-effekt)


## Audio

- schlechter Ton stresst, schlechtes Bild weniger
- Yale Studie: Das Gehör hat von allen Sinnen die höchste emotionale Sensibilität.
- Headset macht aufmerksamer, da es isoliert
- Preis > 100€
  - mein 20€-Mikro stieß in 2a noch nicht auf Kritik
- [Online Voice Recorder](https://online-voice-recorder.com/de/) um den ins Netz übertragenen Ton zu testen

## Video

- WebCam 40 - 100€ , HD-fähig
- z.B.
  - [Logitech HD Pro C920](https://geizhals.de/logitech-hd-pro-c920-960-001055-a732329.html?hloc=de) (ca. 60€)
  - Auch die [Logitech HD C270](https://geizhals.de/logitech-hd-c270-silber-schwarz-960-000635-a539794.html) (ca. 20€) ist oft noch besser als die Kamera im Laptop.
- Bild testen mit [Cheese](https://wiki.ubuntuusers.de/Cheese/)


## siehe auch

- [Vorbereitung auf Videokonferenzen](https://wiki.ubuntuusers.de/Howto/Vorbereitung_auf_Videokonferenzen/) - von einem BA-Studenten
- [Marktplatz – 22. September 2022, DLF, 10:08](https://www.deutschlandfunk.de/marktplatz-22-09-2022-skype-zoom-teams-und-co-dlf-5ba3f369-100.html)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Empfehlungen Hardware HowTo Video<br />
letzte Änderung: 2023-12-07<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>