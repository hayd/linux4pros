# Übungen zu REs - mit Lösungen

mit [grep](./Prgs.d/grep.md)  und [sed](./Prgs.d/sed.md)


## So geht es

- gerne auch in 2-er Gruppen. Wer fertig ist, hilft anderen.
- Testen Sie die REs auf der Kommandozeile oder mit Hilfe von (selbst erstellten) Beispieldateien.
- Verwenden Sie egrep --color  (als alias: `alias egrep='egrep --color '`), um die Treffer erkennbar zu machen.
- Schreiben Sie die gesuchten REs mit ihrer Nummer in dieses [Pad](https://pad.gwdg.de/_5QW5o3XSjiOw_zBDshyGA#).
- Die REs sollten möglichst einfach und verständlich sein, damit sie diese auch 6 Monate später noch schnell verstehen.


## verschieden Schreibweisen (v)

- Finde diese Worte in 2 bzw. 3 verschiedenen Schreibweisen  
  `$ echo "Becker und Bäcker" | egrep  'B(ä|e)cker'`  
  `$ echo "Cäsar, Caesar, Cesar" | egrep 'C(ä|ae|e)sar'`  


## Datum (d)

- Schreiben Sie ein egrep, das nur dieses Datumsformat findet: T.M.JJJJ  
`$ echo "10.12.1972  1.2.2022 3.4.22  1.02.23" | egrep "([0-9]\.){2}([0-9]){4}" `
  
 
## Suche in Listen (l)

Die Suche darf nicht zu viele Treffer liefern, z.B. trusted

- Wie viele Rust-Pakete bietet ihre Distribution an? 
 `$ apt-cache search rust | grep -wi rust`

- Wie viele libraries sind auf ihrem Rechner installiert?  
`$ dpkg --get-selections | egrep "^lib"  | wc -l`


## Suche in URL-haltigen Files (u)

Suchen Sie in dem File /etc/apt/sources.list

1. Extrahieren Sie alle Worte, die komplett groß geschrieben sind.  
`$ egrep "[A-Z]+\s"  /etc/apt/sources.list`
2. Extrahieren Sie alle Zeilen, die nicht auskommentiert sind.  
`$ egrep -v  "^#"  /etc/apt/sources.list`
3. Extrahieren Sie alle Zeilen, die eine URL enthalten.  
`$ egrep "http.*:"  /etc/apt/sources.list`
4. Extrahieren Sie alle Zeilen, die eine URL enthalten und auskommentiert sind.  
`$ egrep "http.*:"  /etc/apt/sources.list | egrep ^#`
1. Extrahieren Sie alle Worte, die 2 Vokale hintereinander enthalten.  
`$ egrep "([aeiuo])\1"  /etc/apt/sources.list`

## Prozesse analysieren (p)

1. Extrahieren Sie aus der Prozessliste (ps -ef) alle Prozesse, die nicht root gehören.  
`$ ps -ef | grep -v “^root”`
2. Extrahieren Sie aus der Prozessliste (ps -ef) alle Prozesse, die nicht root und nicht user gehören.  
`$ ps -ef | egrep -v “^(root|$(whoami))” `  falsch
3. Extrahieren Sie aus der Prozessliste (ps -ef) alle Prozesse, die mehr als 10 Sek. CPU-Zeit verbraucht haben (Spalte TIME).  
`$ ps -ef | grep -E ‘([0-9]+:){2}[1-9][0-9]’`  falsch


## Mail (m)

1. Schreiben Sie einen RE, der eine Mailadresse auf formale Korrektheit überprüft und deren Provider in einer der großen Top-Level-Domains beheimatet ist.  
  `$ echo -e "max.hop@gmail.com \nmax.hop @gmail.com \nmax.hop@gmail.de" | egrep [a-zA-Z0-9\.\-]+@[-zA-Z]+\.` falsch
2. Entfernen Sie ">" am Zeilenanfang eines Zitats in einer Mail.  
`$ echo -e "> Hallo Max,  \n> viel Text  \n> Viele Grüße Otto" | sed  's/>//g' ` 



## Dateinamen mit Endungen (e)

Schreiben Sie einen RE, der all die [Dateiendung](https://de.wikipedia.org/wiki/Liste_von_Dateinamenserweiterungen) ".txt .txt,v  .md .odt" (und nicht mehr)  matcht?  
`$ echo "abc.def.txt abc.txt.def lmn.txt,v  lmn,txt,v lm,n.txt,v opq.rst.md  O.123.md  a/brief.odt bild.jpg"  | egrep '.txt|.txt,v|.md|.odt'`    falsch


## Dubletten (du)

1. Finden Sie Worte mit mind. 2 gleichen aufeinanderfolgenden Zeichen bzw. Buchstaben:  
`$ echo -e "Boot \nDampfer \nSchiff \nbrief..text Tanker \nSchifffahrt" | egrep --color '(.)\1{1}'`
1. Finden Sie doppelte Worte, die direkt hintereinander stehen.  
`$ echo "eins zwei eins drei drei vier fünf fünf fünf sechs" | grep -E '(\w+)\s+\1'`  falsch
1. Ersetzen Sie eine von beiden Dubletten.  
`$ echo "eins zwei drei drei vier fünf fünf fünf sechs" | sed RE-du3`


## Geldbeträge (g)

Extrahieren Sie Geldbeträge in deutscher Schreibweise.  
`$ echo "10  12.345,00  7,777.00  9,99  7,9  999,99  95.00" | egrep RE-g`


## Einspalter (1)

Verwandeln Sie die man page von dpkg in eine einspaltige, lange Textdatei.  
`$ man dpkg | sed 's/\s/\n/g' | egrep -v ^$`


## Telefonnummer (n)

1. Extrahieren Sie die Telefonnummern aus dem [Telefonbuch](https://www.dasoertliche.de/?kw=meier&ci=&form_name=search_nat) zu einer Telefonnummernliste.
1. Anonymisieren Sie die Tel.Nr. so, dass die falsche Nr. nicht auffällt.  
`$ echo "0173 300 44 44 ,  02422 50 24 80 , 037467 235 21" | sed 'y/456789/987654/'`

## Refactoring (r)

Auch für Bachelorarbeiten nützlich, um z.B. eine einheitliche Begrifflichkeit (Laptop, Notebook) oder Schreibweise (Server-Schrank, Serverschrank) sicherzustellen.

1. Vereinheitlichen Sie diese Variablennamen: calculate-pi , CalculatePi, CalcPi, CalcPI, Calc_Pi zu calcPi ohne die Namen "Pi, RasPi , 5Pi" zu verändern.  
`$ echo "calculate-pi CalculatePi CalcPi CalcPI Calc_Pi - Pi RasPi  5Pi" | sed -r 's/(calculate-pi|CalculatePi|CalcPi|CalcPI|Calc_Pi)/calcPi/g'`

1. Um die Les- und Suchbarkeit zu verbessern, führen Sie bitte in Bash-Scripten das Schlüsselwort "function" ein, d.h. ersetzen Sie "function_name()" oder "function_name ()" durch "function function_name ()"  
`$ echo -e "function_name()  \nfunction_name () \nfunction function_name ()" | sed -E '/function function_name \(\)/! s/function_name\(\)|function_name \(\)/function function_name ()/g'
`

## Gendern (ge)

Verwandeln Sie diese gegenderten Substantive in eine korrekte Schreibweise z.B. Benutzer:innen / Benutzer*innen -> Benutzer und Benutzerinnen  
`$ echo -e "Student:innen" |  sed  's/\([A-Z][a-z]*\):/\1 und \1/'`

## /etc/passwd (pw)

1. Ersetzen Sie in einer Kopie des /etc/passwd Files das x im 2. Feld durch ein y, außer wenn im 3. Feld eine min. 4-stellige Zahl steht. RE-pw1  
`$ egrep '^[a-zA-Z]+:.:[0-9]{4,}:' /etc/passwd | sed 's/:x:/:y:/'`


1. Listen Sie alle nummerischen user IDs in /etc/passwd (man 5 passwd) auf und nur diese (Rest weg-substituieren). RE-pw2  
`$ sed 's/^\([^:]*\):\([^:]*\):\([^:]*\):\([^:]*\):..*/\3/' /etc/passwd`

aber: Man wähle das beste Tool: `$ cut -d: -f3 /etc/passwd`

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Uebungen<br />
letzte Änderung: 2024-01-14<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
