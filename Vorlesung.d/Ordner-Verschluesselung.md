# Ordner verschlüsseln mit GoCryptFS

## Basics

Wiederholung: [Mount Point, Loopback Device](../Filesystem.d/Disks.md), [FUSE](../Filesystem.d/FUSE.md)


1. [Home](https://nuetzlich.net/gocryptfs/), [GitHub](https://github.com/rfjakob/gocryptfs), [Changelog](https://github.com/rfjakob/gocryptfs#changelog), [UU](https://wiki.ubuntuusers.de/GoCryptFS/)
2. letzte Version vom [2025-01-23](https://github.com/rfjakob/gocryptfs#changelog)
3. gocryptfs ist nicht nur **einfach**, sondern auch sicher. Komplexität ist prinzipiell ein Risiko.
4. Man muss nicht eine ganze **Partition** verschlüsseln, sondern nur wirklich Schützenswertes. Auf dem ganzen Rest kann man mit den üblichen Methoden und Tools arbeiten.
5. Spätestens bei dieser Gelegenheit sollte man sich Gedanken darüber machen, was man schützen will bzw. muss (**DSGVO**).
6. gocryptfs ist ein [FUSE](https://de.wikipedia.org/wiki/Filesystem_in_Userspace)-Dateisystem (Filesystem im [Userspace](./Kernel.md)) in [Go](https://de.wikipedia.org/wiki/Go_(Programmiersprache)#Nebenl%C3%A4ufigkeit) (schnell zu kompilieren, einfacher als C, Parallel-Programmierung eingebaut)  geschrieben.
7. gocryptfs erzeugt zu jeder Klartext-Datei jeweils eine verschlüsselte Datei. Ein **unverschlüsseltes Verzeichnis** wird in ein **verschlüsseltes Verzeichnis** synchronisiert.
8. gocryptfs nutzt **transparente** Verschlüsselung. Das bedeutet, dass Benutzer und Anwendungen davon nichts mitbekommen und auf Dateien über die gewohnten Verzeichnis und Dateipfade zugreifen. Das Betriebssystem verschlüsselt im Hintergrund automatisch die Daten auf dem Weg von der Anwendung zur Platte und zurück.
9. Dateiinhalte werden mit [AES-256-GCM](https://en.wikipedia.org/wiki/AES-GCM-SIV) verschlüsselt.
10. gocryptfs ist **battle-tested**, hat einen Security-Audit hinter sich, läuft auf Linux, macOS, Windows, Android. Version 1.0 von Juli 2016. gocryptfs wurde inspiriert durch [EncFS](https://de.wikipedia.org/wiki/EncFS). EncFS ist als unsicher entlarvt und damit bei allen unten durch.
11. gocryptfs verschlüsselt jede Datei einzeln und auch den **Dateinamen**. Das erlaubt zwar immer noch Rückschlüsse auf die Anzahl und Größe der Dateien, nicht aber auf deren Inhalt.


## Schlüssel

1. Die Daten sind mit einem **Hauptschlüssel** (master key) gesichert, den der Benutzer mit seinem **Passwort** entsperrt.
1. `gocryptfs -init`  fragt das gewünschte Passwort ab. gocryptfs legt daraufhin die JSON-Datei  gocryptfs.conf mit dem Password-Hash (+ weiteren Infos, z.B. Salt, options, ...) im Ordner ab und zeigt einen Generalschlüssel (master key) an. Den benötigt man zur Entschlüsselung, falls das Passwort verschwindet, d.h. Generalschlüssel sicher aufbewahren.
1. Wenn Passwort und Generalschlüssel weg sind, kann man die verschlüsselten Dateien **hinterherwerfen**.
1. Der master key sieht z.B. so aus:
  
```text
Your master key is:

    8ff251df-51cb2322-19798d45-9d9964f1-
    1d846333-279cbad5-e9e8d982-55e286ac
```


## Funktionsweise

1. Es gibt 2 Ordner (Namen frei wählbar):
    1. **Offen.d**: temporär, enthält Klartext, **Mountpoint**, hier kann man arbeiten
    2. **Tresor.d**: permanent, enthält Verschlüsseltes, es ist ein [loopback device](../Filesystem.d/Disks.md#loopback-device)
2. Mit `gocryptfs -init` initialisiert man den digitalen Tresor Tresor.d/, den man zuvor wie üblich mit `mkdir` anlegt.
3. Ein leeres Verzeichnis Offen.d/ legt man wie üblich (`mkdir`) als **mount point** an und mountet darauf den Tresor via  `gocryptfs`.
4. Im mount-point-Verzeichnis Offen.d/ kann wie gewohnt **arbeiten**. Die Daten werden dann sofort verschlüsselt in das Verzeichnis Tresor.d/ **synchronisiert**.
5. Nach Gebrauch Offen.d/ unmounten, damit Klartext verschwindet.Tresor.d/ bleibt erhalten.


## Gebrauch

1. benötigte Software installieren, [SiriKali](https://www.codingblatt.de/sirikali1.gui-daten-verschluesselung/) ist eine einfache GUI:  
  `$ sudo apt install gocryptfs sirikali`
1. mount point (/tmp/Offen.d) und Tresor (/tmp/Tresor.d) anlegen:  
  `$ mkdir /tmp/Offen.d  /tmp/Tresor.d`
1. Tresor initialisieren:  
  `$ gocryptfs -init /tmp/Tresor.d`
1. ein Passwort auf Anforderung 2x eingeben:  
  `$ ganz-geheim`
1. Tresor mit seinem mount point, der alles im Klartext enthält, verbinden:  
  `$ gocryptfs /tmp/Tresor.d /tmp/Offen.d`
1. Kontrolle:  
  `$ mount -l | grep gocryptfs`
1. Dateien (Bilder und Texte) in /tmp/Offen.d/ anlegen und mit /tmp/Tresor.d/ vergleichen
1. Nach Gebrauch Klartext-Verzeichnis aushängen (/tmp/Offen.d ist dann leer):  
  `$ fusermount -u /tmp/Offen.d`
1. Kontrolle:  
  `$ mount -l | grep gocryptfs`  
  `$ ls -al /tmp/Offen.d`
1. Tresor-Inhalt wieder verfügbar machen (Passwort wird abgefragt):  
  `$  gocryptfs /tmp/Tresor.d /tmp/Offen.d`
1. Inhalt kontrollieren:  
`$ ls -al  /tmp/Offen.d`
  

## use cases

1. Der Tresor könnte ein Verzeichnis in einer Cloud sein. Dort ist es wahrscheinlich sicherer gespeichert, überall verfügbar und doch kann der Cloud-Betreiber die Daten nicht lesen, manipulieren.
1. gut mit [rclone](https://wiki.ubuntuusers.de/rclone/) kombinierbar, das über [40 Cloud-Dienste](https://rclone.org/overview/)  unterstützt


## siehe auch

- [home](https://nuetzlich.net/gocryptfs/)
- [Quickstart](https://nuetzlich.net/gocryptfs/quickstart/) eine Seite
- [man page](https://manpages.ubuntu.com/manpages/impish/man1/gocryptfs.1.html)
- [bei UU](https://wiki.ubuntuusers.de/GoCryptFS/) mit guter Anleitung
- [Anleitung: Home-Verzeichnisse unter Linux verschlüsseln mit Gocryptfs](https://www.heise.de/ratgeber/Anleitung-Home-Verzeichnisse-unter-Linux-verschluesseln-mit-Gocryptfs-6193379.html?seite=all),  c't, 17.09.2021, kostenpflichtig
- [How to Encrypt Files with gocryptfs on Linux](https://www.howtogeek.com/686779/how-to-encrypt-files-with-gocryptfs-on-linux/)
- [Datenträger unter Linux verschlüsseln](https://www.linux-magazin.de/ausgaben/2022/01/datentraegerverschluesselung/) 1.Linux-Magazin 01/2022


## Aufgaben

1. obiges Beispiel nachvollziehen, dabei auch Texte und Bilder in /tmp/Offen.d ablegen. Kontrollieren Sie jeden Schritt mit ls, mount, tree oder mit thunar mit 2 Tabs, ... Was geschieht in den beiden Verzeichnissen /tmp/Tresor.d/ und /tmp/Offen.d/ ?
1. Ändert sich die Größe von Bildern oder Texten?
1. selbiges unter Einsatz von SiriKali?

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: HowTo Security<br />
letzte Änderung: 2025-02-12<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
