# Snap-Pakete

- Snaps sind App-Pakete für Desktop, Cloud und IoT,  plattformübergreifend und abhängigkeitsfrei, weil alles zur Laufzeit benötigte enthalten ist.
- maßgeblich von Canonical entwickelt
- Technisch gesehen sind snaps ein Image mit squashfs-Dateisystem, das als [loop-back-device](https://de.wikipedia.org/wiki/Loop_device) gemouted wird:  
  `$ df -hl | grep snap`
- Alle Snaps liegen nach der Installation in: /var/lib/snapd/snaps
- Alle Binaries liegen in: /snap/bin
- Snaps versammelt im [Snap Store](https://snapcraft.io/store)

## Nutzung
- Snap suchen:  
  `$ snap find spotify`
  - oder im https://snapcraft.io/store
- snap installieren:  
  `$sudo snap install SNAPNAME`
- alle snaps aktualisieren:  
  `$ sudo snap refresh`
- alle Version der installierten snaps anzeigen:  
  `$ snap list`
- snap einer best. Version deinstallieren:  
  `$ sudo snap remove --revision 154 gnome-calculator`
- snaps in allen Versionen deinstallieren:  
  `$ sudo snap remove chromium`
- Ist der Browser Chromium als Snap installiert, dann landen die Downloads in:  /tmp/snap.chromium/ . Dort kann man sie erst nach  
  `$ sudo chmod o+rwx -R /tmp/snap.chromium/`  
herausziehen.


## Pro
- läuft überall
- Software-Entwickler müssen weniger Zeit für Wartung/Paketierung investieren, es bleibt mehr Zeit für neue Funktionen.
- Authentizität: Software kommt (vom Maintainer) unverfälscht direkt von der Quelle
- schneller Aktualisierung, kein Upgrade der ganzen Distribution nötig
- einfacheres Testen einer Software, da keine Nebenwirkungen, Compilierung, ...
- Man kann einfach  zwischen verschiedenen Versionen eines Software-Pakets wechseln.
- Snaps laufen in einer Sandbox mit geregeltem/reduziertem Zugriff auf das Hostsystem.

## Contra
- längere Startzeit, größeres Paket als .deb
- keine Qualitätssicherung durch die vielen Maintainer einer Distri
- Eine defekte Library kann viele Snap-Updates erzwingen.
- Paketquelle von möglicherweise unbekannter Vertrauenswürdigkeit
- Canonical ist der Infrastruktur-Monopolist von Snap




## siehe auch
- [Snap documentation](https://snapcraft.io/docs)
- [Linux erfindet sich neu](https://www.heise.de/select/ct/2018/13/1529631413897687) - App-Formate sollen Softwareinstallation bei Linux revolutionieren, c't 13/2018
- [Paketmanagement mit Snap auf Linux-Systemen](https://www.admin-magazin.de/Das-Heft/2018/01/Paketmanagement-mit-Snap-auf-Linux-Systemen), IT-Administrator 01/2018
- [Hey snap, where’s my data?](https://ubuntu.com//blog/hey-snap-wheres-my-data)
