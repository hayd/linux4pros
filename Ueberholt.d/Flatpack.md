# Flatpack

## Basics
- [Flatpack](https://flatpak.org/) bildet eine Alternative zu Snap.
- Ziel: konfliktfreie Installation von Programme außerhalb der normalen Paketverwaltung
- ein Community-Projekt, per Design komplett dezentral. Jeder kann einen eigenen Flatpak-Server eröffnen, aber [Flathub](https://flathub.org/apps) bildet eine zentrale Anlaufstelle
- 800.000 aktive Nutzer, 1200 Programmierer zur Pflege der Flatpaks, 1600 Flatpaks (ca. 4500 Snaps)
- [FlatHub](https://flathub.org/apps): zentrale, einzige offizielle Repo der zur Zeit verfügbarer Flatpaks
- Flatpak ist ausschließlich für den Desktop und für grafische Programme gedacht.
- Flatpak liefert eine abgeschottete Laufzeitumgebung (Sandbox), die man  systemweit oder für nur einen User installieren kann.
- Über Flatpak installierte Anwendungen können nur auf bestimmte Ordner zugreifen. Will man auf weitere Ordner zugreifen, kann man das [regeln](https://wiki.ubuntuusers.de/Flatpak/#ber-Flatpak-installierte-Anwendungen-koennen-nur-auf-bestimmte-Ordner-zugreifen).
- Snap und Flatpack kann man gleichzeitig nutzen.

## Nutzung
- Installation der Laufzeitumgebung:
  `$ sudo apt-get install flatpak`
- Auflisten der installierten Pakete:
  `$ flatpak list`
- alles updaten:
  `$ sudo flatpak update`
- Deinstallation:
  `$ sudo flatpak uninstall [PAKET] `

## AppImage
- [AppImage](https://appimage.org/) bildet eine weitere Alternative zu Snap.
- Es ist keine Laufzeitumgebung erforderlich: AppImage-Datei herunterladen, `chmod +x`, starten. AppImage hat somit  - im Gegensatz zu snap und flatpak - auch kein Sicherheitskonzept.



## siehe auch
- [bei UU](https://wiki.ubuntuusers.de/Flatpak/#ber-Flatpak-installierte-Anwendungen-koennen-nur-auf-bestimmte-Ordner-zugreifen)
- [Flatpak Is Not the Future](https://ludocode.com/blog/flatpak-is-not-the-future) - detailierte Diskussion der Probleme
- [The issue with flatpak's permissions model](https://whynothugo.nl/journal/2021/11/26/the-issue-with-flatpaks-permissions-model/)
- [Die Rechte von Flatpaks mit Flatseal unter Linux regeln](https://www.linux-bibel-oesterreich.at/viewtopic.php?f=10&t=535&p=1599#p1599)
- [Using Flatpak on Ubuntu and Other Linux Distributions - Complete Guide](https://itsfoss.com/flatpak-guide/)

- [ Linux APP Summit 2022: Flathub wird zum App-Store ](https://www.heise.de/news/Linux-APP-Summit-2022-Flathub-wird-zum-App-Store-7070718.html)









## 2do
- Installation von GocryptFS
