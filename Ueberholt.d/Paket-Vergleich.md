# Paket-Vergleich

## .deb, .rpm
- Aktualisierung aller Bestandteile des gesamten Systems
- Programme werden nach Möglichkeit in ihre Bestandteile zerlegt und z. B. Bibliotheken separiert. Wenn also eine Bibliothek verbessert wird, profitieren alle darauf aufbauenden Programme und es arbeiten mehr Entwickler an deren Optimierung.
-  Eine Distribution ist eine aufeinander abgestimmte Gesamtkomposition, in der idealerweise alles perfekt harmoniert und getestet ist. Keine Dopplungen.
- Man kann nicht einzelne Programme aktualisieren. Ausnahme: Rolling Release Distributionen




## Flatpak, Snap
- einfacher für Software-Anbieter
  - keine Rücksicht auf Umgebung, z.B. Library-Versionen
  - nur ein Paket für alle Distris nötig
- mehr Sicherheit durch Sandbox. Deren Rechte muss man aber definieren.
- Qualitätskontrolle der Flatpaks / Snaps zweifelhaft.
- Sicherheitspatch für eine Library => alle betroffenen Flatpaks / Snaps müssen einzeln aktualisiert werden.
- höherer Speicherplatzverbrauch

## Container
und dann gibt es noch die üblichen Container, die alles können:
- Docker
- [Singularity](https://en.wikipedia.org/wiki/Singularity_(software))
-  [lxc](https://www.ionos.de/digitalguide/server/knowhow/was-ist-lxc-linux-container/)

Probleme und Vortele wie bei  Flatpaks / Snaps, nur eine Nummer größer.





## siehe auch
- [Flatpak / Snap vs. Paketverwaltung – Alles was dazu gesagt werden muss](https://curius.de/2021/09/flatpak-snap-vs-paketverwaltung-alles-was-dazu-gesagt-werden-muss/)
- [Frage zur Sicherheit und Sandbox Snap und Flatpak](https://www.linux-bibel-oesterreich.at/viewtopic.php?f=16&t=966&sid=42449ddfbe7134cfd4ef3c32b3b64807&p=2944#p2944)
- [Flatpak, Snap und AppImage im Vergleich](https://www.linux-community.de/ausgaben/linuxuser/2018/02/dreikampf/) - aus LinuxUser 02/2018
