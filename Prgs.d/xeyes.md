# xeyes

- xeyes guckt dem Cursor nach. Man kann daher leicht erkennen, ob der Prozess noch aktiv ist.
- für Tests ganz nützlich
- kann man mit `Alt+lM` verschieben
- `$ sudo apt install x11-apps`
- entzündete Augen: `$ xeyes -outline  red &`


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Software<br />
letzte Änderung: 2024-10-28<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>