# FFmpeg

![FFmpeg Logo](../Pics.d/FFmpeg_Logo_new.svg.png)

Konverter für Video- und Audiomaterial

## Prinzipielles

- Codec:  ein Algorithmenpaar zur **Co**dierung und **Dec**odierung von digitalen Audio-, Video-Dateien
- viele Codecs müssen lizenziert werden (bei geschäftstüchtigen Firmen)
- u.a. MPlayer, VLC, HandBrake (s.u.) nutzen FFmpeg

## Basics

freie Suite von Bibliotheken und Programmen um Video- und Audiomaterial aufzunehmen, zu konvertieren, zu senden (streamen), zu filtern und in verschiedene Container-Formate zu  verpacken

- FF: fast forward
- [home](https://ffmpeg.org/) , [bei UU](https://wiki.ubuntuusers.de/FFmpeg/)
- wird von vielen großen Projekten, Firmen (youtube) genutzt
- enthält Implementierungen von mehr als [100 (patentierten) Codecs](https://ffmpeg.org/general.html#Supported-File-Formats_002c-Codecs-or-Features)
- das Standard-Programm unter Linux für diese Zwecke
- auf die Schnelle nicht zu durchschauen
- Das Projekt ist meist auf den [Chemnitzer Linux-Tagen](https://chemnitzer.linux-tage.de/2025/de/) vertreten.
- Man findet praktisch immer den gesuchten Befehl im Netz. Inzwischen gibt es auch  Bücher zu FFmpeg.
- Wenn man weiß, wie es geht (und die Defaults passen), kann es ganz einfach sein:  

  `$ ffmpeg -i input.mp4 output.avi`
- [Es gibt](http://trac.ffmpeg.org/wiki/Create%20a%20mosaic%20out%20of%20several%20input%20videos)  aber auch (stellt 4 Vidos gleichzeitig dar):
  
```bash
ffmpeg -i 1.avi -i 2.avi -i 3.avi -i 4.avi -filter_complex "nullsrc=size=640x480 [base]; [0:v] setpts=PTS-STARTPTS, scale=320x240 [upperleft]; [1:v] setpts=PTS-STARTPTS, scale=320x240 [upperright]; [2:v] setpts=PTS-STARTPTS, scale=320x240 [lowerleft]; [3:v] setpts=PTS-STARTPTS, scale=320x240 [lowerright]; [base][upperleft] overlay=shortest=1 [tmp1]; [tmp1][upperright] overlay=shortest=1:x=320 [tmp2]; [tmp2][lowerleft] overlay=shortest=1:y=240 [tmp3]; [tmp3][lowerright] overlay=shortest=1:x=320:y=240" -c:v libx264 output.mkv

```

## HandBrake

- Open-Source-Tool zur Konvertierung von Videos aus nahezu [jedem Format](https://handbrake.fr/features.php) in einige moderne, populäre Codecs mit Hilfe von FFmpeg, aber ungleich einfacher zu bedienen.
- [home](https://handbrake.fr/), [bei UU](https://wiki.ubuntuusers.de/HandBrake/)


## siehe auch

- [QWinFF](https://www.linux-bibel-oesterreich.at/viewtopic.php?f=23&t=489&p=1475#p1475) - vereinfachte GUI für ffmpeg avi


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Audio Software Video<br />
letzte Änderung: 2025-02-05<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
