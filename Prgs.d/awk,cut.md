# Text-Felder herausschneiden

## TL;DR

- awk für das Extrahieren von Feldern aus Zeilen
- cut = awk light


## awk

- [awk](https://de.wikipedia.org/wiki/Awk) ist benannt nach den Autoren Alfred V. **A**ho, Peter J. **W**einberger und Brian W. **K**ernighan
- erstmals veröffentlicht: 1977, ein **Klassiker**
- als Report-Generator konzipiert
- Weiterentwicklung oder Ergänzung des Streameditors [sed](./sed.md) => syntaktische Ähnlichkeiten (z.B. [REs](../Vorlesung.d/REs.md))
- eine **Skriptsprache** zum Editieren und Analysieren von Texten, besonders geeignet zum Extrahieren von Feldern aus Textzeilen (komplexere Aufgaben sollte man mit Python o.ä. lösen)
- awk läuft mit einer **impliziten Schleife** über alle Zeilen eines Text-Files.


### Syntax

`awk Bedingung { Anweisungen } Input-File`

- Bedingung: ein RE
- Anweisungen: meist print
- formatierter Output:
  - C-artige Syntax
  - `%-15s` bedeutet
    - `%` danach kommt Format-Beschreibung
    - `-` linksbündig
    - `15` Feldbreite
    - `s` Datentyp: c: character, d: decimal integer, f: floating point, s: string
    - komplexeres Beispiel: `{ printf "%-10s %s\n", $1, $2 }`
- `$NF` das letzte Feld
- Optionen:
  - -`F:` Feldtrenner : definieren, Default: space

### Beispiele

- Auflistung aller auf dem Rechner vergebenen UIDs und UID-Numbers:  
  `$ cat /etc/passwd |  awk -F: '{print $1 ,$3}' | sort -n`
- Ausgabe des jeweils letzten Wortes (\$NF) aller Zeilen (Es gibt keine einfachere Lösung.):  
  `$ cat /etc/apt/sources.list | awk  '{ print $NF }'`
- lokale IP-Adresse, d.h. im 2. Feld steht "localhost", ausgeben:  
  `$ awk '$2 == "localhost" { print $1 }' /etc/hosts`
- formatierte Ausgabe:  
  `$ echo " 12 34 56" | awk '{printf "%-10s %-6s %-3s \n", $3, $2, $1}'`


### siehe auch

- [Die Programmiersprache AWK](https://www-user.tu-chemnitz.de/~hot/unix_linux_werkzeugkasten/awk.html)
- [Awk Tutorial](https://www.tutorialspoint.com/awk/index.htm)
- [printf Formats](https://bl831.als.lbl.gov/~gmeigs/scripting_help/printf_awk_notes.txt)


## cut

extrahiert Teile aus jeder Zeile eines Files seit 1982

### Optionen

- **-f**: nur diese Felder ausgeben,
  - Zählung beginnt bei 1, nicht bei 0  
  - z.B:  1,4,7  oder  1-3,8   oder  -5,10 oder  3-
- **-d**: (=delimiter) Default = TAB, z.B: `-d':'` (für passwd) ,   `-d" "`  = Blank
- **-c**: (character), absolute Positionsangabe


### Aufgaben

- awk Beispiele in cut Syntax notieren
- Laufzeiten vergleichen
- folgende Zeilen erklären:
  
```sh
cut -c 27-32
cut -d":" -f2
cut -f4 -d " "
cut -f2 -d+
cut -c2,6,10,14,18,22
```

### siehe auch

- [cut at WP](https://en.wikipedia.org/wiki/Cut_(Unix))
- [cut bei UU](https://wiki.ubuntuusers.de/cut/)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Tools<br />
letzte Änderung: 2025-02-09<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
