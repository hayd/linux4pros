# Zeilen umbrechen

## Warum?

- Zeile zu kurz: Lesefluss wird unterbrochen
- Zeile zu lang:
  - Auge verliert die Zeile und findet den Anfang der nächsten Zeile nur schwer
  - Der Vergleich von 2 langen Zeilen ist praktisch nicht möglich.


## Empfehlung

- 45 – 75 Zeichen/Zeile
- Man nehme: **60 Zeichen/Zeile**


## Womit?

- fold - wrap each input line to fit in specified width
- Nur an Leerzeichen umbrechen (-s), spätestens nach 60 Zeichen (-w60):  
`$  fold -s -w60 file.txt`



