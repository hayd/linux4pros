# VLC

![VLC Logo](../Pics.d/VLC_Icon.200.png)

VideoLAN Client

- [home](https://www.videolan.org/) , [bei UU](https://wiki.ubuntuusers.de/VLC/)
- ein vollständiger Medienplayer für diverse Audio-, Videocodecs und Dateiformate als auch DVDs, Video-CDs; nutzt dafür [FFmpeg](FFmpeg.md)
- wird seit 1996 entwickelt
- Es gibt unter Linux keine vergleichbare Software.


## Features

VLC kann auch  

- als Streaming-Server verwendet werden,
- Teletext anzeigen,
- TV-Sendungen zeitgesteuert aufnehmen,
- Musikstücke von einer CD rippen,
- verschlüsselte Blu-ray wiedergeben,
- bei Aufnahme konvertieren,
- Screencasts erstellen,
- Bilder aus Videos extrahieren,
- uvm.


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Audio Software Video<br />
letzte Änderung: 2025-02-05<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
