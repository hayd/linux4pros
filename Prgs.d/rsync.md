# rsync

 **r**emote **sync** - d.h. 2 Filesysteme werden auf den gleichen Stand gebracht

- [home](https://rsync.samba.org/) , [rsync examples](https://rsync.samba.org/examples.html) , [bei UU](https://wiki.ubuntuusers.de/rsync/)
- Erscheinungsjahr: 1996, aktuelle Version: [Jan. 2025](https://download.samba.org/pub/rsync/NEWS#3.4.1) 
- für unsichere, beliebig anfällige (WAN-) Verbindungen entworfen, d.h. Synchronisations-Prozess kann jederzeit abgebrochen und **wieder gestartet** werden.
- nur **unidirektional**
- kann auch **Teile** großer Files übertragen (abschaltbar)
- Es werden ggfs. riesige **File-Listen** verglichen, d.h. es dauert, bis die ersten Files übertragen werden.
- wird gerne zur **Datensicherung**  auf lokale USB-Platte, Disk-Space in der Cloud, ... verwendet
- Unter der Decke wird bei sync zu anderen Hosts **ssh/scp** verwendet.


## Tailing /

Syntax: `rsync [OPTIONEN] Quelle(n) Ziel`

- `dir1`  als Quelle => dir1 kommt mit
- `dir1/` als Quelle => dir1 kommt nicht mit, nur Files, Subdirectories aus dir1

Beispiele:  
`$ rsync -a  /home/ich  /data/backup`  
erzeugt **einen** Unterordner ich/ in /data/backup, weil kein / hinter /home/ich steht, d.h. die Daten werden nicht in /data/backup gespeichert, sondern in /data/backup/ich.

`$ rsync -a /home/ich/  /data/backup`  
Es wird kein Unterordner beim Ziel erzeugt, da nun ein / hinter /home/ich/ steht. D.h.  alle Daten von /home/ich/ werden tatsächlich in das Verzeichnis /data/backup geschrieben.


## Gebrauch

### Optionen

- `-v` (verbose) listet jeden übertragenen File
- `--stats` (statistics) Angaben zum übertragenen Filesystem (#Bytes, #Files, ...)
- `-a` (archive) erhält alle Eigenschaften (owner, access date, ...) und symbolische Links
- `--delete` löscht auf dem Zielrechner Dateien, die auf dem Quellrechner nicht mehr existent sind.
- `-p` (progress) Status-Anzeige bei Übertragung
- `-n` (no action), simuliert nur was passieren würde (dry-run)
- `--exclude file`  file enthält, was nicht synchronisiert werden soll; etwas komplizierte Syntax, kennt wild cards, Reihenfolge wichtig
- `-h`   human-readable
- `-z` Komprimierung nur für die Datenübertragung
- `-c` (checksum) Dateivergleich, basierend auf Prüfsummen statt auf Größe und Zeitstempel


## Beispiele

- Vorlesungsscript in Seafile-Ordner übertragen:  
  `$ rsync  -v  --stats -a ~/Linux-Vorlesung.d/*  /data/Seafile/Linux4Profis.d`

- Sicherung in die Cloud:  
`$ rsync --numeric-ids -avz /home/benutzer benutzer@cloud.com:/backups`
  - `--numeric-ids`: keine Anpassung der UID an remote host
  - ssh ohne Passwort-Eingabe muss eingerichtet sein

- Kontrolle der Übertagung:  
  `$ rsync -c -va --stats  /Quelle /Ziel`



## siehe auch

- [Verzeichnisse mit “rsync” synchronisieren](https://www.linux-community.de/ausgaben/easylinux/2016/01/hueben-wie-drueben/) aus EasyLinux 01/2016
- [Daten mit rsync unter Linux synchronisieren](https://www.thomas-krenn.com/de/wiki/Daten_mit_rsync_unter_Linux_synchronisieren)


## Alternativen

### FreeFileSync

- einfach zu bedienen, Zweispaltenansicht des eigentlichen Synchronisationsbereichs
- Beim Abgleich von Datenbeständen auf internen und externen Datenträgern ist es unschlagbar.
- [FreeFileSync](https://wiki.ubuntuusers.de/FreeFileSync/)
- [Version 12](https://www.s3nnet.de/aufgemerkt-freefilesync-120-erschienen/) erschienen
- das Projekt eines einzelnen Entwicklers


### unison

- [home](https://github.com/bcpierce00/unison) [Unison bei UU](https://wiki.ubuntuusers.de/Unison/)
- arbeitet bidirektional
- optionale grafische Oberfläche: [unison-gtk](https://packages.ubuntu.com/kinetic/unison-gtk)
- in [OCaml](https://de.wikipedia.org/wiki/Objective_CAML)  geschrieben

### rclone

[Rclone als Helfer für Backups außer Haus - LinuxCommunity](https://www.linux-community.de/ausgaben/linuxuser/2019/06/rsync-fuer-die-cloud/), Rsync für die Cloud


## Aufgaben

- Synchronisieren Sie Ihre persönlichen Konfigurationen in ~/.config  nach /tmp.
  - Wie lange dauert das pro File?
  - Fügen Sie in ihrem Workspace-Switcher einen Desktop mit dem Namen "neu" hinzu oder spendieren Sie Ihrem XFCE-Panel ein neues Item.
  - Synchronisieren Sie dann Ihre Konfigurationen erneut mit der Option `-v`. Was meldet diese?


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  HowTo Software Storage<br />
letzte Änderung: 2025-02-08<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
