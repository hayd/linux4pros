# find

- Suche nach einem File/Directory in einem Verzeichnisbaum ab einem gewissen Startpunkt abwärts
- Der Suche kann man **Filter** mitgeben.
- Die Treffer kann man direkt **weiterverarbeiten**.
- Auf Ubuntu vorinstalliert dank findutils.deb


## Optionen

- `-type f|d|l` Beschränkung auf files|directories|symbolic links
- `-size -/+ nk|M|G` Files, die kleiner/größer als n KiB|MiB|GiB sind
- `-name "*.txt"` alle Text-Files
- `-iname "*.txt"` case **i**nsensitive für alle Text-Files
- `-mtime n` modified,  `-n`: bis zu n-Tagen alt, `+n`: älter als n Tage


## Beispiele

- Was haben wir denn hier?  
  `$ find .`
  - Und wie viele sind Files?  
  `$ find . -type f | wc -l`
- Plattenfüller (Files > 10 GiB) finden:  
  `$ find ~ -type f -size +10G`
- alles , was bis zu 4 Tagen alt ist:  
   `$ find ~ -mtime -4`
- Suche nach Markdown-Files von mehr als einem  Startpunkt aus:  
  `$ find dir1 dir2 dir3 -type f -name "*.md"`
- obiges mit Startpunkten aus einer Variablen:  
  `$ dirs=( dir1 dir2 dir3 )`
  `$ find "${dirs[@]}" -type f -name "*.md"`

- `printf` bestimmt nicht nur das Format, sondern auch die Inhalte der Ausgabe:  
  `$ find .  -printf   '%Tc\t%f\n'`  
  `$ find .  -printf  '%TY-%Tm-%Td %TH:%TM:%.2TS\t%f\n'`
  - Details: `man find` , [How to Use find -printf in Linux?](https://www.codedodle.com/find-printf.html) - Tutorial


- Neues finden (Wo hat das gerade installierte Programm seine config files versteckt?):

```bash
  $ touch -t 202211230800 /tmp/timestamp
  $ ls -al /tmp/timestamp
  $ find ~ -newer /tmp/timestamp
```

### find + action

Syntax:`-exec   cmd {} \;`

- `-exec` sagt, dass ein Kommando `cmd` folgt, dem werden die Treffer `{}` übergeben, `\;` muss den Befehl abschließen

**Beispiele:**

- Kopien von Markdown-Files zusammenhängen (Vorsicht: recursives cat):  
   `$ find . -type f -name "*.md" -exec cat {} \; > /tmp/all-files.md`
- Prüfsumme von allen Files erstellen:  
 `$ find  ./SCR  -type f  -exec   md5sum {} \;  >> /tmp/SCR.md5sums`
- Files < 400 kB einpacken (Hier sind 2 Befehle übersichtlicher und flexibler.):  
  `$ find . -size -400k > /tmp/small-files.list`  
  `$ tar -cvj -T /tmp/small-files.list -f small-files.tgz`

- Suchen in Files (stat grep -r):  
  `$ find .  -type f  -name '*.md'  -exec grep -iH 'Wo ist denn' {} \;`
- Ersetzen (irreversibel durch -`i`) in vielen Files:  
   `$ find .  -type f -exec  sed -i 's/serverA/serverB/g' '{}' \;`


## siehe auch

- [bei UU](https://wiki.ubuntuusers.de/find/) mit vielen Beispielen
- [fd](https://github.com/sharkdp/fd) - a simple, fast and user-friendly alternative to find


## Übung

- Wo legt das [xfce4-timer-plugin](https://docs.xfce.org/panel-plugins/xfce4-timer-plugin/start) seinen config file ab?


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  HowTo Tools<br />
letzte Änderung: 2025-01-31<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
