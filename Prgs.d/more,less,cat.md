# more, less, cat

Dateien ansehen

## more

- Datei **seitenweise** durchblättern
- `space`: nächste Seite
- `q`: Blättern beenden

## less

- das bessere more
- `Mausrad, Bild vor, zurück` bewegen im Text
- `alias more=less`


## cat

- cat = con**cat**enate
- gedacht für das **Zusammenhängen** von Dateien. Bei Angabe nur eines Dateinamens, wird in einem Zug ausgegeben.
- **Unsichtbares** (Leerzeichen am Zeilenende, Tabulatoren, Zeilenende) sichtbar machen:  
  `$ cat -vte xyz.txt`
- alle Text-Files zusammenhängen (.TXT vermeidet Rekursion):  
  `$ cat *.txt > all.TXT`
- Alternative: [bat](https://wiki.ubuntuusers.de/bat/)
  - im Repo
  - Verbesserungen gegenüber cat:
    - Syntax-Highlighting für > 150 Sprachen
    - Anzeige von Zeilennummern
    - Anzeige der Unterschiede von in Git verwalteten Dateien
    - seitenweise Anzeige (=  cat | more)
    - `alias cat=bat` (zurück zum Original: `\cat`)

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Tools<br />
letzte Änderung: 2025-02-09<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
