
# Komprimierung mit gzip, bzip2

## TL;DR

lbzip2 wegen Fehlertoleranz und Parallelisierung


## gzip

- [GNU zip](https://de.wikipedia.org/wiki/Gzip)
- die wohl am häufigsten genutzte Komprimierungsmethode, zu Unrecht
- schlechter, langsamer als bzip2
- Dateiendung .gz
- Syntax:
  `$ gzip file.txt`  #  -->  file.txt.gz
- Optionen:
- `-1` ... `-9` schnellste, schlechteste ... langsamste, beste
- `-r` einzeln rekursiv, jeder File wird einzeln komprimiert
- `-d` (decompress)  oder gunzip



## bzip2

- [home](http://www.bzip.org/) , [bei UU](https://wiki.ubuntuusers.de/bzip2/)
- ähnelt im Gebrauch gzip, aber ein ganz anderer Algorithmus
- Komprimierungsalgorithmus: [Burrows-Wheeler-Transformation](https://de.wikipedia.org/wiki/Burrows-Wheeler-Transformation) + [Huffman-Kodierung](https://de.wikipedia.org/wiki/Huffman-Kodierung)
- Komprimierung mehrerer Files, es entstehen dabei alter_name.bz2:  
  `$ bzip2 file1.txt file2.txt`   # --> file1.txt.bz2 file2.txt.bz2
- Option: -1 (schnellste, schlechteste) ... -9 (langsamste, beste),  Default: 5
- Auspacken:  `$ bunzip2 file1.txt.bz2`
  - oder: `bzip2 -d`
- lbzip2: parallelisierte Version
  `$ alias bzip2=lbzip2`
- **Killer-Feature Fehlertoleranz**: bzip2 kann selbst teilweise beschädigte Archive dekomprimieren. bzip2recover extrahiert die noch lesbaren Blöcke und dekomprimiert sie dann.  
  `$ bzip2recover file_kaputt.bz2`
- Weitere bzip2-Tools: bzcat, bzcmp, bzdiff, bzegrep, bzgrep, bzfgrep, bzless


## Performance: gzip, bzip2

- Testobjekt: linux-6.0.10.tar, 1279M, 35.146.257 Zeilen
- verschiedene Komprimierungsgrade, verschiedene Algorithmen, parallelisiert oder nicht

Befehl      | Zeit | Größe
------------|------|------
`gzip -1`   | 12s  | 264M
`gzip -9`   | 50s  | 205M
`bzip2 -9`  | 84s  | 154M
`lbzip2 -9` | 6s   | 155M


## unzip

- Kann .zip Files aus der Windows-Welt dekomprimieren.

## xz

- [XZ-Utilities](https://tukaani.org/xz/) sind eine Suite von Komprimierungstools
- Kernelquellen werden so komprimiert geliefert:
  `$ unxz linux-6.0.10.tar.xz`
- Wurde im  März 2024 durch eine schwerwiegende, weitreichende **Sicherheitslücke** bekannt, die durch 1-jähriges [Social Engineering](https://de.wikipedia.org/wiki/Social_Engineering_(Sicherheit)#:~:text=Eine%20bekannte%20Variante%20des%20Social,an%20die%20potentiellen%20Opfer%20versendet.) durch einen wahrscheinlich staatlicher Akteur eingeschleust wurde. [Eine Analyse der xz-Hintertür](https://www.heise.de/hintergrund/Eine-Analyse-der-xz-Hintertuer-Teil-1-9788145.html)

## Empfehlung

- klarer Favorit: `lbzip2` wegen bzip2recover und Parallelisierung


## siehe auch

- [Die 10 besten Komprimierungstools für Linux](https://de.moyens.net/linux/die-10-besten-komprimierungstools-fuer-linux/)
- [multi-threaded compression benchmarks](https://vbtechsupport.com/1614/)


## Aufgaben

- Besorgen Sie sich große Videos, Audio-Files, PDFs und Bilder in den Formaten
  - .png (Screenshot mit Flameshot),
  - .jpg/jpeg (Bild vom Handy),
  - .webp (Google Bildersuche),
  - .svg  (Google Bildersuche, `$ find /usr/share -name '*.svg'`)  
und komprimieren diese besser und schlechter (-1, -9) mit gzip und lbzip2 und tragen die Zeiten (time cmd) und um wie viel kleiner das Bild geworden ist, [hier](https://pad.gwdg.de/JYMazhuyTZaS8RNG42gq2g?both)  ein.
- In [Testbed](../Hilfen.d/Testbeds.md) ist beschrieben, wie Sie  (große) Bilder bekommen.

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Software <br />
letzte Änderung: 2024-01-22<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
