# tar

tarball: ![tarball](../Pics.d/tarball.jpg)  dt. [Erdölklumpen](https://en.wikipedia.org/wiki/Tarball_(oil))

## TL;DR

Macht aus vielen Files einen, daher für Daten-Transport und Daten-Speicherung nützlich.

## Basics

**t**ape **ar**chiver

- Dateien werden sequentiell zu einer großen Datei **zusammengehängt** (um sie dann sequentiell auf Band zu schreiben).
- erstes Release: 1979
- [home](https://www.gnu.org/software/tar/) , [bei UU](https://wiki.ubuntuusers.de/tar/) , [man page](https://www.mankier.com/1/tar#)
- Ein Tarball macht große Filesysteme (mit vielen Files) besser **handhabbar** für Datensicherung, Backup, Verteilung (z.B. [Kernel-Quellen](https://www.kernel.org/) ), Speicherung.
- Verbreitung:
  - *.tar: ca. 1.730 Mio. Ergebnisse
  - *.tar.gz: ca. 52 Mio. Ergebnisse
  - *.tar.bz2: ca. 28 Mio Ergebnisse


## Gebrauch

- die 3 wichtigsten Operationen:
  - `-c` **c**reate - Tarball erzeugen
  - `-x` e**x**tract - Tarball auspacken
  - `-t` **T**oC - listet den Inhalt eines Tarballs


- Optionen:
  - `-f tarball-name` - direkt nach -f muss der Name des Tarballs stehen
  - `-T listfile` - einzupackende Files stehen in Datei listfile, ein Name pro Zeile
  - `-z` (gzip) oder `-j` (bzip2) - (de-) komprimieren, `-j` bevorzugen

- das Verzeichnis Dir-XY in einen Tarball verwandeln und dabei mit bzip2 komprimieren, mit pseudo-relativem Namen arbeiten, damit beim Auspacken keine Probleme entstehen können:  
  `$ tar -c -vjf /tmp/Dir.tar.bz2 ./Dir-XY`
- den Inhalt eines Tarballs auflisten:  
  `$ tar -t -vjf /tmp/Dir.tar.bz2`
- einen Tarball auspacken:  
  `$ tar -x -vjf /tmp/Dir.tar.bz2`
- einen einzelnen File (my.pdf) aus einem Tarball herausziehen:  
  `$ tar -x -vzf  PDFs.tar.gz  my.pdf`
- "string" suchen im Tarball:  
  `$ bzgrep -a string  file.tar.bz2`
  - `-a` lässt grep den Binär-Fille wie einen Text-File verarbeiten


### Gleichartige Files einpacken

- Die Verwendung von 2 Befehlen bieten die Flexibilität, die Liste zu sichten und ggf. zu editieren:  
  
```sh
$ find . -name "*.txt" > /tmp/textfile-list.txt
$ tar -cvjf allfiles.tar.bz2 -T mylist.txt
```

- es ginge auch in einer Zeile. "-" steht für stdin:  
  `$ find . -name "*.txt" | tar -cvf allfiles.tar -T -`

## siehe auch

- [The tarp Utility](https://github.com/webdataset/tarp) - Tarballs manipulieren ohne auszupacken

## Aufgabe

Zerlegen Sie eine man page in ihre Sektionen und bilden aus den Bruchstücken einen Tarball:

```bash
cd /tmp && mkdir /tmp/TT && cd /tmp/TT 
man wc > ./wc.man.txt
csplit -f 4tar ./wc.man.txt  '/^[A-Z][A-Z]/' '{*}'
tar -cvf ./wc.man.tar  ./4tar*
tar -cvjf ./wc.man.tar.bz2  ./4tar*
ls -alh wc.man.*
```

- Erklären Sie die Größenunterschiede der 3 wc.man.* Files.
  - Sehen Sie sich dazu den Inhalt des unkomprimierten Tarballs mit kwrite an und erklären Sie den Inhalt mit Hilfe dieser Seite an: [tar (computing) - Wikipedia](https://en.wikipedia.org/wiki/Tar_(computing)).



<!---

Physically speaking, a TAR file is a linear sequence of blocks. A TAR file is terminated by two blocks containing zero bytes. Every block has a size of 512 bytes
Logically speaking, a TAR file is a linear sequence of entries. Every entry is represented by two or more blocks. The first block always contains the entry header. Subsequent blocks store the content of the file.

referencing how a tarball collects objects of all kinds that stick to its surface

Each file object includes any file data, and is preceded by a 512-byte header record. The file data is written unaltered except that its length is rounded up to a multiple of 512 bytes. 

most modern tar implementations fill the extra space with zeros.

512 Byte Blöcke: große Blöcke schreibt man schneler auf Band als viele kleine. Manche Laufwerke unterstützen nur fixed-length data blocks.


[In Windows stecken inzwischen curl und tar.](https://www.heise.de/hintergrund/Microsoft-und-Open-Source-Microsofts-Abkehr-von-der-Geheimniskraemerei-9614174.html?seite=all)


-->




---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  HowTo Software Storage<br />
letzte Änderung: 2024-11-20<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
