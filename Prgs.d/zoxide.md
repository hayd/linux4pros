# zoxide

a smarter cd command

- Faster way to navigate your filesystem
- ab jammy (22.04LTS) als .deb


- [Zoxide – Schneller am Terminal unter Linux navigieren](https://linux-bibel.at/index.php/2023/09/03/zoxide-schneller-am-terminal-unter-linux-navigieren/)
- [zoxide | webinstall.dev](https://webinstall.dev/zoxide/)
- [GitHub]  A smarter cd command. Supports all major shells.](https://github.com/ajeetdsouza/zoxide)
- [fzf Integration](https://www.linode.com/docs/guides/how-to-use-zoxide/)


## siehe auch

- autojump - shell extension to jump to frequently used directories
- [GitHub - wting/autojump: A cd command that learns - easily navigate directories from the command line](https://github.com/wting/autojump)


- fasd - command-line productivity booster (tot?)
- [GitHub - clvv/fasd: Command-line productivity booster, offers quick access to files and directories, inspired by autojump, z and v.](https://github.com/clvv/fasd)
- [GitHub - whjvenyl/fasd: Command-line productivity booster, offers quick access to files and directories, inspired by autojump, z and v.](https://github.com/whjvenyl/fasd)
- [Check fasd, it will blow your mind, be prepared! :) Fasd (pronounced similar to ... | Hacker News](https://news.ycombinator.com/item?id=5025348) 2013
- [Fasd - A Commandline Tool That Offers Quick Access to Files and Directories](https://www.tecmint.com/fasd-quick-access-to-linux-files-and-directories/)


zoxide does not appear to support file indexation either, just directory indexation. I agree with OP, fasd is the only tool I've found so far that does both, and unfortunately it's quite stale now. 

Of those fasd is possibly the best in that it is the only one which includes files as well as directories.

My favorite new command is zoxide. It's like a faster z, autojump, or fasd.




---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: 2do <br />
letzte Änderung: 2023-10-29<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>





