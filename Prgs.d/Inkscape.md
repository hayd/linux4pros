# Inkscape

freier SVG-Editor

- SVG: Scalable Vector Graphics
  - XML-basiert
  - .svg ist also Text: `$ kwrite /usr/share/zim/globe.svg`
- weniger für Zeichnungen geeignet, als zur Komposition von Grafiken aus Objekten (z.B. [Icons](https://www.heise.de/ratgeber/Mit-Inkscape-kostenlos-und-ganz-einfach-eigene-Icons-erstellen-7352614.html)), siehe [Gallery](https://inkscape.org/gallery/)
  - Die Objekte kann man unabhängig voneinander verändern.
- [home](https://inkscape.org/), [bei UU](https://wiki.ubuntuusers.de/Inkscape/)
- unangefochten die Nr. 1 unter den SVG-Programmen
- auch für das Editieren von PDFs und Ausfüllen/Umbauen von PDF-Formularen sehr geeignet
- Es gibt zahllose Tutorials, Anleitungen, Videos, z.B:
  - [Inkscape-Wiki](https://wiki.inkscape.org/wiki/index.php/Inkscape)
  - [Galleries](https://wiki.inkscape.org/wiki/Galleries)
  - [Einführung der TU Chemnitz](https://www.tu-chemnitz.de/urz/archiv/kursunterlagen/inkscape/index.html)
- Man kann sich bequem freie SGVs (auch für kommerziellen Gebrauch) von [Openclipart](https://openclipart.org/) zur Weiterverarbeitung ziehen, einer online media collection mit mehr als  160.000 Vektor-Graphiken.
- Inkscape unterstützt einige Grafiktabletts und auch die Drucksensitivität und Neigungssensitivität eines [Grafiktablett-Stifts](https://wiki.inkscape.org/wiki/index.php/Release_notes/1.0#powerpencil) .


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Grafik Software<br />
letzte Änderung: 2024-11-26<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
