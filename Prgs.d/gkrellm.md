# gkrellm

ein Systemmonitor

- [home](http://gkrellm.srcbox.net/) , [bei UU](https://wiki.ubuntuusers.de/GKrellM/)
- Man sollte einen Systemmonitor betreiben, der Zeitreihen anzeigt, um auffällige Abweichungen von einer base line zu sehen.
- Er gibt zahlreiche [Plugins](http://gkrellm.srcbox.net/Plugins.html): `$ apt-cache search gkrellm`
- Er wird nicht mehr gepflegt, aber es gibt keinen gleichwertigen  Ersatz.


## BubbleFishyMon, ein Plugin

- [gkrellm-bfm](https://github.com/JNRowe/bfm)
- zeigt alles auf einen Blick:
  - Fische: Netzwerk-Traffic
  - Wasserstand: RAM-Auslastung
  - Lustblasen: CPU-Auslastung
  - Wenn die Ente umkippt ist der Rechner überlastet.


## Alternative

[Conky](https://github.com/brndnmtthws/conky),  mühsame Konfiguration, daher gibt es eine eigene Sammlung 
[configs](https://github.com/brndnmtthws/conky/wiki/Configs)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Tools<br />
letzte Änderung: 2023-11-27<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>