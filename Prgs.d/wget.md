# wget

- ein GNU-Projekt
- erste Version: January 1996, aktuelle Version: Feb. 2022
- kann einen File herunterladen oder eine ganze Web-Site mit einem Befehl abziehen
- Es gibt zahlreiche Optionen, die bestimmen wie, mit welchen lokalen Anpassungen und Auslassungen der Web-Server-Inhalt auf den eignen Host gezogen wird:  `--convert-links` , `--cut-dirs=number` , `--no-host-directories` ,  `--no-parent` ,`--level`
- Kopiert eine Datei nach /tmp:  
`$ wget -r -L -P /tmp  -nd -np  https://owncloud.de/pDoz3N/EULA.tgz`  
  - `-r` recursive download
  - `-P` dort sollen die Downloads abgelegt werden, Default: $PWD


## 500 - Internal Server Error

Wenn der Download mit dieser Meldung [500 Internal Server Error](https://praxistipps.chip.de/http-error-500-das-ist-die-loesung_27602) abbricht, kann diese Option weiterhelfen: `--content-on-error` . Wirkung: wget versucht den Download, trotz Fehlermeldung des Web-Servers

## Komplizierter

Es ist manchmal komplizierter:

- Oft kann man Dateien aus einer Cloud über öffentliche Links herunterladen. Damit die Datei "hinter" dem öffentlichen Link und nicht die "Klick hier zum Herunterladen"-Seite heruntergeladen wird, muss "/download" an die URL gehängt werden.
- In wget muss man den Schalter `--content-disposition` (ggf. `--trust-server-names`) setzen, um den Dateinamen aus dem Header korrekt zu setzen.
- Rekursive Downloads gehen oft nicht, da die Cloud beim Versuch auf einen öffentlichen Link eines Verzeichnisses zuzugreifen ein Archiv erzeugt oder sich aktiv dagegen wehrt, dass Teile kopiert werden.
- Beispiel:  
`$ wget --content-disposition https://ownCloud.gdg.de/index.php/s/KejFyEAhfVuKxi4/download`


## siehe auch

- [home](https://www.gnu.org/software/wget/)
- [bei UU](https://wiki.ubuntuusers.de/wget/)
- [Wget2](https://linuxnews.de/2021/09/27/gnu-wget2-2-0-freigegeben/) - parallelisiertes wget

