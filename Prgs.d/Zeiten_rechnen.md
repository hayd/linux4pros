# Rechnen mit Zeiten

Berechnen von Zeitdifferenzen, addieren von Zeitintervallen


## date

- `date`  Standard-Unix-Kommando: print or set the system date and time
- mehr dazu: `$ man date`
- Komponenten des Zeitpunkts kann man auswählen und formatieren:  
  `$ date "+%k:%M:%S"`  
  `$ date "+%d.%b.%Y, %a, %H:%M"`
- kann auch [Zeitintervalle](https://www.netzware.net/post/quicktipp-datumsberechnung-im-linux-terminal) (`days week month year`) addieren, subtrahieren:  
  `$ date -d "$date -380 days" +%d.%m.%Y`  
  `$ date -d "$date +2 week" +%d.%m.%Y`
- ausführliche Erklärung bei [UU](https://wiki.ubuntuusers.de/date/)
  
  

## dateutils

- [home](http://www.fresse.org/dateutils/) mit vielen Beispielen
- `$ sudo apt install dateutils`
- Sammlung von Tools um mit Zeitpunkten und Zeitintervallen zu rechnen, z.B:
  - `dateadd` Add durations to dates or times
  - `datediff` Compute durations between dates or times
  - `dategrep` Grep dates or times in input streams
  - `dateseq` Generate sequences of dates or times
  - `datesort` Sort chronologically.
  - `datetest` Compare dates or times
- im Repo: dateutils - nifty command line date and time utilities

### Beispiel

Anwendungsbeispiele: Benchmarks über mehrere Tage, Lieferdatum berechnen

- Zeitdifferenz:
   `$ dateutils.ddiff 2021-10-10   2022-10-10`
- heute in 12 Wochen:
   `$  dateutils.dadd today  +12w`  
- Zeit-Slots für mündliche Prüfung erzeigen:  
  `$ dateutils.dseq 09:00:00 30m 16:00:00`



## Zeitspannen-Rechner im Netz

[Zeitspannen-Rechner](https://www.timeanddate.de/datum/zeitspanne-ergebnis?d1=1&m1=1&y1=2022&d2=7&m2=4&y2=2023) - Anzahl von Jahren, Monaten und Tagen zwischen zwei Daten berechnen.


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  HowTo Software<br />
letzte Änderung: 2025-02-09<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
