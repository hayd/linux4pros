# Video-Editoren

## Kdenlive

- Kdenlive ist der wohl umfassendste Videoeditor unter Linux; kann nicht nur Videos schneiden, sondern diese auch mit Effekten anreichern
- [home](https://kdenlive.org/de/) , [bei UU](https://wiki.ubuntuusers.de/Kdenlive/) , [bei WP](https://de.wikipedia.org/wiki/Kdenlive)
- Erscheinungsjahr: 2002, letzte Version vom Nov. 2024
- aus dem KDE-Universum
- stabil (oft ein Problem)
- Oberfläche frei konfigurierbar
- nutzt FFmpeg
- siehe auch
  - [bei LinuxReviews](https://linuxreviews.org/Kdenlive)
  - [Kdenlive - Professionelle Videobearbeitung unter Linux](https://linux-bibel.at/index.php/2023/09/17/kdenlive-professionelle-videobearbeitung-unter-linux/)


## andere Video-Editoren

- [Liste](https://wiki.ubuntuusers.de/Videobearbeitung/)
- [OpenShot](https://www.openshot.org/de/download/) - zeichnet sich durch seine intuitive Arbeitsweise auch für **Anfänger** aus.
  - [OpenShot 3.0 ist erschienen](https://gnulinux.ch/openshot-3-0-ist-erschienen) mit deutlich verbesserter Stabilität
  - in Python geschrieben, hohe [Entwickleraktivität](https://github.com/OpenShot/openshot-qt/tree/v3.3.0)
- [DaVinci Resolve](https://www.blackmagicdesign.com/de/products/davinciresolve/) - ein Editor des Herstellers Blackmagic Design, die sich an professionelle und ambitionierte Nutzer richtet. Besondere Stärke: **Farbkorrektur**
- [Flowblade](https://github.com/jliljebl/flowblade) - ein rel. neuer und noch bisher wenig bekannter Videoeditor, den man in etwa mit Kdenlive vergleichen kann.
  

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Video Software<br />
letzte Änderung: 2024-12-27<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
