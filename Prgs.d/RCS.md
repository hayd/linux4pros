# RCS

Revision Control System von GNU

- [home](https://www.gnu.org/software/rcs/)
- einfacher geht nicht
- Das folgende beschränkt sich auf die lokale Nutzung durch nur einen User, realisiert durch die Option `-l`. Bei höheren Anforderungen, mehr Usern git mit GitLab verwenden.


## Nomenklatur

- **working file**: Kopie einer Version, an der man arbeitet
- **archival file**: RCS file, Archiv-File, in dem alle Änderungen abgespeichert werden, enden auf ,v (versions)
- Wenn ein Sub-Dir ./RCS/ existiert wird xyz,v dort abgelegt, wenn nicht direkt neben xyz.
- **ci**: check in, speichert Inhalt des aktuellen working files als Differenz zu vorhergehenden Version im RCS file
- **co**: check out, holt eine bestimmte Version des working files wieder aus dem RCS-File


## Basics

RCS geht davon aus, dass mehrere Personen eine Datei bearbeiten.

- RCS sorgt mit Hilfe von Locks dafür, dass immer nur ein Autor eine Version verändern kann.
- `-l` = lock = exclusives Schreibrecht. Wenn man dieses nicht besitzt, hat man nur Leserecht für die Datei. D.h. nur wenn man die Version für die anderen locked, kann man sie selbst bearbeiten.
- Die Optionen -l , -u (lock, unlock) beziehen sich immer auf die Sicht der Allgemeinheit auf den achival file.
- Wenn man eine Version unlocked, kann man den working file nicht mehr editieren, da jetzt auch andere diese Version auschecken könnten.
- Wenn man ein Sub-Directory namens RCS anlegt, werden die archival files dort abgelegt und nicht neben den working files.
- Das Suffix ,v kann man als Markierung für wichtige Files verwenden.
- **Zusammenfassung**: alleine und mit -l arbeiten


## Gebrauch

- working file bleibt erhalten, man kann gleich weiterarbeiten, ohne -l oder -u wird Arbeitskopie gelöscht:  
`$ ci -l  file.md`
- History des Files ansehen:  
`$ rlog  file.md`
- Unterschiede anzeigen, letzte Version vs. working file:  
`$ rcsdiff file.md`
- Vergleich Version 1.1 mit 1.5:  
`$ rcsdiff -r1.1 -r1.5 file.md`
- ältere Version zum working file machen:  
`$ co -l -r1.1 file.md,v`

## Aufgaben

- .md File anlegen
- mehrmals ändern und jeweils einchecken
- History ansehen
- älteste und jüngste Version vergleichen



## siehe auch

- [Versionskontrolle mit RCS](https://www.jfranken.de/homepages/johannes/vortraege/rcs.de.html) - Beispiele, Flussdiagramme
- [Texte einfach, sicher und transparent verwalten](https://www.math.uni-bremen.de/zetem/cms/media.php/250/rcsintro.pdf) - Präsentation, Uni Bremen
- [RCS-Einführung, Tipps und Tricks](https://www.ostc.de/rcs.pdf), 15 S.
- [RCS Handbook](https://courses.cs.vt.edu/~cs2204/fall2004/resources/RCS_Handbook.pdf), 105 S.



---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Informationsmanagement Tool<br />
letzte Änderung: 2023-11-15<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
