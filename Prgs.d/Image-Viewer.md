# Image-Viewer

## gThumb

- [home](https://gitlab.gnome.org/GNOME/gthumb), [bei UU](https://wiki.ubuntuusers.de/gThumb/)
- **einfacher** Bildbetrachter
- für **schnelle Fotobearbeitung** geeignet: Bild drehen und dabei beschneiden, aufhellen
- einfache **Bearbeitungsfunktionen**: Skalieren, Zuschneiden, Drehen, Metadaten ergänzen; Helligkeit, Sättigung, Kontrast und Farben korrigieren


## display

- Unterprogramm von ImageMagick
- zeigt das Bild in **Original-Größe** - und sonst fast nicht
- "Image Info" im Kontext-Menü ist sehr ausführlich


## Alternativen

- [Liste von Bildbetrachtungsprogrammen](https://wiki.ubuntuusers.de/Grafik/#Bildbetrachtung-und-verwaltung) bei UU
- [feh](https://wiki.ubuntuusers.de/Feh/), [Geeqie](https://wiki.ubuntuusers.de/Geeqie/), [GPicview](https://wiki.ubuntuusers.de/GPicview/), [Gwenview](https://wiki.ubuntuusers.de/Gwenview/),  [Loupe](https://wiki.ubuntuusers.de/Loupe/)  (löst Eye of GNOME ab), [LXImage-Qt](https://wiki.ubuntuusers.de/LXImage-Qt/) , [Ristretto](https://wiki.ubuntuusers.de/Ristretto/) , [Viewnior](https://wiki.ubuntuusers.de/Viewnior/) , [Nomacs](https://wiki.ubuntuusers.de/nomacs/) , [gThumb](https://wiki.ubuntuusers.de/nomacs/) , ...

Hier sollte man die gesuchte Kombination an Features mit einer genehmen GUI finden.

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Empfehlungen Grafik Software<br />
letzte Änderung: 2024-11-26<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
