# clipboard, Zwischenablage

## Allgemeines

Linux kennt 2 Zwischenablagen für den Austausch von Daten zwischen den Fenstern:

1. primary clipboard
   - wird mit Maus-Selektion gefüllt
   - enthält nur eine Selektion
   - Inhalt kann mit mittlerer Maus-Taste wieder eingefügt (paste) werden
2. default clipboard
   - wird mit Ctrl-C gefüllt
   - sammelt alte Maus-Selektionen
   - enthält konfigurierbar viele Selektionen (size of history)


## Clipman, XFCE

- [home](https://docs.xfce.org/panel-plugins/xfce4-clipman-plugin/start)
- `$ sudo apt install xfce4-clipman-plugin`
- Damit kann man älter Maus-Selektionen zurückholen.
- Clipman kann beide Zwischenablagen verwalten, die primäre nur nach entsprechender Konfiguration: "Ignore selections" abwählen
- Clipman kann automatisch QR-Codes aus den Einträgen der Zwischenablage erzeugen.


## CopyQ

- [home](https://hluk.github.io/CopyQ/)
- `$ sudo apt install copyq copyq-doc copyq-plugins`
- _advanced clipboard manager with editing and scripting features_
- Kann alles und noch viel mehr.


## Siehe auch

- [Zwischenablage](https://wiki.ubuntuusers.de/Zwischenablage/) bei UU


<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Tools<br />
letzte Änderung: 2024-01-08<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>