# DigiKam


- bestes freies Bilderverwaltungs-Programm aus dem KDE-Universum für Linux
- [home](https://www.digikam.org/), [UU](https://wiki.ubuntuusers.de/digiKam/)
- Leuchttisch, diverse Möglichkeiten der (einfachen) Bildbearbeitung (drehen, ausschneiden, ...)
- ergänzte Meta-Daten auch im Bild abspeichern: `Album > Write Metadata to Files`
  - oder noch besser: mit dem [ExifTool](ExifTool.md) direkt in das Bild schreiben und dann "Reread Metadata From File"
  - Exit-Strategie!
- neueste Version im [Snap Store](https://snapcraft.io/digikam)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Grafik Software<br />
letzte Änderung: 2025-02-05<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>