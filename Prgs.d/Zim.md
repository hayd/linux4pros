
# Zim

## Anforderungen an ein persönliches Wiki

- Informationen (URLs, eigene Texte, Bilder, Videos) sammeln muss einfach sein.
- Exit-Möglichkeiten
- Markdown-Unterstützung
- Einsatzmöglichkeiten von Tools, die über Jahrzehnte entwickelt wurden, wie  grep, [pdfgrep](https://pdfgrep.org/), sed, Recoll, pandoc, VLC, rsync, ...
- leichtgewichtig


## Zim Features

ein Desktop-Wiki, als genereller Anfang eigener Notizen oder als Einstieg in größere Sammlungen gut geeignet

- [home](https://www.zim-wiki.org/) , [ChangeLog](https://github.com/zim-desktop-wiki/zim-desktop-wiki/blob/develop/CHANGELOG.md)
- als .deb im [Repo](https://packages.ubuntu.com/search?keywords=zim&searchon=names&suite=all&section=all) und im [PPA](https://www.zim-wiki.org/downloads.html)
- in **Python** und [GTK](https://de.wikipedia.org/wiki/GTK_(Programmbibliothek)) geschrieben
- Funktionalität durch [Plugins](https://github.com/jaap-karssenberg/zim-wiki/wiki/Plugins) erweiterbar
- Alle Inhalte stehen in **.txt Files** mit einer DokuWiki ähnlichen [Syntax](https://zim-wiki.org/manual/Help/Wiki_Syntax.html), d.h. **keine DB**, leicht zu durchsuchen, bearbeiten, portieren, transformieren, synchronisieren, sichern, ...
- Import, Export von **Markdown** möglich
- **WYSIWYG-Editor**
- integrierte **Suche**
- **URLs** leicht einzubinden
- Seiten einfach zu **vernetzten** und mit Portal-Seiten zu Themengebieten zusammenzustellen.
- Man kann Seiten **hierarchisch** ablegen, d.h. Seiten mit Unterseiten.
- **2do-Listen** mit Checkboxen
- Config Files liegen in ~/.config/zim/


## Gebrauch

mit Empfehlungen

- [Key Bindings](https://zim-wiki.org/manual/Help/Key_Bindings.html)
- **mehrere Notizbücher** oder nicht? Entscheidung leicht zu korrigieren, da alles in .txt Files und Re-Indexierung einfach.
- Bei vielen Seiten **alphabetische Unterteilung**, d.h. Seite "A" mit Sub-Pages, Seite "B" mit ...
- **CamelCase** für einfache Verlinkung besser abschalten und diese Syntax nutzen `:Thema` (oder Ctrl + l) bzw. `+Sub-Page` nutzen
- Seiten, auch zwischen Notebooks (**Inter-Notebook-Links**), sind durch drag 'n drop aus der History-Zeile im Editor leicht zu vernetzen.
  - explizit:  `Software?Languages:Java`
- Daten via Cloud bzw. [git Plugin](https://zim-wiki.org/manual/Plugins/Version_Control.html) sichern, synchronisieren (Datenschutz beachten)
- eigenes Verzeichnis für [Anhänge](https://zim-wiki.org/manual/Help/Attachments.html) anlegen
- [zim-clip](https://addons.mozilla.org/en-US/firefox/addon/zim-clip/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search) als Firefox add-on installieren, erleichtert Einbinden von URLs
- [Tags](https://zim-wiki.org/manual/Help/Tags.html) (keywords) für  Kategorisierung der Seiten
  - Syntax: `@tag`
  - Ein [Plugin](https://zim-wiki.org/manual/Plugins/Tags.html) zeigt alle Tags als tag cloud und in einer side pane.
- **Attachments**
  - alle in einem Directory sammeln, z.B. auf oberster Ebene der Notebook-Files ein ./Files.d
  - nicht in-line darstellen, sondern Aufruf mit geeignetem Viewer ([mupdf](https://wiki.ubuntuusers.de/MuPDF/), [geeqie](https://wiki.ubuntuusers.de/Geeqie/), ...), cut 'n paste-fähig hinterlassen.  
  Z.B: `mupdf ~/Zim/Files/xyz.pdf`
  
    - Vorteile: optimale Darstellung, kann skaliert und editiert werden, Zim-Seite und Attachment parallel sichtbar


## siehe auch

- [Ubuntu - Installing the latest ZIM Desktop Wiki from the PPA](https://github.com/jaap-karssenberg/zim-wiki/wiki/Ubuntu-PPA)
- [Notizen: Zim & Joplin im Vergleich](https://linuxnews.de/2021/01/07/21559/)
- [Getting Work Done in Zim](https://www.glump.net/content/getting-work-done-in-zim/getting-work-done-in-zim.html#advanced-topics)
- [How to use Zim, a multi-tasking desktop Wiki](https://www.techrepublic.com/article/how-to-use-zim-a-multi-tasking-desktop-wiki/)
- [Arround Zim Desktop](https://wiki.ordi49.fr/doku.php/en:tech:dev:around_zim_desktop)
- [Das plattformübergreifende Desktop-Wiki Zim](https://www.linux-community.de/ausgaben/linuxuser/2017/03/tagebuch/)


## Aufgaben

Ziel: Entscheidung für oder gegen Zim auf Grund eigener (sparsamer) Erfahrungen.

- Zim installieren
- min. 3 Seiten anlegen mit URLs und untereinander verlinken
- an eine Seite eine Sub-Page (`+SubPage`) anhängen
- Binden Sie ein Video nach obigen Empfehlungen ein.
- Erstellen Sie eine 2do-Liste.

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Empfehlungen Informationsmanagement Software<br />
letzte Änderung: 2025-02-03<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
