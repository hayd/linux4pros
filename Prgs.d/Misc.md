# Gute Programme für ...

## Zeiterfassung


Timewarrior is Free and Open Source Software that tracks time from the command line.
• https://timewarrior.net/
• .deb: timewarrior - feature-rich time tracking utility
• -> sudo apt install timewarrior

-> timew start IT-Zulage
-> timew stop           # Tag kann weggelassen werden
-> timew summary


## Lupe für Bildschirm

kmag screen magnifier tool


## Farbe auswählen oder identifizieren

kcolorchooser    color chooser and palette editor

- Anzeige in 2 Farbmodellen



---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  HowTo Software Storage<br />
letzte Änderung: 2025-02-20<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>


<!---


-->
