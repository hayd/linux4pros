# Recoll

eine Dokumentensuchmaschine

- Recoll bietet eine Suche über praktische alle Daten in den unterschiedlichsten Formaten, die auf dem Rechner liegen.
- Suchmaschine: [Xapian](https://en.wikipedia.org/wiki/Xapian)
- geschrieben in: C++,  Qt
- ab Werk werden [folgende Formate](https://www.lesbonscomptes.com/recoll/pages/features.html) unterstützt:
  - Text-Dateien (.txt)
  - Markdown (.md)
  - HTML-Dateien (.html)
  - Mails (maildir und mailbox)
  - iCalendar-Dateien (.ics)
  - Mozilla-Kalenderdaten (.ics / .sqlite)
  - Scribus-Dateien (.sla)
  - man pages
  - Dia-Diagramme (.dia)
- mit zusätzlichen Programmen, Filtern werden unterstützt:
  - Abiword-Dateien (.abw)
  - Microsoft Office Open XML (.docx)
  - Powerpoint & Excel (.ppt & .xls), Filter: catdoc
  - LibreOffice/Apache OpenOffice (.odt/.ods/.odc/.odp)
  - Scalable Vector Graphics (.svg/.svgz)
  - PDF
  - PostScript-Dateien (.ps)
  - tar-Archive
  - ZIP-Archive
  - RAR-Archive
  - Rich Text Format (.rtf)
  - LaTeX-Dateien (.tex)
  - DVI-Dateien (.dvi)
  - Audio-Metadaten (z.B. Song-Titel aus MP3-Files)
  - Metadaten von Bildern (z.B. Kamera-Modell in .jpg)


## Installation

- Recoll und alle benötigten Konvertierungsprogramme sind im [Repo](https://packages.ubuntu.com/search?keywords=recoll&searchon=names&suite=all&section=all)


## Konfiguration

- Alle Daten und config files liegen in ~/.recoll/
- Es ist vor allem das zu konfigurieren:
  - **Einstiegsverzeichnisse**, in denen die Indexierung gestartet werden soll: topdirs
  - Verzeichnisse, die ausgelassen werden sollen: **skippedPaths**
  - **Datentypen**, die nicht indexiert werden sollen. Die Default-Werte sind bereits gut gewählt.
  - ggfs. ein [cronjob](../Vorlesung.d/cron.md) für den regelmäßigen **Update** des Indexes


## Gebrauch

- Man kann sich mehrere Suchen einrichten, z.B. für alle beruflich relevanten Daten und für die privaten. Hierfür muss man recoll mit unterschiedlichen config files aufrufen. Z.B:
  `$ recoll -c ~/.recoll.job/`
- Die Indexierung kann man per cron job oder aus der GUI heraus starten. Der Index kann mehrere GB groß werden.
- Alle Suchbegriffe sind defaultmäßig AND-verknüpft.


## Aufgaben

- Testbed einrichten gemäß [Testbeds](../Hilfen.d/Testbeds.md): `## Viele Text-Dokumente`
- installieren: `$ sudo apt install recoll`
- starten: `$ recoll &`
- konfigurieren:  `Preferences` > `Index configuration`: "Top directories" und "Skipped paths"
- über Testbed laufen lassen: `File` > `Update Index`
- Zeit für Indexierung stoppen, Größe des Index-Files ermitteln im Vergleich zu den Daten
- Suchen Sie (z.B. nach IPv6 Multicast, DNS server, Xerox, cisco, trump, docsBpiMulticastCmMacAddress, Reliability)
- Wo ist der Index entstanden und wie groß ist er? Nutzen Sie dafür [find](./find.md).


## siehe auch

- [Faqs and Howtos](https://www.lesbonscomptes.com/recoll/faqsandhowtos/index.html)
- [Search tips](https://www.lesbonscomptes.com/recoll/usermanual/usermanual.html#RCL.SEARCH.GUI.TIPS)
- [bei UU](https://wiki.ubuntuusers.de/Recoll/)
- [in der Linux Bibel Oesterreich](https://www.linux-bibel-oesterreich.at/viewtopic.php?t=1274&sid=623ea538b3556295a6bde7a6c5d49eb8)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: HowTo Informationsmanagement<br />
letzte Änderung: 2024-11-20<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
