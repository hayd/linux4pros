# ExifTool

Kommandozeilen-Werkzeug zum Lesen und Schreiben der Metainformationen von Bild-, aber auch Audio- und Video-Dateien: "Read, Write and Edit" der Metadaten

- [home](https://exiftool.org/) , [source](https://github.com/exiftool/exiftool) , [exiftool Application Documentation](https://exiftool.org/exiftool_pod.html) , [FAQs](https://exiftool.org/faq.html),  [bei UU](https://wiki.ubuntuusers.de/ExifTool/)
- unübertroffener **Klassiker**  zur Metadaten-Bearbeitung, zum Zusammenführen von Foto-Serien,
- kann **Metadaten** (Tags, Kommentare, Bildunterschriften, ...) untrennbar ins Bild schreiben und nicht in die DB eines  Bildverwaltungsprogramm (Exit-Strategie). Fast alle Bildverwaltungsprogramme beherrschen ein Metadaten-Harvesting.
- hochgradig optimiert, gereift; kann große Bildsammlungen mit einem Kommando bearbeiten
- in Perl


## Gebrauch

- alle Metadaten anzeigen:  
  `$ exiftool -s foto.jpg`
- alle Metadaten (aus Datenschutzgründen entfernen):  
  `$ exiftool -all= foto.jpg`
- Metadaten setzen:  
  `$ exiftool -Caption-Abstract="My Caption" -Headline="My Title" foto.jpg`
  - Metadaten immer in das Bild schreiben und nicht in eine DB des Foto-Verwaltungsprogramms, z.B. [digiKam](https://wiki.ubuntuusers.de/digiKam/)

### mehrere Foto-Sammlungen an Hand des Datums zusammenführen

- sehr hilfreich, wenn man Bilder aus mehreren Quellen chronologisch zu einer Sammlung (z.B. Weihnachtsfeier) zusammenführen will. Bedingung: korrekte Zeit im EXIF-Header
- Prinzip: Alle Bilder werden nach ihrem Entstehungsdatum benannt:  
  `$ exiftool '-filename<CreateDate' -d %y%m%d_%H%M%S%%-c.%%le $PWD`
  - `-filename<CreateDate` means rename the image file using the image's creation date and time.
  - `-d`   Set format for date/time values
  - `%y%m%d`  hat übliche Bedeutung, y mit 2 Ziffern
  - `%H%M%S`  hat übliche Bedeutung, jeweils mit 2 Ziffern
  - `%%-c` means that if two images have the same file name up to this point in the naming process, add "a copy number which is automatically incremented" to give each image a unique name.
  - `.%%le` means keep the original file name extension, but make it lower-case if it was originally upper-case - normally .jpg
  - `$PWD` bezeichnet das Directory, in dem die Bilder umbenannt werden sollen


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Grafik HowTo Software<br />
letzte Änderung: 2024-11-27<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>