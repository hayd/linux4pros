# ImageMagick

ImageMagick ist ein großes, freies Softwarepaket zur Erstellung und Bearbeitung von Rastergrafiken von der Kommandozeile aus. Es kann momentan mit mehr als 200 Bildformaten umgehen.

ImageMagick umfasst 11 Unterprogramme, z.B:

- **convert** - convertiert Bildformate
- **compare** - gibt die Unterschiede 2-er Bilder als Differenzbild aus
- **composite** - überlagert mehrere Bilder zu einem
- **display** - zeigt nur Bild (keine Menüs, ...) in Originalgröße, unter jeder Maustaste liegt aber ein Menü
- **identify** - gibt Informationen über das Bild aus, mit `-verbose` sehr viele
- **montage** - montiert mehrere Bilder zu einem großen

## Einsatzszenarien

ImageMagick ist unschlagbar, wenn es gilt

- viele Bilder zu bearbeiten,
- immer wiederkehrende Routinearbeiten zu automatisieren,
- komplexe Bearbeitungen zu dokumentieren und in einem Shell-Script festzuhalten.

Es macht oft einen unprofessionellen Eindruck, Bilder so weiterzugeben, wie sie aus dem Handy herausfallen: zu groß, zu große Ränder, keine Markierungen, mit verräterischen Metadaten (Zeit, Ort)



## Dokumentation

bei der Vielzahl der Möglichkeiten und Komplexität einiger Optionen besonders wichtig

- [home](https://imagemagick.org/script/index.php)
- [command-line tools](https://imagemagick.org/script/command-line-tools.php) - [neue Syntax](https://imagemagick.org/script/porting.php) ab ImageMagick Version 7
- [Options](https://imagemagick.org/script/command-line-processing.php#option)
- [unterstützte Formate](https://www.imagemagick.org/script/formats.php)
- [Examples of ImageMagick Usage](https://legacy.imagemagick.org/Usage/) - unzählige Beispiele thematisch sortiert
- [bei UU](https://wiki.ubuntuusers.de/ImageMagick/)
- [GraphicsMagick](http://www.graphicsmagick.org/) - ein Fork, der  effizienter, kleiner, sicherer, fehlerfreier, ... sein will.


## Optionen

[Liste aller Optionen](https://imagemagick.org/script/command-line-options.php), einige oft benötigte:

- `-scale` erlaubt keine zusätzlichen Filter, schnell
- `-resize` erlaubt Filter, Liste der Filter: `$ convert -list filter`, default: [Lanczos](https://en.wikipedia.org/wiki/Lanczos_resampling) -Filter => besser als scale
  - [What is the difference for sample/resample/scale/resize/adaptive-resize/thumbnail](https://stackoverflow.com/questions/8517304/what-is-the-difference-for-sample-resample-scale-resize-adaptive-resize-thumbnai)
  - [Resize or Scaling](https://legacy.imagemagick.org/Usage/resize/)
- `-thumbnail` geschwindigkeitsoptimiert, eingebettete [Farbprofile](https://de.wikipedia.org/wiki/Farbmanagement#Farbprofile) werden entfernt
- `-density 72`
- `-colors 256`
- `-fuzz x%` = Farben innerhalb dieses Abstands werden als gleich betrachtet.


## Informationen über ein Bild

- ohne  `-verbose` ein Einzeiler, mit 80 -160 Zeilen:  
  `$  identify  -verbose bild.jpg`


## Format konvertieren

- Wahl der Suffixe bestimmt Umkodierung:  
  `$ convert bild.png  bild.pdf`
- und dabei EXIF-Metadaten entfernen:  
  `$ convert -strip  bild.jpg bild.pdf`


## Bild verkleinern

### Zahl der Pixel reduzieren

- Qualität der JPG-Kompression reduzieren, kleiner Wert (0-100) => viele Artefakte  
  `$ convert  bild.jpg   -quality 60  bild60.jpg`
- Größe halbieren:  
  `$ convert -resize 50% bild.jpg  bild50.jpg`
- Breite 200 Pixel:  
  `$ convert bild.png -resize 200 bild200.png`
- Höhe 100 Pixel:  
  `$ convert bild.png -resize x100 bild100.png`
- Größe ändern  mit erzwungener (!) Verzerrung:  
  `$ convert bild.png -resize 200×100! bild.png`
- auf eine vorgegeben Pixel-Zahl verkleinern:  
  `$ convert bild.gif  -resize 4096@  bild.4096pixel.gif`


### Format wechseln

- [TIFFs](https://de.wikipedia.org/wiki/Tagged_Image_File_Format) (fallen oft aus Scanners raus) sind meist sehr groß. Eine derartige Umkodierung kann das Bild um den Faktor 25 verkleinern:  
  `$ convert bild.tiff -quality 70  bild70.jpg`


## Bild vergrößern

- Beim Vergrößern ist der Einsatz eines Weichzeichners (-gaussian-blur) sinnvoll:  
  `$ convert bild.tiff -strip -interlace Plane -gaussian-blur 0.05 -quality 70 bild.jpg`


## Bild beschriften

- Text in Bild einbauen:  
  `$ convert bild.jpg -background white label:'Bild mit Smileys' -gravity center -append bild+text.jpg`
  - `-background Farbe`: Hintergrundfarbe des Textes
  - `-append`  Schrift vertikal unter dem Bild,  `+append` horizontal neben dem Bild
  - `-label:' Text '` der eingesetzte Text
  - weitere Optionen: `-fill Farbe` (Schrift-Farbe), `-pointsize 16` (Schriftgröße), `-gravity west|east|center` (Position des Textes), `-font helvetica`


## Filter

- verfügbare Filter auflisten:  
  `$ convert -list filter`


## Misc

- `-strip`: EXIF-**Metadaten** werden entfernt
- Hintergrund **transparent** machen, Weiss ist meist nicht nur genau ein Farbton =>  `-fuzz`:  
  `$ convert bild.pdf -fuzz 2% -transparent white bild2.png`
- **schiefes** Bild geraderücken (mit [gThumb](https://wiki.ubuntuusers.de/gThumb/) einfacher, da mit GUI und Beschneidung):  
  `$ convert -rotate 11.9   bild.jpg bild-rot.jpg`
- in **PDF** in A4-Größe wandeln:  
  `$ convert bild.png -background white -page a4 bild.pdf`
- Bild **schärfen**, das dabei evtl. sehr groß wird, entscheidend ist der Wert nach `0x`:  
   `$ convert bild.jpg -sharpen 0x3.0 bild-scharf.jpg`


## Bilder zusammen-montieren

- Bilder auf einer A4-Seite zusammenkopieren:  
`$ montage  bild1.jpg  bild2.jpg -tile 1x2 -geometry +2+1 -page A4   P.jpg`
- `-tile`: Zahl der Zeilen und Spalten
- `-geometry`  {Breite}x{Höhe}+{X}+{Y} ,  ohne +2+1: Bilder zu klein auf A4 Seite


## Aufgaben

- Besorgen von Bildern für die Aufgaben siehe [Testbeds](../Hilfen.d/Testbeds.md#bilder)
- Lösen Sie die Aufgaben mit Scripten, mit denen man die Erkenntnisse jederzeit mit anderem Bildmaterial reproduzieren kann.
- Wählen Sie  geeignetes Ausgangsbild für die Aufgabe. Ein schräger schwarzer Strich auf weißem Hintergrund ist vielleicht besser geeignet ein [Wimmelbild](https://unishop.rub.de/Wimmelbild-Poster-in-A2/RUB10019).
- Setzen Sie das Startbild und das Ergebnis der Bemühungen und das Script in das [Pad 5](https://pad.gwdg.de/CW3nJ65gSVKJ3aRAOKZRsA?both#). 

Anwendungsfälle aus der Praxis:

- Folien einer Online-Präsentation mitnehmen, die man nicht im Original bekommen wird (Apple).
- Dokumentation eines Schadensfalls in vielen Bildern, die per Mail an Hersteller gehen sollen.
- Texte als PNG oder JPEG speichern?
- Ausschnittvergrößerung um Fahrraddieb zu identifizieren.


Die Aufgaben:

1. **Vergrößern** Sie ein  Bild um den Faktor 10 in x-Richtung. Mit welchem Filter und welchen **Filter-Parametern** erzielt man das beste Ergebnis (keine Pixel erkennbar, Gesicht wirkt noch natürlich und nicht verschwommen, ...) ?
2. Machen Sie den Unterschied zwischen **scale** und **resize** sichtbar.
3. Machen Sie einen großen Screenshot eines Textes oder schreiben Sie einen Text mit Inkscape (in verschiedenen Farben auf grauem Hintergrund). Ab welcher **JPEG-Qualität** werden **Blasen** um die Buchstaben sichtbar? Leiten Sie daraus eine Empfehlung ab.
4. Konvertieren Sie ein hochaufgelöstes PNG-Bild in 2 JPEG-Bilder der Qualität 10 und 90. **Vergleichen** Sie die 3 Bilder **graphisch**.
5. **Montieren** Sie alle Bilder eines Verzeichnisses (> 10) auf eine A4-Seite. Diese Seite darf nicht größer (in MB, nicht cm) als 25% der Summe aller Bilder sein.
6. Schreiben Sie ein Bash-Script, das Bilder **verkleinert**, bis die als Parameter übergebene Größe in kByte erreicht ist. Der Inhalt der Bilder muss noch erkennbar sein.
7. Wandeln Sie 10 Screenshots in möglichst kleine (MB), noch gut lesbare PDFs und hängen Sie diese **chronologisch** zum einem einzigen  **PDF** zusammen (`pdfunite`).
8. Wie viele **Farben** braucht das Bild eines Herbstwaldes mindestens um noch natürlich zu wirken?
9. Welches der Fotos des Paketes "ubuntu-mate-wallpapers-photos" hat den höchsten **Blau-Anteil**?

Lösungen bitte in [Pad 5](https://pad.gwdg.de/CW3nJ65gSVKJ3aRAOKZRsA?both#) eintragen.

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Grafik Software<br />
letzte Änderung: 2025-02-07<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
