# grep

## Allgemeines

- **g**lobal (search) **r**egular **e**xpression (and) **p**rint
- grep durchsucht Files nach Strings oder [REs](../Vorlesung.d/REs.md)
- fgrep: fast grep, kann keine REs
- egrep: beherrscht extended regular expressions, = `grep -E`
- ideal zur Suche in großen Haufen an Textfiles, z.B. Log-Files, Quellcode, PDF-Sammlungen, ...


## Konfiguration

- `export  GREP_COLOR='01;31'`
- `alias grep="grep  --color=always -s -i "` # Treffer in Farbe, suppress error messages, ignore-case
- [Modifying the color of grep](https://askubuntu.com/questions/1042234/modifying-the-color-of-grep)



## Performance

- Messungen in [linux-6.0.9.tar](https://www.kernel.org/): 35.145.060 Zeilen in 5.047 Dirs
- Suche nach Nishanth, 52 Treffer
- Suche im Filesystem auf einem Standard-PC:  
  `$ time egrep -r Nishanth linux-6.0.9` => real 0m0,835s
- Suche im Tarball (ein großer File, Teerklumpen):  
  `$ time grep -a  "Nishanth" linux-6.0.9.tar` => real 0m0,401s
- Wozu eine **Datenbank**, wenn es grep gibt ? [Flat-file database](https://en.wikipedia.org/wiki/Flat-file_database)  + grep statt relationaler DB oft ausreichend, schneller und wesentlich einfacher
- [Kap. 5 in der GNU Grep 3.8 Doc](https://www.gnu.org/software/grep/manual/grep.html#Performance) beschreibt an Hand der Algorithmen und deren Implementierung, wann es zu Performance-Problemen kommen kann. `LC_ALL='C'` ist z.B. wichtig.


## Optionen

- `-r`
- `-v` (in**v**ert), selektiert Zeilen ohne Match
- `-o` (**o**nly), gibt nur den Match aus, nicht die ganze Zeile; beste Möglichkeit zum Extrahieren von Strings
- `-A 2` (after)  gibt zusätzlich 2 Zeilen nach Treffer aus
- `-B 5` (before) gibt zusätzlich 5 Zeilen vor Treffer aus
- `-c` (count) nur Zahl der Treffer, nicht diese selbst
- `-a` deklariert einen (fälschlich) als binary erkannten File gegenüber grep zum Text-File. Das erlaubt z.B. die Suche in Tarballs oder Bildern.
- `-f Datei`  mehrere Suchmuster aus Datei übernehmen
- `-w` matched nur ganze Worte


## Beispiele

`$ alias grep="egrep  --color=always"`  # damit man Treffer sieht

- Hat das System (Alle Daemons, der Kernel und manche Userprogramme schicken Nachrichten über ihre Aktivitäten an [syslogd](https://de.wikibooks.org/wiki/Linux-Praxisbuch/_Syslog) .) heute (Datum anpassen) Fehler gemeldet:  
    `$ cd /var/log &&  grep -i error syslog.1 | grep "Oct\s*24"`
- oder ist gar Schlimmeres passiert in der Vergangenheit:  
  `$ zcat syslog*gz | egrep -i "crit|alert|emerg"`
- Alle Zeilen ohne Zahl:  
   `$ echo -e " abc \n 123 \n x4z" | grep -v [0-9]`
- Max als Wort suchen:  
   `$ echo -e " Max \n Maximilian \n Max Muster \n Max-Uwe" | grep -w Max`
- IP-Nummern in /etc suchen:  
   `$ egrep -r  "([0-9]{1,3}\.){3}[0-9]{1,3}" /etc 2>/dev/null`
- siehe auch [30 Grep Examples](https://linuxhint.com/30_grep_examples/)


## agrep, tre-grep

= fehlertolerante Suche.

- Ein einziger Rechtschreibfehler kann einen ganzen Text unauffindbar werden lassen!
- Mit [tre-agrep](https://wiki.ubuntuusers.de/tre-agrep/) steht eine Neuimplementierung von agrep zur Verfügung, die auf der [TRE-Library](https://github.com/laurikari/tre) (tre = **t**ext **r**eg. **e**x.) basiert.
- 1 Fehler ist: eine Auslassung, eine Einfügung, eine Ersetzung - Zahl der Zeichen spielt keine Rolle
- Optionen:
  - `-1` mit 1 Fehler gemäß [Levenshtein-Distanz](https://de.wikipedia.org/wiki/Levenshtein-Distanz), max. 9 Fehler zulässig
  - `--color` matches einfärben
  - `-e PATTERN`  PATTERN als regular expression
  - `-B` best  match
- Die folgenden Beispiele mögen erklären, was "1 Fehler" bedeutet:  
  
```bash
echo "abcdef"    | tre-agrep  --color  "abc" -1 
echo "a123bcdef" | tre-agrep  --color  "abc" -1  # d.h. das a darf verloren gehen
echo "a1b2cdef"  | tre-agrep  --color  "abc" -1
echo -e "Mueller \nMueler \nMüller \nMuueller \nMöller \nMoeller" | tre-agrep --color -1 Mller
```

- Suche in  Arztbriefen:  
`$ echo -e "Katar  \nKatarh \nKatarrh" | tre-agrep --color -1 Katar`


- Installation: `$ sudo apt install tre-agrep`


## Alternativen

- [ack](https://beyondgrep.com/) - für große source code Bäume
- [The Silver Searcher](https://github.com/ggreer/the_silver_searcher) - schneller als ack
- [ripgrep is faster than {grep, ag, git grep, ucg, pt, sift}](https://blog.burntsushi.net/ripgrep/) - kann auch komprimierte Files durchsuchen


## Übungen

- Beispiele schrittweise nachvollziehen (copy 'n paste), verstehen, erklären
  

## siehe auch

- [grep\_colors at master](https://github.com/Makpoc/dotfiles/blob/master/grep_colors) - Script, das unterschiedliche Farben für die Matches setzt
- [pdfgrep](../PDF.d/suchen.md)
- [grep Linux Tutorial mit Beispielen](https://kushellig.de/linux-unix-grep-befehl-beispiele/)
- [Using grep (and egrep) with Regular Expressions](https://linuxhint.com/grep_egrep_regex/)
- [GNU Grep 3.8](https://www.gnu.org/software/grep/manual/grep.html) - ausführlich, offizielle Doku
- agrep
  - [Unscharfe Suche in Texten mit Agrep](https://www.linux-community.de/ausgaben/linuxuser/2016/01/besser-finden/)
- [tre-agrep, a Grep Replacement](https://www.linux-magazine.com/Issues/2016/186/Command-Line-tre-agrep)

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Programmierung<br />
letzte Änderung: 2025-01-26<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>


