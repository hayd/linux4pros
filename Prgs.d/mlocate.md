# locate, mlocate , plocate

... wissen, wo die Dateien und Ordner im Filesystem liegen.


- Suche und Indexierung aller Files auf einem Rechner.
- [home](https://pagure.io/mlocate), [UU](https://wiki.ubuntuusers.de/locate/)
- **mlocate** (m=merge) ist schneller als locate, da es bereits vorhandene Indices weiterverwendet und nicht immer neu erstellt.
- `$ sudo apt install mlocate`
- [plocate](https://plocate.sesse.net/) ist nochmals schneller als mlocate, ab Ubuntu 21.04 im Repo.


## Index

- /var/lib/mlocate/mlocate.db
- Bei Installation wird ein cron-Job für regelmäßige Indexierung angelegt: /etc/cron.daily/mlocate
  - Indexierung selbst anstoßen: `$ sudo updatedb`
- Liste, was nicht indexiert wird: /etc/updatedb.conf  
- Zahl der Einträge:  
   `$ locate "*" | wc -l`
  - 1,6 Mio Files => Index: 43MB


## Gebrauch

- Wie oft steht .bashrc im Index?  
   `$  locate -c .bashrc`  
   `-c`: count
- Wieviele Videos liegen auf dem PC?  
   `$  locate --regex -i "(\.mp4$|\.avi$)"`  
   `-i`: ignore-case
- Wo liegen meine Markdown-Files:
   `$ locate -r ".*helmut.*\.md$"`  
   `-r`: basic regexp REGEXP instead of patterns


## GUI

- [Catfish File Search](https://docs.xfce.org/apps/catfish/start)

## Aufgaben

1. Was ist installiert? locate oder mlocate oder plocate?
1. Wie groß ist der Index im Vergleich zu den Daten?
1. Beispiele für Suche mit Patterns
1. In welcher Sprache ist mlocate geschrieben?
1. Warum ist plocate viel schneller?


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Tools<br />
letzte Änderung: 2025-01-31<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
