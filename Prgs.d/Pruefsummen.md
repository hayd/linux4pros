# Prüfsummen

- Prüfsummen erlauben die **Integrität** (unveränderter Zustand) von Datei zu überprüfen
  - Integrität kann durch Übertragungsfehler, gezielte **Manipulationen**, Fehler in der lokalen Software oder Elektronik, kosmische Strahlung, ... verloren gehen
- [kollisionsresistent](https://de.wikipedia.org/wiki/Kollisionsresistenz):  es ist schwer, verschiedene Eingaben zu finden, die auf denselben Wert abgebildet werden. D.h. böswillige **Manipulationen** können nur schwer versteckt werden.
- Prüfsummen werden durch **Hashfunktionen** berechnet. Diese liefern schnell, immer gleich lange, einzigartige Hash-Werte

## Berechnung

- Einzelstücke:  
`$ echo "aaa" | md5sum`
`$ cat /etc/passwd | md5sum`

- Massen-Hashing:  
`$ find /etc -type f -exec md5sum {} > /tmp/etc-checksums.md5 \;`

- Verifizieren:  
`$ md5sum -c  -w --quiet /tmp/etc-checksums.md5`

- Optionen:  
`-c`: check der Integrität an Hand einer vorhandenen Prüfsumme
`--quiet`: sonst sieht man vor lauter OK-Zeilen nichts  
`-w`: Warnung bei Formfehlern in der Liste  


## MD5

- MD5: [Message-Digest Algorithm 5](https://de.wikipedia.org/wiki/Message-Digest_Algorithm_5)
- Heute ist bekannt, dass MD5 keine hohe Kollisionsresistenz bietet.
- Befehl: md5sum
- 128-Bit-Hashwert => 32-stellige Hexadezimalzahl


## sha224sum, sha256sum, sha384sum, sha512sum

- SHA: [Secure Hash Algorithm](https://de.wikipedia.org/wiki/Secure_Hash_Algorithm)
- Befehle: sha224sum, sha256sum, sha384sum, sha512sum
- Zahl gibt Bits der Prüfsumme an
- Syntax wie bei md5sum
- 256 => 64 Zeichen
- 512 => 128 Zeichen


## Aufgaben

- Geschwindigkeitsunterschied zwischen md5sum und sha512sum


## Siehe auch

- [UbuntuHashes](https://help.ubuntu.com/community/UbuntuHashes)
- [Hashfunktionen](https://wiki.ubuntuusers.de/Hashfunktionen/)



---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Security Tools<br />
letzte Änderung: 2024-11-20<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
