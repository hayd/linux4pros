
# sed

= stream editor

- 1974 veröffentlicht
- sed unterliegt keinen (realen) Beschränkungen hinsichtlich der Dateigrößen.

## Arbeitsweise

1. Input-File wird zeilenweise eingelesen.  
2. Die Zeile landet im Pattern Space. Dort werden die Anweisungen umgesetzt.  
3. Das Ergebnis wird auf stdout geschrieben.


- beherrscht [REs](../Vorlesung.d//REs.md).
- Folgende Zeichen muss man maskieren: `( )` und  `[ ]`

## Performance

- 35.145.059 Zeilen (Kernel-Quellen)
- PC der Messung: AMD Ryzen 5, 3.40-3.90GHz, SSD
- `$ time  sed -i 's/Nishanth/Nishhhhanth/'  kernel-sources.txt`
- 52 Ersetzungen in 0m6,515s


## Einsatzmöglichkeiten

- Zeilennummern im Text ergänzen
- Leerzeilen einfügen
- Suffixe vereinheitlichen (bild.jpeg --> bild.jpg)
- [Refactoring](https://de.wikipedia.org/wiki/Refactoring), z.B. Variablennamen in großen Programmen ändern
- Schreibweise korrigieren, vereinheitlichen in langen Texten
- automatisierte Anonymisierung von Texten (089/987654 --> 089/9xx)
- alle Filenamen sanieren ("neue Öffnungszeiten.pdf" --> neue_Oeffnungszeiten.pdf)
- ...


## Syntax

`sed '/Beginn/,/Ende/ c/alt/NEU/' inputfile`

- `Beginn`, `Ende` können auch entfallen. Sie definieren die Grenzen, zwischen denen die Anweisung ausgeführt wird.
- `c`: eine der folgenden Anweisungen



### Anweisungen

- `s` (substitute) einmalige Ersetzung auf einer Zeile
  - mehrmalige Ersetzungen mit g (global):  
       `sed  's/alt/NEU/g'`
- `y`  (yank = herausziehen)  transformiert "alt" in die entsprechenden Zeilen von  "NEU", d.h. beide Zeichenmengen müssen gleich mächtig sein
   `sed  'y/alt/NEU/'`
- Es gibt auch noch: **a**ppend, **c**hange, **d**elete, **i**nsert, **n**ext, **p**rint, **q**uit, **r**ead

## Optionen

- `-i`  (inplace) Input-File wird verändert, nicht nach stdout geschrieben
- `-E, -r`  verwende extended regular expressions
- `/alt/neu/g` g (global), sonst nur eine Ersetzung


## Character Class definieren

- `[  ]` begrenzen eine [Character Class](https://www.gnu.org/software/sed/manual/sed.html#Character-Classes-and-Bracket-Expressions). `{ }` können nicht verwendet werden, da diese bereits Kommandos gruppieren (s.u.).
- die Sonderzeichen `:.*,` ersetzen:  
`$ echo 'aaa * . : , zzz' |  sed 's/[:.*,]//g'`


## Delimiter ändern

- Backslash-Orgien zwecks Maskierung z.B. in Pfadnamen (ersetze "a/b/c/" durch "d/e/f/"):  
   `$ echo "/a/b/c/" | sed 's/\/a\/b\/c\//\/d\/e\/f\//'`
- Für das "s" Kommando kann jeder beliebe Delimiter verwendet werden. sed interpretiert das auf "s" folgende Zeichen (hier ;) als Delimiter:  
   `$ echo "/a/b/c/" | sed 's;/a/b/c/;/d/e/f/;'`


## Beispiele

HTML lässt fasst mehrere Blanks in der Darstellung zu einem zusammen!

1. Suffix (.md) entfernen:  
   `$ echo "/usr/share/doc/udisks2/README.md" | sed 's/\.md$//'`
1. alle Zahlen entfernen, g (global) entfernt nicht nur den 1. Treffer:  
   `$ cat /etc/fstab | sed 's/[0-9]*//g'`
1. Erstes Wort einer Zeile ausgeben, den Rest löschen mit Hilfe von back-references:  
  `$ cat /proc/cpuinfo | sed 's/\([a-z]*\).*/\1/'`
1. die Reihenfolge von 2 Worten vertauschen ([Begründung](https://de.wikipedia.org/wiki/Lorbeerkirsche)) mit back-references:  
   `echo "Kirsch-Lorbeer" | sed 's/\([a-Z]*\)-\([a-Z]*\)/\2\-\1e/'`
1. eine, zwei Leerzeilen (für Anmerkungen des Korrektors) einfügen:  
   `$ cat /etc/fstab | sed G`
   `$ cat /etc/fstab | sed 'G;G'`
1. Zeilennumern vor jede Zeile schreiben:  
   `$ sed '=' /etc/apt/sources.list`
1. Kommata durch Zeilenumbruch ersetzen:  
   `$ echo "Zeile1 am Anfang, Zeile2,Zeile 3" | sed "s/,/\n/g"`
1. Leerzeilen löschen:  
   `$ echo -e "Zeile 1 \n\nZeile 2 \nZeile 3" | sed '/^$/d'`
1. nur ein Wort pro Zeile als Beginn einer linguistische Analyse (Worthäufigkeiten):  
   `$ man ls | sed "s/ /\n/g" | egrep -v "^$" | sort`
1. Entfernung der Blanks am Zeilenende (werden manchmal interpretiert z.B. bei Markdown), angezeigt mit `cat -vte`  
  `$ echo "3 blanks am Zeilenende   "  | cat -vte`  
  `$ echo "3 blanks am Zeilenende   " | sed 's/[ \t]*$//' | cat -vte`
1. Alle Zeilen 3 Blanks einrücken, die keine URL enthalten:  
    `$  sed '/http/!s/^/   /g'  /etc/apt/sources.list`  
1. Löschen der ersten 10 Zeilen:  
    `$ sed '1,10d' /etc/apt/sources.list`
1. Vokale durch (entsprechende) Zahlen ersetzen (um eine regelgerechte pass phrase zu erzeugen):  
    `$ sed 'y/aeiou/12345/' /etc/apt/sources.list`

## Zusammengesetzte Kommandos

- Gruppierung durch `sed  '{    }'`
- So kann man z.B. eine Markup-Syntax in eine andere wandeln.
  
```bash
# simple sed script to convert from moinmoin to GitLab-Wiki syntax

sed  '{
# reformat H1, H2, H3
   s/^===/###/
   s/^==/##/  
   s/^=/#/
   s/=*.$//
# unordered list, 2nd level
   s/^  \*/ -/
# unordered list, 1st level
   s/^ \*/-/
# ToC
   s/<<TableOfContents.*/\[\[_TOC_\]\]/
# URLs
   s/\(\[\[http.*\|\)\(.*\]\]\)/\1/
# in line code
   s/^{{{/```sh/
   s/^}}}/```/    
   }' input-file.txt
```


## Aufgaben

- Beispiele schrittweise nachvollziehen (copy 'n paste), verstehen, erklären


## siehe auch

- [Handy one-liners for SED](https://edoras.sdsu.edu/doc/sed-oneliners.html)
- [Sed - Snipcademy](https://snipcademy.com/shell-scripting-sed)
- [50 sed Command Examples](https://linuxhint.com/50_sed_command_examples/) - sehr lehrreich
- [An Introduction and Tutorial by Bruce Barnett](https://www.grymoire.com/Unix/Sed.html) - 2400 Zeilen
- [Die GNU Doku](https://www.gnu.org/software/sed/manual/sed.pdf), PDF, 85 S., von Jan. 2020
- [Sed Tutorial](https://www.tutorialspoint.com/sed/index.htm)
- [Eine Einführung in sed](https://tty1.net/sed-tutorium/sed-tutorium.pdf), PDF, 31 S.
- [Einführung, Tipps und Tricks](https://www.ostc.de/sed.pdf), PDF, 25 S.
- [Suche nach Mustern über mehrere Zeilen](https://www.tutonaut.de/sed-suche-nach-mustern-ueber-mehrere-zeilen/)
- [SED - eine kleine Einführung und Übersicht über ein mächtiges UNIX-Werkzeug](https://linupedia.org/opensuse/Sed) - gute Erklärung der Funktionsweise
- `$ man fold` - wrap each input line to fit in specified width


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Programmierung<br />
letzte Änderung: 2024-10-23<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
