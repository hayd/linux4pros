# sort , uniq

## sort

[sort](https://www.mankier.com/1/sort) sortiert Files zeilenweise


### Beispiele

`$ sort -r -t ":" -n -k 3 /etc/passwd`

- `-r` (reverse) Umkehrung der Sortierreihenfolge
- aber: `-R` (random) Sortierung in zufälliger Reihenfolge
- `-t` Feld**t**renner wird definiert
- `-n` **n**ummerische nicht lexikalische Sortierung
- `-k 3`  (key) sortiert nach Inhalt der 3. Spalte (Zählung beginnt mit 1)

### siehe auch

- [bei UU](https://wiki.ubuntuusers.de/sort/)
- [Gut sortiert](https://www.linux-community.de/ausgaben/linuxuser/2005/01/zu-befehl-sort/) - aus  LinuxUser-Magazin



## uniq

- uniq kann mehrfach vorkommende Zeilen manipulieren, wenn sie direkt aufeinander folgen. Damit das der Fall ist, ist meist vorher ein `sort` fällig.

### Optionen

- default: Ausgabe der sortierte Dateien ohne doppelte Zeilen
- `-d` (double, nicht delete) nur doppelte Zeilen ausgeben
- `-u` nur einzigartige Zeilen ausgeben
- `-c` (count) Vor jeder Zeile steht, wie oft sie vor der Behandlung mit uniq im File stand.


### Beispiele

`echo | tr`   um ein minimales Beispiel mit mehreren Zeilen zu generieren

- bereinigte Datei anzeigen:  
- `$ echo "aa:bb:cc:aa:dd:dd:cd:dd" |   tr ":" "\n" | sort | uniq`
- ohne vorherige Sortierung aber kein Effekt  
  `$ echo "aa:bb:cc:aa:dd:dd:cd:dd" |   tr ":" "\n" |  uniq`
- Wie oft kam jede Zeile vor?  
  `$ echo "aa:bb:cc:aa:dd:dd:cd:dd" | tr ":" "\n" | sort | uniq -c`
- nur die einzigartige Zeilen ausgeben:  
   `$ echo "aa:bb:cc:aa:dd:dd:cd:dd" | tr ":" "\n" | sort | uniq -u`
- nur doppelte Zeilen ausgeben:  
  `$ echo "aa:bb:cc:aa:dd:dd:cd:dd" | tr ":" "\n" | sort | uniq -d`


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Tools<br />
letzte Änderung: 2023-11-27<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>