# file

bestimmt den Filetyp

- dafür wird nicht das Suffix herangezogen, sondern [3 Sets an Tests](https://de.wikipedia.org/wiki/File), z.B. dateitypspezifische Muster in den ersten Bytes einer Datei



## Bilder

- Bilder erkennen unabhängig von der Endung:
` $ file -i *`


- weitere Metadaten wie Bildauflösung

```bash
$ cp file.jpg file.txt
$ file *
file.jpg: JPEG image data, JFIF standard 1.02, aspect ratio, density 100x100, segment length 16, baseline, precision 8, 480x360, components 3
file.txt: JPEG image data, JFIF standard 1.02, aspect ratio, density 100x100, segment length 16, baseline, precision 8, 480x360, components 3
```

- [man page](https://www.mankier.com/1/file)



---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Tools<br />
letzte Änderung: 2025-01-31<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
