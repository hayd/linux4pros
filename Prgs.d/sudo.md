# sudo

## TL;DR

- **Nie dauerhaft als root arbeiten!**
- sudo ist eigentlich zu kompliziert für den Desktop. `doas` ist eine einfache Alternative.


## Basics

- sudo = **su**peruser **do**
- sudo — execute a command as another user
- für **Rechnernetze 1980** entwickelt => für Desktop-Rechner eigentlich zu kompliziert. Meist passt aber die Default-Konfiguration.
- sudo merkt sich das Passwort für 15 Min.


Es ist kompliziert:

- [Privilegienstufen](https://de.wikipedia.org/wiki/Privilegienstufe) (abhängig von der CPU-Architektur), [capabilities](https://www.mankier.com/7/capabilities#Description-Capabilities_list), [Pluggable Authentication Modules](https://de.wikipedia.org/wiki/Pluggable_Authentication_Modules) (PAM) uvm. spielen bei der Rechte-Zuweisung eine Rolle.
- `$ sudo command` verschafft einem User **Zugriff auf Dateien ohne Beschränkungen** für die Dauer eines Kommandos. Mehr nicht, da dieser Befehl immer noch in user space läuft. Da Dateien unter Linux eine zentrale Rolle spielen  ("Everything is a file"), wird trotzdem  praktisch alles möglich, zumindest indirekt. Es können z.B. Kernel-Module installiert werden, die Schaden anrichten.
- **Container**- und Virtualisierungs-Techniken erfordern eine komplexe Rechte-Abstufung.


## Konfiguration

- Wenn die Default-Konfiguration geändert werden soll, muss man genau wissen was man tut.
- **Syntax** : User <space> OnHost = (Runas-User:Group) <space> Commands
- typischer Eintrag in /etc/sudoers:  `root ALL = (ALL:ALL) ALL`
- das bedeutet:  User Root can Run Any Command as Any User from Any Group on Any Host.


## Editieren

- Nicht trivial, daher vorher informieren oder Finger weg.
- /etc/sudoers editieren;
`$ sudo cp /etc/sudoers  /etc/sudoers.orig`  
`$ EDITOR=vim.tiny  sudoedit  /etc/sudoers`
- Die untersten Einträge haben die höchste Priorität.



## siehe auch

- [sudo bei ubuntuusers.de](https://wiki.ubuntuusers.de/sudo/)
- [Konfiguration bei ubuntuusers.de](https://wiki.ubuntuusers.de/sudo/Konfiguration/)
- [mit Root-Rechten arbeiten bei ubuntuusers.de](https://wiki.ubuntuusers.de/mit_Root-Rechten_arbeiten/)


## doas

- `doas` ist eine Alternative zu dem für den Desktop-Anwender oft völlig überfrachteten Platzhirsch sudo.
- ab jammy (22.04LTS) in Repo

- [Linux-Rechtemanagement: Sudo durch Doas ersetzen](https://linuxnews.de/linux-rechtemanagement-sudo-durch-doas-ersetzen/)
- [Mit Doas statt Sudo administrative Aufgaben erledigen](https://www.linux-community.de/ausgaben/linuxuser/2021/08/mit-doas-statt-sudo-administrative-aufgaben-erledigen/)

## Aufgaben

- Installieren Sie einen Editor mit doas.


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Tools<br />
letzte Änderung: 2025-01-20<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
