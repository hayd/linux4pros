# Screenshot-Programme

- nützlich z.B. für Dokumentation von Online-Präsentationen


## Flameshot

- [home](https://github.com/flameshot-org/flameshot), [bei UU](https://wiki.ubuntuusers.de/Flameshot/)
- im Repo
- viele **Bearbeitungsmöglichkeiten** direkt nach dem Screenshot, z.B. Nachjustieren des Ausschnitts.
  - Ausschnitt um 1 Pixel verschieben: Pfeiltasten
  - Man kann auch Zahlen, Pfeile platzieren in separate Layers (ab [Version 12](https://gnulinux.ch/flameshot-12)) z.B. für **bebilderte Dokus**
  - Farben und Dicke der Zeichen-Objekte sind einstellbar
- Namen können auch konfiguriert werden. Man kann/sollte Zeitangaben einbauen.
- Konfiguration im Kontext-Menü des Icons
- config file: ~/.config/Dharkael/flameshot.ini
- kann auch über die Kommandozeile gesteuert werden:  
   `$ flameshot gui -p /tmp`


## siehe auch

- [Flameshot entdeckt Bildschirmfotos neu](https://www.linux-community.de/ausgaben/linuxuser/2018/09/bitte-recht-freundlich-2/) - aus LinuxUser 09/2018
- [lange Liste](https://wiki.archlinux.org/title/Screen_capture) von Screenshot-Software



## Aufgabe

- konkrete Anwendung: Mitschnitt einer Online-Präsentation (, die nicht verteilt werden darf)
- 12 Screenshots machen von Texten in einem Browser
- zentrale Begriffe mit Pfeil markieren
- ein Bash-Script schreiben, das folgendes leistet:
  - Screenshots in PDFs wandeln (mit ImageMagick)
  - chronologisch zum einem PDF zusammenhängen


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Grafik Software<br />
letzte Änderung: 2025-02-05<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
