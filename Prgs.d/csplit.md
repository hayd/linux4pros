# cspilt

split a file into sections determined by context lines

- gehört zu den Bordmitteln
- Syntax: `csplit [OPTION]... FILE PATTERN...`
- csplit (context split) kann einen File an Hand inhaltlicher Kriterien, definiert mit Hilfe von REs, in Teile zerlegen. Die Namen der Fragmente kann man aus einem fixen Teil (Default xx.) und einem Zähler, der von csplit pro Fragment um 1 erhöht wird, zusammenbauen.
- Beim Splitten gibt csplit die Größe der Fragmente in #Zeichen aus.
- Einsatz:
  - In einen File soll an einer bestimmten Stelle etwas eingefügt werden. Eine Lösung kann sein, den File mit `csplit` zu zerlegen und mit `cat` unter Einfügen der Ergänzung wieder zusammenzubauen. Voraussetzung: ein geeignetes csplit-Muster


## Beispiele

- zerlegt einen Text-File an den Leerzeilen (`/^$/`). Die Namen der Fragmente (`-f old.`) werden von einer hochzählten Nummer ergänzt.  
  `$ csplit a.txt  -f old. "/^$/" {*}`
- zerschneidet a.txt an den Zeilen 12 und 23:  
  `$ csplit a.txt 12 23`
- zerschneidet b.txt nach jeder 5. Zeile und zählt die Namen hoch. `-k`  = keep-files, kein Abbruch mit Löschen der Fragmente, wenn nicht durch 5 teilbar:  
  `$ csplit -k b.txt 5 {*}`

## Aufgaben

- Kontrollieren Sie die Ergebnisse mit [bat](https://wiki.ubuntuusers.de/bat/).
- Zerschneiden Sie die man page von ls an den Leerzeilen.
- Zerlegen Sie das [Telefonverzeichnis LRA Cham ](https://www.landkreis-cham.de/media/36396/telefonverzeichnis_nov_2023_internet.pdf) so, dass in einem File die Anfangsbuchstaben der Nachnamen gleich sind.

<!--
{*}      => repeat the previous pattern as many times as possible
--> 




## siehe auch

- [mankier](https://www.mankier.com/1/csplit)
- [csplit - Splitting a File Into Two or More on Linux Command Line](https://www.putorius.net/csplit-splitting-files-on-linux-command-line.html)



---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Informationsmanagement Software<br />
letzte Änderung: 2023-11-25<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
