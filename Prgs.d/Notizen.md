# Notizen

## Allgemeines
- gelbe Klebezettel

## notes, XFCE
- [home](https://docs.xfce.org/panel-plugins/xfce4-notes-plugin/start)
- `$ apt install xfce4-notes-plugin`
- kein Ctrl-s nötig
- Notizen liegen in: ~/.local/share/notes/Notes



## Siehe auch
- [Notizen](https://wiki.ubuntuusers.de/Notizen) bei UU
