# pandoc

## Basics

- [Pandoc](https://wiki.ubuntuusers.de/Pandoc/) ist geschrieben von einem Berkeley-Professor für Philosophie und Logik: [John MacFarlane](https://www.paris-iea.fr/en/fellows/john-macfarlane-2) startete das Projekt 2006 -  ursprünglich, um  [Haskell](https://de.wikipedia.org/wiki/Haskell_(Programmiersprache)) zu lernen
- beliebte Basis für Publishing Workflows, z.B. vom lokalen Markdown-Dokument zum Seite auf dem Web-Server
- [Liste](https://pandoc.org/#) der konvertierbaren Formate
- kann durch Plug-ins erweitert werden


## Syntax und Optionen

`pandoc -f markdown -t odt eingabe.md -o ausgabe.odt`

- `-f` FORMAT (=from) Format der Eingabedatei
- `-t` FORMAT  (=to) Format der Ausgabedatei
- `-s`  (standalone) Ausgabedatei enthält bereits passende Header- und Footer-Felder  
- `-o` DATEI Ausgabe in DATEI, nicht nach stdout
- `--toc` Automatische Generierung von Inhaltsverzeichnis (ToC) in Ausgabedokument


## Gebrauch

- PDF aus Text-File erzeugen:  
  `$ pandoc input.txt --pdf-engine=wkhtmltopdf  -o output.pdf`
  - [wkhtmltopdf](https://wkhtmltopdf.org/) : Command line utilities to convert html to pdf or image using WebKit
  - Default-Engine: LaTeX, ist wesentlich umfangreicher
- Seiten aus einem [Twiki, Foswiki](https://en.wikipedia.org/wiki/TWiki) nach Markdown konvertieren:  
`$ pandoc -f twiki -t markdown_github Twiki.txt  -o Markdown.md`


### Markdown für Präsentationen

- wandelt Markdown in eine  HTML-Präsentation im slidy Stil  
`$  pandoc -t slidy -s $file.md -o $file.slidy.html --metadata pagetitle="$file" --slide-level=2`
  - slidy - Gliederung kann man einblenden
- siehe auch:
  - [Schnell Präsentationen erstellen mit Markdown, Pandoc und Beamer](https://www.gonicus.de/aktuelles/20220720-praesentation_mit_pandoc/)
  - [Presentations with Slidy](https://ox-it.github.io/OxfordIDN_shiny/slidy_presentation_format.html)


## siehe auch

- [Pandoc User’s Guide](https://pandoc.org/MANUAL)
- [Markdown und Pandoc](https://kofler.info/ebooks/markdown_pandoc/)
- [pandoc Examples](https://pandoc.org/demos.html), 39 Stück
- [Pandoc Power-Features](https://programm.froscon.org/2015/system/attachments/334/original/froscon2015-pandoc-power-features.html)
- [Pandoc](https://baireuther.de/page/pandoc/) - Dokumente konvertieren
- [Pandoc Filters](https://github.com/jgm/pandoc/wiki/Pandoc-Filters)
- [bei UU](https://wiki.ubuntuusers.de/Pandoc/)


## Aufgaben

1. Konvertieren Sie die 2 Backup-PDFs in [dieser Sammlung](https://owncloud.gwdg.de/index.php/s/TJ8jikG7S1n3DdZ) mit pandoc nach Markdown (oder besser ePub?), so dass folgende Anforderungen erfüllt werden:
    1. Formatierung nahe am Original
    2. als Mail-Attachment auf dem SmartPhone gut zu lesen
1. Werden die Ergebnisse besser, wenn man die PDFs zuerst in einen Text-File konvertiert?
1. Konvertieren Sie mit dem [pandoc online Tool](https://pandoc.org/try/)  einen MarkDown-Text (aus Zim) nach PDF als slice show.
  
---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: HowTo Software<br />
letzte Änderung: 2025-02-05<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
