# Verlustfreie Komprimierung

## Prinzip

1. **Identifizierung** möglichst großer, mehrfach vorkommender **Blöcke**. Das ist am aufwendigsten.
2. Anlegen eines **Dictionaries** (Lagers) für diese Blöcke
3. effektive **Ver-Pointer-ung** der Blöcke, kurze Pointer auf häufig Blöcke
4. Das Komprimat wird möglichst nur noch aus **Verweisen** auf die Blöcke und eine einmalige Kopie dieses Blockes aufgebaut.


## Abschätzung

Sie wollen jemand den Text am Telefon durchsagen. Je aufwendiger das ist, desto schlechter wird eine Komprimierung.

Vergleiche [dd-Übung](../Filesystem.d/Everything_is_a_file.md).

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  HowTo<br />
letzte Änderung: 2025-02-07<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
