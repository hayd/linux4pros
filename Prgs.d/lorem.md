# lorem

Ich brauche mal schnell viel/ganz viel Text für einen Test.

lorem erzeugt zufälligen Text, der lateinisch wirkt.


- `$ sudo apt-get install libtext-lorem-perl`
- 20 Absätze erzeugen:
  `$ lorem -p 20`
- Man kann auch Zahl der Worte (w) oder Sätze (s) vorgeben.
