# hunspell   - Spell Checker

- Rechtschreibkorrektur auf der Kommandozeile, und noch mehr (stemmer and morphological analyzer)
- Installation mit Wörterbüchern:  
`$ apt install hunspell hunspell-tools`  
`$ apt install hunspell-de-de hunspell-de-med hunspell-en-us`
- `alias hunspell='hunspell -d en_US,de_DE'`
- Korrekturen werden direkt im Fenster vorgenommen.
- Arztbrief korrigieren:  
  `$ hunspell -d de_DE,de_med befund.txt`


| Befehl        |   Aktion
| :------------ | :------------
| R             | Das Wort komplett ersetzen  
| space         | Das Wort dieses eine Mal akzeptieren  
| A             | Das Wort für die derzeitige Sitzung akzeptieren  
| I             | Das Wort akzeptieren, und in das Benutzerwörterbuch aufnehmen  
| U             | Das Wort akzeptieren und eine kleingeschriebene Version aufnehmen  
| S             | Einen Stamm, und entsprechende Beispielwörter erfragen, und aufnehmen. Der Stamm wird auch mit den Affixen der Beispielwörter akzeptiert  
| 0 , 1 , 2 ... | Ersetzung mit einem der vorgeschlagenen Wörter  
| X             | Prüfung abbrechen, und nächste Datei bearbeiten (falls vorhanden)  
| Q             | Sofort abbrechen; Bestätigung mit y erforderlich, Datei bleibt unverändert  
| Strg + Z      | Programm pausieren, Wiederaufnahme mit fg-Befehl
| ?             | Anzeigen der Hilfe

**Qualitätsvergleich von OCR-Texten**:

- print only correct words or lines  
`$ hunspell -G -d de_DE text.ocr.txt`
- print misspelled words  
`$ hunspell -l -d de_DE  text.ocr.txt`

## Aufgabe

- Untersuchen Sie die in LaTeX geschriebene [Nutzerordnung](../Materialien.d/Nutzerordnung.tex) auf Rechtschreibfehler mit Hilfe ihres Lieblingseditors und mit Hunspell auf der Kommandozeile.


## siehe auch

- [home](https://github.com/hunspell/hunspell)
- [Hunspell bei UU](https://wiki.ubuntuusers.de/Hunspell/)
  

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: HowTo PDF Software<br />
letzte Änderung: 2025-02-03<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
