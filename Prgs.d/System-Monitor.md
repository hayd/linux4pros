# System-Monitor

- wichtig: Zeitreihe statt Tacho - sonst werden Ausreißer übersehen

## gkrellm
![gkrellm](../Pics.d/gkrellm.png)

- Systemmonitor mit [Plugins](http://gkrellm.srcbox.net/Plugins.html) und Themes
- [home](http://gkrellm.srcbox.net/), [UU](https://wiki.ubuntuusers.de/GKrellM/)
- `$ apt install gkrellm gkrellm-cpufreq gkrellm-bfm gkrelltop`
- gkrellm-bfm: Wenn Ente umkippt, Rechner platt. Wenn Fische hektisch werden, sollte man wissen warum.
- Entwicklung stagniert wohl




## Alternativen
### Conky
- schlanker Systemmonitor für jeden Desktop unter Linux, Ausgabe im  Root-Fenster (verliert man leicht aus dem Blick), erweiterbar
- [home](https://github.com/brndnmtthws/conky), [UU](https://wiki.ubuntuusers.de/Conky/), [Linux Bibel](https://www.linux-bibel-oesterreich.at/viewtopic.php?f=50&t=461&sid=4ff93ac339e9ee4fd33ed6f5507e632f&p=1390#p1390)
- Die Einarbeitung in die Conky-Konfigurationsdatei erfordert Zeit.
  - [Configs 4 Conky](https://github.com/brndnmtthws/conky/wiki/Configs)


### Glances
- System-Monitor für die Kommandozeile
- [home](https://github.com/nicolargo/glances/wiki), [source](https://github.com/nicolargo/glances), [docs](https://glances.readthedocs.io/en/latest/)
- in Python basierend auf der Bibliothek [psutil](https://github.com/giampaolo/psutil)

### Stacer
- system optimizer and application monitor
- [https://oguzhaninan.github.io/Stacer-Web/](home)

## siehe auch
- [Best System Monitoring Tools for Ubuntu](https://linuxhint.com/best_system_monitoring_tools_for_ubuntu/) - 9 Stück
- [Best System Monitoring tool for Linux](https://linuxconfig.org/best-system-monitoring-tool-for-linux) - Conky, Stacer, GNOME System Monitor, Bashtop, hTOP



## Aufgaben
- beste Alternative
- gkrellm nachbauen
