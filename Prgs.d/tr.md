# tr

tr transliterate - ersetzt eine Zeichenmenge durch eine andere
  
- = [sed](./sed.md) light
- tr ersetzt systematisch eine **Zeichen-Menge** durch eine andere.
- Sind die Mengen nicht gleich mächtig, wird es unübersichtlich, sollte man [sed](./sed.md) nutzen.
- Syntax: `tr [OPTION]... SET1 [SET2]` , SET in  `' '`
- vordefinierte Zeichenmengen:  `[:digit:]`, `[:alpha:]` , `[:lower:]`, `[:upper:]` . Es gibt noch [mehr](https://www.mankier.com/1/tr).


## Optionen

- `-s` (squeeze) mehrere identische aufeinanderfolgende Zeichen durch ein einzelnes ersetzen
- `-d`  delete statt ersetzen


## Beispiele

1. Vokale durch passende Zahlen ersetzen:  
  `$ echo 'abcdefghi-o-u' | tr 'aeiou' '12345'`  
  `$ echo 'abcdefghi-o-u' | tr 'aeiou' '1245'`
1. Großschreiben:  
  `$ echo "Windows oder Linux" | tr [:lower:] [:upper:]`  
  `$ echo "Windows oder Linux" | tr [a-z] [A-Z]`
1. Umlaut ersetzen:  
  `$ echo Müller | tr ü ue`  
1. aber:  
         `$ echo "äöü" | tr 'äöü' 'aeoeue'`  , d.h. sed verwenden
1. Tel.-Nr. entfernen, mehr oder weniger auffällig:  
   `$ echo "Max Muster, mobile; +49 1234 56789" | tr -d '0-9'`  
   `$ echo "Max Muster, mobile; +49 1234 56789" | tr '13579' '24680'`
1. viele Blanks --> 1 Blank:  
  `$ echo " zu    viele    blanks  ." | tr -s ' '`
1. Zeile an ":" umbrechen:  
   `cat /etc/passwd | tr ":" "\n"`


## Aufgaben

- Erklären Sie obige Beispiele.



## siehe auch

- [tr: Translate Characters -> Syntax und Beispiele](http://www.prontosystems.org/tux/tr)
- [tr command in Linux with 10 Examples](https://linuxopsys.com/topics/tr-command-in-linux)
- [Tr Command in Linux with Examples](https://linuxize.com/post/linux-tr-command/) - 5 min read


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Tools<br />
letzte Änderung: 2024-12-06<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
