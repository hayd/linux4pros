
# KeePassXC

## Notwendigkeit

- Für jeden Server ein anderes Passwort, da [viele Server](https://en.wikipedia.org/wiki/List_of_data_breaches) gehackt werden.
  


## der Passwortmanager

- [KeePassXC](https://keepassxc.org/) ist ein community **fork** von KeePass.
- [Open Source](https://github.com/keepassxreboot/keepassxc), [Security Review](https://molotnikov.de/docs/KeePassXC-Review-V1-Molotnikov.pdf)
- KeePassXC ist **plattformunabhängig**, läuft also unter Windows, macOS und Linux und integriert sich auf Wunsch auch direkt in den Browser.
- KeePassXC unterstützt die Verwendung von Schlüsseldateien (auf USB-Stick) und [YubiKey](https://de.wikipedia.org/wiki/YubiKey).
- Die Passwörter werden in einer mit sehr starker Verschlüsselung gesicherten Datenbank-Datei, auch als “Container” bezeichnet, abgelegt.
- config file: ~/.config/keepassxc/keepassxc.ini
- im Repo
- Anbindung an die *Have I Been Pwned*-Datenbank
- [Browser-Integration](https://keepassxc.org/docs/KeePassXC_GettingStarted.html#_setup_browser_integration) (Auto-Type)  für Chrome und Firefox
- Empfehlungen:
  - mehrere DBs anlegen, z.B. für On-Line Shopping, Bnking, Server-Zugänge
  - Kopie der DBs bei Online-Bank  in virtuellen Tresor einer Bank legen, Eh-da, d.h. Zugang mit TAN gesichert


## siehe auch

- [home](https://keepassxc.org/)
- [KeePassXC: User Guide](https://keepassxc.org/docs/KeePassXC_UserGuide.html#_setup_browser_integration)
- [Changelog](https://github.com/keepassxreboot/keepassxc/blob/develop/CHANGELOG.md)
- [source](https://github.com/keepassxreboot/keepassxc)
- [bei UU](https://wiki.ubuntuusers.de/KeePassXC/)
- [25 Passwortmanager für PC und Smartphone](https://www.heise.de/select/ct/2021/5/2101108560374094255) - Test der c't 5/2021
- [bei Kuketz IT-Security](https://www.kuketz-blog.de/keepassxc-auto-type-und-browser-add-on-im-alltag-nutzen-passwoerter-teil1/)
- [von Heinrich-Heine-Universität Düsseldorf](https://wiki.hhu.de/display/HHU/Passwortmanager+nutzen+-+Beispiel+anhand+von+KeePassXC)
- [von TU Braunschweig](https://doku.rz.tu-bs.de/doku.php?id=software:keepassxc)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Empfehlungen Security Software<br />
letzte Änderung: 2024-12-03<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
