# Rechnen


## bc

bc = basic calculator

- inzwischen nicht nur basic sondern: _arbitrary precision calculator language_
- Nach dem Aufruf befindet man sich in der bc Umgebung, in der man arbeitet und die man mit quit wieder verlässt.
- amerikanisches Format bezüglich `.` und `,`
- `-l`  ohne diese Option nur Integer-Operationen
- kann man in Scripte einbauen:
  - `$ echo " 7/2 " | bc`
  - `$ echo " 7/2 " | bc -l`
  - `$ echo " scale=3; 7/2 " | bc -l`
  - `$ echo " scale=300; 22/7 " | bc -l`
- kann man nutzen um Last zu erzeugen für Tests, Benchmarks:  
  `$ time echo "scale=5000; 4*a(1)" |  bc -l`
  - berechnet den Arkustangens von 1 (= Pi) auf 5000 Stellen genau.  Trigonometrischen Funktionen zu berechnen ist aufwändig: Reihenentwicklung
- siehe auch:
  - [GNU Manual](https://www.gnu.org/software/bc/manual/html_mono/bc.html)
  - [How To Use GNU Basic Calculator (Bc) In Linux For Math Calculation?](https://fossbytes.com/how-to-use-gnu-basic-calculator/)

**Alternative**: gnome-calculator


## in der Shell

- mit `(( .. ))`
- nur Integer
- Beispiele, die auf der Kommandozeile und im  Shell-Script funktionieren:  
    `$ echo $((3+4))`

- Hochzählen:
  
    ```bash
    N=10
    N=$(($N+5))
    echo $N
    ```

- oder kürzer bei Increment 1:

    ```sh
    N=10
    ((N++))
    echo $N
    ```  


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  HowTo<br />
letzte Änderung: 2025-02-09<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
