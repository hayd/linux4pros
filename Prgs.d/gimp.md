# GIMP

GNU Image Manipulation Program

- das am weitesten verbreitete freie Bildbearbeitungsprogramm
- Pendant zu **Photoshop**, wird von Photoshop-lern meist abgelehnt (weil zu anders, nicht vergleichbar mächtig), [GIMPshop](https://en.wikipedia.org/wiki/GIMPshop) wurde eingestellt, wird auch von Profis genutzt
- erweiterbar durch [GIMP-Plugins](https://www.heise.de/tipps-tricks/GIMP-Plugins-finden-und-installieren-4316241.html), automatisierbar z.B. mit [Skript-Fu](https://docs.gimp.org/2.10/de/gimp-filters-script-fu.html)
- KI hält auch in GIMP Einzug, z.B. [GIMP-ML](https://kritiksoman.github.io/GIMP-ML-Docs/index.html)
- Nach 20 Jahren GIMP 2.x kommt in Kürze [GIMP 3.0](https://www.heise.de/news/GIMP-3-0-RC1-Neue-Version-biegt-auf-Zielgerade-ein-10009285.html).
- für Zeichnen, Malen und Erzeugen von Skizzen weniger geeignet als:
  - [krita](https://wiki.ubuntuusers.de/Krita/) - ein freies Mal- bzw. Zeichenprogramm
  - [Inkscape](https://wiki.ubuntuusers.de/Inkscape/) - Editor für 2D  Vektorgrafiken
  - [dia](https://wiki.ubuntuusers.de/Dia/) - Programm zum Zeichnen von Diagrammen (einfach, Symbol-Sammlungen, Gummibänder)


## Gebrauch

### Gesicht unkenntlich machen

- `$ gimp gruppenbild.jpg`
- Bereich umranden: `Tools > Selection Tools`
- Verpixeln: Kontext-Menü: `Filters > Blur > Gaussian | Pixelize`
- Ergebnis abspeichern: ` File > Export as `


### Unauffälliges Entfernen von Objekten

- [Bereiche eines Bildes mit GIMP unter Linux klonen](https://linux-bibel.at/?s=Bereiche+eines+Bildes+mit+GIMP+unter+Linux+klonen) um unauffällig etwas verschwinden zu lassen


## Aufgaben

- Machen Sie ein Gesicht in einem Gruppenbild möglichst unauffällig unkenntlich. Mit welchem Filter, welcher Kopierfunktion gelingt das am besten?
  - Ergebnisse bitte in [Pad 3](https://pad.gwdg.de/k9DZCteMTBiUb6OjZXpNeA?both#)  eintragen.


## siehe auch

- [bei UU](https://wiki.ubuntuusers.de/GIMP/)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Grafik Software<br />
letzte Änderung: 2024-11-26<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
