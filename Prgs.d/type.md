# type

type zeigt für jeden Kommando-Typ an, wie er bei der Eingabe interpretiert wird. Sinn und Zweck siehe unten.


## Beispiele

```bash
type firefox
firefox is /usr/bin/firefox
```

- Firefox kommt also aus einem der Standard-Verzeichnisse für executables. D.h. er wurde als .deb installiert und nicht als Snap-Paket.

```bash
type chromium  
chromium is /snap/bin/chromium  
```

- Im Gegensatz zu Firefox wurde der Browser [Chromium](https://www.wintotal.de/chrome-vs-chromium/)  als Snap-Paket installiert.

```bash
type tmp
tmp is aliased to `cd /tmp'
```

- Der Befehl tmp ist ein schlichter alias.



```bash
type -a ll
ll is a function
ll () 
{ 
    ls -lhF --color=always $* | more
}
```

- Der Befehl ll ist eine Bash-Funktion, um Parameter in der Befehlsmitte übergeben zu können.


```bash
type type
type is a shell builtin
```

- type selbst ist ein Shell-Kommando.



## Hintergrund

Die Umgebungsvariable PATH gibt an, wo im Dateisystem ausführbare Programme gesucht werden sollen.

Die Environment-Variable PATH beinhaltet daher alle Verzeichnisse (getrennt durch ':'), in denen ein ausführbarer Befehl bzw. Programm gesucht wird, so dass es nur mit dem Befehl ohne vollständigen Pfad aufgerufen werden kann. Diese Variable kann an die eigenen Anforderungen [angepasst](https://wiki.ubuntuusers.de/Umgebungsvariable/typische_Anwendungsf%C3%A4lle/#PATH-erweitern) werden, was nicht ungefährlich ist.


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Software Wissen<br />
letzte Änderung: 2023-10-27 13:07<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
