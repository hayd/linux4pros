# DiskTools

- die wichtigsten Features
- Mängel
- Finden der größten Files, Directories



## dust () - Vladi

- Installation sowie Befehle https://www.linuxlinks.com/essential-system-tools-dust-intuitive-du/
- ![](https://pad.gwdg.de/uploads/2e056423-0fa6-4511-a60f-7921a699bd53.png)



Weitere vorhandene Commands mit dem dust snap
- wird verwendet um die Partitionen und Festplatten eines Computers zu verwalten
- "fdisk -l" Liste aller angeschlossenen Festplatten anzeigen lassen
- "df" zeigt Informationen über die verfügbaren und verwendeten Speicherplatz auf den einzelnen Partitionen und Dateisystemen an
- "du" zeigt die Größe von Verzeichnissen  an
- "lsblk" listet alle angeschlossenen Blockgeräte, einschließlich Festplatten, USB-Laufwerke und SD-Karten
- "mkfs" neues Dateisystem auf einer Partition zu erstellen,

## gdu () -  Jonas / Marcel
- go DiskUsage()
- ist ein Tool zur Analyse von Speicherplatten mit Fokus auf geschwindigkeit
- Installtion mit "apt install gdu"
- Platten anzeigen mit "gdu -d"
- Garbage collection mit "gdu -g"
- Informationen können in JSON File geschrieben und ausgelesen werden
- Suchpfade blacklisten

Mängel : - sehr schlichte Grafik (nur terminal)

Größte Datei: Muss manuell gefunden werden



## ncdu () - Kevin
- Steht dür "NCurses Disk Usage"
    - basiert auf Curses -> Terminal Steuerungsbibliothek
- Programmiert in C und ZIG

###
- Anzeigen der Festplatten Nutzung
- **insatll** `$ sudo apt install ncdu`
- **Aufruf** mit `$ ncdu`
    - Anzeige hängt von unserer Positionierung im Dateisystem ab
- **Navigation** mit den Pfeiltasten durch das Dateisystem
- existenz von Hifeseite und Manpage 

## dutree () - Simona
-> freies und quelloffenes Kommandozeilentool
-> die Auslastung des Festplattenplatzes in einem farbigen Format lässt sich anzeigen
-> geschrieben in RUST
- Features:
    - Es zeigt den Baum der Dateisysteme in einer Hierarchie an
    - Verzeichnisse oder Dateien können ausgeschlossen werden
    - Durchführung eines Vergleichs verschiedener Verzeichnisse ist möglich
- Installation:
    - `$ apt install curl` -> Übertragung von Daten in URL-Syntax
    - `$ curl https://sh.rustup.rs -sSf | sh` -> RUST installieren
    - `$ apt install git` -> git installieren
    - `$ git clone https://github.com/nachoparker/dutree.git` -> Dutree git repository klonen
    - `$ source $HOME/.cargo/env`
    - `$ cargo install dutree` -> Dutree mithilfe von cargo installieren
- Übersicht: `$ dutree -h`
- Festplattennutzung in hierarischem, farbigen Format: `$ dutree`
- ASCII-Zeichen anzeigen lassen: `$ dutree -A`

## pdu () parallel disk usage utility (Pascal)
Installation: download  "pdu-x86_64-unknown-linux-gnu" von  https://github.com/KSXGitHub/parallel-disk-usage/releases

Im Terminal in Download-Verzeichnis wechseln und Datei mit chmod 100 Ausführ-Rechte vergeben.

Datei starten ./pdu-x86_64-unknown-linux-gnu
Verzeichnis analysieren z.B.: ./pdu-x86_64-unknown-linux-gnu /etc

Infos: https://www.linuxlinks.com/pdu-parallel-disk-usage-utility/

Analysiert Verzeichnisse in Baum-Struktur

Features include:

    Fast.
    Relative comparison of separate files.
    Extensible via the library crate or JSON interface.
    Optional progress report.
    Customize tree depth.
    Customize chart size.


![](https://i0.wp.com/www.linuxlinks.com/wp-content/uploads/2021/06/pdu.png?resize=700%2C388&ssl=1)

![](https://camo.githubusercontent.com/8a2134763e60aef24f795081af81c4a764c70a6590c5f1bbb23bf3066b038072/68747470733a2f2f6b73786769746875622e6769746875622e696f2f706172616c6c656c2d6469736b2d75736167652d302e382e312d62656e63686d61726b732f746d702e62656e63686d61726b2d7265706f72742e636f6d706574696e672e626c6b73697a652e737667)

## filelight () - Diana
- KDE-Anwendung zum Anzeigen von grafischen Informationen zur Speicherplatznutzung
- Installation: sudo apt install filelight
### Features:
- Scannen von lokalen, entfernten oder Wechseldatenträgern
- Zeigt detaillierte Informationen zu Dateien und Ordnern an
- Dateien oder Ordner löschen, die zu viel Speicherplatz beanspruchen
- Integration in Dateimanager Dolphin, Konqueror und Krusader
- Konfigurierbare Farbschemata

### Mängel:
- man kann nicht zum Start der Anwendung zurückkehren, erst vorher schließen und neustarten

### Finden größter Files, Directories
- durch Kreisdiagramme und Beschriftungen
- /usr/ (5,8 GiB)
- /var/ (3,1 GiB)
- /swapfile (2,0 GiB)

## baobab () - Anne
- Disk Usage Analyzer
- grafische Oberfläche
![](https://lh3.googleusercontent.com/-L1SItwWzy5Q/YKSDbL1XjoI/AAAAAAAAffw/sYye8qFpfR8iW5cgoLFWWzkiXeO9PEu2QCLcBGAsYHQ/w400-h329/Screenshot_20210519_101758.png)
![](https://lh3.googleusercontent.com/-matuaKCkokM/YKSCKULHyHI/AAAAAAAAffo/y8_FZBJJmegzn0_jka853KMJsk7UBN1nACLcBGAsYHQ/s16000/Screenshot_20210519_101233.png)
Quelle: https://www.ubuntubuzz.com/2021/05/how-to-use-ubuntu-disk-usage-analyzer.html

### Installation:
`$ sudo apt install baobab`

### wichtigsten Features:
- full system scan
- single folder scan
- farbliche Darstellung als Kreisdiagramm & als Kacheldiagramm
    - dunkles Rot = sehr groß
    - gelb = es geht noch
    - grün = ist noch frei

### Mängel:
-  Ohne eine Einleitung, wie man es benutzt und liest, fand ich es etwas unübersichtlich
    - Diese [Seite](https://www.ubuntubuzz.com/2021/05/how-to-use-ubuntu-disk-usage-analyzer.html) hat es mir ein wenig erklärt 
- Kacheldiagramm ist sehr unübersichtlich

### Finden der größten Dateien:
- `snap` hat den größten Anteil im `Home Ordner` 
    - in `snap` hat dann `Firefox` den größten Anteil
        - und dann `common`
        - man kann immer tiefer gehen

- auf den `5-VM-Xubuntu` ist `usr` am größten,
    -  dann ist in `usr` ist `lib` am größten
        - und dann `x86_64-linux-gnu`

Die größten Ordner/Dateien werden immer ganz oben dargestellt.
