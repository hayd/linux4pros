# Linux-Vorlesung, Pad 4, Passphrase-Generator 
```- ls -l /home/user/snap/firefox | sha512sum |cut -b1-15 ```
```bash
#!/bin/bash
dateiname=$1
md5sum=$(md5sum $dateiname | cut -d " " -f 1)
gpg --batch --output $dateiname.gpg --passphrase "$md5sum" --symmetric "$dateiname"
```


```bash
echo -n /tmp/LaTeX.d | sha512sum | tr '479' '&#$' | cut -c 1-40
```

```bash
#!/bin/bash
read -p 'Dateiname:' DATEI
read -p 'Laenge [1 bis 512]:' long
read -p 'Mit Zahlen (y/n):' NUMERS
read -p 'Mit Sonderzeichen (y/n):' SONDERZ

sha=$(sha512sum $DATEI | cut -d ' ' -f 1)
if [[ -n $long ]]
then   
    sha=$(echo $sha | cut -c-$long)
elif [[ $NUMERS = "n" ]]
then
    sha=$(echo $sha | sed 's/[0-9]*//g')
elif [[ $SONDERZ = "y" ]]
then
    sha=$(echo $sha | tr 1 \! | tr 3 \# | tr 5 \? |tr 7 \& | tr 9 \+)
fi
echo $sha
```
``` bash
#! /bin/bash

if [ $# -eq 0 ];
then
  echo "$0: Dateiname fehlt"
  exit 1
fi

FILENAME=$1

PWLENGTH=20
if [ ! -z "$2" ];
then
    PWLENGTH=$2
fi

PW= echo $FILENAME | tr 'kjasdlb' 'polafdc' | md5sum | sha512sum | cut -c 1-$PWLENGTH

echo $PW

```#! /bin/bash

if [ $# -eq 0 ];
then
  echo "$0: Dateiname fehlt"
  exit 1
fi

FILENAME=$1

PWLENGTH=20
if [ ! -z "$2" ];
then
    PWLENGTH=$2
fi

PW= echo $FILENAME | tr 'kjasdlb' 'polafdc' | md5sum | sha512sum | cut -c 1-$PWLENGTH

echo $PW
```

#! /bin/bash
pw=$( $1 | sha512sum)
if [[ $2  == "zn" || $3 == "zn" ]] ; then
        pw=$(echo $pw | tr '1234567890' 'qwertzuiop')
fi
if [[ $3  == "sj" || $2 == "sj" ]] ; then
        pw=$(echo $pw | tr 'aeiou' '!%&(?')
fi
echo $pw





