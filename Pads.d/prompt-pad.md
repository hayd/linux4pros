

## bester Prompt
- Syntax möglichst einfach
- Prompt darf nicht zum 2-Zeiler werden
- conditional prompt: zeigt bspw. den aktuellen branch an, wenn man sich in einem Git Repo befindet, ansonsten stattdessen den Username
- aussagekräftige Farbgebung (auch conditional) z.B. Statuscodes färben

- ` PS1='\[\e[0;2m\][\[\e[0;1m\]\!\[\e[0;2m\]] \[\e[0;38;5;69m\]\u\[\e[0;2m\]@\[\e[0;38;5;112m\]\w \[\e[0m\]\$ \[\e[0m\]' `

- ```PS1='\[\e[0;38;5;196m\]\u\[\e[0m\],\[\e[0;38;5;208m\]\t\[\e[0m\],\[\e[0;38;5;226m\]exit code: \[\e[0;38;5;226m\]$?\[\e[0m\],\[\e[0;38;5;82m\]$(ip route get 1.1.1.1 | awk -F"src " '"'"'NR==1{split($2,a," ");print a[1]}'"'"')\[\e[0m\],\[\e[0;1;38;5;21m\]output\[\e[0m\]'```
- `PS1='\[\e[0;1;38;5;28m\]\u\[\e[0;1;38;5;22m\]@\[\e[0;1;38;5;28m\]\h \[\e[0m\](\[\e[0;38;5;41m\]$(history | wc -l)\[\e[0m\])\[\e[0m\]:\[\e[0m\] '`

- `PS1='\[\e[0m\]\!\[\e[0m\]:\[\e[0m\]\w\[\e[0m\]: \[\e[0m\]'`
- `PS1='\[\033[;32m\]┌──(\[\033[1;31m\]\u@\h\[\033[;32m\])-[\[\033[0;1m\]\w\[\033[;32m\]]\n\[\033[;32m\]└─\[\033[1;31m\]\$\[\033[0m\] '`

### [Bash Prompt Generator](https://scriptim.github.io/bash-prompt-generator/)
- macht prompt..mehr nicht
- kann fett und kursiv und so
- [Mit vorschau](https://www.youtube.com/watch?v=dQw4w9WgXcQ)
- drag & drop
- Import für nachträgliche Bearbeitung
- vergleichsweise mehr Auswahl an Bausteinen und Sytling
