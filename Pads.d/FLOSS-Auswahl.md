# FLOSS-Auswahl
- Kriterien für gute Freie Software (Vorlesung)
- Definition der Anforderungen
- Begründung für Auswahl (Killer-Feature)

## bester Filemanager
- z.B. nemo

## bestes Snapshot-Tool
- z.B. flameshot

## bester Audioplayer
- z.B. Clementine

## bester Bildbetrachter
- z.B. geeqie

## bester Videoplayer
- z.B. VLC media player

## bester Video-Editor
- z.B. https://kdenlive.org/de/

## bester PDF-Reader
- z.B. mupdf 
-
