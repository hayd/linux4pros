# Passphrase-Generator





## Tim & Nils
```
#!/bin/bash
echo "Eingabe: "
read Eingabe
echo "Laenge: "
read Laenge
Eingabe="$(echo $Eingabe | tr 'aeiou' '43!0&')" 
NewPassword="$(echo $Eingabe$Eingabe$Eingabe | sha512sum)"
NewPassword="$(echo $NewPassword | tr '8uanhe46d' '#&4BX3U!C')"
NewPassword="$(echo $NewPassword | cut -c 1-$Laenge)"
echo $NewPassword
                 
```
## Anne

```sh
#! /bin/bash
#set -x

# Dateneingabe
echo "Bitte Dateiname angeben."
read in_file

echo "Bitte Passwortlänge angeben."
read pass_l

# Erstellung des Passwortes (5 Varianten)
for i in {1..5}; 

do (tr -cd $in_file < /dev/urandom | fold -$pass_l | head -n 1); 

done

exit
```
## Marcel
```sh
#! /bin/bash

# the shebang line

#set -x

file=$1

    echo "Mit Zahlen:"
    shasum=$(echo $file | sha256sum)
    mdsum=$(echo $shasum | md5sum | tr 'aeiou' '12345' | tr -d '-')
    echo $mdsum

    echo "Mit Sonderzeichen:"
    shasum=$(echo $file | sha256sum)
    mdsum=$(echo $shasum | md5sum | tr 'aeiou' '!#?' | tr -d '-')
    echo $mdsum

    echo "Ohne Zahlen:"
    shasum=$(echo $file | sha256sum)
    mdsum=$(echo $shasum | md5sum | tr -d '0-9- ')
    echo $mdsum

exit
```


## Angie
```sh
#!/bin/bash
set -x
filename=$1
size=$2
special=$3
numbers=$4

passphrase=$(echo $filename | tr 'aeiou' 'frosch' | tr 'wsdx.' '42069')
 
if [[ "$special" == "y" ]]
then
passphrase=$(echo $passphrase | tr 'tmng' '+-/=')
fi

if [[ "$numbers" == "n" ]] 
then
passphrase=$(echo $passphrase | tr -d '0-9')
fi

echo ${passphrase:0:$2} 
```
## Pascal

```sh
#! /bin/bash
# the shebang line
#set -x


eins=1
echo Geben Sie einen Dateinamen ein:
read fileName
echo Geben Sie eine PassPhrase-Laenge ein:
read passPhraseLength
halfLength=$(expr $passPhraseLength / 2)
halfLengthPlusEins=$(expr $halfLength + $eins)
eins=$(echo $fileName | sha512sum | cut -c 1-$halfLength | tr '13579' '!$%/(')
zwei=$(echo $fileName | sha512sum | cut -c $halfLengthPlusEins-$passPhraseLength | tr [a-z] [A-Z])
echo $eins$zwei

exit 0
```
## Simona
```sh
#! /bin/bash
# the shebang line
#set -x

echo Bitte geben Sie die Passwortlänge ein:
read passwordlength

if [ $passwordlength -gt 10 ]
then
    echo Bitte geben Sie den Dateinamen ein:
    read filename

    echo Prüfsumme:
    checksum=$(echo $filename | sha512sum)
    echo $checksum

    echo Passwort:
    password=$(echo $checksum | cut -c 1-$passwordlength | tr [:lower:] [:upper:] | tr '123456' '/+!$(#' | tr '7890' '1234')
    echo $password
else
    echo "Die Passwortlänge reicht nicht"
fi

exit


