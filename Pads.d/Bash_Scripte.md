# Bash Scripte
## 1.

```bash
#!/bin/bash

directory="/myscript/"

newest_file=$(find "$directory" -type f -printf "%T@ %p\n" | sort -n | tail -n 1 | awk '{print $2}')

file_name=$(basename "$newest_file")
creation_date=$(stat -c %w "$newest_file")
formatted_date=$(date -d "$creation_date" +%Y-%m-%d)

new_file_name="${file_name%.*}_${formatted_date}.${file_name##*.}"
mv "$newest_file" "$(dirname "$newest_file")/$new_file_name"
```

## 3.
```bash
#! /bin/bash

pfad="/tmp/myscript"

count=1

for datei in "$pfad"/*; do

	suffix="${datei##*.}"

	mv "$datei" "$pfad/$count.$suffix"
	
	((count++))
done


```
## 5. 

```bash
#!/bin/bash

# Überprüfen, ob ImageMagick installiert ist
if ! command -v identify &> /dev/null; then
    echo "ImageMagick ist nicht installiert. Bitte installiere es, um dieses Skript zu verwenden."
    exit 1
fi

# Verzeichnis mit Bildern
image_dir="/home/admin/Desktop/Linux-Uni"

# Gehe durch jedes Bild im Verzeichnis
for image in "$image_dir"/*.{jpg,jpeg,png,gif}; do
    if [ -f "$image" ]; then
        # Holen der Auslösung des Bildes
        resolution=$(identify -format "%wx%h" "$image")
        
        # Dateiname ohne Erweiterung
        filename=$(basename -- "$image")
        filename_no_ext="${filename%.*}"
        
        # Neuer Dateiname mit Auslösung
        new_filename="${filename_no_ext}_${resolution}.${filename##*.}"
        
        # Umbenennen des Bildes
        mv "$image" "$image_dir/$new_filename"
        
        echo "Die Auslösung von $filename wurde hinzugefügt. Neuer Name: $new_filename"
    fi
done

echo "Alle Bilder im Verzeichnis wurden aktualisiert."
```


## 6.

```bash
#! /bin/bash

USAGE="Calculate Brutto to Netto"


echo "Welche Zahl Soll umgerechnet werden ?"
read num 

echo "b2n or n2b?"
read opt

if [[ $opt = "b2n" ]]
then 
echo "scale=2; $num * 1.19" | bc -l 
elif [[ $opt = "n2b" ]]
then 
echo "scale=2; $num / 1.19" | bc -l 
else
echo "enter Valid option!"
fi
```

## 7.
```bash
NOW=$( date '+%F_%H:%M:%S' )
echo "$USERNAME $NOW" > footer.txt
cat footer.txt >> $1
```

## 8. 
```bash
while : 
do
	time=$(ps -p $(pgrep glxgears) -o etime)
	if [[ $time > 29 ]];
	then
		^Z
		bg
	fi
	sleep 1
done
```
## 9.

```bash
#! /bin/bash
ll=$(ls -l $1)
filename=$(echo "${1}" | sed 's/\..*//')
filename=$(echo "${filename}.sidecar")
echo $ll > $filename
echo "Zum ergänzen jetzt Notizen schreiben"
read note
echo $note >> $filename
```


