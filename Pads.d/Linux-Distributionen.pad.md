# Linux-Vorlesung, Pad 2 - Linux-Distributionen
 16. Jan. 2023


## Videobearbeitung - Kevin

### AV Linux
- http://www.bandshed.net/avlinux/
- Basis Distribution: MX Linux
    - Fokus auf besonders hohe Stabilität und Performance 
    - in Kombination mit optisch ansprechenden und effizienten Desktops
- custom Kernel für geringe Audio Latenzen
- große Sammlug an Audio- und Video-Produktions Software
    - nach Entwicklern die besten Werkzeuge zur Multimedia-Bearbeitung
- sehr große Anpassungsmöglichkeiten
- **installation** über DVD, USB oder auf Hard-Disk möglich

## Gaming - Vladi

### Drauger OS
- https://draugeros.org/
- populär
- basiert auf Ubuntu
- rein auf gaming ausgelegt
- perfekt um noch mehr Leistung fürs Gaming herauszuholen aus dem System
    - auch auf älteren/schwächeren Systemen
- ermöglicht viele Modifikationen/Optimierung Möglichkeiten
- Große Spiele Plattformen wie Steam, Epic, Origin ... vorinstalliert
- kommt direkt im dark-mode
- Wiki, Forum etc vorhanden
- Große Social Media Plattform/Community
- Open-Source/kostenlos

Falls man aber auch normal arbeiten möchte und nicht nur spielt, bietet sich klassich Ubuntu an. 

###### [NVIDIA GPU Kernel](https://developer.nvidia.com/blog/nvidia-releases-open-source-gpu-kernel-modules/)

## uralte Hardware - Elias, Tim

- Debian (am verbreitetsten)
    - Minimum RAM: 512MB.
    - Recommended RAM: 2GB.
    - Hard Drive Space: 10 GB.
    - Minimum 1GHz Pentium processor.

- Linux Lite (xfce)
    - 700 MHz processor
    - 512 MB of RAM
    - At least 8 GB of hard disk space

- PuppyOS (letzte Version von 2020)
    - Intel Pentium III 1GHz processor
    - 128 MB RAM
    - 10 GB hard drive
    - vom USB-Stick bedienbar

- Q4OS
    - basiert auf Debian
    - dient als Ersatz für Betriebssysteme, die auf veralteter Hardware nicht mehr unterstützt werden
    - mindestens 128 MB RAM
    - eine CPU mit 300 MHz
    - 3 GB Speicher
    - ähnelt vom Gefühl und Aussehen stark Windows (Windows 2000 und Windows XP) 
    - open source
    - wird regelmäßig geupdatet (letzte Version: 24. Dezember 2022)

- Ausnahmefall Raspberry Pi -> Raspberry Pi OS
- TinyCore außen vor, kann mit sehr niedrigen Anforderungen laufen, kann aber auch Probleme mit der Hardware an sich haben und ist zu minimalistisch als für einen [tagtäglichen Gebrauch](https://www.fosslinux.com/43848/tiny-core-linux-installation-and-review.htm) eines normalen Nutzers 

## MacBooks
### [Elementary OS](https://elementary.io)
> Der durchdachte, leistungsfähige und ethische Ersatz für Windows und macOS
- great out-of-the-box experience
- orientiert sich an "Look" und "Feel" von macOS
- vorinstallierte Software + Store
- basiert auf Ubuntu 
- mehrere Unterprojekte und nicht ein Monolith
    - Vala als beliebte Programmiersprache
    - Bsp: `Pantheon`als Desktop (basierend auf GNOME)

**First stable release:** 31. März 2011, "Jupiter"

Requirements:
- Aktueller Intel i3 oder vergleichbarer 64-Bit Dual-Core Prozessor
- 4 GB Arbeitsspeicher (RAM)
- Solid state drive (SSD) with at least 32 GB of free space
- Internetzugriff
- Eingebaute oder kabelgebundene Maus/Touchpad und Tastatur
- 1024×768 Minimale Bildschirmauflösung

### Fedora
- Wayland Protokol für Gestenunterstützung
- macOS Mojave Theme verfügbar 

## Wissenschaft
### Scientific Linux
-> auf der Red Hat Enterprise Linux basiert
-> von Entwicklern am Fermilab, CERN, ETH Zürich und DESY entwickelt
-> binärkompatibel zu RHEL
-> ein Enterprise-Betriebssystem
-> kann über zehn Jahre benutzt werden, ohne Pakete migrieren zu müssen
-> geeignet für den kommerziellen Einsatz
-> wird auf der ISS eingesetzt
-> die gemeinsame Distribution sorgt für eine gemeinsame Installationsbasis
https://de.wikipedia.org/wiki/Scientific_Linux
-> letzter Release: 04.12.2018
-> es ist ein neuer Release geplant, aber soll erst 2024 eingestellt werden
https://distrowatch.com/table.php?distribution=scientific

### CAELinux
-> Computer-Aided Engineering
-> optimal für alle, die mit CAD, 3D-Modeling und -Drucken und physikalische Simulationen arbeiten
-> keine neue Releases, aber wird immer noch unterstützt
-> auf Xubuntu 18.04 LTS basiert
-> einige unterstützte Software:
- SALOME: 3D CAD
- ParaView: 3D-Visualisierung
- Scilab: mathematische Programmierung
-> letzter Release: 11.08.2020
https://www.caelinux.com/CMS3/index.php/download

### Fedora Scientific
-> default Desktopumgebung ist KDE
-> repräseniert den Mittelweg zwischen einer hochspezialisierten und einer gewöhnlichen Linux-Distribution
-> eignet sich für Forscher und Studenten aller Fachrichtungen
-> einige unterstützte Software:
- Trifecta: Versionsverwaltung
- Mayavi: 3D-Datenvisualisierung
- LaTeX: Dokumente erstellen
- Maxima: Algebra-Software
https://labs.fedoraproject.org/scientific/download/index.html

## AI
### Ubuntu oder Arch Linux
-> guter Code Editor
-> Support für Python, R, Go und andere Sprachen
-> Virtualisierungs Software
-> Source Code Management Software Bsp.: Git
-> DevOps tools wie GitLab
-> Ubuntu hat Support für Kubeflow, Kubernetes, Docker, CUDA etc.
-> Populäre Distribution mit viel Online Suppport
-> Alternativ Arch Linux für fortgeschrittene User


### Fedora
-> unterstützt neueste Fortschritte
-> große Benutzerbasis, deswegen sind meisten Tools leicht verfügbar
-> gut, wenn Machine Learning-App von einem Server oder einer Cloud ausgeführt werden soll (meisten Server führen RHEL aus)


## Steuerfahnder
>Einsatz bei Deloitte: Windows
#### Alternativen (lt. Distrochooser)
Vorraussetzungen:
- Hohe Benutzerfreundlichkeit
- Datenschutz vollständig gegeben (zb. keine Verbindung zu Dritten)
- Kein hoher Installationsaufwand

Ergebnisse:
- openSUSE
- Zorin OS
- elementary OS

Probleme:
-> Hoher Anteil an Firmen haben Kalkulationen und Steuerthemen Office Produkten wie Microsoft Excel

-> Knoppix
auch auf den Linux tagen vertreten und Open Source
## Sparkasse - Anne
### Voraussetzungen:
- Benutzerfreundlich
- visuell ansprechend und wenige Veränderungen am Aussehen
- daily use
- sollte Anfänger freundlich sein
- Datenschutz und Anonymität sollte gegeben sein
    - Sicherheitspatches sollen dauerhaft gegeben sein
- Wenige Kosten bis keine Kosten
- leicht Hilfe zu finden
- Innstallation sollte einfach sein
        - geringer Aufwand
        - wenig selber was einstellen

### Empfehlungen von Distrochooser:
Diese Distri erfüllen alle Voraussetzungen:
- Linux Mint (Ubuntu based)
- Zorin OS (Windows like interface)
- openSUSE
- elementary OS (Ubuntu based)


-------

## Zorin OS
https://zorin.com/os/
Entwickler: Zorin Group (Irland)
Finanzierung: Spenden, Pro-Version
abgeleitet von Ubuntu (LTS-Version 16.04 "Xenial Xerus")
seit 2008
aktuelle Version: 16.2 (27.10.2022)
unterstützt mehr als 50 Sprachen
soll Windows-Nutzern den Umstieg erleichtern
mittels PlayOnLinux/WINE kann Windows-Software genutzt werden 
Bietet einen GUI-basierten Softwaremanager (ähnl. App-Store)
Bietet die Möglichkeit, Android-Handys mit dem PC zu synchronisieren
Versionen:
* Core: kostenfrei, Standardversion
* Education: kostenfrei, basiert auf Core, richtet sich an Schulen, beinhaltet Lern-Software 
* Lite: kostenfrei, mit geringerem Ressourcenbedarf für ältere Computer (ebenfalls 32 Bit)
* Pro: 39 € zzgl. Steuern, mehrere Desktop-Layouts, bitetet Installations-Support und andere Programme


