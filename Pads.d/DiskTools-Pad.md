# DiskTools-Pad

## dust (Marvin T)
+ du in Rust
+ kein weiterleiten des outputs an sort oder head nötig -> Standard Konfiguration
+ Verzeichnisse als ASCII Art dargestellt, beginnend bei Verzeichnisswurzel, baut sich rekursiv  auf, limitiert durch Terminal-Höhe
+ Balken-Diagramm stellt dar, wie viel Speicherplatz relativ zum Parent directory genutzt wird
+ Installation über Third-Party Repositories bzw. Tools wie Cargo, Homebrew, deb-get etc.
+ Nutzung:
```shell
Usage: dust
Usage: dust <dir>
Usage: dust -n 30  (Shows 30 directories instead of the default [default is terminal height])
Usage: dust -r (reverse order of output)
Usage: dust -z 10M (min-size, Only include files larger than 10M)
Usage: dust -e regex (Only include files matching this regex)
```
+ ![](https://raw.githubusercontent.com/bootandy/dust/master/media/snap.png)
+ [Github](https://github.com/bootandy/dust)

## gdu (Sebastion, Max)
![](https://pad.gwdg.de/uploads/a3ba9e50-aa98-4931-bfa1-80647295c126.png)⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
- In Go geschrieben, schnell
- Disk Usage Tool für die Konsole
- Navigation mit Pfeiltasten, erzeugt eine Art GUI im Terminal
- fühlt sich an wie Dateinexplorer mit Tastatur und ohne Bilder
- Hauptmerkmal ist Geschwindigkeit (vor allem auf SSDs durch Parallelverarbeitung)
- gibt auch nicht-interaktiven Modus, ist dann wie `du` mit Farben
- Exportfunktion
- Gibt es für Windoof und Linüx

Installation
`
(sudo) apt install gdu
`
Basic Controlls
```
q = Quit
? = Help
==============================
Left = Upper Directory
Right = Open marked Directory
==============================
r = rescan current directory
d = delete marked file
e = emty file/directory
```


[Git](https://github.com/dundee/gdu)


## ncdu (Marvin, (S)Ch(l)ucken4000und1)
![](https://upload.wikimedia.org/wikipedia/commons/d/d7/Ncdu_screenshot.png)
- text based UI 
- navigation mit Pfeiltasten
- löschen von Dateien mit d
- Entwickelt zur Analyse der Festplattennutzung
- Yoran Heling hat es Entwickelt um die Sprache c zu lernen 
- Festplatten auf Remoteservern ohne UI analysieren
- auch in tty nutzbar sollte das system stark beschädigt sein
- Version 2.0 wurde in Zig umgesetzt
- Linux, FreeBSD, OpenBSD, NetBSD, DragonFlyBSD, MirBSD, z/OS, macOS und Cygwin
- sehr perfomant gegenüber grafischen Lösungen

## dutree (Paula, Jan)
> a tool to analyze file system usage written in Rust
- farbige Ausgabe, entsprechend der Umgebungsvariablen LS_COLORS
- Dateisystembaum anzeigen
- große Dateien aggregieren
- Dateien/Verzeichnissen ausschließen/filtern
- Dateien/Verzeichnissen vergleichen
- schnell, denn wurde in Rust geschrieben

[Git](https://github.com/nachoparker/dutree)

### Usage:
```
 $ dutree --help
Usage: dutree [options] <path> [<path>..]

Options:
    -d, --depth [DEPTH] show directories up to depth N (def 1)
    -a, --aggr [N[KMG]] aggregate smaller than N B/KiB/MiB/GiB (def 1M)
    -s, --summary       equivalent to -da, or -d1 -a1M
    -u, --usage         report real disk usage instead of file size
    -b, --bytes         print sizes in bytes
    -f, --files-only    skip directories for a fast local overview
    -x, --exclude NAME  exclude matching files or directories
    -H, --no-hidden     exclude hidden files
    -A, --ascii         ASCII characters only, no colors
    -h, --help          show help
    -v, --version       print version number
```

### Example Screenshot:
![](https://github.com/nachoparker/dutree/raw/master/resources/dutree_featured.png)

## pdu (SK;MS)
- Parallel Disk Usage
- in Rust geschrieben, Rust erforderlich
- Benutzung:  
 "pdu [FLAGS] [OPTIONS] [--] [files]""
- wertet Ordnerstruktur graphisch aus
- relative Vergleiche verschiedener Dateien
- anpassbare Baumtiefe und Graph-Größe
- kann Ausgabe im JSON-Format -> für möglich weiterverarbeitung interessant 
- Options: 
    - --column-width
    - --max-depth
    - --min-ratio
    - --total-width

![](https://i0.wp.com/www.linuxlinks.com/wp-content/uploads/2021/06/pdu.png?resize=700%2C388&ssl=1)

## filelight (Nico, Paul)
![](https://pad.gwdg.de/uploads/3128fbec-9349-4f0d-ab68-36477f7832fb.png)
- stellt Disk Usage graphisch dar, statt als Tree in der Konsole (concentric pie charts)
- einfache installation
- in c++ geschrieben
- scannen von lokalen u. entfernten Datenträgern u. Wechseldatenträgern
- kann auch einzelne Directories öffnen
- graphisches User Interface anstatt Konsolenkommandos
- Disk Usage Darstellung kann als SVG gespeichert werden
- Navigation durch Klicken auf einzelne Diagrammteile um in Unterordner zu gelangen und diesen darzustellen
- auch für windows, macOS, IOS, Android verfügbar
- quelloffen (auf [github](https://github.com/KDE/filelight)) unter  GNU Free Document License
- wird kontinuierlich weiterentwickelt


## baobab (Julian, Lukas)
- früher Baobab, heute "Disk Usage Analyzer"
- graphical disk usage analyzer für GNOME
- Analyse der Partitions- bzw. Verzeichnisstruktur
- macht keinen Unterschied zwischen Partitionen und Verzeichnissen
- Scan von Ordnern, Geräten, remote locations
- Information über verbrauchten Speicherplatz von jedem Element
- bietet baumartige Struktur und grafische Darstellung
- in Vala geschrieben


![](https://help.ubuntu.com/community/Baobab?action=AttachFile&do=get&target=scrn-baobab1.png)
![](https://help.ubuntu.com/community/Baobab?action=AttachFile&do=get&target=scrn-baobab2.png)
![](https://wiki.gnome.org/Apps/DiskUsageAnalyzer?action=AttachFile&do=get&target=screenshot2.png)

