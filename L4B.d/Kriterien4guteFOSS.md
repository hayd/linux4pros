# Woran erkennt man gute FOSS ?

Wie finde ich ein gutes freies Programm für .... ?

## TL;DR

1. Wahl: paketierte Software aus einer renommierten Linux-Distribution

## Finden

- `$ apt-cache search xyz | grep - i xyz`
  - xyz: editor, terminal
- googeln: _Linux best xyz_   => Vergleiche, Reviews, ...


## Kriterien

### Entwicklung

- Von wann letztes stable release ([Emacs](https://de.wikipedia.org/wiki/Emacs) vs. [XEmacs](https://de.wikipedia.org/wiki/XEmacs))?
- Wann last commit?
- Wie schnell werden security patches geliefert?
- \#Autoren
- Wo gehostet (GitHub, GitLab, berlios.de)?
- Unter einer seriösen freien Lizenz (GPL, BSD, ...)?
- ein Beispiel: [flameshot](https://github.com/flameshot-org/flameshot)


### Integration, Verbreitung, Bewertungen

- [Sternchen](https://www.heise.de/news/3-1-Millionen-boesartige-Fake-Sterne-auf-GitHub-entdeckt-Tendenz-steigend-10223115.html), Likes werden in großem Stil gefälscht um Malware zu promoten und damit zu verteilen.Daher sollte man andere Kriterien heranziehen.
- Integration in eine  [Distribution](https://packages.ubuntu.com/): `$ apt-cache search xyz`
  - d.h. min. ein Maintainer (ein "Ausbildungsberuf") findet die Software so gut und wichtig, dass er Arbeit in die Paketierung investiert.
  - Software wird Teil eines großen Projektes mit z.B. gemeinsamen Libraries, Tests und Qualitätskontrolle
- Unterstützung [offener Schnittstellen](https://www.se-trends.de/offene-schnittstellen-erzwingen/) => Kooperation mit anderen Programmen möglich
- Zahl positiver Hits bei Suche


### Finanzierung

- Wer finanziert Entwickler (z.B. Kernel: RH, IBM, Microsoft, ...)?
- Was will mit die Firma sonst noch verkaufen?
- Spenden ([Blender](https://www.golem.de/news/3d-grafiksuite-epic-spendet-1-2-millionen-us-dollar-an-blender-1907-142585.html) , [krita](https://krita.org/en/donations/)): Was passiert, wenn Spendenbüchse leer?



### Funktionalität

Wenn bisher kein KO-Kriterium aufgetreten ist, ...

- Anforderungen zusammenstellen
- Erfüllung der Anforderungen in der Praxis testen:
  - Anlernzeit, ähnlich vertrauten Programmen
  - Doku
  - Usability (z.B. Zahl der nötigen Klicks)
  - usw.



## Übungen

Was ist gemäß obiger, formaler Kriterien der/die ...

1. bester Dateimanager
2. bestes Snapshot-Tool (Bildschirmfotos)
3. bester Text-Editor
4. bester Terminal-Emulator
5. bester Audioplayer
6. bester Videoplayer
7. bester Bildbetrachter
8. bester PDF-Viewer
  

Vorschläge bitte in dieses [Pad](https://pad.gwdg.de/Ptv7t3wdRLyIDysySFwqUg?both) (mit Namen) eintragen.


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Admin  Software<br />
letzte Änderung: 2025-02-19<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>




