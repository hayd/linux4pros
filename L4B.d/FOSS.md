# Freie Software

## TL;DR

Die GPL garantiert die 4 Freiheiten (use, study, share, improve) mittels Copyleft am besten.


## Begriffe

- FOSS: Free and Open Source Software
- FLOSS: Free/Libre Open Source Software - L für andere Sprachen und wegen frei != gratis
- oft synonym verwendet: Freie Software, Open-Source-Software

## frei

- Freiheit, nicht  Freibier
- Die 4 Freiheiten (ussi)
    1. use
    2. study
    3. share
    4. improve



## Lizenzen

### GPL

- [GPL](https://de.wikipedia.org/wiki/GNU_General_Public_License) - wichtigste, freie [Lizenz](https://ifross.github.io/ifrOSS/Lizenzcenter)
- mit Copyleft


### Copyleft-Prinzip

- Virulenz: Alle abgeleiteten Programme eines unter der GPL stehenden Werkes dürfen von Lizenznehmern nur dann verbreitet werden, wenn sie von diesen ebenfalls zu den Bedingungen der GPL lizenziert werden.


### BSD-Lizenz

- Berkeley Software Distribution
- ohne Copyleft
- für kommerzielle Nutzung
- Beispiele: [Chromium (web browser)](https://en.wikipedia.org/wiki/Chromium_(web_browser)), [Nginx ( web server)](https://en.wikipedia.org/wiki/Nginx)


### Praxis

- Software nicht unter GPL => bei Änderung (nach vielen Jahren) muss man alle Autoren fragen


## Damit USSI Realität wird

Folgendes erleichtert es, seine Rechte (ussi) zu nutzen und an Freier Software mitzuwirken (Code, Doku, Illustrationen, Vorträge, ...).

- KISS: Keep it Simple, Stupid  oder Keep it Small and Simple
- ein Programm für eine Aufgabe, keine eierlegende Wollmilchsau
- einfache Kommunikation der Bausteine
- freie Schnittstellen: Sie erlauben die Kopplung von Programmen.
- Plugin-Systeme: erleichtern das Beisteuern von Software
  - z.B. [Firefox](https://addons.mozilla.org/en-US/firefox/), [GIMP](https://www.heise.de/tipps-tricks/GIMP-Plugins-finden-und-installieren-4316241.html#best), [Editoren](https://www.heise.de/tipps-tricks/GIMP-Plugins-finden-und-installieren-4316241.html#best)


## Digitale Souveränität

- = selbstbestimmten Nutzung und Gestaltung von Informationstechnik
- 3 Hauptziele: Möglichkeit zu **wechseln**, Möglichkeit zu **gestalten**, **Einfluss** auf Anbieter
- Beispiele, die nachdenklich machen sollten:  
  - [Microsoft schaltet Linux-Bootloader ab](https://www.heise.de/select/ct/2022/20/2223813341738463324)
  - [Atlassian zwingt Kunden in die Cloud](https://www.heise.de/select/ix/2021/2/2029610133871592636)
  - [Abhängigkeit von Microsoft gefährdet die digitale Souveränität](https://www.heise.de/newsticker/meldung/EU-Experten-warnen-Abhaengigkeit-von-Microsoft-gefaehrdet-die-digitale-Souveraenitaet-3679559.html)
  - Ergebnisse der [Sondierung](https://www.tagesspiegel.de/politik/schriftgrosse-11-calibri--so-soll-die-ampel-koalition-gebaut-werden-4285528.html) der Ampel-Parteien müssen 2021 in Schriftgröße 11, [Calibri](https://de.wikipedia.org/wiki/Calibri), Zeilenabstand 1,5 ausgeschrieben werden.

## Siehe auch

- [CLT2025 · Chemnitzer Linux-Tage 2025](https://chemnitzer.linux-tage.de/2025/de) am 22. und 23. März 2025

- [Open-Source-Lizenzen – Grundlagen und Entscheidungshilfen](https://www.bitfactory.io/de/blog/open-source-lizenzen/)
- [ifrOSS](https://ifross.org) Institut für Rechtsfragen der Freien und Open Source Software


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Software Politik<br />
letzte Änderung:2025-02-20<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>










