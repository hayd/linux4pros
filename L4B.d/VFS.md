# VFS - Virtual File System

## Basics

- VFS: eine  Schicht zwischen Betriebssystem und Dateisystem. Sie dient dazu, den **Zugriff** (z.B. Datei anlegen, Daten schreiben, Ordner löschen) auf die verschiedenen Dateisysteme zu **vereinheitlichen** und so die Integration neuer Filesysteme zu erleichtern.
- Das VFS bildet also die **Abstraktion** und die einheitliche Schnittstelle, in die alle Dateisystem-Treiber integriert werden können.

![VFS](../Pics.d/vfs.gif)

- Nebenprodukt: Co-Existenz beliebig vieler File-Systeme in einem einzigen Namensraum. Z.B: 
  - /home1/user1 auf lokaler Platte mit **ext4**, 
  - /home2/user2  auf Fileserver im Netz via **NFS**.
- VFS kann keine Dateinamen länger als 255 Bytes verwalten.
  - Man sollte den Namen nicht als Abstract (*Beschreibung_des_Linux_Kernels_und_seine_Entwicklung.pdf*) verwenden.


## Page Cache

- freier Speicher ist verschwendeter Speicher
- beschleunigt Schreib-, Lese-Zugriffe
- Beim erstmaligen Lesen von oder Schreiben auf Datenträgern  werden Daten in ungenutzten Bereichen des Arbeitsspeichers gecacht.
- Anzeige (Spalte buff/cache) wie viel gerade gecacht wird: `$ free -h`


## Inode

- Inode ist eine Datenstruktur, die alle Metadaten für eine Datei enthält. z.B: Art, Größe, owner, Zugriffsrechte, [atime, mtime, ctime](https://de.wikipedia.org/wiki/Modification,_Access,_Change), ...
- `ls -al` listet einen großen Teil davon auf.


## siehe auch

- [Introduction to the Linux Virtual Filesystem](https://www.starlab.io/blog/introduction-to-the-linux-virtual-filesystem-vfs-part-i-a-high-level-tour)
- [Wikipedia](https://en.wikipedia.org/wiki/Virtual_file_system)

## Zusätzliche Infos

- Allgemeine Objekte im VFS:
  - Superblock: speichert Informationen über ein Dateisystem (Größe, Zeiger auf Liste der freien Blöcke, Anzahl freier Inodes, div. Zeiger)
  - Inode: s.o.
  - Fileobject: speichert Information zur Interaktion zwischen einer geöffneten Datei und einem Prozess, existiert nur zur Laufzeit im Kernelspeicher
  - Dentry: (=directory entry) ordnet einer inode number einen file name zu.

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Wissen Storage<br />
letzte Änderung: 2024-10-28<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>