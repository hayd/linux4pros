
# sed

= stream editor

- 1974 veröffentlicht
- sed unterliegt keinen (realen) Beschränkungen hinsichtlich der Dateigrößen.


## Arbeitsweise

1. Input-File wird **zeilenweise** eingelesen.  
2. Die Zeile landet im **Pattern Space**. Dort werden die Anweisungen umgesetzt.  
3. Das Ergebnis wird auf **stdout** geschrieben.


- beherrscht [REs](./REs.md).
- Folgende Zeichen muss man maskieren: `( )` und  `[ ]`


## Performance

- 35.145.059 Zeilen (Kernel-Quellen)
- PC der Messung: AMD Ryzen 5, 3.40-3.90GHz, SSD
- `$ time  sed -i 's/Nishanth/Nishhhhanth/'  kernel-sources.txt`
- 52 Ersetzungen in 0m6,515s


## Syntax

`sed '/Beginn/,/Ende/ c/alt/NEU/' inputfile`

- `c`: eine der folgenden Anweisungen
- `Beginn`, `Ende` können auch entfallen. Sie definieren die Grenzen, zwischen denen die Anweisung ausgeführt wird.


### Anweisungen

- `s` (substitute) einmalige Ersetzung auf einer Zeile
  - mehrmalige Ersetzungen mit g (global):  
       `sed  's/alt/NEU/g'`
- `y` (yank = herausziehen) transformiert die Zeichenmenge "alt" in  Zeichenmenge "NEU", d.h. beide Zeichenmengen müssen gleich mächtig sein:  
   `sed  'y/alt/NEU/g'`
- Es gibt auch noch: append, change, insert, next, print, quit, read

## Optionen

- `-i`  (inplace) Input-File wird verändert, nicht nach stdout geschrieben
- `/alt/neu/g` g (global)



## Delimiter ändern

- um (Back-) Slash-Orgien zwecks Maskierung zu verhindern:  
   `$ echo "/a/b/c/" | sed 's/\/a\/b\/c\//\/d\/e\/f\//'`  
   ersetzt "/a/b/c/" durch "/d/e/f/"
- Für das "s" Kommando kann jeder beliebe Delimiter verwendet werden. sed interpretiert das auf "s" folgende Zeichen, hier ; , als Delimiter:  
   `$  echo "/a/b/c/" | sed 's;/a/b/c/;/d/e/f/;'`


## Beispiele

Vorsicht: HTML fasst mehrere Blanks in der Darstellung zu einem zusammen!

1. Suffix (.md) entfernen:  
   `$ echo "/usr/share/doc/udisks2/README.md" | sed 's/\.md$//'`
1. Zeilennumern vor jede Zeile schreiben:  
   `$ sed '=' /etc/apt/sources.list`
1. Leerzeilen entfernen:  
`$ man ls | sed '/^$/d'`
1. Kommata durch Zeilenumbruch ersetzen:  
   `$ echo "Zeile1 am Anfang, Zeile2,Zeile 3" | sed "s/,/\n/g"`
1. die Reihenfolge von 2 Worten vertauschen mit back-references (weil es [Lorbeer-Kirsche](https://de.wikipedia.org/wiki/Lorbeerkirsche) heißt):  
`$ echo "Kirsch-Lorbeer" | sed 's/\([a-Z]*\)-\([a-Z]*\)/\2\-\1e/'`
1. Alle Zeilen 10 Blanks einrücken, die eine URL enthalten:  
    `$  sed '/http/s/^/          /g'  /etc/apt/sources.list`  
1. nur ein Wort pro Zeile als Beginn einer linguistische Analyse (Worthäufigkeiten):  
`$ man ls | sed "s/ /\n/g" | egrep -v "^$" | sort`



## Aufgaben

Beispiele nachvollziehen (copy 'n paste) und dann erklären.


## Zusammengesetzte Kommandos

- Gruppierung durch `sed  '{    }'`
- So kann man z.B. eine Markup-Syntax in eine andere wandeln.
  
```bash
# simple sed script to convert from moinmoin to GitLab-Wiki syntax

sed  '{
# reformat H1, H2, H3
   s/^===/###/
   s/^==/##/  
   s/^=/#/
   s/=*.$//
# unordered list, 2nd level
   s/^  \*/ -/
# unordered list, 1st level
   s/^ \*/-/
# ToC
   s/<<TableOfContents.*/\[\[_TOC_\]\]/
# URLs
   s/\(\[\[http.*\|\)\(.*\]\]\)/\1/
# in line code
   s/^{{{/```sh/
   s/^}}}/```/    }' input-file.txt
```

## siehe auch

- [Handy one-liners for SED](https://edoras.sdsu.edu/doc/sed-oneliners.html)
- [Sed - Snipcademy](https://snipcademy.com/shell-scripting-sed)
- [50 sed Command Examples](https://linuxhint.com/50_sed_command_examples/) - sehr lehrreich
- [An Introduction and Tutorial by Bruce Barnett](https://www.grymoire.com/Unix/Sed.html) - 2400 Zeilen
- [Die GNU Doku](https://www.gnu.org/software/sed/manual/sed.pdf), PDF, 85 S., von Jan. 2020
- [Sed Tutorial](https://www.tutorialspoint.com/sed/index.htm)
- [Eine Einführung in sed](https://tty1.net/sed-tutorium/sed-tutorium.pdf), PDF, 31 S.
- [Einführung, Tipps und Tricks](https://www.ostc.de/sed.pdf), PDF, 25 S.
- [Suche nach Mustern über mehrere Zeilen](https://www.tutonaut.de/sed-suche-nach-mustern-ueber-mehrere-zeilen/)
- [SED - eine kleine Einführung und Übersicht über ein mächtiges UNIX-Werkzeug](https://linupedia.org/opensuse/Sed) - gute Erklärung der Funktionsweise


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Programmierung<br />
letzte Änderung: 2024-03-06<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
