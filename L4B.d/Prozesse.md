# Prozesse

- Prozess: Programm, das auf dem System (im Hintergrund) läuft
- Zahl der gerade laufenden Prozesse:  
 `$ ps -ax | wc -l`


## Scheduler

- **verteilt Ressourcen** (z.B. Zeitscheiben der CPUs) an Prozesse
  - Das ist eine mehrdimensionale (CPU, RAM, I/O, ...) **Optimierungsaufgabe**, bei der es auch Verlierer geben muss.
- **Aufgaben**: Bedienung unterschiedlichster Hardware (z.B. CPU mit 1 oder 64 cores, Server mit 4 CPUs); Bevorzugung bestimmter Prozesse/Prozess-Gruppen (z.B. für I/O, für interaktives Arbeiten); Einhaltung der Systemregeln, Fairness gegenüber allen Usern (zahlende User in Cloud)
- **Optimierung**: Gesamt-Durchsatz vs. reaktivem Desktop, wenige Kontext-Wechsel
- **Kontext-Wechsel**: Prozess A wird angehalten und im gleichen Kontext Prozess B gestartet. Sie sind teuer wegen Speichern und Laden von Register-Inhalten, internen Tabellen der Prozesse, [Caches](https://de.wikipedia.org/wiki/Cache) => Prozess sollte Core nicht wechseln und selten angehalten werden.
- Kernel in **unterschiedlichsten Umgebungen**: Server für Web-Services, Ampelsteuerung, Telefonanlagen, DNS-Anfragen; Spiele-PC, Office-Laptop benötigen unterschiedliche Scheduler
- Es werden verschieden Scheduler eingesetzt. Z.B:
  - [Completely Fair Scheduler](https://de.wikipedia.org/wiki/Completely_Fair_Scheduler)
  - [Fair-Share-Scheduling](https://de.wikipedia.org/wiki/Fair-Share-Scheduling)
  - [Brain Fuck Scheduler](https://de.wikipedia.org/wiki/Brain_Fuck_Scheduler) - von einem australischen Anästhesist entwickelt
- [Kernel 6.6](https://www.heise.de/news/Linux-6-6-bringt-neuen-Scheduler-9350044.html) tauscht nach 15 Jahren den Scheduler CFS aus
  - CFS meist gut, ignoriert aber:
    - CPU-Wechsel teuer, da Cache-Inhalte neu angelegt werden müssen
    - [hybride Systeme](https://www.heise.de/news/Ryzen-AMD-stellt-seine-ersten-Hybrid-Prozessoren-vor-9352226.html) (stromsparende Cores, high performance cores)
    - Power-Management, bei Laptops, IoT devices wichtig
    - [Latenz](https://en.wikipedia.org/wiki/Latency_(engineering)) (Reaktionszeit, z.B. zwischen Tastendruck und Erscheinen des Zeichens)
  - daher **neuer Scheduler**: "Earliest Eligible Virtual Deadline First" (EEVDF) Scheduler
    - \#Zeitscheiben in der Vergangenheit beeinflusst #Zeitscheiben in der Zukunft
    - auch latency bekommt eine Priorität


## Prozesspriorität

- Über die Vergabe von **Prioritäten** steuert der Kernel die Zuweisung von Ressourcen (z.B. CPU-Zeit, Plattenzugriff), damit wichtige Prozesse bevorzugt behandelt werden.
- Je **kleiner** der Prioritäts-Wert, desto **mehr** Ressourcen erhält ein Prozess vom Scheduler.


## Prozesse bei der Arbeit

### Beobachtung

`$ htop`   interactive process viewer

- task = Process
- Tasks (mitte oben): alle Prozesse, aktiv oder inaktiv = running oder sleeping
- Prozesse werden in threads (thr) parallelisiert
  - alle threads eines Prozesses haben den gleichen virtuellen Speicher (Adressraum), Dateien und Netzwerkverbindungen
- **sortieren** durch anklicken der Spaltenüberschrift
- **PID**: **P**rocess **Id**entifier
- **PRI**: Priorität im kernel space.
- **NI**: (Niceness) Priorität im user space
  - kann man mit F8 erniedrigen
- PRI = NI + 20 (normalerweise)
- `F5` zeigt Verwandtschaft der Prozesse

### Aufgaben

- Welche Prozesse verbrauchen am meisten CPU-Zeit und welche am meisten Memory?
- Welcher Prozess hat die meisten Kindprozesse?
- Welche Prozesse haben die besten nice Werte?
- Starten Sie `xeyes` und killen es mit htop.



### Arbeit

- Arkustangens von 1 (= Pi) auf 5000 Stellen genau berechnen mit [bc](https://de.wikipedia.org/wiki/Bc_(Unix)). Das erzeugt Last auf dem Rechner.
- Anweisungen werden dem interaktiven `bc` mittels `echo ... | ` von außen übergeben
- `time` vor einem Befehl misst dessen Ressourcen-Verbrauch
- Mit `nice` kann man die Priorität eines Prozesses verschlechtern.
- `grep` filtert die Zeiten raus
- In der `for`-Schleife werden mehrere Prozesse mit unterschiedlichen nice-Werten im Hintergrund (`&`) gestartet, die sich um die Ressourcen streiten.
  
```bash
for i in 0 2 4 6 8 10 12 14 16 18 20
do
   echo "nice Wert: $i"
   time echo "scale=5000; 4*a(1)" | nice -$i bc -l | grep real &
done
```


## Prozesssteuerung auf CLI

- Prozesse kann man über [Signale](https://www.linux-community.de/ausgaben/linuxuser/2014/11/klar-signalisiert/) steuern, die man ihnen schickt.
- Alle Eingaben im gleichen Terminalfenster machen.
- [xeyes](../Prgs.d/xeyes.md) kann man mit `Alt+lM` verschieben
- Programm starten und beenden via CLI:  
`$ xeyes`  starten  
`$ Ctrl-c`  sendet Signal SIGINT (Interrupt) an Prozess
- Programm in Hintergrund schicken:
  - `$ xeyes`
  - `$ Ctrl-z`  sendet das Signal SIGTSTP (Terminal Stop) an Prozess  
  - `$ bg` belebt den Prozess im **b**ack**g**round wieder durch ein Signal: SIGCONT (**sig**nal **cont**inue)
- Programm gleich im Hintergrund starten:  
  `$ xeyes &`


## Killen mit der Maus

- Ctrl-Alt-Esc => Totenkopf-Cursor (o.ä)
- zu killendes Fenster mit Totenkopf anklicken


## Programme

### ps

- Wieviele Prozesse laufen auf dem Rechner:
   `$ ps -e | wc -l`
- Ein Prozess kommt selten allein:
  `$ htop , F5`


### kill, killall

- killt nicht wirklich, sondern schickt Signal an Prozess: `$ kill 45522`
- [Signale](https://de.wikipedia.org/wiki/Signal_(Unix)):
  - -9: kill, unblockable (Signal kann vom Program nicht abgefangen werden)
- [killall](https://wiki.ubuntuusers.de/killall/) - kill processes by name, not by PID


### top, htop

- [top](https://wiki.ubuntuusers.de/top/) zeigt Prozessliste und (deren) Resourcen-Verbrauch.
  - Anzeige wird defaultmässige jede Sekunde aktualisiert
- [htop](https://wiki.ubuntuusers.de/htop/)
  - interactive processes viewer
  - extended top mit ncurses GUI
  - mit F3 Prozess suchen und mit F9 killen


## siehe auch

- [The Linux Scheduler: a Decade of Wasted Cores](https://people.ece.ubc.ca/sasha/papers/eurosys16-final29.pdf)
- [Linux process priorities demystified](https://blog.sigma-star.at/post/2022/02/linux-proc-prios/)
- `$ man 7 signal` (overview of signals)
- [How to Set Linux Process Priority Using nice and renice Commands](https://www.tecmint.com/set-linux-process-priority-using-nice-and-renice-commands/)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  <br />
letzte Änderung: 2025-02-26<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
