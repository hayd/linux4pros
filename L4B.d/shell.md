# Die Shell

## Basics

![alt text](../Pics.d/shell_user_interface.svg)

- eine Mensch-Maschine-Schnittstelle: User --> Shell --> Linux
- Die **Shell interpretiert** die Eingaben des Users und übermittels das Ergebnis an das Linux-System.
  - Also Vorsicht bei Eingabe interpretierbarer Zeichen (z.B. *).
  - Leerzeichen?
- Die Shell ist eine Programmiersprache.
  - Man kann kleine Programme auf der Kommandozeile ausführen:  
    `$ for i in  1 $(seq 1 20); do P=$(($RANDOM%500)); xeyes -geometry $Px$P+$P+$P  & sleep 1; done`
  - Man kann Kommandos (c1, c2) verknüpfen:
    - `$ c1 | c2`: (pipe), Weiterreichen des Outputs
    - `$ c1 ; c2`: erst c1, dann c2 (ohne Bedingung)
    - `$ c1 || c2`: c2, wenn c1 Fehler liefert, erfolglos ist
    - `$ c1 && c2`: c2, wenn c1 erfolgreich  
    - Beispiel:  
  `$ echo "123" | grep 2 && echo "2 enthalten" || echo "2 nicht enthalten"`
  - Tests sind auf der Kommandozeile möglich:  
       `$ echo "ein Test." | tr '[a-z]' '[A-Z]'`


## Shells

### Bourne-Shell

- sh
- die Mutter aller Shells, von Stephen R. Bourne entwickelt, 1977/1978 veröffentlicht


### Bash

- wohl am weitesten verbreitet und überall installiert
- Bash= Bourne again shell = sh 2.0
- Erscheinungsjahr: 1989


### xonsh

- [xonsh shell](https://xon.sh/)
- erlaubt das Mischen von Python- und Shell-Befehlen auf der Kommadozeile



## siehe auch

- [Shell](https://wiki.ubuntuusers.de/Shell/) bei UU


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Wissen<br />
letzte Änderung: 2024-11-04<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
