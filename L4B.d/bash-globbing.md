# Globbing

## TL;DR

- globbing = [RE](./REs.md)  light
- Nichts von der Shell-Interpretierbares in Namen verwenden.
- `*` `?`

## Basics

- globbing = RE light
- Die Bash unterstützt keine Regulären Ausdrücke (RE) wie Perl, Python, egrep, sed, awk, …
- Globbing: Die Shell interpretiert bestimmte **wild cards**, die man in ihrer Bedeutung noch erweitern kann. Es wird hauptsächlich für die Suche nach Datei-  oder Verzeichnis-Namen genutzt.


## Wildcards und ihre Erweiterungen

- **?**    Genau ein beliebiges Zeichen
- **\***    0 oder beliebig viele Zeichen

Beispiel:

```bash
cd /tmp
touch a.pdf b.pdf c.PDF   1.jpg 2.JPG 3.jpeg 4.JPEG

# Welche Files matchen die folgenden 3 Zeilen?
ls -al *.pdf
ls -al *.jp*g
ls -al *.jp?g
```

## Zeichenklassen

- `[abc] [a-z] [5-9]`   Genau ein Zeichen aus Liste/Bereich (Zeichenmenge)
- `[!abc] [!a-z]`  Genau ein Zeichen **nicht** aus Liste/Bereich

- komplexere Klassendefinitionen:  
  - `[0-9a-Z]`
  - `[012a-h]`


## Zeichenmengen

`{abc,def,123}`  d.h. `,` = oder

- komplexere Mengendefinitionen:  
  - {?????.sh,*xy.txt}
  - {a,b{c,d}}e.txt

## Maskierung, Quoting

Maske: hebt besondere Bedeutung auf, macht aus einer wild card wieder ein normales Zeichen

- `\` maskiert das eine folgende Zeichen
- `'  '`   maskieren alles zwischen den Hochkommata
- `"  "`  Weak Quotes maskiert alles außer **$** und den anderen Maskenzeichen.

- Beispiele:
  - Abräumen eines Files mit Blank im Namen: `$ rm a\ b.txt`  
  - `$ echo "meine Shell: $SHELL"`  aber
  - `$ echo 'meine Shell: $SHELL'`



## Konsequenzen für Filenamen

- Keine Filenamen mit Zeichen, die von der Shell interpretiert werden.
- Keine Blanks, weil `$ mv a b c`
- Zeichen für Filenamen:
  - **Buchstaben**, Umlaute können Probleme machen in manchen Programmen
  - **Zahlen**
  - **diese Sonderzeichen**: . , _ - (Minus nie als 1. Zeichen)

## Aufgaben

- Spielwiese anlegen:  
`$ mkdir /tmp/SW ; cd /tmp/SW`  
`$ touch a.Pdf b.PDF c.pdf xyz .xyz xyz2  k.jpg l.JPG m.jpeg n.JPEG o.JPg`
- Welche Ausdrücke listen (ls ..) folgende Filenamen:
  - alle .PDF und .pdf Dateien
  - xyz  .xyz
  - JPEGs in 4 Schreibweisen: .jpg .JPG .jpeg .JPEG



## Siehe auch

- [HOWTO zum Shell-Globbing](https://www.ostc.de/howtos/shell-globbing-HOWTO.txt)
- [Bash Globbing Tutorial](https://linuxhint.com/bash_globbing_tutorial/)
- [How To Use Bash Wildcards for Globbing?](https://www.shell-tips.com/bash/wildcards-globbing/#gsc.tab=0)
- [Quoting](https://bash.cyberciti.biz/guide/Quoting)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Shell<br />
letzte Änderung: 2025-02-26<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
