
# Die Vorlesung

- Linux for Beginners (3. Semester):  
[https://t1p.de/linuxba3](https://t1p.de/linuxba3)  = **Linux** für **BA**-Studierende im **3**. Semester

![URL zur Vorlesung 3. Sem.](../Pics.d/QR_Code_t1p-de-linuxba3.png)

## Dozent

- Helmut Hayd
- 30a UNIX
  - high performance computing, Admin, Grafik, Sicherheitsfragen, ...
- 25a Linux
- 25a IT-Leiter an einem MPI
- IT-Sprecherkreis, Berater des Präsidenten, ...
- Linux-Tage der MPIs, Workshops zu Freier Software, ...
- ab 1997 Betreuung des 1. BA-Studenten, danach noch ca. 60 Studenten aller Hochschulen
- Motivation:
  - Linux ist wichtig
  - Meckern genügt nicht
  - Alternativen aufzeigen
  - Geld, Beschäftigung ... wohl kaum
- Erwartung: mitmachen, mal das Handy weglegen



## Ziele

- erste, kompetente Eindrücke von Linux
- Linux als Alternative berücksichtigen


## Inhalte

- siehe [ToC](./1-ToC.md)
- Grundlagen
- bewährte Tools


## Stil der Vorlesung

- konkrete Beispiele statt [Metasprache](https://de.wikipedia.org/wiki/Backus-Naur-Form) zur Syntax-Erklärung
- Mitmachen auf der Linux-VM
- Vermittlung von Erfahrungen; googeln, KI fragen kann jeder
- viele URLs
- Die Halbwertszeit des IT-Wissens liegt bei 2a, daher lebendes Dokument (MarkDown, git)
- anfangs ein didaktischer Hindernislauf: Mitmachen, aber noch nicht alles verstehen


## Auditorium

- Wer nutzt privat Linux?
  - Welche Distribution?
- Wer nutzt beim Praxispartner Linux?
  - Wofür?


---



<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  <br />
letzte Änderung: 2025-02-19<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>

