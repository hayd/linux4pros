# Prompt

Rechts vom **Prompt** kann man einen Befehl auf der Kommandozeile eintragen.

## TL;DR

Konfiguration unterirdisch, daher nur bei Bedarf von Empfehlung abweichen.

## Konvention

(in Dokus) üblich:
- `$ user` (Fehler kosten Geld.)
- `# root` (Fehler führen hinter Gitter.)

## Anpassung

- In der Variablen **PS1** wird der Prompt definiert.
- Syntax max. kryptisch
- root: rot
- user: blau, ...
- in Prompt:
  - user
  - host
  - Befehlsnummer
  - $PWD in Terminal-Rahmen, nicht im Prompt

## Empfehlung

- zeigt host name, user name, Befehlsnummer
- in ~/.bashrc eintragen:  
`PS1="\[\033[34m\]\u@\h(\#)->\[\033[0m\]"`


## siehe auch

- [bei ubuntuusers](https://wiki.ubuntuusers.de/Bash/Prompt/)
- Prompt-Generator:
  - [Easy Bash PS1 Generator](https://ezprompt.net/)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Admin Empfehlungen<br />
letzte Änderung: 2024-03-06<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>