# Der Linux-Befehl

am Beispiel `ls`

## Basics

![die Anfänge](../Pics.d/cray1.jpg)

- kurz (w, wc, ls, du, awk, ...) da ursprünglich von Spezialisten für faule Spezialisten
- GUIs kamen erst viel später.
- meist: `Befehl` `-Optionen` `Parameter`


## häufige Optionen

- **-a**: all
- **-h**: human-readable, 3,7G statt 3873501184
- -**h**: help
- **-l**: long
- **-v**: verbose

oft auch Langversion: `--all`, `--human-readable`, `--verbose`


## mehrere Optionen

`$ ls -a -l -h -F`  oder `ls -alhF`

daher: `ls --all` zur Unterscheidung von `ls -a -l`  (= ls -al)


## Alle Optionen und ihre Erklärung

Optionen werden in verschiedenen Dokus beschrieben:

- kürzeste Doku: `$ ls --help`
- man(ual) page, komplette Doku: `$ man ls`
- [ManKier](https://www.mankier.com/1/ls) - übersichtlicher, mit Beispielen als Einstieg

## Beispiele

- `$ ls -ahlF`
- `$ ls --all -l -hF`
- `$ ls -al *.png`

## Hilfen

- Mit `TAB` kann man den Befehl vervollständigen lassen (und Tippfehler vermeiden), sobald Eindeutigkeit gegeben ist:  
`$  ls -al  /bin/grub-` TAB TAB  
`$  ls -al  /bin/grub-sy` TAB  

- `$ history` zeigt die bisher eingetippten Befehle

- Mit den Pfeiltasten (hoch, runter) kann man Befehle aus der history zurückholen.


## Die wichtigsten Befehle

- [Befehlsübersicht](https://wiki.ubuntuusers.de/Shell/Befehls%C3%BCbersicht/)
  - Grundkommandos  
`cat`, `cd`, `cp`, `ln`, `ls`, `man`, `mkdir`, `more`, `mv`, `pwd`, `rm`,`sudo`
  - Systemüberwachung  
`df`, `du`, `free`, `htop`, `kill`, `killall`

## Aufgabe

Beschreiben Sie kurz (Einzeiler) die Aufgabe der Befehle in diesem
[Pad](https://pad.gwdg.de/cUhwUyrQSiuuTQx6xk83kQ#) und schreiben Sie die 3 wichtigsten Optionen dazu.

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Wissen <br />
letzte Änderung: 2024-10-27<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>

[1](./Befehl_allg.1.spl.md) [10](./Befehl_allg.10.spl.md) [15](./Befehl_allg.15.spl.md)
