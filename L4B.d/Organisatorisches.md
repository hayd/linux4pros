# Organisatorisches

## Tage der Vorlesung, Klausur

- Do., 20. Feb. 2025,  14:00 - 17:15
- Do., 27. Feb. 2025,  10:00 - 17:15
- Fragestunde per Zoom vor Klausur am ... ???
  - Uhrzeit?


## Prüfung

- Do., 27.3.2025
- schriftlich
- 30 Min.
- Hilfsmittel: lokale VM
- Inhalt: eine Idee, wie, mit welchen Linux-Mitteln man eine Aufgabe lösen könnte. Eher Allgemeines/Grundlegendes, keine Details.
- nicht Prüfungsgegenstand: Details wie Befehls-Optionen auswendig
- nächstes Mal noch mehr


<!--

## Wiederholung

Ähnliche Fragen könnten in der Klausur erscheinen. Die zitierten Antworten gaben keine Punkte.

1. Warum sind Linux und Windows so verschieden?
   - weil Linux kostenlos ist
2. Unterschiede hinsichtlich Sicherheit?
   - weil Linux für Server ist
3. Warum Pakete?
   - weil dann alles einfacher ist
4. Warum Snap-Pakete?
5. Warum ist .deb besser als Flaptpack-Paket?
   - weil .deb von Debian ist
6. Was ist .rpm?
7. Wichtigste Sicherheitsmaßnahme unter Linux?
   - aufpassen
8. Wie konkret?
9. Was ist dpkg?
10. Wozu benötigt man dann noch apt?
    - apt kann mehr
11. Welche formalen, schnell zu überprüfenden Kriterien zeichnen ein gutes, freies Software-Paket aus?
    - Es kostet nichts.
12. Wie finde ich ein Software-Paket für einen bestimmten Zweck ...
13. ... in meiner aktuellen Distri?
      - googeln
14. Komponenten einer Distribution?
      - viele Pakete
15. Welche Distribution ist warm fast immer eine gute Wahl?
      - Red Hat

-->

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Org<br />
letzte Änderung: 2025-02-19<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
