# Interaktive Bash

## TL;DR

- Die interaktive Bash ist die **Mensch-Maschine-Schnittstelle**, mit der man im Terminal arbeitet.
- Sie sollte durch Eintragungen in **~/.bashrc** ungefährlicher und effektiver gestaltet werden.
- Sie **interpretiert** alle Eingaben, bevor sie fies an das OS weitergibt.


## Environment

- Die Gesamtheit der Environment-Variablen bestimmt die Bash im Terminal maßgeblich mit.
- Auflistung aller gesetzten Environment-Variablen (typischerweise > 60):  
  `$ env`


## Konfiguration

- Pro-File wird gelesen bei Start der Shell und nimmt Einstellungen vor.
- /etc/bash.bashrc = globaler (d.h. für alle User auf dem System) Profile für die Bash
- ~/.bashrc = eigener Profile
- Kontrolle von [$PATH](./Variable.md)
- neue Konfiguration testen: `$ source ~/.bashrc`


## Die Konfigurationsdateien

### ~/.bashrc

Das sollte (unbedingt) im ~/.bashrc stehen:


```bash
# no rights for group and other (must have)
umask 0077  

# make destroying commands safe (must have)
alias cp='cp -ip'
alias mv='mv -i'
alias rm='rm -i'

# some usefull aliases
alias .='echo "cd $PWD"'
alias ..='cd ..'
alias h='history  | tail -50 | more'

# extended alias
function kw   { kwrite  $1 --geometry 700x800  2> /dev/null  & }

# good promt
PS1="\[\033[34m\]\u@\h(\#)->\[\033[0m\]"

# redefine keys
xmodmap -e "keycode 106 = slash          "
```

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Shell<br />
letzte Änderung: 2024-11-04<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
