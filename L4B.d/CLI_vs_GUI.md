# CLI oder GUI

## TL;DR

auf Dauer: CLI

## CLI - Command Line Interface

- reproduzierbar, dokumentierbar
- copy 'n paste
- kann man scripten
- aber: Absaufen im Wust von (unbekannten) Optionen, z.B: `$ tar --help`, Abhilfe: Aliases und Scripte mit Kommentaren


## GUI - Grafische User Interface

- Mausschubser sind das gewohnt
- andersartige Eingabe-Möglichkeiten realisierbar (z.B. [color picker](https://github.com/keshavbhatt/ColorPicker))
- Überprüfung der Eingabe (per Java-Script)
- Hilfe bei Eingabe: Vordruck in Feld, eingabe-abhängige De-/Aktivierung von GUI-Elementen
- User können geführt werden
- z.B. [gimp](https://www.gimp.org/release-notes/gimp-2.10.html)


## Empfehlung

- CLI: bei häufiger Nutzung, Doku mit copy 'n paste-Möglichkeit wichtig
- GUI: bei seltener Nutzung, wenn sehr komplexe Bedienelemente nötig


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Wissen<br />
letzte Änderung: 2024-03-06<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>