# Debian-Pakete

## Warum Pakete ?

- **einfache** Installation, rückstandsfreie De-Installation
- **Abhängigkeiten** werden aufgelöst, z.B. fehlende Libraries
- formale Vorgaben, so dass die Tools **dpkg** und **apt** damit umgehen können.
- Der Paketbau beinhaltet ein **Qualitätsmanagement**, daher muss sich der [Maintainer](https://wiki.debian.org/DebianMaintainer)  qualifizieren und Vertrauen erarbeiten.
- Alle Pakete einer Version einer Distri kommen sich normalerweise nicht in die Quere und beruhen auf einer **gemeinsamen Basis** (Satz von Libraries), die ausführlich **getestet** wird (im [Feature Freeze](https://linuxnews.de/2022/10/17/debian-freeze-fuer-debian-12-bookworm-rueckt-naeher/)).


## Pakete ala Debian

- Ein Binärpaket muss min. bestehen aus:
  - ausführbaren Dateien,
  - Konfigurationsdateien,
  - man/info-Seiten,
  - MD5-Prüfsummen,
  - Copyright/left-Informationen,
  - Maintainer-Skripten: [preinst, postinst; prerm, postrm](https://debiananwenderhandbuch.de/debianpaketeerstellen.html#debianpaketeerstellenpostinst)


## dpkg

- [home](https://github.com/guillemj/dpkg)
- dient zum Installieren einzelner .deb-Pakete
- Basis der höheren Installationstools apt*
- löst keine Abhängigkeiten auf
- .deb installieren, z.B. [Zoom](https://zoom.us/download?os=linux):  
  `$ sudo dpkg -i ./zoom_amd64.deb`
- .deb de-installieren:  
  `$ sudo dpkg -r  agrep`
- Konfigurationsdateien auch noch entfernen:  
  `$  sudo dpkg -r --purge agrep`

dpgk kann man auch für Inventuren nutzen:
  
- Anzeige und Zählen aller installierten Pakete:  
  `$ dpkg -l`  
  `$ dpkg -l | wc -l`

- Anzeige aller Files in einem Paket, um z.B. den Namen des executables zu finden:  
  `$ dpkg -L apt` #   372 Files
  - zoom_amd64.deb besteht aus 1380 Files
- siehe auch: [dpkg bei UU](https://wiki.ubuntuusers.de/dpkg/)


## Advanced Packaging Tool, APT

- Tool, um der [Dependency hell](https://en.wikipedia.org/wiki/Dependency_hell) zu entkommen

![ Dependency hell](../Pics.d/dependency-hell.png)

Beispiel [Gentoo Linux](http://disfunksioneel.blogspot.com/2011/04/linux-software-dependencies.html): 63.988 Abhängigkeiten zwischen 14.319 Paketen

- `apt` und `dpkg` nutzen die gleiche Datenbank (flat file DB = ein Haufen Dateien), die alle Metadaten über die Pakete und ihren Installationszustand enthält
- `apt` nutzt `dpkg`
- Oft wird statt `apt` auch der Vorläufer [apt-get](https://itsfoss.com/apt-vs-apt-get-difference/) benutzt.
- heruntergeladenes Zoom-Paket installieren:  
  `$ sudo apt install /tmp/zoom_amd64.deb`
- Text-Editor mit vielen Abhängigkeiten installieren:  
  `$ sudo apt install kwrite`
  - Wie viele Pakete zieht kwrite nach sich als Abhängigkeiten?
- Paket entfernen:  
  `$ apt remove kwrite`
- Paket + Konfigurationsdateien entfernen:  
  `$ apt purge kwrite`


## Quellen für Pakete

### Repositories

- Repository = Quelle für Paket(e)
- aufgelistet in:
  - `/etc/apt/sources.list` - die offiziellen Pakete der Distribution,
  - `/etc/apt/sources.list.d/xyz.list` - Pakete "privater"  Anbieter, z.B.  vscode.list, google-chrome.list, phoerious-ubuntu-keepassxc-focal.list
- [Paketquellen](https://wiki.ubuntuusers.de/Paketquellen/) - deren Kategorisierung und Beurteilung
- [sources.list bei UU](https://wiki.ubuntuusers.de/sources.list/)
- zusätzliche [Paketquellen freischalten](https://wiki.ubuntuusers.de/Paketquellen_freischalten/): Vorsicht! Seriosität der Quellen prüfen. Vertrauensbildend sind z.B:
  - Erwähnung in seriösen Journals, Online-Magazinen, Wikis, ...
  - viele positive Google-Treffer (nicht Warnungen)


## PPA

- PPA = Personal Package Archive
- Pakete von Personen, die keinen [Maintainer](https://wiki.debian.org/DebianMaintainer)-Status haben oder aktuellere Versionen anbieten, in einem privaten, unkontrollierten Repository
- Beispiele: [Zim](https://zim-wiki.org/downloads.html) (personal wiki), [VS Code](https://code.visualstudio.com/) (Entwicklungsumgebung), [KeePassXC](https://keepassxc.org/) (Passwort-Safe)
- PPA hinzufügen (für KeePassXC):  

```shell
$ sudo add-apt-repository ppa:phoerious/keepassxc
$ sudo apt update
$ sudo apt install keepassxc
```

- In /etc/apt/sources.list.d/ erfolgt ein zusätzlicher Eintrag.
- [PPA bei UU](https://wiki.ubuntuusers.de/Paketquellen_freischalten/PPA/#PPA-hinzufuegen)


## Was ist im Angebot?

... bei Ubuntu in meiner installierten Version  
`$ apt search editor`  
`$ apt-cache search editor  # kompaktere Output`

- [Ubuntu-Paketsuche](https://packages.ubuntu.com/de/) via Browser
- Stöbern mit Hilfe von [Kategorien](https://packages.ubuntu.com/de/jammy/) im Browser



## siehe auch

- [Grundlagen des Debian-Paketverwaltungssystems](https://www.debian.org/doc/manuals/debian-faq/pkg-basics.de.html)
- [APT Übersichtsseite](https://wiki.ubuntuusers.de/APT/)
- [apt bei UU](https://wiki.ubuntuusers.de/apt/apt/)
- [Linux Packages Under The Hood](https://www.juliensobczak.com/inspect/2021/05/15/linux-packages-under-the-hood.html)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Admin Ubuntu<br />
letzte Änderung: 2025-02-20<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>

<!--- 2do: ein paar Aufgaben  -->
