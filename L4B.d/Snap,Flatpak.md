# Snap, Flatpak

## TL;DR

Welches Paket soll ich nehmen?

1. Wahl: .deb
2. Wahl: snap  (auf Ubuntu)
3. Wahl: Flatpak


## Zielstellungen

- ein Paket **für alle** Distributionen
- ein stets **aktuelles** Paket **unabhängig** von allen anderen Paketen der Distribution


## Realisierung

- **alle benötigten** Binaries, Bibliotheken, Konfigurationsdateien und sonstige Dateien werden in das Snap-, Flatpak-Paket gesteckt. Keine Abhängigkeiten zu Programmen außerhalb dieses Paketes.
- keine Rücksicht auf **Umgebung**, z.B. Library-Versionen, [FHS](./FHS.md)
- mehr **Sicherheit** durch Sandbox. Deren Rechte muss man aber definieren.


## Pro

- läuft **überall**, d.h. in jeder Distribution in jeder Version
- **Aktualisierung** eines Programms kann unabhängig von der Betriebssystem-Basis und den anderen .debs erfolgen.
- Parallele Nutzung **unterschiedlicher Versionen** von Programmen.
- Software-Entwickler müssen **weniger Zeit** für Wartung/Paketierung investieren, es bleibt mehr Zeit für neue Funktionen.
- **Authentizität**: Software kommt (vom Maintainer) unverfälscht direkt von der Quelle
- einfacheres **Testen** einer Software, da keine Nebenwirkungen, Compilierung, ...
- Snaps, Flatpacks laufen in einer **Sandbox** mit geregeltem/reduziertem Zugriff auf das Hostsystem.


## Contra

- **keine Qualitätssicherung** durch die vielen Maintainer einer Distribution
- **Paketquelle** von unbekannter Vertrauenswürdigkeit
- Bei **Sicherheitsupdates** für einzelne Bibliotheken müssen alle Snaps, Flatpaks aktualisiert werden, die diese Bibliotheken enthalten. Es besteht ein Risiko, dass dies vereinzelt nicht konsequent umgesetzt wird.
- Ein höherer **Speicherplatzverbrauch**, da letztlich dieselbe Bibliothek (ggf. in unterschiedlichen Versionen) mehrfach installiert wird.
- längere **Startzeit**, größeres Paket als .deb


## Snap

- maßgeblich von **Canonical** entwickelt
- Canonical (Firma hinter Ubuntu) ist der Infrastruktur-**Monopolist** von Snap
- Snaps versammelt im [Snap Store](https://snapcraft.io/store) mit "thousands of snaps"
- snap installieren:   `$ sudo snap install SNAPNAME`


## Flatpack

- [Flatpack](https://flatpak.org/) bildet eine Alternative zu Snap.
- ein **Community-Projekt**, per Design komplett dezentral. Jeder kann einen eigenen Flatpak-Server eröffnen, aber [Flathub](https://flathub.org/apps) bildet eine zentrale Anlaufstelle
- **1600** Flatpaks (ca. **4500** Snaps) werden von 1200 Programmierer für 800.000 aktive Nutzer gepflegt.


## Aufgabe

Suchen Sie den in einer [früheren Aufgabe](./Kriterien4guteFOSS.md) bestimmten

- besten Dateimanager
- bestes Snapshot-Tool (Bildschirmfotos)
- besten Text-Editor
- besten Terminal-Emulator
- besten Audioplayer
- besten Videoplayer
- besten Bildbetrachter
- besten PDF-Viewer

als Debian-, als Snap- ud als Flatpack-Paket.  
Wie groß sind die Versionsunterschiede?

## siehe auch

### Vergleiche

- [Linux erfindet sich neu](https://www.heise.de/select/ct/2018/13/1529631413897687) - App-Formate sollen Softwareinstallation bei Linux revolutionieren, c't 13/2018
- [Flatpak / Snap vs. Paketverwaltung – Alles was dazu gesagt werden muss](https://curius.de/2021/09/flatpak-snap-vs-paketverwaltung-alles-was-dazu-gesagt-werden-muss/)
- [Frage zur Sicherheit und Sandbox Snap und Flatpak](https://www.linux-bibel-oesterreich.at/viewtopic.php?f=16&t=966&sid=42449ddfbe7134cfd4ef3c32b3b64807&p=2944#p2944)
- [Flatpak, Snap und AppImage im Vergleich](https://www.linux-community.de/ausgaben/linuxuser/2018/02/dreikampf/) - aus LinuxUser 02/2018


### Flatpak

- [bei UU](https://wiki.ubuntuusers.de/Flatpak/#ber-Flatpak-installierte-Anwendungen-koennen-nur-auf-bestimmte-Ordner-zugreifen)
- [Flatpak Is Not the Future](https://ludocode.com/blog/flatpak-is-not-the-future) - detailierte Diskussion der Probleme
- [The issue with flatpak's permissions model](https://whynothugo.nl/journal/2021/11/26/the-issue-with-flatpaks-permissions-model/)
- [Die Rechte von Flatpaks mit Flatseal unter Linux regeln](https://www.linux-bibel-oesterreich.at/viewtopic.php?f=10&t=535&p=1599#p1599)
- [Using Flatpak on Ubuntu and Other Linux Distributions - Complete Guide](https://itsfoss.com/flatpak-guide/)
- [Linux APP Summit 2022: Flathub wird zum App-Store](https://www.heise.de/news/Linux-APP-Summit-2022-Flathub-wird-zum-App-Store-7070718.html)
- [Flatpak im Rückwärtsgang](https://gnulinux.ch/flatpak-im-rueckwaertsgang)


### Snap

- [Debugging](https://wiki.ubuntuusers.de/snap/Debugging/) - snap bietet einige Tools für das Debugging von snaps.
- [Snap documentation](https://snapcraft.io/docs)
- [Paketmanagement mit Snap auf Linux-Systemen](https://www.admin-magazin.de/Das-Heft/2018/01/Paketmanagement-mit-Snap-auf-Linux-Systemen), IT-Administrator 01/2018
- [Hey snap, where’s my data?](https://ubuntu.com//blog/hey-snap-wheres-my-data)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Admin Security Ubuntu<br />
letzte Änderung: 2025-02-26<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>

<!---  2do: snap statt /usr/bin starten  -->
