# Shell-Scripte

## Allgemeines

- Ein gutes (= mit guten Kommentaren) Skript ist auch eine gute Doku.
- einfaches Debugging
- Davon lebt die Anpassung des Linux-Arbeitsplatzes.
- [beliebteste Programmiersprachen im 3. Quartal 2022](https://www.heise.de/news/Programmiersprachen-Ranking-JavaScript-gewinnt-das-Rennen-7317740.html): 14. Platz: Shell


## Bash oder Python?

Bash- oder Python-Script ?

### pro Bash

- nur einfache Abfolge von Linux-Befehlen mit schlichten Datenstrukturen (max. kleines, lineares Array; temporäre Datei) und einfachen  [Kontrollstrukturen](https://de.wikipedia.org/wiki/Kontrollstruktur).
- Manche Linux-Befehle sind wie komplexe Unterprogramme, z.B. grep, [ImageMagick](https://de.wikipedia.org/wiki/ImageMagick)

### pro Python

- objekt-orientierte Programmierung
- längere Scripte, die man strukturieren sollte
- Verbreitung in der jeweiligen Community (z.B. AI)
- komplexe Datenstrukturen
- Nutzung mächtiger Module, Libraries (z.B. [pandas](https://de.wikipedia.org/wiki/Pandas_(Software)), [NumPy](https://de.wikipedia.org/wiki/NumPy), [Matplotlib](https://matplotlib.org/))
  - aber: Programme wie sed, [Gnuplot](https://de.wikipedia.org/wiki/Gnuplot), [QPDF](https://qpdf.sourceforge.io/), usw., die man über ein Bash-Script bedient, fungieren wie mächtige Libaries.

### aus [Google Shell Style Guide](https://google.github.io/styleguide/shellguide.html#s7-naming-conventions)

*If you are writing a script that is more than **100 lines** long, or that uses non-straightforward control flow logic, you should rewrite it in a more structured language now. Bear in mind that **scripts grow**. Rewrite your script **early** to avoid a more time-consuming rewrite at a later date.*


## Das 1. Script

### Vom File zum Programm

1. Spielwiese anlegen: `$ mkdir /tmp/Script && cd /tmp/Script`
1. unten stehendes Programm in einen Editor kopieren
1. abspeichern als xyz.sh
1. ausführbar machen: `$ chmod ugo+x xyz.sh`
1. ausführen: `$  ./xyz.sh`
1. ggf. Script dauerhaft abspeichern (in ~)


### Die ersten Zeilen

- `#!` sagt, dass die folgenden Kommandos von dem angegebenen Interpreter ausgeführt werden sollen.
- [set -x](https://linuxhint.com/set-x-command-bash/) listet jede Zeile mit expandierten Variablen vor der Ausführung auf. Gut für Debugging

```bash
#! /bin/bash
# the shebang line
#set -x

for file in *.jpg
do
    convert -resize 200  $file  $file.jpg
done

exit

--------------- some notes  ------------------

2do:
- more commands
- [Bash For Loop Examples](https://www.cyberciti.biz/faq/bash-for-loop/#TOC)
```

## Aufgaben

- Laden Sie einige große Bilder als Paket herunter und konvertieren Sie diese mit obigem Script:

```bash
sudo apt install ubuntu-mate-wallpapers-photos
cp -r /usr/share/backgrounds/ubuntu-mate-photos /tmp
cd /tmp/backgrounds/ubuntu-mate-photos
./obiges_script
```

- Wie viele MB werden dadurch eingespart?
- Konvertieren mit `set -x`.


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Programmierung Shell<br />
letzte Änderung: 2024-11-04<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>

