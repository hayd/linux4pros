# Desktop-Umgebungen (DE)

## TL;DR

- Programme sind viel wichtiger als und (fast) unabhängig von DE (Desktop Environment)
- DE ist auch nur ein Grafik-Programm.


## Basics

- die graphische **Mensch-Maschine-Schnittstelle**
- Jede DE bringt ihre **eigenen Komponenten** mit. Z.B: Look+Feel, Dateimanager,Texteditor, Terminal-Emulation, ...
- Es gibt eine **Interoperabilität** von Desktop-Umgebungen für das [X-Window-System](https://de.wikipedia.org/wiki/X_Window_System)  durch desktop-übergreifende Standards, d.h. die Komponente von DE A funktioniert auch auf DE B.
  - Beispiel: Reaktion auf Einstecken eines USB-Sticks

- **Entscheidung** für ein DE ist leicht zu ändern.
- Ein User pro DE anlegen, damit sich **benutzerbezogene Konfigurationen** sich nicht in die Quere kommen.
- Es gibt mehr als [20 brauchbare DEs](https://de.wikipedia.org/wiki/Desktop-Umgebung#Galerie).


## Die wichtigsten Desktop-Umgebungen

### GNOME Shell

- Konzept:  schick, einfach, überzeugend, elegant
- viel vorkonfiguriert: Entwickler wollen User nicht überfordern vs. bevormunden User. Abhilfe: [GNOME Tweaks](https://www.linuxfordevices.com/tutorials/linux/gnome-tweaks)
- bei vielen Distributionen Default


### KDE

- graphisch am anspruchsvollsten
- konfigurieren bis der Arzt kommt
- \>200 [KDE-Anwendungsprogramme](https://apps.kde.org/de/), deren Name normalerweise mit K beginnen


### Xfce

- leichtgewichtiges DE: weniger Entwickler, weniger Komponenten, weniger Ressourcen-Verbrauch
- [home](https://www.xfce.org/)
- [Documentation](https://docs.xfce.org/), [Developer Wiki](https://wiki.xfce.org/), [ubuntuusers](https://wiki.ubuntuusers.de/Xfce/)
- Erscheinungsjahr: 1996
- eine offizielle Variante von Ubuntu: [Xubuntu](https://wiki.ubuntuusers.de/Xubuntu/)
- gut konfigurierbar
- komplette Liste der XFCE-Pakete (=Addons) im Repository:  
    `$ apt-cache search xfce | grep xfce4-`

#### Die wichtigsten Komponenten

- [Dock, Startmenü](https://wiki.ubuntuusers.de/Whisker_Menu/) Whisker Menu  ist ein alternatives, besseres Startmenü für Xfce.
- [Panel, Menüleiste](https://wiki.ubuntuusers.de/Xfce_Panel/): Für sie gibt es zahlreiche [kleine Plugins](https://wiki.ubuntuusers.de/Xfce_Panel/#Erweiterungen).
  - [Workspace Switcher](https://docs.xfce.org/xfce/xfce4-panel/pager):  Umschalten zwischen virtuellen Desktops im Panel
- [Xfdashboard](https://wiki.ubuntuusers.de/Xfdashboard/) liefert Überblick über alle Fenster auf allen Desktops, Exposé
- [Clipboard, Zwischenablage](https://wiki.ubuntuusers.de/Zwischenablage/#Xfce), heisst bei Xfce clipman
- [digitale Post-its](https://docs.xfce.org/panel-plugins/xfce4-notes-plugin/start):  
  `$ apt install xfce4-notes-plugin`


#### Die wichtigsten Konfigurationen

- Panel senkrecht:  
  - via Kontext-Menü (rechte Maus-Taste)
- Workspace Switcher ins Panel installieren:
  - Kontext-Menü: > Panel > Add New Items > Workspace Switcher
- Workspace Switcher im Panel: 4 Zeilen, 1 Spalte einstellen, sonst nur ein kleiner Streifen
- Schriften an Icons im Panel entfernen, damit dieses nicht platzt:
  - Kontext-Menü: Panel > Panel Preferences > Items (Reiter) > Windows Button (Menü-Eintrag, Doppel-Klick) > Uncheck 'Show button labels'


## Aufgaben

Nehmen Sie die obigen Konfigurationen in ihrer VM vor.


## siehe auch

- [The 8 Best Desktop Environments](https://itsfoss.com/best-linux-desktop-environments/) mit Pros und Cons
- [Linux-Desktops 2020](https://www.pcwelt.de/article/1140962/der-optimale-linux-desktop.html) mit Steckbriefen
- [Best Linux desktops of 2023](https://www.techradar.com/best/best-linux-desktop), sortiert nach  Einsatzgebiet


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Software<br />
letzte Änderung: 2025-02-26<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
