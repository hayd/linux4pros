# Shell-Variable

- XYZ: die Variable (Schachtel mit Namen)
- $XYZ: Wert der Variablen (Inhalt der Schachtel)
- Variable einen Wert zuweisen: `$ a="ein string"`
- Inhalt der Variablen anzeigen: `$ echo $a`

## Environment

Environment: Gesamtheit der Variablen, die die Linux-Arbeitsumgebung (z.B. in  Shell, Terminal-Emulator, grep-Ausgabe, Datumsformat, Sprachwahl, ...) bestimmen.


## Environment-Variable

- Environment-Variablen bestimmen die **Umgebung** ganz wesentlich.
- Syntax: `export ENV_VARIABLE="some value"`
  - zum Vergleich: `my_shell_variable="some value"`
  - d.h. `export` macht aus einer normalen Variablen eine Environment-Variable.
- Die **Vererbung** unterscheidet Environment-Variable von normalen Variablen. Environment-Variablen werden bei der Prozessgenerierung vererbt, d.h. Kindprozesse bekommen eine Kopie des Environments des Vaterprozesses und vererben dies auch wieder an ihre Kinder weiter.
- Environment-Variable werden daher oft in Profiles (**~/.bashrc**) gesetzt.
- **Scope**: Environment-Variablen sind lokal, d.h. Veränderungen in Terminal 1 sind in Terminal 2 unbekannt.
- systemweite Definitionen (für alle User) in /etc/bash.bashrc
- Anzeige aller Environment-Variablen:  
   `$ printenv`  oder einfach nur `$ env`


### Wichtige Exemplare

Variable  | Bedeutung
----------|--------------------------------------------------
$HOME     | selbsterklärend
$HOSTNAME | selbsterklärend
$PS1      | main prompt
$PWD      | aktuelles Verzeichnis


### PATH

- PATH: Diese aneinandergereihten Directories werden von links nach rechts nach dem eingegebenen Befehl durchsucht. Daher: Vorsicht bei der Reihenfolge und den Erweiterungen. Am Anfang sollte stehen: `/usr/sbin:/usr/bin:/sbin:/bin`
- In [/opt, /usr/local](../Filesystem.d/FHS.md) können executables stehen, die aus nicht seriösen Quellen stammen.
- PATH erweitern:
  
```bash
LPATH=~/opt/bin:~/Scripts.d
export PATH=$PATH:$LPATH
```

- Kontrolle: `$ echo $PATH`
- Woher kommt ein Befehl?  
   `$ type bc`  


## Daten-Typen

Es gibt 3 nicht-typisierbare Daten-Typen:

1. String (default)
1. Integer
1. Array


## siehe auch

- [Shell Style Guide](https://google.github.io/styleguide/shellguide.html#s7-naming-conventions) of many Googlers
- [Lesen und Einrichten von Umgebungs- und Shell-Variablen unter Linux](https://www.digitalocean.com/community/tutorials/how-to-read-and-set-environmental-and-shell-variables-on-linux-de) - ein Tutorial
- [A Complete Guide to the Bash Environment Variables](https://www.shell-tips.com/bash/environment-variables/#gsc.tab=0)
- [Bash variables — Things that you probably don’t know about it](https://medium.com/unboxing-the-cloud/bash-variables-things-that-you-probably-dont-know-about-it-8a5470887331) - scopes and types
- [Advanced Bash-Scripting Guide](https://tldp.org/LDP/abs/html/internalvariables.html) 9.1. Internal Variables
- [typische Anwendungsfälle](https://wiki.ubuntuusers.de/Umgebungsvariable/typische_Anwendungsf%C3%A4lle/) für den Einsatz von Environment Variablen
- [Positional Parameters, Other Special Parameters](https://tldp.org/LDP/abs/html/internalvariables.html)



---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Shell<br />
letzte Änderung: 2024-03-05<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>