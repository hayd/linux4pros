# Kernel

## Basics

- [Micro-Kernel](https://de.wikipedia.org/wiki/Mikrokernel)  oder [Monolithischer Kernel](https://de.wikipedia.org/wiki/Monolithischer_Kernel) ([Andrew S. Tanenbaum vs. Linus Torvalds](https://en.wikipedia.org/wiki/Tanenbaum%E2%80%93Torvalds_debate))
- Sprachen: C oder C++, ab Kernel  [6.1](https://www.golem.de/news/torvalds-rust-im-kernel-soll-mit-linux-6-1-erscheinen-2209-168396.html) (Dez. 2022) [Rust](https://www.heise.de/hintergrund/Programmiersprache-Rust-fuer-Neugierige-9345899.html) aus [Sicherheitsgründen](https://www.heise.de/news/Rust-Code-im-Linux-Kernel-Merge-steht-laut-Linus-Torvalds-ab-Linux-5-20-bevor-7154453.html) (Vermeidung von  Speicherzugriffsfehlern oder Pufferüberläufen = Angriffsvektoren)
- [kernel.org](https://www.kernel.org/) - Quelle für Kernel-Quellen
- Der Kernel unterstützt mehr als 20 [Haupt-Architekturen](https://en.wikipedia.org/wiki/List_of_Linux-supported_computer_architectures).
- Der Kernel hat (vereinfacht) 4 Funktionen:
  1. **Speicherverwaltung**: er verteilt Speicher an die Prozesse und überwacht Einhaltung der Grenzen
  2. **Prozessmanagement**: er bestimmt, welche Prozesse die CPU  wann und wie lange nutzen können
  3. **Gerätetreiber**: er verbindet Hardware mit den Prozessen
  4. **Systemaufrufe und Sicherheit**: er nimmt Serviceanfragen von den Prozessen entgegen


## Linux-Kernel 6.0

[Some 6.0 development statistics](https://lwn.net/Articles/909625/), Oct. 2022:

- 2034 Entwickler haben beigetragen
- 15.402 Non-Merge-Commits
- \> 1,1 Million lines of code
- 60% der Änderungen sind neue beziehungsweise aktualisierte Treiber.
- die meisten Änderungen von AMD


## User/Kernel Space

Auf Hardware-Ebene kann man verschiedene **Privilegienstufen** einstellen, durch die der auf der CPU nutzbare **Befehlssatz** und der verwendbare **Speicherbereich** dynamisch eingeschränkt wird.

### Kernel Space

- Befehle dort dürfen **alles** (z.B. Teile des Linux löschen)
- direkter (=**schnellst** möglicher) Kontakt zur Hardware
- Fehler können zum **Absturz des Rechners** führen.


### User Space

- Kontakt zur Hardware nur mit Hilfe und unter **Kontrolle** von Kernel-Routinen
- **langsamer**, da kein direkter Kontakt zu Hardware; Beispiel Schreiben auf Platte:
  - Programm --> Kernel (Rechte-Check, Treiber) --> Platte
- Dort laufen die normalen Programme mit den **Rechten eines Users**.
- Fehler können zum **Absturz des Programms**, nicht des Rechners führen.
  

### Details

- [Linux fundamentals: user space, kernel space, and the syscalls API surface](https://www.form3.tech/engineering/content/linux-fundamentals-user-kernel-space)
- [A Brief Explanation of Kernel Space and User Space](https://www.fir3net.com/UNIX/Linux/a-brief-explanation-of-kernel-space-and-user-space.html)


## Kernel-Module

- Ein Modul implementiert ein **Feature** jenseits der grundlegenden Kernel-Leistungen, z.B. Treiber, Filesysteme, Schnittstellen ...
- Kernel-Module **modularisieren**, verkleinern den Monolithen. Nur benötigte Module werden dem Kernel zur Verfügung gestellt.
- Module können zur Laufzeit **automatisch nachgeladen** werden, um Funktionalität zu erweitern.
- Module ( .ko = kernel object) liegen in /usr/lib/modules/  
`$ find /usr/lib/modules/ -type f -name '*.ko'  | more`
- kmod - Program to manage Linux Kernel modules
- geladene Module anzeigen: `$ kmod list`
  - Size in Byte
- Infos zu einem Modul anzeigen: `$ modinfo snd | more`
- Man kann Module entladen und gezielt andere laden, um z.B. einen anderen GraKa-Treiber zu verwenden. Details s. [Anleitung](https://wiki.ubuntuusers.de/Kernelmodule/).


## Linux-Kernel selbst kompilieren

- Man sollte keinen Performance-Gewinn erwarten.
- Konfiguration (welche Module werden benötigt: make menuconfig) zeitraubend, aber lehrreich
- Anleitungen: [1.](https://www.renefuerst.eu/linux-kernel-6-x-fuer-ubuntu-fedora-debian-rhel-centos-mint-installieren-lpic-2/), [2.](https://blog.desdelinux.net/de/compilar-kernel-debian/)


## /proc

Das /proc-Verzeichnis ist kein übliches Dateisystem, sondern erlaubt einen Blick in aktuelle Datenstrukturen des Kernels.

```bash
$ cd /proc
$ ls -al cpuinfo    # Größe: Zahl vor Monat
$ file cpuinfo
$ more cpuinfo

$ more filesystems
```


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Wissen<br />
letzte Änderung: 2024-10-27<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>

<!--- 4 Funktionen besser erklären  -->

