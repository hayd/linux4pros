# Auswahl einer Distribution

## TL;DR

- Im Zweifelsfall das letzte [Ubuntu LTS](https://de.wikipedia.org/wiki/Ubuntu#Versionstabelle)
- [c't Linux-Praxis](https://shop.heise.de/ct-linux-praxis-2023/Print) kann helfen


## GNU
![GNU / Linux](../Pics.d/gnu-linux.700.png)

- [GNU](https://www.gnu.org/) = GNU ist Nicht Unix
- UNIX-Nachbau mit freier Software
- Software in Form von min. 386 Paketen [GNU packages](https://www.gnu.org/manual/blurbs.html) verfügbar


```


```

## Was ist eine Distribution?

![distribution](../Pics.d/distribution.png)

- [The Linux Kernel](https://www.kernel.org/)
- GNU Utilities: vor allem [C-Compiler](https://de.wikipedia.org/wiki/GNU_Compiler_Collection)
- [additional Software](https://packages.ubuntu.com/de/noble/) in Form von Paketen
- [Package Manager](./deb-Pakete.md)

```

```

## Wie viele Distributionen gibt es?

- [Liste von Linux-Distributionen](https://de.wikipedia.org/wiki/Liste_von_Linux-Distributionen)
- [DistroWatch.com: Put the fun back into computing. Use Linux, BSD.](https://distrowatch.com/dwres.php?resource=popularity) - Ranking korreliert nicht mit realer Nutzung oder Qualität, sondern nur mit Aufmerksamkeit
- [Debian Family Tree](https://de.wikipedia.org/wiki/Liste_von_Linux-Distributionen#/media/Datei:DebianFamilyTree1210.svg)
- [Ubuntu Family Tree](https://de.wikipedia.org/wiki/Liste_von_Linux-Distributionen#/media/Datei:UbuntuFamilyTree1210.svg)
- Es gibt zu viele Distris aus Unfähigkeit zur Kooperation.


## Auswahl einer Distribution

- nicht nur humorvoll  
 ![nicht nur humorvoll](../Pics.d/rich_daddy.png)

---

- ernsthafter  
![ernsthafter](../Pics.d/linux_auswahl.png)

- [c't–Linux-Netzplan](https://www.heise.de/ratgeber/Der-c-t-Linux-Netzplan-So-finden-Sie-die-passende-Distribution-6330180.html)


## Kriterien

- Wie schnell werden Security-Patches geliefert?
- Aktualität
- Pflegeaufwand
- kommerzieller Support verfügbar
- Zahl der Maintainer
- Zahl der Pakete im offiziellen Repository
- Für den speziellen Zweck (z.B. alte Hardware, Tonstudio) geeignet?


## Die wichtigsten Distributionen
### Debian
- Name:  **Ian** Murdock und seine Ehefrau **Deb**ra Lynn
- Erstveröffentlichung: 1993
-  [Mutter vieler abgeleiteter Distris](https://de.wikipedia.org/wiki/Linux-Distribution#/media/Datei:Linux_Distribution_Timeline.svg)
- \> 60.000 Programmpakete
- \> 1.000 offizielle Entwickler
- Erfinder eines leistungsfähigen Paketformates: .deb
- eher dogmatisch, weniger pragmatisch (systemd, unfreie Teile, ZFS, ...)



### Ubuntu

- user-freundlicheres Debian-Derivat
- initiiert durch den Multimillionär (1999 hatte er 500 Millionen Dollar mit Software verdient.) [Mark Shuttleworth](https://en.wikipedia.org/wiki/Mark_Shuttleworth) 
- Alleingänge, die irgendwann eingestampft werden: Display-Server Mir (statt Wayland), Unity (Desktop), Smartphone OS
- 2 [Versionen](https://de.wikipedia.org/wiki/Ubuntu#Versionstabelle)/a: .4, .10
- alle 2a eine LTS-Version, L = 5a
- kommerzieller Support


#### Ubuntu Pro

- [Ubuntu Pro](./UbuntuPro.spl.md) ist eine  LTS-Version mit bis zu 10 Jahre Sicherheitsaktualisierungen für bestimmte Pakete, z.Z.  23.000 Pakete
- Ab Oktober 2022 ist Ubuntu Pro für bis zu fünf Rechnern mit Desktop Installationen kostenfrei nutzbar.



### Red Hat

- 1993 gegründet
- 1995 Paketformat .rpm eingeführt
- 2018 von IBM übernommen
- trägt viel zum Kernel bei
- Fedora: freie Version von Red Hat
  - Lebenszyklus auf 13 Monate angelegt
  - [upstream](https://gnulinux.ch/upstream-downstream) (= Quelle für Distribution) source for Red Hat
- Red Hat Enterprise Linux (RHEL): Enterprise-Betriebssystem, Marktführer bei kommerziellen Unternehmen, [teuer](https://www.redhat.com/en/store/linux-platforms?intcmp=701f20000012m33AAA)
- CentOS: frei, zu RHEL binärkompatibel, ausgelaufen. Ersatz: [AlmaLinux](https://de.wikipedia.org/wiki/AlmaLinux), [Rocky Linux](https://de.wikipedia.org/wiki/Rocky_Linux)

### Aufgabenspezifische Distributionen

- AV Linux (AV = Audio Video)
  - [home](https://www.bandshed.net/avlinux/)
  - Debian basiert, vorkonfiguriert als eine Workstation für Audio- und Video-Produktion
  - Integration unfreier Codecs
- [Linux-Distributionen für Gamer](https://www.heise.de/ratgeber/Linux-Distributionen-fuer-Gamer-vorgestellt-9342336.html)
  - [Nobara](https://nobaraproject.org/) und [Pop!_OS](https://pop.system76.com/) wollen  einfach bedienbare Distributionen sein, die auch Gaming-tauglich sind. Das erleichtert manchen den Umstieg von Windows auf Linux.
- Asahi Linux
  - Linux für Macs mit ARM-Hardware
  - [home](https://asahilinux.org/), [Test](https://www.heise.de/news/Linux-auf-dem-Mac-Asahi-im-Test-9587737.html) 

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Empfehlungen Software<br />
letzte Änderung: 2024-10-27<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>