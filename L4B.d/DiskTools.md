# DiskTools

## Standard-Befehle

### df (disk free)

- listet Füllstand aller Datenträger mit Namen und mount point
- wichtigste Optionen: -h -l
- ohne snap Pakete:  
   `$ df -hl | grep -v snap`
- Wo bin ich und wie viel Platz habe ich hier noch?  
   `$ df -h .`


### du (disk usage)

- zeigt Größe von Ordnern
- wichtigste Optionen: -h -s
  - `-B` = --block-size=SIZE, SIZE:  K,M,G,T,P,E,Z,Y
- Auflistung alle Dateien und Ordner in aktuellen Ordner in MB, sortiert nach Größe, größte zuerst:  
   `$ du -s -BM  * | sort -nr`


## Graphische Tools

### baobab

- grafischer Disk Usage Analyzer von GMOME
- 2 verschiedene charts
- [home](http://www.marzocca.net/linux/baobab/), [Baobab – Wikipedia](https://de.wikipedia.org/wiki/Baobab_(Software))



### filelight

- Anwendung zur Darstellung der Festplattennutzung
- Alternative von KDE
- [home](https://apps.kde.org/de/filelight/)


### Aufgaben

- Starten Sie `$ baobab`  oder `$ filelight` und beantworten Sie diese Fragen:
- Wie heisst das größte Verzeichnis auf der Platte?
- Wie heisst der größte File in ihrem Home-Directory?


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Tools<br />
letzte Änderung: 2025-02-26<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
