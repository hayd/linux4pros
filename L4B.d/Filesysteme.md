# Filesysteme

## TL;DR

- Linux unterstützt [mehr als 80](https://de.wikipedia.org/wiki/Liste_von_Dateisystemen#Linux)  ganz unterschiedliche Filesysteme.
- Im Zweifelsfall nehme man **ext4** als lokales Filesystem.


## Allgemeines

- [Filesysteme im Storage Stack Diagram](../Pics.d/Linux-storage-stack-diagram_v6.2.png)
- Anzeige der auf dem Host gerade unterstützen Filesysteme:  
 `$ more /proc/filesystems`


## lokale Filesysteme

### Wie heisst mein Filesystem?

Auflisten, was lokal gemountet (dem Betriebssystem verfügbar gemacht) ist und Zeilen herausfiltern, die mit "/dev/" beginnen:

`$ mount -l | grep ^/dev/`

Hinter "type" steht das Filesystem.

### ext4

- [home](https://www.kernel.org/doc/html/latest/filesystems/ext4/)
- Wenn keine besonderen Anforderungen, dann ext4.
- ext = extended filesystem
- ext3:  journaling eingeführt
  - [Journaling](https://de.wikipedia.org/wiki/Journaling-Dateisystem#Auswahl_von_Journaling-Dateisystemen),   sorgt dafür, dass das Filesystem auch nach einem Absturz konsistent ist.
- ext4: bessere Performance, größere Files möglich
- ext4 seit Linux Kernel 2.6.28 (=Oktober 2008),  rock solid
- File max. 16 TB
- Partition max. 1 Exabyte (1 Mio. Terabyte)


### sonstige

- FAT32, vFAT,  [exFAT](https://wiki.ubuntuusers.de/exFAT/)  für überall nutzbare USB-Sticks u.ä.
- ZFS
  - eher für File-Server
  - Designziele, Features: einfache Administration (nur 2 Befehle), hohe Geschwindigkeit, Kapazität wird nur durch Hardware begrenzt
  - mehr als ein Next Generation Filesystem. Folgende Komponenten bilden eine funktionale Einheit, die Informationen austauschen und daher mehr leisten als die Summe der Einzelteile:
    - **Filesystem** mit Komprimierung, Snapshots, Replication, Prüfsummen, Verschlüsselung
    - **Logical Volume Manager** - erlaubt dynamisch veränderbare Partitionen auch über viele Festplatten hinweg
    - [RAID](https://de.wikipedia.org/wiki/RAID) - sorgt für Redundanz
    - **Monitoring-Tool**

  - [ZFS zum Üben zu Hause](../Pics.d/ZFS-Stick-Pool.jpg)

## netzwerkweite Filesysteme

![Alt text](../Pics.d/NFS.png)

- [NFS](https://wiki.ubuntuusers.de/NFS/) - Mutter der netzwerkweiten Filesysteme unter Unix



  - erlaubt das Mounten einer externen Platte an einem NFS-Server als wäre es eine lokale
- [Ceph](../Filesystem.d/ceph.md) skaliert horizontal und vertikal


## Tools

[VFS](../Filesystem.d/VFS.md) erleichtert das Schreiben von Tools für Filesysteme

- [gparted](https://wiki.ubuntuusers.de/GParted/) - Partitionierung eines Laufwerks
  - GPT-Schema verwenden
  - [Platte labeln](https://wiki.ubuntuusers.de/Labels/#Label-in-der-Praxis-verwenden)
- [tunefs](https://wiki.ubuntuusers.de/ext/#tune2fs) - Änderungen der  Parameters eines Dateisystems (z.B. Deaktivierung des Journalings, Intervall zwischen Filesystem-Checks)
- [bonnie++](https://wiki.ubuntuusers.de/bonnie%2B%2B/) - einfaches Tool für I/O-Benchmark


## siehe auch

- [Dateisystem](https://wiki.ubuntuusers.de/Dateisystem/) - gute Auflistung mit wichtigsten Merkmalen bei UU
- [Dateisysteme für Linux](https://apfelböck.de/dateisysteme-fuer-linux/)
- [Filesystems in the Linux kernel](https://docs.kernel.org/filesystems/index.html)
- [Archlinux-Wiki](https://wiki.archlinux.org/title/file_systems)
- [Comparison of file systems](https://en.wikipedia.org/wiki/Comparison_of_file_systems) - lange Liste
- [Introduction to the Linux File System](https://phoenixnap.com/kb/linux-file-system)
- [Comparison of kernel and user space
file systems](https://edoc.sub.uni-hamburg.de/informatik/volltexte/2015/210/pdf/bac_duwe.pdf)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Admin Empfehlungen<br />
letzte Änderung: 2025-02-26<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
