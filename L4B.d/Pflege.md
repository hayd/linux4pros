# Software-Pflege

## TL;DR

`$ sudo apt update && sudo apt upgrade`

unbedingt bzw. zu Beginn jeder Session


## Notwendigkeit

- In der IT-Administration gibt es keine **Projekte**, nur **Pflegefälle**.
- Ohne permanente Pflege sind die Daten des Rechners irgendwann verschlüsselt, ausgeleitet, Munition für Erpressung, ... Beispiel [Bitterfeld](https://www.spiegel.de/netzwelt/ransomware-cyberangriff-auf-anhalt-bitterfeld-gravierender-als-bislang-bekannt-a-d538f846-1926-484d-b8d6-19554ad8c4c5)
- Eine Software ohne zeitnahe Security-Patches muss entfernt (oder zumindest gekapselt) werden.


## Konkretes

- `sudo`: für einen Befehl lang root (Superuser) werden

- **Download** der neuesten Repository-Kataloge und **Vergleich** der aktuellen Installation mit den jetzt verfügbaren Paketen:  
`$ sudo apt update`  

- **Installation** der neueren Pakete:  
`$ sudo apt upgrade`  

- Update, Upgrade: keine neuen Features, sondern weniger/andere Fehler, Fixen von  Sicherheitsproblemen. Ausnahme: Browser ([firefox.deb](https://packages.debian.org/de/sid/amd64/firefox/download) : 61,3 MB)


**Snaps** und **Flatpacks** auch noch aktualisieren:  
`$ sudo snap refresh`  
`$ sudo flatpak update`




## siehe auch

- [Long Term Support, UU](https://wiki.ubuntuusers.de/Long_Term_Support/)
- [Unterschiede LTS und normale Version, UU](https://wiki.ubuntuusers.de/Unterschiede_LTS_und_normale_Version/)
- [Paketquellen, UU](https://wiki.ubuntuusers.de/Paketquellen/#main)
- [Repositories - Ubuntu Wiki](https://help.ubuntu.com/community/Repositories)
- [Ubuntu Pro](../Distributionen.d/UbuntuPro.spl.md) - 10 Jahre Patches


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Admin Security Ubuntu<br />
letzte Änderung: 2025-02-26<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
