# Linux-Windows-Vergleich

## Begrifflichkeit

![windows](../Pics.d/windows-logo.30.png) OS mit etwas Zubehör (z.B. Browser)

![tux](../Pics.d/tux-logo.30.png) OS (Kernel) + tausende Software-Pakete (=Distribution)


## Die Ursprünge

Unix-Rechner (Cray-1: 1976) - Großrechner für ein Rechenzentrum (umzingelt Tausenden ahnungsloser/alles ausreizenden/mutwilligen Usern)
![Cray1](../Pics.d/cray1.jpg)

Windows-PC (MS-DOS:1981, Windows 1.0: 1985) - verkaufbarer PC für einen User
![PC](../Pics.d/IBM-PC.jpg)


## Philosophie

![windows](../Pics.d/windows-logo.30.png) Komplexer Aufbau, aber einfache Bedienung

![tux](../Pics.d/tux-logo.30.png)

1. Schreibe Computerprogramme so, dass sie nur **eine** Aufgabe erledigen und diese gut machen.
2. Schreibe Programme so, dass sie **zusammenarbeiten**.
3. Schreibe Programme so, dass sie **Textströme** verarbeiten, denn das ist eine universelle Schnittstelle.  [mehr](https://de.wikipedia.org/wiki/Unix-Philosophie)


## Die Initiatoren

![windows](../Pics.d/windows-logo.30.png) Bill Gates will Geld verdienen.

![tux](../Pics.d/tux-logo.30.png) Linus Torvalds will mit Software Aufgaben lösen.


## Standards

![windows](../Pics.d/windows-logo.30.png) Setzen von Industriestandards um Profit zu maximieren

![tux](../Pics.d/tux-logo.30.png) Nutzung freier Standards, um Verbreitung zu erhöhen; [POSIX](https://de.wikipedia.org/wiki/Portable_Operating_System_Interface), X/Open, [RFC](https://de.wikipedia.org/wiki/Request_for_Comments), ...


## Entwicklungsmodell

![windows](../Pics.d/windows-logo.30.png) nur Microsoft bestimmt

![tux](../Pics.d/tux-logo.30.png) [200 - 300](https://www.heise.de/select/ct/2019/16/1564403599540881) Firmen tragen bei, Linus Torvalds hat das letzte Wort (Do-Ocracy)

- Entwicklungsmodelle: [Die Kathedrale und der Basar](https://de.wikipedia.org/wiki/Die_Kathedrale_und_der_Basar)

## Programmcode

![windows](../Pics.d/windows-logo.30.png)  nicht vollständig offengelegt und schlecht dokumentiert

![tux](../Pics.d/tux-logo.30.png)(fast, z.B. kundenspezifische Patches von RH) alles frei


## Konfiguration

![windows](../Pics.d/windows-logo.30.png) registry

![tux](../Pics.d/tux-logo.30.png) alles in /etc


## Lizenz

![windows](../Pics.d/windows-logo.30.png) günstige, ungefährliche Lizensierung ohne [Beratung](https://www.kerstin-friedrich.com/consulting.php) nur in Sonderfällen möglich (*"Allein von Microsoft gibt es dazu 12 Vertragsarten, hinzu kommen monatlich aktualisierte Produktlisten und neue Produktbenutzungsrechte."*)

![tux](../Pics.d/tux-logo.30.png) Kernel:  GPLv2, Rest: meist FOSS


## Kosten

![windows](../Pics.d/windows-logo.30.png) sehr unterschiedlich - Druck zur Cloud, Chronifizierung der Einnahmen

![tux](../Pics.d/tux-logo.30.png) meist keine (TCO kann trotzdem hoch sein: Schulung, Support, ...)


## Ressourcen-Bedarf

![windows](../Pics.d/windows-logo.30.png) höher

![tux](../Pics.d/tux-logo.30.png) [minimal](https://www.heise.de/ratgeber/Alte-PCs-und-Notebooks-mit-Linux-wieder-flott-machen-9201189.html), [klein](https://www.rasppishop.de/Raspberry-Pi_5_alt ) bis hoch


### Hardware-Unterstützung

![windows](../Pics.d/windows-logo.30.png) Windows muss laufen; meist vorinstalliert

![tux](../Pics.d/tux-logo.30.png) teilweise fehlen Treiber, daher vorher informieren; selten vorinstalliert bzw. [kein OS installiert](https://www.pro-com.org/hochschulen-firmen-notebooks/?p=1&o=1&n=12&f=3123)


### CPU-Unterstützung

![windows](../Pics.d/windows-logo.30.png) Intel, AMD

![tux](../Pics.d/tux-logo.30.png) für etwa 30 Prozessorplattformen verfügbar



### Stabilität

![windows](../Pics.d/windows-logo.30.png) besser geworden

![tux](../Pics.d/tux-logo.30.png) besser


### Sicherheit

![windows](../Pics.d/windows-logo.30.png) schlechter;  schlechtere Trennung Admin - User  
 [sudo kommt für Windows](https://www.heise.de/news/sudo-kommt-fuer-Windows-9622489.html)(2025?)

![tux](../Pics.d/tux-logo.30.png) besser, auch weil kleinerer Markt;  Trennung root - User von Anfang an (Multi-User-System)


### Updates

![windows](../Pics.d/windows-logo.30.png) inzwischen zwangsweise

![tux](../Pics.d/tux-logo.30.png) eigenverantwortlich, Security-Patches oft früher


## Support

![windows](../Pics.d/windows-logo.30.png) durch viele Firmen, weil so verbreitet; praktisch nicht vom Hersteller

![tux](../Pics.d/tux-logo.30.png) freundliche Communities (Kultur: man hilft sich im Netz), durch wenige Firmen, teure Linux-Abos.

- Neubewertung wegen Fachkräfte-Mangel (indische Studentin im Chat vs. dt. Firma mit 2 Wochen Reaktionszeit vor Ort)


## Benutzerfreundlichkeit

![windows](../Pics.d/windows-logo.30.png) für Nutzer ohne/mit geringen IT-Kenntnisse, viele Automatismen, Halbwissen genügt (?)  
![tux](../Pics.d/tux-logo.30.png) an manchen Stellen hohe Einstiegshürden. Meist so einfach wie Windows, aber anders.


- [Wenn man's weiss, ist alles ganz einfach!](https://gnulinux.ch/probleme-und-gnu-linux-wenn-man-s-weiss-ist-alles-ganz-einfach) - und umgekehrt




## GUI

![windows](../Pics.d/windows-logo.30.png) normal, Standards gesetzt, Programme ähneln sich

![tux](../Pics.d/tux-logo.30.png) gut, schlecht, gar nicht; hohe Diversität


## Desktop-Umgebung

![windows](../Pics.d/windows-logo.30.png) nur eine,  GUI und Windows zu einer untrennbaren Einheit verschmolzen. Nachteile: keine Auswahl, Komplexität versteckt, Fehlersuche schwieriger, Automatisierung schwieriger, Power-User lieben CLI

![tux](../Pics.d/tux-logo.30.png) ist auch nur eine graphische Applikation, kann man austauschen, [>20](https://de.wikipedia.org/wiki/Desktop-Umgebung#Galerie)


## Software

![windows](../Pics.d/windows-logo.30.png) für fast alles (auch viele [Fachanwendungen](https://de.wikipedia.org/wiki/Fachanwendung))

![tux](../Pics.d/tux-logo.30.png) weniger Anwendungen, manche Bereiche fehlen weitgehend (z.B. Fachanwendungen der Verwaltungen)



### Spiele

![windows](../Pics.d/windows-logo.30.png) optimal

![tux](../Pics.d/tux-logo.30.png) [hat sich zur praxisreifen Gaming-Plattform gemausert](https://www.heise.de/hintergrund/Der-grosse-Linux-Gaming-Guide-9355598.html)



## Zukünftige Entwicklungen

![windows](../Pics.d/windows-logo.30.png) + ![tux](../Pics.d/tux-logo.30.png)

- Das  proprietäre Betriebssystem Windows scheint auch nach und nach an Bedeutung zu verlieren – in der Cloud betriebene Abodienste, die man sich per Browser im Grunde auf ein beliebiges OS holen kann, dürften eher das Geschäft der Zukunft sein.
- [Microsoft hat eine Anleitung veröffentlicht](https://www.heise.de/news/Microsoft-hilft-bei-Linux-Installation-9333190.html), wie Nutzer  Linux installieren können.


## Linux statt Windows 11 !?

Ein ungepflegtes OS zu verwenden ist ein hohes Risiko!

- [Das Ende von Windows 10 ist der beste Anlass, endlich auf Linux zu wechseln](https://www.derstandard.de/story/3000000256195/das-ende-von-windows-10-ist-der-beste-anlass-endlich-auf-linux-zu-wechseln)
- [Linux-Umstieg: Linux Mint oder Fedora Workstation statt Windows 11](https://www.heise.de/ratgeber/Linux-Umstieg-Linux-Mint-oder-Fedora-Workstation-statt-Windows-11-9717187.html)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Admin<br />
letzte Änderung: 2025-02-20<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>















