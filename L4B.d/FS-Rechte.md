# Rechte im Filesystem

## TL;DR

- **rwx** für **ugo**
- umask 077
- Falsche Rechte können sehr unangenehme Folgen haben.

## Basics

- In den [Inodes](https://de.wikipedia.org/wiki/Inode) (grundlegende Metadaten-Struktur zur Verwaltung von Datei-Systemen) werden die Rechte festgehalten.
- Jedes Objekt (Datei, Ordner, Link, ...) hat 3 Besitzer:
  - ugo = user, group, other
  - o = other = Rest der Welt
- Jedes Objekt kennt 3 Rechte für seine 3 Besitzer:
  - rwx = **r**ead, **w**rite, e**x**ecute
    - x bei Files: nur bei Programmen sinnvoll, Voraussetzung für deren Start
    - x bei Ordner: Erlaubnis in diesen zu wechseln
- Die Rechte von Windows-Filesystemen können nicht 1:1 auf Unix-Filesysteme abgebildet werden, u.U. wichtig z.B. bei USB-Sticks
- Anzeige der Rechte: `$ ls -al`  
    Interpretation von links nach rechts:
    Daten-Typ u-Rechte (rwx) g-Rechte(rwx) o-Rechte (rwx)

```shell
drwxr-xr-x 12 linuxer users 4.2K Apr  9 20:51 Ordner/
|[-][-][-]    [-----] [---]
| |  |  |        |      |       
| |  |  |        |      +------------> Group
| |  |  |        +-------------------> Owner
| |  |  +----------------------------> Others Permissions
| |  +-------------------------------> Group Permissions
| +----------------------------------> Owner Permissions
+------------------------------------> File Type
```


## Ändern der Rechte

- mit GUI: `$ thunar /tmp`
  - Kontext-Menü > Properties > Permissions
- auf der Kommadozeile
  - Syntax: `chmod -R ugo+/-rwx  Datei/Ordner`
    - `-R`: recursiv, d.h. für alles darunter im Filesystem
  - der ganzen Gruppe Schreibrechte an Text-Files geben:  
  `$ chmod g+w *.txt ; ls -al *.txt`
  - Ausführungs- und Schreib-Rechte eines Ordners auf den owner beschränken:  
  `$ chmod -R go-rw /tmp/Xyz ; ls -al`
  

## Ändern der Besitzer

### owner ändern

- chown = **ch**ange **own**er
- Syntax: `chown [OPTIONEN] [BENUTZER][:[GRUPPE]] Datei/Ordner`
- Optionen:
  - `-R`   recursiv (-r gibt es nicht)
    - allgemeine, oft ignorierte Notation: -r: reverse ,  -R: recursive
  - `-v`   verbose
- Dem user "ba" wird ein Verzeichnis mit allen Inhalten übergeben. Das [darf](https://elearning.wsldp.com/pcmagazine/chown-operation-not-permitted/) nur root, da das nicht ganz unproblematisch ist: Quota können platzen, Inhalte können untergeschoben werden, ...  
  `$ sudo chown -Rv ba /tmp/Test`

### group ändern

- `chgrp` erlaubt es, an eine Gruppe von Nutzern Rechte zu vergeben.
- Syntax: `chgrp [Option] Gruppe Datei/Ordner`



## Voreinstellung umask

- Mit `umask` definiert man die Rechte, die automatisch bei Erzeugung neuer Files, Ordner vom System vergeben werden.
- Die Einstellungen sind von großer Bedeutung für Sicherheit und Vertraulichkeit. Bei falsch gesetzten Rechten, können andere User/Kriminelle  
  - vertrauliche Texte lesen und löschen,
  - einem kompromittierende Daten unterschieben,
  - Malware installieren,
  - Zugriffsrechte ändern, ...
- Anzeige des aktuellen Wertes in symbolischer Form: `$ umask -S`
- umask + Rechte (octal) = 777 ,  (r=4, w=2, x=1)
- **Empfehlung**: `umask 077` , d.h. owner darf alles, alle anderen nichts
- zur dauerhaften Änderung in ~/.bashrc eintragen


## Aufgabe

- Legen Sie in /tmp ein Verzeichnis mit einem File an:  
`$ cd /tmp && mkdir Rights && cd Rights && ls > a && ls -alh`
- Machen Sie Datei a lesbar für alle.
- Nehmen Sie Rights/ das x und versuchen Sie: `$ cd /tmp/Rights`


## siehe auch

- [Rechte](https://wiki.ubuntuusers.de/Rechte/)
- [Was ist umask unter Linux?](https://www.howtoforge.de/anleitung/was-ist-umask-unter-linux/)
- [Linux umask command](https://www.computerhope.com/unix/uumask.htm)
- [Rechte an Dateien und Verzeichnissen unter Linux](https://www.linux-bibel-oesterreich.at/viewtopic.php?f=10&t=1186&sid=9071b8d920690f36c75a0d3119ec26af&p=4190#p4190)
- [Rechte mit PolicyKit unter Linux vergeben](https://www.linux-bibel-oesterreich.at/viewtopic.php?f=10&t=1192&sid=9071b8d920690f36c75a0d3119ec26af&p=4214#p4214) - wenn chmod nicht mehr ausreicht
  


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Security<br />
letzte Änderung: 2025-02-26<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
