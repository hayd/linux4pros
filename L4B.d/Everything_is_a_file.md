# Everything is a file

Eines der grundlegenden Prinzipien in Linux ist:  

**"In Linux and UNIX, everything is a file"**  
   oder genauer:  
“everything is a stream of bytes”,  Linus Torvalds

Das bedeutet:

- **einheitlicher Zugriff** auf alles, was Bits enthält oder transportiert
- Unter Linux werden viele Ressourcen und Geräte **als Dateien** behandelt, unabhängig davon, ob es sich tatsächlich um herkömmliche Dateien handelt.
- **Ein-/Ausgabe-Ressourcen** wie Dateien, Verzeichnisse, Geräte  und sogar Interprozess- und Netzwerk-Verbindungen sind als einfache Byteströme via Dateisystem verfügbar.
- Nicht alles, was wie eine normale Datei aussieht, ist auch eine.
- **Vorteil**: Werkzeuge und Programmierschnittstellen für Dateien können für den Zugriff auf all diese Ressourcen genutzt werden. Dies trägt zur Einfachheit, Konsistenz und Flexibilität des Linux-Betriebssystems bei.


## Arten von Files, Streams

In Linux repräsentiert eine Datei nicht nur die üblichen Daten wie Texte, Programme, Bilder, usw. sondern diese 8 Arten von Ressourcen:

1. Files
2. Directories
3. Links
4. Mass storage devices (z.B. HDDs, SSDs, CD-ROM, Tape, USB-Sticks, ...)
5. Inter-process communication (z.B. Pipes, Shared Memory, UNIX Sockets)
6. Network connections
7. interactive terminals (z.B. /dev/console)
8. andere Geräte (z.B. Printer, Grafik-Karten)

Es gibt 3 Standard-Datenströme:

![Alt text](../Pics.d/std-streams.png)

1. Standardeingabe (stdin),  file descriptor: 0
2. Standardausgabe (stdout),  file descriptor: 1
3. Standardfehlerausgabe (stderr),  file descriptor: 2

## Redirect

- Alle Versuche in /tmp durchführen: `cd /tmp`
- Umlenken von Datenströmen, Datei-Inhalten
  - Verfolgen Sie jede Aktion mit `$ more filelist.txt` (more: seitenweise Anzeige, weiterblättern mit Leertaste)

- Output in File umlenken:  
  `$ ls -la > filelist.txt`
- Output an File anhängen:  
  `$ date >> filelist.txt`  
- Input aus File beziehen:  
  `$ wc -l < filelist.txt`
- Output und Input über Pipe direkt verknüpfen:  
  `$ ls -la |  wc -l`
- Vorsicht:  
`$ ls -al filelist.txt`  
`$  echo "noch eine Zeile" >> filelist.txt`  
`$  ls -al filelist.txt`  
`$  echo "noch eine Zeile" > filelist.txt`  
`$  ls -al filelist.txt`


## /dev/

- /dev/ enthält Pseudo-Files, die (Pseudo-) Geräte verfügbar machen, keine Treiber
- Beispiele:
  - **/dev/null** - Müllschlucker
  - **/dev/zero** - bezeichnet eine virtuelle Gerätedatei , liefert 0's
  - **/dev/urandom**  liefert Zufallszahlen
  - **/dev/tty0**     Current virtual console
  - `$ ls -al /dev/null /dev/zero /dev/urandom`  # Größe steht vor dem Monat  

## Anwendungen

- Daten verschwinden lassen: `$ ls -al > /dev/null`
- (nervige) Fehlermeldungen wegwerfen:  
 `$ kwrite file.txt 2> /dev/null &`
- Files (1GB) erzeugen mit [dd](https://wiki.ubuntuusers.de/dd/) (disk dump zum bit-genauen Kopieren von Daten):  
`$ time dd bs=1G count=1 if=/dev/zero     of=/tmp/1GBzeros`  
`$ time dd bs=1G count=1 if=/dev/urandom  of=/tmp/1GBrandom`  
`$ ls -alh /tmp/1GB*`
  - und dann komprimieren:  
`$ cd /tmp`  
`$ time lbzip2 -1  1GBrandom`  
`$ time lbzip2 -1  1GBzeros`  
`$ ls -alh /tmp/1GB*`
    - Erklären sie die Unterschiede hinsichtlich Größe und Zeiten.

- Platte löschen:
  - `dd bs=1M if=/dev/zero    of=/dev/sdX`
  - `dd bs=1M if=/dev/urandom of=/dev/sdX`


- [netcat](https://linuxize.com/post/netcat-nc-command-with-examples/) für simplen Dateitransfer im Netz
  - auf sendendem Host:  
`$ nc receiving.host.com 5555 < file_name`
  - auf empfangendem Host Port 5555 öffnen und was dort eintrifft in File umlenken:  
`$ nc -l 5555 > from_net`

## /proc/

/proc/ liefert  Schnittstellen zum Kernel in Form eines Pseudo-Dateisystems.


### Beispiele

- Uptime: `$ cat /proc/uptime ; sleep 3 ; cat /proc/uptime`
  - 1. Zahl: Uptime in Sek., vergleiche:  `$ w`
  - 2. Zahl: aufsummierte idle time (core tut nichts) aller Cores

- Eine Liste aller Dateisystemtypen, die der Kernel im Augenblick kennt:  
`$ more /proc/filesystems`

- Wie groß sind die in /proc betrachteten Files?

- siehe auch: [Das /proc-Dateisystem](https://www.linux-praxis.de/das-proc-dateisystem)



## siehe auch

- [WP](https://de.wikipedia.org/wiki/Everything_is_a_file)
- [In UNIX Everything is a File](https://hackmd.io/@jkyang/unix-everything-is-a-file)
- [Standard-Datenströme – Wikipedia](https://de.wikipedia.org/wiki/Standard-Datenstr%C3%B6me)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Wissen<br />
letzte Änderung: 2025-02-26<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
