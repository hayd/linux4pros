# Wie heißen die 10 wichtigsten Linux-Befehle?

Antwort von ChatGPT am 29.10.2024:

Hier sind 15 der wichtigsten Linux-Befehle, die für grundlegende Operationen auf der Kommandozeile unverzichtbar sind:

1. **`ls`**  
   Zeigt den Inhalt eines Verzeichnisses an.

2. **`cd`**  
   Wechselt in ein anderes Verzeichnis.

3. **`pwd`**  
   Zeigt den aktuellen Verzeichnispfad (Present Working Directory).

4. **`mkdir`**  
   Erstellt ein neues Verzeichnis.

5. **`rm`**  
   Löscht Dateien oder Verzeichnisse (mit `rm -r` für Verzeichnisse rekursiv).

6. **`cp`**  
   Kopiert Dateien oder Verzeichnisse.

7. **`mv`**  
   Verschiebt oder benennt Dateien und Verzeichnisse um.

8. **`touch`**  
   Erstellt eine neue, leere Datei oder aktualisiert den Zeitstempel einer Datei.

9. **`cat`**  
   Zeigt den Inhalt einer Datei an.

10. **`nano`** / **`vim`**  
    Texteditoren für das Bearbeiten von Dateien direkt im Terminal.

11. **`sudo`**  
    Führen Befehle als Superuser aus (mit Administratorrechten).

12. **`apt`** / **`yum`** / **`dnf`** / **`pacman`**  
    Paketmanager-Befehle, um Software zu installieren, aktualisieren oder entfernen (je nach Distribution unterschiedlich).

13. **`ps`**  
    Zeigt eine Liste der laufenden Prozesse an.

14. **`kill`**  
    Beendet Prozesse anhand ihrer Prozess-ID.

15. **`man`**  
    Zeigt das Handbuch zu Befehlen an (z. B. `man ls` für die Anleitung zu `ls`).

Diese Befehle decken die grundlegende Navigation und Verwaltung im Linux-Dateisystem ab und erleichtern den Umgang mit Prozessen und Berechtigungen.
