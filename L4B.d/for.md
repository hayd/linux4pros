# for

## die übliche for-Schleife

- allg. Syntax des Schleifenzählers:
  - `{start..end}` oder
  - `{start..end..increment}`

```bash
for i in {1..10}
do
   echo "round no. $i"
done
```

```bash
for i in {2..20..2}
do
   echo "only even numbers:  $i"
done
```

## Schleifen über Objekte

- Schleife läuft über alle nach "in" angegebenen oder durch einen Befehl erzeugten Objekte:  

```bash
for animal in dog fox 'flying fox' elephant rabbit
do ...
```

- [Konvertierung](../Prgs.d/ImageMagick.md)  aller .jpg und .jpeg Bilder:  

```bash
for file in *.jp*g
do
    convert $file  $file.png
done
```



## siehe auch

- [Bash For Loop Examples](https://www.cyberciti.biz/faq/bash-for-loop/#TOC)
- [Bash For Loop](https://linuxize.com/post/bash-for-loop/)
- [How to Use Bash For Loop in Linux](https://www.tecmint.com/bash-for-loop-linux/)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Programmierung  Shell<br />
letzte Änderung: 2023-11-13<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
