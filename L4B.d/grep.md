# grep

- **g**lobal (search) **r**egular **e**xpression (and) **p**rint
- grep durchsucht Files nach Strings oder [REs](./REs.md)
- fgrep: fast grep, kann keine REs
- egrep: beherrscht extended regular expressions


## Konfiguration

grep mit extended REs und Treffer in Farbe

- `export  GREP_COLOR='100;8'`
- `alias grep="grep -E --color=always"`
- [Modifying the color of grep](https://askubuntu.com/questions/1042234/modifying-the-color-of-grep)


## Performance

- Messungen in [linux-6.0.9.tar](https://www.kernel.org/): 35.145.060 Zeilen in 5.047 Dirs
- Suche nach Nishanth, 52 Treffer
- Suche im Filesystem auf einem Standard-PC:  
  `$ time egrep -r Nishanth linux-6.0.9` => real 0m0,835s
- Suche im Tarball (ein großer File, Teerklumpen):  
  `$ time grep -a  "Nishanth" linux-6.0.9.tar` => real 0m0,401s
- Wozu eine Datenbank, wenn es grep gibt ?
  

## Optionen

- `-i` **i**gnore-case = egal, ob klein oder groß geschrieben
- `-r` **r**ecursive = von hier bis zum Ende des Filesystems nach unten
- `-v` (in**v**ert), selektiert nur Zeilen ohne Match
- `-o` (**o**nly), gibt nur den Match aus, nicht die ganze Zeile; beste Möglichkeit zum Extrahieren von Strings
  
---  

- `-A 2` (after)  gibt zusätzlich 2 Zeilen nach Treffer aus
- `-B 5` (before) gibt zusätzlich 5 Zeilen vor Treffer aus
- `-c` (count) nur Zahl der Treffer, nicht diese selbst
- `-a` deklariert einen (fälschlich) als binary erkannten File gegenüber grep zum Text-File. Das erlaubt z.B. die Suche in Tarballs.
- `-f Datei`  mehrere Suchmuster aus Datei übernehmen
- `-w` matched nur ganze Worte


## Beispiele

mit alias von oben

- Wie viele cores hat die CPU?  
`$ grep processor /proc/cpuinfo`
- Welche Repositories nutzt apt?  
`$ grep http /etc/apt/sources.list | grep -v \#`
- Max als Wort und nicht als Sub-String suchen:  
`$ echo "Maximilian Max Maximal"| grep -w Max`
- Mailadressen extrahieren aus einem Filesystem:  
`$ grep -r  [-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]\{2,4\} /usr/share/doc`
- IP-Nummern in /etc suchen:  
`$ grep -r  "([0-9]{1,3}\.){3}[0-9]{1,3}" /etc 2>/dev/null`
- System-Meldungen nach Problemen durchsuchen:  
`$ grep -i "crit|alert|emerg"  /var/log/syslog*`
- System-Meldungen nach Start und Stop von Diensten durchsuchen:  
`$ grep -i "Started|stopped"  /var/log/syslog*`


## agrep

= fehlertolerante Suche

- Ein einziger Rechtschreibfehler kann einen ganzen Text unauffindbar werden lassen!
- Installation: `$ sudo apt install glimpse`
- Optionen:
  - `-1` mit 1 Fehler gemäß [Levenshtein-Distanz](https://de.wikipedia.org/wiki/Levenshtein-Distanz)
  - `-B` best  match
- Suche nach Müller mit und ohne Umlaut:  
   `$ echo -e "Müller \nMoeller \nMueller \nMaier" | agrep -1 Mller| grep -v o`


## Aufgaben

Spielen Sie obige Beispiele mittels *copy 'n paste* durch, so dass Sie sie erklären können.


### Alternativen

- [Tre-agrep](https://www.linux-magazine.com/Issues/2016/186/Command-Line-tre-agrep) - langsamer als agrep, beherrscht aber Unicode-Zeichen; nutzt [TRE library](https://laurikari.net/tre/about/)
- [ack](https://beyondgrep.com/) - für große source code Bäume
- [The Silver Searcher](https://github.com/ggreer/the_silver_searcher) - schneller als ack
- [ripgrep is faster than {grep, ag, git grep, ucg, pt, sift}](https://blog.burntsushi.net/ripgrep/) - kann auch komprimierte Files durchsuchen


## siehe auch

- [pdfgrep](../PDF.d/suchen.md)
- [grep Linux Tutorial mit Beispielen](https://kushellig.de/linux-unix-grep-befehl-beispiele/)
- [Using grep (and egrep) with Regular Expressions](https://linuxhint.com/grep_egrep_regex/)
- [GNU Grep 3.8](https://www.gnu.org/software/grep/manual/grep.html) - ausführlich, offizielle Doku
- [Unscharfe Suche in Texten mit Agrep](https://www.linux-community.de/ausgaben/linuxuser/2016/01/besser-finden/)


---  

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Programmierung<br />
letzte Änderung: 2024-11-04<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
