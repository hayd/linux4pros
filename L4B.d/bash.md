# Bash

- eine Programmiersprache
- mit einer gewissen [Beliebtheit](https://www.heise.de/news/Programmiersprachen-Ranking-JavaScript-gewinnt-das-Rennen-7317740.html)

## Versionen

- Version 4.0: 2009 erschienen
- Version 5.0: 2019 erschienen
  - Version 5.2.21 vom 9. November 2023
- Die Bash-Versionen 4 und 5 bringen Erweiterungen der Syntax mit, so dass man eine Suche nach Bash-Lösung auf den Zeitraum 2019 bis heute beschränken sollte.
- Abfrage der installierten Version:  
  `$ bash --version`


## Doku

- [Bash-Skripting-Guide für Anfänger bei UU](https://wiki.ubuntuusers.de/Shell/Bash-Skripting-Guide_f%C3%BCr_Anf%C3%A4nger/)
- [Bash Reference Manual](https://www.gnu.org/software/bash/manual/bash.html) - GNU manual vom Sep. 2022; harte Kost, aber maßgeblich
- [Shell Style Guide](https://google.github.io/styleguide/shellguide.html#s7-naming-conventions) of many Googlers
- [GitHub - Code Repository for Complete Bash Shell Scripting](https://github.com/PacktPublishing/Complete-Bash-Shell-Scripting-)
- [Suche bei Stack Overflow](https://stackoverflow.com/questions/tagged/bash) mit Tag  `[bash]`: > 156T Fragen und viel mehr Antworten 
- [13 resources for learning to write better Bash code](https://www.redhat.com/sysadmin/learn-bash-scripting) - Red Hat


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Shell Programmierung<br />
letzte Änderung: 2024-11-03<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
