# Linux für Einsteiger

## Einstieg

- [Die Vorlesung](./Die_Vorlesung.md) - Zielgruppe, Stil
- [Organisatorisches](./Organisatorisches.md)


## FOSS

- [Linux vs. Windows](./Linux_vs_Windows.md) - Unterschiede, Einsatzgebiete
- [Freie Software](./FOSS.md) - Was ist Freie Software?
- [Kriterien für gute Freie Software](./Kriterien4guteFOSS.md)


## Distributionen

- [Linux-Distributionen](./Distribution.md)
- [Debian-Pakete](./deb-Pakete.md) - xyz.deb
- [Snap-, Flatpack-Pakete](./Snap,Flatpak.md)
- [Pflege der Installation](./Pflege.md)
- [Desktop-Umgebungen](./Desktop-Umgebungen.md)


## Linux

### Basics

- [der Linux-Befehl](./Befehl_allg.md)
- [Kernel](./Kernel.md)
- [Prozesse](./Prozesse.md)
- [Memory](./Memory.md)

#### Filesysteme

- [VFS](./VFS.md) - das Virtual File System
- [Everything is a file](./Everything_is_a_file.md)
- [FHS](./FHS.md) - Filesystem Hierarchy Standard
- [Filesysteme](./Filesysteme.md)
- [Filesystem-Befehle](./Filesystem-Befehle.md)
- [Rechte im Filesystem](./FS-Rechte.md)
- [Disk-Tools](./DiskTools.md) - Standard-Befehle und Tools


## Die interaktive Arbeitsumgebung

- [CLI vs GUI](./CLI_vs_GUI.md)
- [Prompt](./Prompt.md)
- [Die Shell](./shell.md)


## Bash

- [bash](./bash.md)
- [Variable](./Variable.md)
- [interaktive Bash](./interaktive_Bash.md)
- [globbing](./bash-globbing.md)
- [Reguläre Ausdrücke](./REs.md)
- [grep](./grep.md) und [sed](./sed.md) REs im Einsatz
- [for](./for.md) - exemplarisches Sprachelement
- [Shell-Scripte](./Shell-Scripte.md)


## Programme

- [Browser](./Browser.md)


---

<sub>
Autor: Helmut Hayd<br>  
Schlagwörter:  Admin  Software<br>
letzte Änderung: 2024-02-29<br>  
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
