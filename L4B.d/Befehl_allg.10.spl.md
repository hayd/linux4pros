# Wie heißen die 10 wichtigsten Linux-Befehle?

Antwort von ChatGPT am 29.10.2024:

Hier sind 10 grundlegende und häufig verwendete Linux-Befehle, die jeder kennen sollte:

1. **`ls`** – Zeigt den Inhalt eines Verzeichnisses an.

2. **`cd`** – Wechselt das Verzeichnis.

3. **`pwd`** – Zeigt den aktuellen Verzeichnispfad an.

4. **`cp`** – Kopiert Dateien oder Verzeichnisse.

5. **`mv`** – Verschiebt oder benennt Dateien oder Verzeichnisse um.

6. **`rm`** – Löscht Dateien oder Verzeichnisse (Achtung: unwiderruflich!).

7. **`mkdir`** – Erstellt ein neues Verzeichnis.

8. **`rmdir`** – Löscht ein leeres Verzeichnis.

9. **`touch`** – Erstellt eine neue leere Datei oder ändert das Änderungsdatum einer Datei.

10. **`man`** – Zeigt die Handbuchseite für einen Befehl an, um mehr über ihn zu erfahren.

Diese Befehle decken viele grundlegende Dateisystem- und Navigationsfunktionen ab, die im täglichen Umgang mit Linux nützlich sind.
