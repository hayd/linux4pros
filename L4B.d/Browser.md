# Browser

## TL;DR

- aus Sicherheitsgründen nur Browser mit aktuellem Support einsetzen
- Firefox gut konfiguriert für das Tagesgeschäft
- Chromium mit Werkseinstellung, wenn Firefox versagt (wegen diverser Blocker)


## Firefox

- [Firefox](https://www.mozilla.org/de/firefox/new/) wird von der  Non-Profit-Organisation Mozilla Foundation entwickelt
- Rendering-Engine: [Gecko](https://de.wikipedia.org/wiki/Gecko_(Software)), in C++ geschrieben
- alle 6 Wochen eine neue Version

### Erweiterungen, AddOns

- [Tree Style Tab](https://addons.mozilla.org/en-US/firefox/addon/tree-style-tab/) - stellt Tabs senkrecht, hierarchisch dar
  - [home](https://github.com/piroor/treestyletab)
  - hat zahlreiche [eigene Erweiterungen](https://github.com/piroor/treestyletab/wiki/API-for-other-addons)
- [Copy as Markdown](https://github.com/yorkxin/copy-as-markdown)
- [LanguageTool](https://languagetool.org/de) - beste Grammatik-, Stil- und Rechtschreibprüfung


### siehe auch

- [soeren-hentzschel.at](https://www.soeren-hentzschel.at/projekt-unterstuetzen/) - umfangreichste Berichterstattung über Mozilla im deutschsprachigen Raum
- [bei UU](https://wiki.ubuntuusers.de/Firefox/)


## Chromium

- [home](https://www.chromium.org/Home/), [UU](https://wiki.ubuntuusers.de/Chromium/)
- technische Basis von **Google Chrome**, Edge, Opera, Vivaldi, Brave
- FOSS
- Rendering-Engine: Blink in  C++
- saubere Implementierung der Web-Standards, z.B. WebRTC
- gegenüber Google Chrome fehlen: Digitale Rechteverwaltung (= Art von Kopierschutz), automatische Update-Funktion, einige patentierte Codecs, Senden von Fehlerberichten an Google


## Siehe auch

- [Liste](https://wiki.ubuntuusers.de/Internetanwendungen/#Webbrowser) der Webbrowser bei UU


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Empfehlungen Software<br />
letzte Änderung: 2025-02-26<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
