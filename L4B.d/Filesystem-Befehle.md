# Filesystem-Befehle

## Pfade

- absolute
  - beginnend bei /
  - `./my_script`  ist ein absoluter Pfad.
- relative
  - beginnend bei `$ echo $PWD`


## Standard-Befehle

### pwd (print working directory)

- `$ pwd` zeigt, in welchem Ordner ich stehe
- in Fenster-Oberkante anzeigen (nicht im Prompt)


### cd (change directory)

- `$ cd  /etc/apt/sources.list.d/`
- `$ cd ..`
- `TAB` ergänzt Pfad, sobald eindeutig
- Alternativen: [Broot](https://github.com/Canop/broot),  [zoxide](https://github.com/ajeetdsouza/zoxide)


### ls (list)

- listet Dateien und Ordner mit ihren Metadaten auf
- die wichtigsten Optionen: -a -l -h -S -F -1
- aliases definieren
- `$ ls -1 | wc -l`
- Alternativen: [exa](https://the.exa.website/), [lsd](https://github.com/Peltoche/lsd)


### tree

- `$ tree  -L 2`  # L = Level, wie weit im Baum


### cp (copy)

- Datenverlust möglich
- die wichtigsten Optionen: -i -r -p
- Ordner kopieren: `$ cp -r ~/.config/xfce4 /tmp`


### mv (move)

- verschieben im hierarchischen Dateibau und umbenennen
- Datenverlust möglich  
- die wichtigste Optionen: -i


### rm

- Kann der Anfang einer wunderbaren Neuinstallation werden. Getreu der Unix-Leitlinie: Der User weiß, was er tut.  
- die wichtigsten Optionen: -i -r -f


### mkdir (make dir)

- die wichtigste Option: -p


### ln (link)

- vergibt einen Zweitnamen
- die wichtigste Option: -s
- Syntax: `$ ln -s alt neu`
- Einsatz:
  - Auswahl einer Konfigurationsdatei aus n  möglichen:  
       `$ ln -s xfce4-22Mai2020 xfce4`
  - Markierung des zu bearbeitenden Textes:  
      `ln -s kapitel-8.txt hier-weitermachen`


### locate

... weiß, wo die Dateien und Ordner im Filesystem liegen. Es nutzt dafür eine Datenbank, die automatisch aktualisiert wird (1x/Tag).

- Wo liegen all meine Bilder:  
 `$ locate *.jpg | grep user`


## Aufgaben

Führen Sie folgende Befehle aus und erklären Sie die Ergebnisse:

```bash
cd /tmp
mkdir Test
cd Test
pwd
echo "abc" > a
echo "bcd" > b
cp a b
ls -al
cat a b
echo "bcd" > b
cp -i a b
rm -r /tmp/Test
```


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  <br />
letzte Änderung: 2025-02-26<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
