# Filesystem Hierarchy Standard (FHS)

- Hierarchisches Filesystem: hierarchisch = hat Unterverzeichnis, die eine baumartige Struktur bilden können
- FHS: ein Standard, an den sich die meisten Linux-Distributionen weitgehend halten. Debian-Derivate setzen ihn exakt um.


## Die wichtigsten Verzeichnisse

### /sbin = /usr/sbin/

- **s**ystem **bin**aries = ausführbare Programme für essentielle Aufgaben der Systemverwaltung
- Z.B: [mount](https://www.mankier.com/8/mount) , [mkfs](https://www.mankier.com/8/mkfs) , [fsck](https://www.mankier.com/8/fsck) , [nvme](https://www.mankier.com/1/nvme), [route](https://www.mankier.com/8/route), [grub](https://www.mankier.com/8/grub2-install) , [modprobe](https://www.mankier.com/8/modprobe) , [adduser](https://www.mankier.com/8/useradd)
- nur für root nötig, meist für device management


### /bin = /usr/bin

- **bin**aries = ausführbare Programme
- Befehle, Programme für alle (root und user)
- Sub-Dirs verboten (Wie überprüfen? `$ ls -al | egrep "^d"` )


### /etc

- alle systemweiten Konfigurations-Dateien aller Programme, entspricht registry
- aber alle Dateien menschen-lesbar
- typischerweise 18 MB groß, d.h.  ganze Systemkonfiguration einfach zu sichern


### /home

- Home-Verzeichnis der User außer root (/root)
- in der Shell abgekürzt mit `~`  
 `$ cd ~/.config && pwd`


### /tmp

- temporäre Dateien
- wird bei Booten geleert: ein Feature und eine Gefahr


### /dev

- device files != Gerätetreiber
- Treiber liegen z.B. in /lib/modules, /boot, /usr/lib/xorg/modules/drivers
- enthält alle Gerätedateien, über welche die Hardware/Pseudo-Hardware im Betrieb angesprochen wird, z.B: /dev/zero, /dev/null, /dev/random, /dev/ecryptfs


### /proc (processes)

- virtuelles Dateisystemen/Pseudodateisysteme, das Informationen über den Systemzustand und Prozesse hierarchisch strukturiert verfügbar macht
- `$ ls -l /proc/cpuinfo`    aber   `$ more  /proc/cpuinfo`
- /proc/123/: Infos zum Prozess 123
  - `$ tree /proc/1`


### /lib

- wichtige shared libraries und Kernel-Module


### /var

- variable Dateien
- Log-Dateien, zu druckendes, caches


## Jenseits von FHS üblich

- **~/.config** - dort legen viele Programme die persönlichen config file ab
- Ein **.** vor dem Namen macht den File, Ordner bei vielen Auflistungen unsichtbar (hidden files), z.B. ~/.mozilla/. Damit werden Listen kürzer und konzentrieren sich auf selbst generierte Daten.

## Aufgaben

- Welches der Verzeichnisse /usr/bin, /usr/sbin, /etc, /proc enthält die meisten Dateien?
- Tipp: `$  find /tmp -type f | wc -l`
  - `/tmp`: von /tmp aus abwärts
  - `-type f`: nur den Typ file (nicht directory) suchen
  - `| wc -l`: wc zur Zählung übergeben


## see also

- [UU](https://wiki.ubuntuusers.de/Verzeichnisstruktur/)
- [A Detailed guide to Linux Filesystem Hierarchy Standard](https://www.linuxfordevices.com/tutorials/linux/linux-filesystem-hierarchy)
- [FHS](https://de.wikipedia.org/wiki/Filesystem_Hierarchy_Standard) Hierarchy Standard – Wikipedia
- [Filesystem Hierarchy Standard](https://refspecs.linuxfoundation.org/FHS_3.0/fhs-3.0.pdf), PDF, 50 S.


---


<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Wissen<br />
letzte Änderung: 2025-02-26<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
