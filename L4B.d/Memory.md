# Memory

![alt text](../Pics.d/virtual-memory.ger.png)

Quelle: [Virtuelle Speicherverwaltung – Wikipedia](https://de.wikipedia.org/wiki/Virtuelle_Speicherverwaltung)

- Jeder Prozess bekommt einen eigenen **virtuellen Adressraum** (= virtuelle Speicher).
- Der Kernel übersetzt mit Hilfe der **Memory Management** Unit (MMU, Teil der CPU) die virtuellen Adressen in reale physische Adressen.
- Die physische Adressen können auf RAM, Festplatten, SSDs, Graphikkarten, ... zeigen.
- virtuelle Arbeitsspeicher auf Platten (Partition, Datei) =  **swap space**
  - swap space sollte 2-3x RAM groß sein
- freier Speicher ist verschwendeter Speicher => RAM wird zur **Beschleunigung** eingesetzt (buff/cache)
- **Puffer/buffer**: schnelles Zwischenlager im RAM für Daten auf dem Weg von einem Gerät zu einem anderen oder einem Programm (z.B. Bild aus dem Netz auf die Platte, Bild von Platte in Bildbetrachtungsprogramm)


## RAM

- nur RAM kann wirklich knapp werden
- zu wenig RAM bremst
- zu wenig virtueller Speicher führt zu Problemen/Abstürzen
  - Abhilfe: mehr swap space

## free

- `free` zeigt den eingebauten Speicher und die aktuelle Speicherbelegung durch Programme, Puffer und die Nutzung des swap spaces
- `-h` verwenden:  `$ free -h`


## Swap

- swap space kann man nachträglich  durch eine [Swap-Datei](https://wiki.ubuntuusers.de/Swap/#Swap-Datei-erstellen) vergrößern => klein anfangen
- swap-on-file bietet mehr Flexibilität als eine HDD-Partition und sollte auf einer SSD liegen.


## Speicher bei der Arbeit

`$ htop`

- Bedeutung des memory bars (links oben): `$ F1`
- **VIRT**: belegter virtueller Speicher, meist keine nützliche Zahl
- **RES**: aktiv genutzter RAM eines Prozesses
- **SHR**: Speicheranteil, der mit anderen Prozessen geteilt wird
- **MEM\%**: RAM-Anteil, den ein Prozess belegt

## Aufgaben

- Welche Prozesse verbrauchen am meisten RAM?

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Wissen<br />
letzte Änderung: 2024-10-28<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
