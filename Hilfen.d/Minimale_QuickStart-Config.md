# Minimal-Konfiguration für QuickStart mit Xubuntu

## Warum?

## aliases

- einzufügen in `~/.bashrc`
- essentielle aliases, die vor Datenverlust schützen
  
```bash
alias cp='cp -ip'
alias mv='mv -i'
alias rm='rm -i'
```

- oft genutzt:
  
```bash
alias ll="ls -alhF"
alias lls="ls -alSh --color=always $*"    # sort by size
alias llt="ls -alFht --color=always $*"   # sort by date
```

## Firefox

die wichtigsten [Add-Ons](https://addons.mozilla.org/en-US/firefox/search/) für Firefox:

- [Tree Style Tab](https://addons.mozilla.org/en-US/firefox/addon/tree-style-tab/)
- [TST-MiddleClick](https://addons.mozilla.org/en-US/firefox/addon/tst-middleclick/)
- [Copy as Markdown](https://addons.mozilla.org/en-US/firefox/addon/copy-as-markdown/)


## XFCE

Konfigurationen, die man nicht missen möchte:

- Workspace Switcher
  - installieren: Panel-Kontext-Menu: Add New Items , Workspace Switcher (Listeneintrag), "Add" (Knopf um)
  - 4 Zeilen, 1 Spalte einstellen, sonst nur ein kleiner Streifen
- [Whisker](https://wiki.ubuntuusers.de/Whisker_Menu/) Menü = alternatives Startmenü für Xfce  
`$ sudo apt install xfce4-whiskermenu-plugin`
- Schriften an Icons im Panel entfernen. Sie machen Panel zum Chaos:
  - Rechtsklick im Panel => Kontextmenü:  `Panel> Panel Preferences`
  - "Item" (Reiter om) anklicken
  - Double click "Windows Buttons' (Icon im großen weissen Fenster)
  - "Show button labels" (checkbox ol in neuem Fenster) Häkchen wegnehmen


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  HowTo<br />
letzte Änderung:2023-10-19, 17:15<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
