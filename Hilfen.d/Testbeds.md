# Testbed einrichten


## Allgemeines

- Richten Sie sich zunächst eine Umgebung (in /tmp) ein, in der Sie das Script, Programm testen können.
- Legen Sie eine Kopie von der Test-Umgebung an, damit Sie den Test ggf. schnell wiederholen können.


## Bilder

### Viele Bildchen, Icons

- \> 900 PNGs besorgen:  

```bash
$ sudo apt install gnome-extra-icons 
$ cp -pr /usr/share/pixmaps/other /tmp/PNGs
```

### Bilder-Pakete

Diese Sammlungen können als Debian-Paket installiert werden:

- [ubuntu-mate-wallpapers-photos](https://packages.ubuntu.com/jammy/ubuntu-mate-wallpapers-photos)  , 26 MB
- [ubuntu-mate-wallpapers-complete](https://packages.ubuntu.com/jammy/ubuntu-mate-wallpapers-complete)  , eine Meta-Paket, das 19 andere nachzieht und 128 MB Plattenplatz benötigt

- Die Bilder werden unter /usr/share/backgrounds abgelegt.


### große Fotos

#### Sammlung "Sample Videos"

- Bilder und Videos in verschiedenen, leicht ersichtlichen Größen (MB) und Formaten: [Sample Videos and Images](https://sample-videos.com/)  of different resolution and sizes to test

#### Sammlung "Free-images"

- Auflösung nicht so sofort ersichtlich, die Größe erst nach dem Download: [Free Images](https://free-images.com/)
  - Auflösung sollte min. 4000 x n sein.


## Viele .txt-Files

- \> 2400  kleine Text-Files besorgen:  

```bash
$ sudo apt install doc-rfc-informational
$ mkdir /tmp/Texte
$ cp /usr/share/doc/RFC/informational/*.txt.gz   /tmp/Texte
# ggfs. nicht alle Texte mitnehmen: 
$ cp /usr/share/doc/RFC/informational/rfc8???.txt.gz  /tmp/Texte
$ cd /tmp/Texte && gunzip *.gz
```

## Viele Text-Dokumente

- 2463 Text-Dateien  jeweils in den Formaten HTML, json, .txt
- 24 PDFs
- 17 Postscript-Files

```bash
$ sudo apt install doc-rfc-informational
$ mkdir /tmp/Texte
$ cp /usr/share/doc/RFC/informational/rfc*   /tmp/Texte
$ cd /tmp/Texte
$ gunzip  *.gz
```

- ggfs. Dubletten erzeugen
- Script testen


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Materialien Grafik<br />
letzte Änderung: 2024-01-24<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
