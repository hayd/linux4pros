# Find new Files

## Sinn und Zweck

Oftmals will man wissen, welche neuen Files oder Veränderungen an Files 

- eine neues Debian-Paket,
- eine Änderung der Konfiguration,
- der Lauf eines Programms,
- ...

mit sich bringen.


## Änderungen finden

`$ touch /tmp/timestamp`  
`$ find ~ -newer /tmp/timestamp`

- newer: letzte Änderung, nicht letztes Lesen


## Erweiterungen
### älterer Zeitstempel

- statt aktueller Zeit
- Format: CCYYMMDDhhmm  
`$ touch -t 201303051310 /tmp/timestamp`

### nur Files oder Directories

`$ find  ./SCR  -type f  -newer /tmp/timestamp`

- f: file, d: directory, l: symbolic  link, ...


### Ergebnis aufzeichnen

`$ find . -newer /tmp/timestamp > /tmp/filelist.txt`


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: HowTo<br />
letzte Änderung: 2023-10-21<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
