# Advanced Packaging Tool, APT

- = Programm-Familie (mit Zombies) für Paketmanagement
- Tool, um der [Dependency hell](https://en.wikipedia.org/wiki/Dependency_hell) zu entkommen

![Dependency hell](../Pics.d/dependency-hell.png)

- [Gentoo Linux package dependency graph](http://disfunksioneel.blogspot.com/2011/04/linux-software-dependencies.html): 14319 Pakete mit 63988 Abhängigkeiten zwischen ihnen


## apt vs apt-get

- apt ist eine eigenständige **Weiterentwicklung** von apt-get
- apt ist gegenüber apt-get stärker für das **interaktive** Arbeiten auf der Kommandozeile optimiert.
- [Vergleichstabelle](https://itsfoss.com/apt-vs-apt-get-difference/) apt commands vs. apt-get commands
- apt-get ist eigentlich überflüssig; nutzen, wenn einem z.B. Ausgabeformat von apt nicht gefällt
- **Fazit: apt nutzen**


## apt

- apt und dpkg nutzen eine Datenbank, die alle Metadaten über die Pakete und ihren Installationszustand enthält
- apt baut auf dpkg auf


## apt-cache

- [apt-cache](https://wiki.ubuntuusers.de/apt/apt-cache/#apt-cache-showpkg) tut nichts, es dient nur der Verwaltung der APT-DB
- Anzeige der Metadaten eines Paketes:  
  `$ apt-cache showpkg agrep`  
  `$ apt-cache showpkg gcc`    # 427 Zeilen  


## Suche nach Paketen

- [Ubuntu-Paketsuche](https://packages.ubuntu.com/de/) via Browser
- Suche nach dem Paket, das ein bestimmtes Programm/Befehl enthält am Beispiel von "free":
  - [Ubuntu-Paketsuche](https://packages.ubuntu.com/de/) > "Durchsuchen des Inhalts von Paketen" >  "Pakete, die Dateien dieses Namens enthalten"
    - free wird als /usr/bin/free im Paket "procps" gefunden.
- Stöbern in der letzten LTS-Version an Hand von [Kategorien](https://packages.ubuntu.com/noble/) im Browser
- [How to Search for Ubuntu Packages](https://linuxhint.com/search-ubuntu-packages/)
- apt search sucht in Namen und Beschreibung:  
`$ apt search agrep`
- nur nach Namen (-n) suchen:  
`$ apt search -n agrep`

  
## Paket (de-) installieren

- Paket installieren mit Auflösung von Abhängigkeiten:  
  `$ sudo apt install agrep`
- bereits heruntergeladenes Paket installieren mit Auflösung der Abhängigkeiten, Pfadangabe zwingend erforderlich:  
  `$ sudo apt install ./zoom_amd64.deb`
- Paket entfernen:  
  `$ sudo apt remove agrep`
- Paket + Konfigurationsdateien entfernen:  
  `$ sudo apt purge agrep`
- Reste eines missglückten Installationsversuchs abräumen:  
  `$ sudo apt --fix-broken install`


## Abhängigkeiten auflisten

- Wovon hängt [Evince](https://wiki.ubuntuusers.de/Evince/) ab?  
`$ apt depends evince`
- Was hängt von [libc6](https://de.wikipedia.org/wiki/C-Standard-Bibliothek) ab?  
`$ apt rdepends libc6`

## Graphische Tools

### Synaptic

- Synaptic: ein GUI für apt
- Problem: "Last change: 07. Januar 2009"
- mächtiges Werkzeug mit vielen Sonderfunktionen; Filterfunktionen, die seinen Nachfolgern fehlen
- Man sollte es nur für Anzeigen verwenden.
- [Home](https://www.nongnu.org/synaptic/), [bei UU](https://wiki.ubuntuusers.de/Synaptic/)


## Sinnlose Tools

- [Ubuntu Software](https://wiki.ubuntuusers.de/Ubuntu_Software/)
  - Es wird nur ein Bruchteil der ca. 40.000 Pakete  angezeigt.
- [Gnome-Software](https://wiki.ubuntuusers.de/Gnome-Software/)
  - ignoriert Programme ohne GUI, z.B. Programm, Commandline-Tools


## siehe auch

- [APT Übersichtsseite](https://wiki.ubuntuusers.de/APT/)
- [apt bei UU](https://wiki.ubuntuusers.de/apt/apt/)
- [Nala: APT on Steroids](https://linuxnews.de/2022/02/24/nala-apt-on-steroids/) - ein Front-End für libapt-pkg in Python: schneller, informativer, hübscher - meint zumindest der Autor



## Aufgaben

Die Aufgabe soll einen ersten Eindruck vermitteln, von der Vielfalt und Anzahl der Pakete.

Wie viele

1. Text-Editoren,
1. Audio-Editoren,
1. Video-Editoren,
1. Libraries,
1. Python3-Pakete,
1. Rust-Pakete,
1. Sprachpakete für aspell, ispell, hunspell,
1. Pakete für Docker,
1. Pakete für Astronomie,
1. Pakete mathematischen Inhalts,
1. Pakete für LibreOffice vs. OpenOffice,

gibt es im Repository? Suchen Sie nur nach Namen.

- Zählen Sie die Ergebnisse mit `$ cmd1 | wc -l`
- Verfeinern Sie die Ergebnisse mit  
   `$ cmd1 | egrep audio | egrep editor`    bzw.  
   `$ cmd1 | egrep -v lib`

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Admin Ubuntu<br />
letzte Änderung: 2025-01-07<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
