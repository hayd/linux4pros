# Debian-Pakete

## Warum Pakete ?

- **einfache Installation**, rückstandsfreie De-Installation
- **Abhängigkeiten** werden aufgelöst, z.B. fehlende Libraries
- formale Vorgaben, so dass Tools wie **dpkg**, **apt** damit umgehen können.
- Der Paketbau beinhaltet ein **Qualitätsmanagement**, der [Maintainer](https://wiki.debian.org/DebianMaintainer) muss sich  qualifizieren und Vertrauen erarbeiten.
- Alle Pakete einer Version einer Distri kommen sich normalerweise nicht in die Quere und beruhen auf einer gemeinsamen Basis (Satz von Libraries), die **ausführlich getestet** wird (im [Feature Freeze](https://linuxnews.de/2022/10/17/debian-freeze-fuer-debian-12-bookworm-rueckt-naeher/)).


## Pakete ala Debian

- Es gibt 2 Arten von Debian-Paketen: [Binärpakete, Quellpakete](https://de.wikipedia.org/wiki/Debian-Paket)
- Ein Binärpaket muss min. bestehen aus:
  - ausführbaren Dateien,
  - Konfigurationsdateien,
  - man/info-Seiten,
  - MD5-Prüfsummen,
  - Copyright/left-Informationen,
  - Maintainer-Skripten: [preinst, postinst; prerm, postrm](https://debiananwenderhandbuch.de/debianpaketeerstellen.html#debianpaketeerstellenpostinst)
- Pakete kann man mit [lintian](https://book.dpmb.org/debian-paketmanagement.chunked/ch37s01.html) prüfen.
- Datenbank: Flat file database in /var/lib/dpkg:  
  `$ find /var/lib/dpkg  -type f  | wc -l`


## dpkg

- [home](https://github.com/guillemj/dpkg)
- begonnen 1994
- dient zum Installieren einzelner .deb-Pakete
- Basis der höheren Installationstools apt*
- managt Flat-File-DB /var/lib/dpkg
- löst keine Abhängigkeiten auf
- .deb installieren, z.B. [Zoom](https://zoom.us/download?os=linux):  
  `$ sudo dpkg -i ./zoom_amd64.deb`
- .deb de-installieren:  
  `$ sudo dpkg -r  agrep`
- Konfigurationsdateien auch noch entfernen:  
  `$  sudo dpkg -r --purge agrep`

dpgk kann man auch für **Inventuren** nutzen:
  
- Anzeige aller installierten Pakete:  
  `$ dpkg -l`

- vereinfachte Anzeige aller installierten Pakete:  
  `$ dpkg --get-selections > /tmp/packages_list.txt`

- Die angezeigte Liste kann man verwenden, um eine [identische Installation](https://linuxprograms.wordpress.com/2010/05/13/dpkg-set-selections-clone/), soweit es die installierten Pakete betrifft, zu erstellen:  
  `$ sudo dpkg --set-selections <  /tmp/packages_list.txt`

- Anzeige aller Files in einem Paket, um z.B. den Namen des executables zu finden:  
  `$ dpkg -L apt` #   372 Files
  - zoom_amd64.deb besteht aus 1380 Files
- siehe auch: [dpkg bei UU](https://wiki.ubuntuusers.de/dpkg/)


## Repositories

### Allgemeines

- Repository = Quelle für Paket(e)
- die offiziellen Repositories von Ubuntu sind aufgelistet in: `/etc/apt/sources.list`  
- [Paketquellen](https://wiki.ubuntuusers.de/Paketquellen/) - deren Kategorisierung und Beurteilung
- [sources.list bei UU](https://wiki.ubuntuusers.de/sources.list/)


### Inoffizielle, private Repositories

- Entwickler (Privatpersonen, Firmen) können auch Software über eigene  Repositories anbieten.
- **Vorteile**: kein Maintainer nötig, Aktualisierungen unabhängig vom offiziellen Release-Zyklus der Distribution
- **Nachteile**: ungeprüfte Vertrauenswürdigkeit, nicht die übliche Qualitätskontrolle
- Ubuntu bietet PPAs (Personal Package Archive) um private Repos aufzubauen.
- privates Repository hinzufügen und nutzen am Beispiel [Zim](https://zim-wiki.org/index.html):

```shell
$ sudo add-apt-repository ppa:jaap.karssenberg/zim
$ sudo apt update
$ sudo apt install zim
```

- In /etc/apt/sources.list.d/ werden damit Files abgelegt, die solche Zeilen enthalten (für [Zim](https://zim-wiki.org/index.html
), VS Code):

PPA: `deb http://ppa.launchpad.net/jaap.karssenberg/zim/ubuntu focal main
`

firmeneigenes Repo:  `deb [arch=amd64,arm64,armhf] https://packages.microsoft.com/repos/code stable main`


- siehe auch: [PPA bei UU](https://wiki.ubuntuusers.de/Paketquellen_freischalten/PPA/#PPA-hinzufuegen), [Paketquellen freischalten](https://wiki.ubuntuusers.de/Paketquellen_freischalten/PPA/)



## Files

- In /var/cache/apt/archives werden .deb beim Download abgelegt.


## Open Build Service

- .deb, .rpm Pakete haben Vorteile
- Ihr Erstellung ist mühsam  und verlangt eine besonderen Status des Paketbauers oder ein PPA
- Der [Open Build Service](https://openbuildservice.org/) kann in einem Zug Pakete für alle wichtigen Distris bauen - zur privaten Verwendung, ...



## Wie kommt ein File auf meine Installation?

Jedes "Betriebssystem-Datei",d die nicht über ein Paket installiert wurde ist suspekt. Daher sollte man herausfinden können aus welchem Paket eine Datei stammt. Dies soll am Beispiel "qrcode" exemplarisch gezeigt werden:

- Wie heisst diese Datei genau?  
`$ locate qrcode`  
Ergebnis: qrcode.sty
- In welchem Paket steckt diese Datei?  
`$ dpkg -S qrcode.sty`  
 `-S` <pattern>...         Find package(s) owning file(s).  
Ergebnis: texlive-pictures



## siehe auch

- [Grundlagen des Debian-Paketverwaltungssystems](https://www.debian.org/doc/manuals/debian-faq/pkg-basics.de.html)
- [Linux Packages Under The Hood](https://juliensobczak.com/inspect/2021/05/15/linux-packages-under-the-hood/)
- [Paketquellen im Format deb822](https://wiki.ubuntuusers.de/Paketquellen_im_Format_deb822/)

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Admin Ubuntu<br />
letzte Änderung: 2025-02-12<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
