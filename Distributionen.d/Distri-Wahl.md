# Auswahl einer Distribution

## TL;DR

Im Zweifelsfall das letzte [Ubuntu LTS](https://de.wikipedia.org/wiki/Ubuntu#Versionstabelle)

## graphisch

- nicht nur humorvoll  
 ![nicht nur humorvoll](../Pics.d/rich_daddy.png)

---

- ernsthafter  
![ernsthafter](../Pics.d/linux_auswahl.png)

- [c't–Linux-Netzplan](https://www.heise.de/ratgeber/Der-c-t-Linux-Netzplan-So-finden-Sie-die-passende-Distribution-6330180.html)


## Kriterien

- Wie schnell werden Security-Patches geliefert?
- Aktualität
- Pflegeaufwand
- kommerzieller Support verfügbar
- Zahl der Maintainer
- Zahl der Pakete im offiziellen Repository
- Für den speziellen Zweck (z.B. alte Hardware, Tonstudio) geeignet?


## Distrochooser

- https://distrochooser.de/
- Fragen zu diversen Themen => Vorschlag für eine Distribution



## Aufgaben

- [Auswahl einer Distribution](./Distri-Wahl.ex.md)

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Admin Empfehlungen<br />
letzte Änderung: 2023-10-23, 16:47<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>

