# Software-Pflege

## TL;DR

`$ sudo apt update && sudo apt upgrade`

Alle 2 Jahre auf die nächste LTS-Version von Ubuntu wechseln.

## Notwendigkeit

- In der IT-Administration gibt es keine Projekte, nur **Pflegefälle**.
- Es gibt eine [Softwareverrottung](https://www.golem.de/news/it-in-unternehmen-wie-sich-software-rot-verhindern-laesst-2103-154383.html) - Akkumulation von Programmfehlern, Inkompatibilitäten mit neuen Softwareumgebungen, ...
- Ohne permanente Pflege sind die Daten des Rechners irgendwann verschlüsselt, ausgeleitet, Munition für Erpressung, ... Beispiel [Bitterfeld](https://www.spiegel.de/netzwelt/ransomware-cyberangriff-auf-anhalt-bitterfeld-gravierender-als-bislang-bekannt-a-d538f846-1926-484d-b8d6-19554ad8c4c5)
- Eine Software ohne zeitnahe Security-Patches muss entfernt (oder zumindest gekapselt) werden.


## Konkretes

Pflege: erst werden  die neuesten Repository-Kataloge gezogen, dann die Differenzen zu den vorherigen Katalogen bestimmt und schließlich entsprechend upgegradet:

`$ sudo apt update &&  sudo apt upgrade`  

- Update, Upgrade: keine neuen Features, sondern Sicherheitspatches, weniger/andere Fehler

Snaps und Flatpacks auch noch aktualisieren:  
`$ sudo snap refresh`  
`$ sudo flatpak update`


## Komponenten

Die Pakete entstammen sind in [4 Komponenten](https://en.wikipedia.org/wiki/Ubuntu#Package_classification_and_support) (Kategorien), was 4 verschiedene Repositories entspricht, eingeteilt:

1. **main**: frei, von Ubuntu gepflegt
2. **restricted**: unfrei, von Ubuntu gepflegt, aber wichtig , z.B. bcmwl-kernel-source = Broadcom propietärer wireless driver
3. **universe**: frei,  von Ubuntu  nicht gepflegt, z.B. [JOE](https://joe-editor.sourceforge.io/)
4. **multiverse**: unfrei,  von Ubuntu  nicht gepflegt, z.B. Multimedia-Codecs


|                                   | Free software | Non-free software |
|-----------------------------------|---------------|-------------------|
| Officially supported by Canonical | Main          | Restricted        |
| Community supported/Third party   | Universe      | Multiverse        |



Welche Komponenten man nutzt steht in: /etc/apt/sources.list

- alle 6 Monate (.04, .10) erscheint eine Ubuntu-Version mit Security-Patches für 9 Monate
- LTS = Long Term Support, long = 5a
- alle 2 Jahre erscheint eine LTS-Version
  - nur bug fixes, security patches, keine neuen Features
  - Support für Ubuntu Derivate K|L|Xubuntu, Ubuntu Budgie, Mate nur 3 Jahre.
  - nur für Pakete aus den Rubriken main und restricted, **nicht** universe und multiverse


## noch gepflegt?

- Auflistung, welche Pakete wie lange noch gepflegt werden:  
`$ ubuntu-security-status`


## HWE-Stack, LTS Enablement Stacks

- [HWE](https://wiki.ubuntuusers.de/LTS_Enablement_Stacks/) = Hardware Enablement
- neue Kernel und Graphik-Treiber für Distribution mit feature freeze, damit **neue Hardware** unterstützt, Performance verbessert  wird
- HWE-Stack enthält nicht alle Module
- [Details](https://www.thomas-krenn.com/de/wiki/Ubuntu_LTS_Hardware_Enablement_Stack) - Installation, Liste der verfügbaren Kernel


## Upgrade

= Wechsel zu einer höheren, neueren Ubuntu-Version

- Upgrade oder Neuinstallation?
  - **Neuinstallation**: Malware weg; saubere Konfiguration ohne Altlasten; neue, bessere (?) Defaults; mehr Arbeit
- **Trennung** von Software und Daten ($HOME) erleichtert Neuinstallation. Problem: ~/.config u.ä.
- [Details und Anleitung](https://wiki.ubuntuusers.de/Upgrade/)


## ergänzendes Material

- [Ubuntu Pro](./UbuntuPro.spl.md) - 10 Jahre Patches



## siehe auch

- [Long Term Support, UU](https://wiki.ubuntuusers.de/Long_Term_Support/)
- [Unterschiede LTS und normale Version, UU](https://wiki.ubuntuusers.de/Unterschiede_LTS_und_normale_Version/)
- [Paketquellen, UU](https://wiki.ubuntuusers.de/Paketquellen/#main)
- [Repositories - Ubuntu Wiki](https://help.ubuntu.com/community/Repositories)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Admin Security Ubuntu<br />
letzte Änderung: 2025-01-12<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>


<!--- 

Ermittlung der upzugradeden Pakete erfolgt bei update, nicht bei upgrade

 Neueinlesen der Paketlisten 
 
Fetched 14,3 MB in 4s (3.475 kB/s)                                 
Reading package lists... Done
Building dependency tree       
Reading state information... Done
13 packages can be upgraded. Run 'apt list --upgradable' to see them.
[1]+  Done                    source ~/.bashrc.d/xmodmap.bash

helmut@tux2(2)->sudo apt upgrade
Reading package lists... Done
Building dependency tree       
Reading state information... Done
Calculating upgrade... Done
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.


mit neuen Paketen ----------


-> sudo apt update

Reading package lists... Done
Building dependency tree       
Reading state information... Done
7 packages can be upgraded. 

-> sudo apt upgrade

Reading package lists... Done
Building dependency tree       
Reading state information... Done
Calculating upgrade... Done
The following NEW packages will be installed:
  linux-headers-5.4.0-208 ...
  
  The following packages will be upgraded:
  libcap2...

  
-->




















