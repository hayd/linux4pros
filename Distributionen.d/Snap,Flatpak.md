# Snap, Flatpak

## TL;DR

Welches Paket soll ich nehmen?

1. Wahl: .deb
2. Wahl: snap  (auf Ubuntu)
3. Wahl: Flatpak


## Zielstellung

- ein Paket für alle Distributionen
- ein stets aktuelles Paket unabhängig von allen anderen Paketen


## Realisierung

- **alle benötigten** Binaries, Bibliotheken, Konfigurationsdateien und sonstige Dateien in einem eigenen Verzeichnis, das dann paketiert wird
  - keine Rücksicht auf Umgebung, z.B. Library-Versionen, [FHS](../Filesystem.d/FHS.md)
  - Installation nicht mit den bewährten Mechanismen von **dpkg** und **apt**



## Pro

- läuft **überall**, d.h. in jeder Distribution in jeder Version
- Software-Entwickler müssen **weniger Zeit** für Wartung/Paketierung investieren, es bleibt mehr Zeit für neue Funktionen.
- **Authentizität**: Software kommt (vom Maintainer) unverfälscht direkt von der Quelle
- **Aktualisierung** eines Programms kann unabhängig von der Betriebssystem-Basis und den anderen .debs erfolgen.
- einfacheres **Testen** einer Software, da keine Nebenwirkungen, Compilierung, ...
- Parallele Nutzung **unterschiedlicher Versionen** von Programmen.
- Snaps, Flatpacks laufen in einer **Sandbox** mit geregeltem/reduziertem Zugriff auf das Hostsystem. Deren Rechte muss man aber definieren (z.B. Zugriff auf Downloads).


## Contra

- keine **Qualitätssicherung** durch die vielen Maintainer einer Distri
- Paketquelle von möglicherweise unbekannter **Vertrauenswürdigkeit**
- Bei **Sicherheitsupdates** für einzelne Bibliotheken müssen alle Snaps, Flatpaks aktualisiert werden, die diese Bibliotheken enthalten. Es besteht ein Risiko, dass dies vereinzelt nicht konsequent umgesetzt wird.
- längere Startzeit, größeres Paket als .deb
- Ein höherer Speicherplatzverbrauch, da letztlich dieselbe Bibliothek (ggf. in unterschiedlichen Versionen) mehrfach installiert wird.
- Canonical ist der **Infrastruktur-Monopolist** von Snap


## Snap

- maßgeblich von Canonical entwickelt
- Canonical ist der Infrastruktur-Monopolist von Snap
- Snaps versammelt im [Snap Store](https://snapcraft.io/store) mit "thousands of snaps"
- snap installieren:   `$ sudo snap install SNAPNAME`
  - [ausführliche Anleitung](https://wiki.ubuntuusers.de/snap/) bei UU


## Flatpack

- [Flatpack](https://flatpak.org/) bildet eine **Alternative** zu Snap.
- ein **Community-Projekt**, per Design komplett dezentral. Jeder kann einen eigenen Flatpak-Server eröffnen, aber [Flathub](https://flathub.org/apps) bildet eine zentrale Anlaufstelle
- 800.000 aktive Nutzer, 1200 Programmierer zur Pflege der Flatpaks, 1600 Flatpaks (ca. 4500 Snaps)


## siehe auch

- [Übungen zu Paketen](Snap,Flatpak.spl.md)
- [Debian-Paket, Snap, Flatpak - Supplement](Snap,Flatpak.ex.md)

### Vergleiche

- [Linux erfindet sich neu](https://www.heise.de/select/ct/2018/13/1529631413897687) - App-Formate sollen Softwareinstallation bei Linux revolutionieren, c't 13/2018
- [Flatpak / Snap vs. Paketverwaltung – Alles was dazu gesagt werden muss](https://curius.de/2021/09/flatpak-snap-vs-paketverwaltung-alles-was-dazu-gesagt-werden-muss/)
- [Flatpak, Snap und AppImage im Vergleich](https://www.linux-community.de/ausgaben/linuxuser/2018/02/dreikampf/) - aus LinuxUser 02/2018


### Flatpak

- [bei UU](https://wiki.ubuntuusers.de/Flatpak/#ber-Flatpak-installierte-Anwendungen-koennen-nur-auf-bestimmte-Ordner-zugreifen)
- [Flatpak Is Not the Future](https://ludocode.com/blog/flatpak-is-not-the-future) - detailierte Diskussion der Probleme
- [The issue with flatpak's permissions model](https://whynothugo.nl/journal/2021/11/26/the-issue-with-flatpaks-permissions-model/)
- [Die Rechte von Flatpaks mit Flatseal unter Linux regeln](https://www.linux-bibel-oesterreich.at/viewtopic.php?f=10&t=535&p=1599#p1599)
- [Using Flatpak on Ubuntu and Other Linux Distributions - Complete Guide](https://itsfoss.com/flatpak-guide/)
- [Linux APP Summit 2022: Flathub wird zum App-Store](https://www.heise.de/news/Linux-APP-Summit-2022-Flathub-wird-zum-App-Store-7070718.html)
- [Flatpak im Rückwärtsgang](https://gnulinux.ch/flatpak-im-rueckwaertsgang)


### Snap

- [Debugging](https://wiki.ubuntuusers.de/snap/Debugging/) - snap bietet einige Tools für das Debugging von snaps.
- [Snap documentation](https://snapcraft.io/docs)
- [Paketmanagement mit Snap auf Linux-Systemen](https://www.admin-magazin.de/Das-Heft/2018/01/Paketmanagement-mit-Snap-auf-Linux-Systemen), IT-Administrator 01/2018
- [Hey snap, where’s my data?](https://ubuntu.com//blog/hey-snap-wheres-my-data)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Admin Security Ubuntu<br />
letzte Änderung: 2025-01-06<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
