# Linux-Distributionen


## GNU

![GNU / Linux](../Pics.d/gnu-linux.700.png)

- [GNU](https://www.gnu.org/) = GNU ist Nicht Unix
- UNIX-Nachbau mit freier Software
- Software in Form von min. 386 Paketen [GNU packages](https://www.gnu.org/manual/blurbs.html) verfügbar


## Was eine Linux-Distribution?

![distribution](../Pics.d/distribution.png)

Linux-Distribution = Kernel + Auswahl aufeinander abgestimmter Software in Form von Paketen aus einem offiziellen Repository


## Die Listen

- [DistroWatch](https://distrowatch.com/dwres.php?resource=popularity)
  - Zugriffe auf die Beschreibungsseiten => Ranking
  - Ranking korreliert nicht mit realer Nutzung oder Qualität, sondern nur mit Aufmerksamkeit
  - anfällig für Manipulation
  - gute Quelle für:
    - URLs, Daten der Releases
    - Versionsunterschiede einzelner Distributionen
    - Liste der Pakete
- [Liste von Linux-Distributionen](https://de.wikipedia.org/wiki/Liste_von_Linux-Distributionen)
  - Stammbäume
  - Alleinstellungsmerkmale


## Die wichtigsten Distributionen

### Debian

- Name:  **Ian** Murdock und seine Ehefrau **Deb**ra Lynn
- Erstveröffentlichung: 1993
- eine der ersten umfassenden Linux-Distributionen
- [Mutter vieler abgeleiteter Distris](https://de.wikipedia.org/wiki/Linux-Distribution#/media/Datei:Linux_Distribution_Timeline.svg)
- \> 60.000 Programmpakete
- \> 1.000 offizielle Entwickler
- Erfinder eines leistungsfähigen Paketformates: .deb
- eher dogmatisch, weniger pragmatisch (systemd, unfreie Teile, ZFS, ...)
- 4 Zweige des Lebenszyklus eines Paketes:
  1. **Experimental**: neue Pakete, für produktiven Einsatz nicht empfohlen
  2. **Unstable**: vollständiges Paket-Repository mit aktuellsten stabilen Paketen
  3. **Testing**: Vorabversion des nächsten Debian Stable
  4. **Stable**: nur bug fixes, keine neuen Features - außer Browser, ca. alle [2 Jahre](https://de.wikipedia.org/wiki/Debian#Versionsgeschichte)  eine neue Version


### Ubuntu

- user-freundlicheres Debian-Derivat
- initiiert durch den Multimillionär (1999 hatte er 500 Millionen Dollar mit Software verdient.) Mark Shuttleworth
- Alleingänge, die irgendwann eingestampft werden: Display-Server Mir (statt Wayland), Unity (Desktop), Smartphone OS
- 2 Versionen/a im April und Okt.: .4, .10
- alle 2a eine LTS-Version, L = 5a
- kommerzieller Support


#### Ubuntu Pro

- [Ubuntu Pro](./UbuntuPro.spl.md) ist eine  LTS-Version mit bis zu 10 Jahre Sicherheitsaktualisierungen für bestimmte Pakete, z.Z.  23.000 Pakete
- Ab Oktober 2022 ist Ubuntu Pro für bis zu fünf Rechnern mit Desktop Installationen kostenfrei nutzbar.



### Red Hat

- 1993 gegründet
- 1995 Paketformat .rpm eingeführt
- 2018 von IBM übernommen
- trägt viel zum Kernel bei
- Fedora: freie Version von Red Hat
  - Lebenszyklus auf 13 Monate angelegt
  - [upstream](https://gnulinux.ch/upstream-downstream) (= Quelle für Distribution) source for Red Hat
- Red Hat Enterprise Linux (RHEL): Enterprise-Betriebssystem, Marktführer bei kommerziellen Unternehmen, [teuer](https://www.redhat.com/en/store/linux-platforms?intcmp=701f20000012m33AAA)
- CentOS: frei, zu RHEL binärkompatibel, ausgelaufen. Ersatz: [AlmaLinux](https://de.wikipedia.org/wiki/AlmaLinux), [Rocky Linux](https://de.wikipedia.org/wiki/Rocky_Linux)


### Arch Linux

- Rolling Releases
- Basis-Betriebssystem für fortgeschrittene Anwender: Zugunsten der Einfachheit wird auf grafische Installations- und Konfigurationshilfen verzichtet.
- gute Doku: https://wiki.archlinux.org/


## Spezielle Distributionstypen

### Rolling Releases

- Keine statische OS-Version, sondern die Software-Pakete **kontinuierlich** und unabhängig voneinander aktualisiert.
- **Vorteil**: Man bekommt immer die **neueste Version** eines Programms.
- **Nachteile**:
  - Man ist bei allen Kinderkrankheiten live dabei.
  - Man weiss nicht, womit man morgen arbeitet.
- **Ausweg**: autonome Software-Pakete/Universalpakete (Flatpak, **Snap**, AppImage) - neueste Version eines Programms ohne den Rest zu stören
- verbreitete Linux-Distris: [Arch](https://de.wikipedia.org/wiki/Arch_Linux), [Manjaro](https://de.wikipedia.org/wiki/Manjaro_Linux) (basiert auf Arch), [Gentoo](https://de.wikipedia.org/wiki/Gentoo_Linux)
- **Empfehlung**: Debian Unstable ('Sid')
  - In Debian Experimental bereits getestet.
  - ein vollständiges Paket-Repository
  - Eine bekannte Distri, bei der man ggf. in den Stable-Zweig wechseln kann.
- [Keep Rolling! Ein subjektiver Vergleich verschiedener rollender Distros](https://gnulinux.ch/keep-rolling-ein-subjektiver-vergleich-verschiedener-rollender-distros)


### Aufgabenspezifische Distributionen

- Beispiel: AV Linux
  - [home](https://www.bandshed.net/avlinux/)
  - Debian basiert, vorkonfiguriert als eine Workstation für Audio- und Video-Produktion
  - Integration unfreier Codecs
- [Linux-Distributionen für Gamer](https://www.heise.de/ratgeber/Linux-Distributionen-fuer-Gamer-vorgestellt-9342336.html)
  - [Nobara](https://nobaraproject.org/) und [Pop!_OS](https://pop.system76.com/) wollen  einfach bedienbare Distributionen sein, die auch Gaming-tauglich sind. Das erleichtert manchen den Umstieg von Windows auf Linux.


### Immutable Distributions

Haben ein  read-only core system, Änderungen werden in separaten Partitionen oder über Overlay-Dateisysteme gespeichert.

- z.B. Android, [Fedora Silverblue](https://silverblue.fedoraproject.org/), [openSUSE MicroOS](https://microos.opensuse.org/)
- OS soll **stabiler** werden.
- **Aktualisierungen**: Im Hintergrund wird ein neuer Snapshot des Root-Systems angelegt und beim Reboot dieses System gestartet.
- Ein roll back ist möglich.
- OS als Plattform für **containerisierte Anwendungen** (Flatpaks). Applikationen werden vom Betriebssystem getrennt und unterschiedlich installiert, upgedatet.
- _The long-term goal for this effort is to transform Fedora Workstation into an image-based system where applications are separate from the OS, and updates are atomic_. [phoronix](https://www.phoronix.com/news/Atomic-Team-Silverblue)
- **Nachteile**: eingeschränkte Flexibilität, keine Änderungen am core möglich, Komplexität in der Handhabung wegen separaten Partitionen, Overlay-Dateisysteme, Container für jede App
- **Einsatz** dort wo Stabilität wichtiger ist als Flexibilität: Server, IoT-Geräte
- mehr: [Überblick: Immutable-Linux Distributionen](https://www.it-schulungen.com/wir-ueber-uns/wissensblog/ueberblick-immutable-linux-distributionen.html)


### GoboLinux

- [home](https://gobolinux.org/)
- nicht [FHS](../Filesystem.d/FHS.md), sondern jedes Programm in einem eigenen Subtree


## Siehe auch

- [Die Forschungsinstitute Cern und Fermilab setzen auf CentOS-Alternative Alma Linux](https://www.golem.de/news/rhel-klon-cern-und-fermilab-setzen-auf-centos-alternative-alma-linux-2212-170352.html)
- [Linux-Gaming: Die richtigen Tools im Überblick](https://www.heise.de/ratgeber/Linux-Gaming-Die-richtigen-Tools-im-Ueberblick-9339711.html?seite=all)
- [Holarse - die Seite für Linuxspiele](https://holarse.de/) - eine große Sammlung an Informationen über Linux-Tools, -Spiele und Troubleshooting-Tipps
- [Linux-Distributionen - eine Übersicht](https://tube.tchncs.de/w/vUrYDBgk7BWr2V766JHhVs) - Vortrag auf den Kieler Open Source und Linux Tagen 2022, Länge: 44:30 min


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Empfehlungen Software<br />
letzte Änderung: 2025-01-06<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>

