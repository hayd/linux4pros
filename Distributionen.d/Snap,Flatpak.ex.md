# Übungen zu Paketen

am Beispiel Firefox

## Größe des Paketes

- Vergleichen Sie die Paketgrößen der Varianten .deb und snap.
- Hilfen:
  - .deb-Pakete werden in /var/cache/apt/archives zwischengelagert.
  - [How to install Firefox as a traditional deb package (without snap)](https://askubuntu.com/questions/1399383/how-to-install-firefox-as-a-traditional-deb-package-without-snap-in-ubuntu-22)
  
## Abhängigkeiten

Von welchen anderen Debian-Paketen hängt firefox_*_amd64.deb ab?


## Größe des Fußabdrucks im Speicher

- `$ htop`

## Startzeit

- Stoppuhr


## Download

- Laden Sie ein Bild herunter und speichern Sie es unter /tmp.