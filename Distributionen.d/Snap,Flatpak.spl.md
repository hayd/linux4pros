# Debian-Paket, Snap, Flatpak - Supplement


## bei Admins beliebt

Admins ziehen .debs den Snap- und Flatpak-Paketen vor:

- Mit wenigen Befehlen (dpkg, apt, ...) kann man zehntausende von Paketen pflegen, updaten, ...
- Fehler in Libraries, die von vielen Paketen genutzt werden werden schneller gefunden.
- Der [Freeze](https://release.debian.org/bookworm/freeze_policy.html) vor dem Release einer neuen Linux-Version, bzw. der Weg dorthin sind ein gewaltiges Qualitätssicherungsprogramm, an dem bei Debian und seinen Derivate Hunderte von Maintainern beteiligt sind. Snap,Flatpak werden nur von einem bis wenigen Entwicklern gepflegt.


## Firefox
## nur noch als Snap

Seit Ubuntu 22.04 LTS (Jammy) gibt es Firefox nur noch als Snap-Paket. Canonical hat gute Gründe dafür:

- [Die](https://www.heise.de/select/ct/2022/12/2212209021533834467) Pflege und Aktualisierung im Snap-Store übernimmt Mozilla selbst, womit die aktuelle Firefox-Version deutlich schneller auf den Rechner gelangt.
- Zudem laufen Snap-Programme abgeschottet in einer Sandbox. Das soll verhindern, dass bei einer Sicherheitslücke Tür und Tor ins System offenstehen.
- Viele der obigen Vorteile gehen verloren, da es bei Browsern keinen Feature-Freeze mehr gibt. 
- Ggfs. sind Libraries in mehreren Versionen zu pflegen: für .deb und snap.
- In Zeiten schneller SSDs spielen längere Startzeiten spielen kaum noch eine Rolle.


### Firefox als .deb

- [Der](https://www.golem.de/news/statt-snap-mozilla-pflegt-erstmals-eigene-pakete-fuer-debian-und-ubuntu-2311-179006.html) Browserhersteller Mozilla hat im Nov. 2023 angekündigt, Debian-Pakete des Firefox-Browsers für Debian-basierte Linux-Distributionen bereitzustellen.
- [Anleitung](https://www.heise.de/select/ct/2022/12/2212209021533834467) Firefox über Apt installieren
  - Es wird empfohlen Firefox ESR (Extended Support Release) zu installieren. Zwar hinkt der der normalen Firefox-Version hinterher. Allerdings nutzt er andere Dateinamen und Pfade und beugt so einem Kuddelmuddel der Firefox-Einstellungen mit den offiziellen Paketen vor.

Hier 2 weitere Anleitungen:

- [Ubuntu 22.04 LTS: Firefox über Apt installieren | heise online](https://www.heise.de/ratgeber/Wie-Sie-den-Mozilla-Browser-in-Ubuntu-22-04-LTS-ueber-Apt-installieren-7101752.html?seite=all)
- [How to install Firefox as a traditional deb package (without snap) in Ubuntu 22.04 or later versions?](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Native_messaging)



### Firefox als Snap mit Kontakt nach draußen

Da der Browser bei der Installation als Snap-Paket abgeschirmt in einer Sandbox läuft, scheitert die Kommunikation von Erweiterungen mit anderen auf dem System installierten Programmen, beispielsweise dem Passwortmanager KeepassXC.  Daher haben Ubuntu-Entwickler eine neue Schnittelle entwickelt, die einen Austausch gezielt erlaubt, die  [Native Messaging API](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Native_messaging).


## siehe auch
- [Linux-Distribution Ubuntu 21.10 mit Gnome 40 und Firefox als Snap-Paket | heise online](https://www.heise.de/news/Linux-Distribution-Ubuntu-21-10-mit-Gnome-40-und-Firefox-als-Snap-Paket-6218231.html)



<sub>
Autor: Helmut Hayd<br />
Schlagwörter: Admin <br />
letzte Änderung: 2024-01-01<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>