
# Auswahl einer geeigneten Linux-Distribution

Training der Entscheidungskompetenz

## Spezial-Distribution

- Suchen Sie eine Distribution für:
  1. Anfänger, Umsteiger,
  1. Videobearbeitung,
  1. FLOSS-Purist,
  1. extrem Lernwilligen bezüglich Linux,
  1. Gaming,
  1. uralte Hardware,
  1. MacBooks,  
  1. Wissenschaft,
  1. AI,
  1. Steuerfahnder


## exotische Distribution

- Suchen Sie sich bei DistroWatch eine unbekanntere Distribution aus und stellen Sie kurz vor:
    Alleinstellungsmerkmale
  - Unterschiede zu Ubuntu
  - abgeleitet von
  - was lässt Vergangenheit erwarten
  - Entwicklergemeinde
  - Finanzierung



## Ergebnisse

Ergebnisse in [dieses Pad](https://pad.gwdg.de/ugs5kwPXRSuXjYRM0UISxA#)


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  Übung<br />
letzte Änderung: 2023-10-23<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>

