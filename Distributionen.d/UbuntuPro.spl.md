
# Ubuntu Pro

## Leistungen

- "[Security](https://ubuntu.com/pro) and compliance on top of Ubuntu LTS: 10 years of coverage for over 25,000 packages"
- Canonical verspricht 10 Jahre Patches für Sicherheitslücken mit Eintrag im Schwachstellenverzeichnis CVE (Common Vulnerabilities and Exposures) auszuliefern, also doppelt so lange wie die fünf Jahre für Ubuntu LTS (Long Term Support).
- Updates für die Repos Main, Universe
- Privatanwender und Unternehmen dürfen Ubuntu Pro auf bis zu 5 Rechnern kostenfrei nutzen.
- Ubuntu Pro ist für jedes Ubuntu LTS ab 16.04 LTS ­verfügbar.


## Doku
- [UU](https://wiki.ubuntuusers.de/Ubuntu_Pro/)
- [Get started with Ubuntu Pro](https://ubuntu.com/pro/tutorial) - Tutorial von Ubuntu
- c’t 2023, Heft 5, S.50

## do it
- [Registrieren](https://ubuntu.com/pro)
- Client installieren mit apt
- Token auf Rechner übertragen



---

<sub>
Autor: Helmut Hayd  
Schlagwörter:  Admin Security Ubuntu  
letzte Änderung: 2023-10-15, 11:34    
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
