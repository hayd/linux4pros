# Fragerunde zur Wiederholung


## Informationsquellen

1. Wie findet man die Infos zu einen installierten Programm, Befehl?
1. Gute Informationsquellen im Netz zum Thema Linux?
1. Woran erkennt man gute Informationsquellen?


## Unterschiede zwischen Linux und Windows

1. Was sagt dieses Bild über Unix/Linux aus?  ![Cray1](../Pics.d/cray1.jpg)
2. Nennen Sie die 3 wichtigsten Unterschiede zwischen Linux und Windows.
3. Steve Ballmer, Microsoft-CEO, 2001:  "*Linux ist ein Krebsgeschwür, das in Bezug auf geistiges Eigentum alles befällt, was es berührt.*"  
Warum würde Herr Ballmer das heute nicht mehr sagen?


## FLOSS

1. Welche Bezeichnung ist warum besser: Open Source - Freie Software?
2. Welche 4 Freiheiten sind mit "Freie" gemeint?
3. Wie heißt die wichtigste Lizenz für FLOSS?
4. Was ist das Copyleft-Prinzip?
5. Welche Kriterien sollte eine Freie Software erfüllen, die man installieren, nutzen will?


## Distributionen

1. Woraus besteht eine Distribution?
2. Nennen Sie die beiden wichtigsten Distributionen und ihre wichtigsten Unterschiede.
3. Was ist ein Rolling Release und seine größten Vor- und Nachteile?
4. Wenn kein triftiger Grund dagegen spricht verwende man die Installation Xyz.


## Pakete

1. Warum verwendet Ubuntu Software-Pakete in der Form von xyz.deb? [#.debs](https://www.debian.org/doc/manuals/debian-faq/basic-defs.en.html#whatisdebian)
2. Was sind dpkg und apt und wie sie hängen zusammen?
3. Was ist eine *flat file database* und wer nutzt sie?
4. Warum gibt es auch noch Snap- und Flatpak-Pakete?
5. Wie wollen diese ihr Ziel erreichen?
6. Was unterscheidet Snap- und Flatpak-Pakete?
7. Nennen Sie die 3 größten Nachteile von  Snap- und Flatpak-Paketen gegenüber .debs.
8. `$ sudo apt update && sudo apt upgrade`  Was macht dieser Befehl und warum ist es so wichtig?


## VFS

1. Was ist VFS?
1. Welche Vorteile bietet VFS?
1. Was steht in der Inode eines Files?


## Everything is a file

1. 8 Arten von Files, Streams?
1. Die 3 Standard-Datenströme eines Prozesses heissen?
1. Was sind: < > <<  | >> 
1. /dev/null , /dev/zero
1. Was ist /proc/ ?


## FHS

1. Unterschied /usr/bin vs. /bin ?
1. Was wird abgelegt in: /bin , /etc , /home , /tmp , /dev , ~/.config
1. Vorteile eine HDD?
1. Vorteile einer SSD, Nachteil einer SSD?
1. MTBF (Mean Time Between Failures), TBW (TeraBytes Written)


## SMART

1. Was ist das?
1. die wichtigsten Werte?
1. [Achtung Betrug: Gebrauchte Festplatten als neu verkauft ](https://www.heise.de/news/Gebrauchte-Seagate-Festplatten-als-Neuware-im-Umlauf-10254276.html)


## Filesysteme

1. Die wichtigsten Eigenschaften eines Filesystems?
1. Was zeichnet diese Filesysteme aus:
  1. ext4
  1. exFAT
  1. XFS
  1. ZFS
  1. NFS
  1. Samba
  1. Ceph
1. Was machen: cd cp ls mkdir mv pwd rm  ?
1. Welche Befehle können warum gefährlich werden?
1. Wozu sind df , du , baobab nützlich?

## Rechte im Filesystem

1. Warum so wichtig?
1. abc für xyz ?
1. Was bedeutet: chmod 724 a.sh ?
1. umask ?


## Storage-Clouds

1. Welche Clouds sind für Linux geeignet?
1. Worauf bei der Auswahl achten?


## Backup , Archiv

1. Unterschied Backup , Archiv ?
1. Wichtigster Grundsatz bei Backup?
1. Programme für Backup? Was ist bei der Auswahl wichtig?
1. HSM: Definition, Realisierung, Vorteile, Nutzung ?


------------------------------------




1. Was ist für eine Software für Notizen, Dokumentation wichtig?
2. Formatierung: Sinn und Unsinn
3. Was ist das besondere am Markdown?
4. Tools für Markdown?
5. Was zeichnet ein Wiki aus?
6. Komponenten eines Wikis?
7. Zim: Was ist das? Was kann das?
8. Recoll: Was ist das? Was kann das?
9. PDF: Was ist das?
10. Was macht ein PDF gefährlich?
11. Wie kann man ein PDF entschärfen?
12. Wie kann man PDFs editieren?
13. Nennen Sie 5 Tools für die Arbeit mit PDFs?
14. Was ist bei der Beschaffung eines Scanner für Linux zu beachten?
15. Was macht eine echte Digitalisierung eines Papierstapels aus?
16. Was ist OCR? Wie heisst das bekannteste Freie Programm für diesen Zweck?
17. Was ist CUPS?
18. Was ist pandoc?
19. Welches Office Paket hat welches Alleinstellungsmerkmale?
20. Welches Alleinstellungsmerkmal hat bzip2?

1. Programme um Bilder anzusehen?
2. Was ist Inkscape und welche Alternative gibt es?
3. Was kann GIMP und was nicht bez. Bildern?
4. Was ist ImageMagick?
5. Wofür ist ImageMagick geeignet (besser als GIMP)?
6. Welche Funktionen bietet ImageMagick?

  


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  <br />
letzte Änderung: 2025-02-12<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
