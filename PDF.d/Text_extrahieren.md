# Text extrahieren aus PDFs

## pdftotext (+)

- 1.Wahl
- [home](https://www.xpdfreader.com/pdftotext-man.html)
- pdftotext ist Teil von [Poppler](./div_Operationen.md)
- Optionen
  - `-layout`  for maintaining the original physical layout
    - liefert deutlich weniger Differenzen bei Vergleichen, die nur auf formelle Unterschiede zurückzuführen sind
- Beispiel
  - Text extrahieren, es wird xyz.txt erzeugt:  
  `$  pdftotext -layout  xyz.pdf`


## pdf2txt

- 2.Wahl
- Installation: `$ sudo apt install  python3-pdfminer`
- in Python
- Optionen:
  - `-n` Suppress layout analysis
  - `-A` Force layout analysis for all the text strings
  - `-o file` Output nach file und nicht nach stdout schreiben
- Beispiel: $ pdf2txt -n xyz.pdf > xyz.txt



## Pdf2Text

- 2.Wahl
- [home](https://www.pdftron.com/documentation/cli/guides/pdf2text/#why-pdf2text)
- nicht im Repo
- Intelligent text recognition and logical structure engine used to recognize words, lines, paragraphs, and the reading order in PDF documents.
- PDF2Text was from ground-up  designed to be run in high throughput server-based and multi-threaded applications. A regular and rigorous Q&A process sets high standards for the reliability of all PDFTron products.
- Advanced text recognition and content analysis
- `-A`    Force layout analysis ,  ist aber  schlechter als  `pdftotext -layout`


## siehe auch

- [PDF-Dateien extrahieren und katalogisieren](https://www.linux-community.de/ausgaben/linuxuser/2019/12/gemolken/) aus LinuxUser 12/2019,  Texte, Tabellen oder Bilder extrahieren



## Aufgaben

- Finde die Unterschiede in den Adobe EULAs aus den Jahren 2021 und 2022 im PDF tarball (/tmp/PDF-Spielwiese/PDF-Vergleich).
- Extrahiere Texte aus PDF mit pdftotext und pdf2txt mit jeweils verschiedenen Optionen und vergleiche Ergebnisse mit `kompare`
  - Wie erhält man den am besten leserlichen Text?
  - Wie erhält man den am besten durchsuchbaren Text?


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: PDF Software<br />
letzte Änderung: 2024-01-17<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>