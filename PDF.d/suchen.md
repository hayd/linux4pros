# Suchen in PDFs


## pdfgrep

- ein [grep](../Prgs.d/grep.md), das direkt mit PDFs arbeiten kann
- [home](https://pdfgrep.org/)
- im Repo

## Optionen

- `-i` ignore-case
- `-r` recursiv
- `-n` Seitennumer des Treffers


## Beispiele

- alle PDFs in einem Baum nach "Linux" durchsuchen:  
    `$ pdfgrep -r Linux *.{pdf,PDF}`

## siehe auch

- [bei UU](https://wiki.ubuntuusers.de/pdfgrep/)

## Aufgaben

Suchen Sie nach Jahreszahlen in den Adobe-EULAs des tarballs.


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: PDF Software<br />
letzte Änderung: 2023-11-20<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>