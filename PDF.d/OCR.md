# OCR

- OCR: **o**ptical **c**haracter **r**ecognition, Texterkennung
- Grundidee: Buchstaben als Muster erkennen und dann Worte in Wörterbüchern finden
- Ohne OCR sind eingescannte Text nur Bilder, also nicht durchsuchbare **Datenfriedhöfe**.


## PDF und OCR

- [PDF/A](https://de.wikipedia.org/wiki/PDF/A) ist ein Dateiformat zur Langzeitarchivierung digitaler Dokumente
- **mehrschichtiges** PDF/A: **Pixelbild** und dahinter ein **Text-Layer** mit dem OCR-Text, beide über Koordinaten verknüpft, über `Ctrl-a` im Viewer erkennbar


## OCR mit Tesseract

- Tesseract wurde 1984 bis 1994 von HP entwickelt, seit 2005 FOSS
  - Texterkennungsdaten für mehr als 100 Sprachen und viele Schriften, z.B. auch für [Fraktur](https://madoc.bib.uni-mannheim.de/53748/1/2019-11-18.pdf)
  - seit 2016 mit KI
- Grundlage von [Google Books](https://de.wikipedia.org/wiki/Google_Books): mehrere [Milliarden Bücher und Zeitschriften](https://praxistipps.chip.de/genauere-suche-bei-google-books-die-besten-tipps_11944) in mehr als 500 Sprachen
- [UU](https://wiki.ubuntuusers.de/tesseract-ocr/), [Introduction](https://tesseract-ocr.github.io/tessdoc/Installation.html), [user manual](https://github.com/tesseract-ocr/tessdoc/tree/main), [Finetuning von Tesseract-OCR](https://www.statworx.com/content-hub/blog/finetuning-von-tesseract-ocr-fuer-deutsche-rechnungen/)


### Installation

- via PPA um Version 5 zu bekommen und 2 Sprachdateien
  
```bash
$ sudo add-apt-repository ppa:alex-p/tesseract-ocr5
$ sudo apt update
$ sudo apt-get install tesseract-ocr tesseract-ocr-deu  tesseract-ocr-eng
```

### Gebrauch

- Syntax allg., Platzierung der Optionen unüblich:  
`tesseract image text [-l lang] [-psm pagesegmode] [configfile oder Optionen]`
  - [Page Segmentation Modes](https://planet.kde.org/quoc-hung-tran-2022-06-27-lt-weeks-1-2-gt-tesseract-page-segmentation-modes-psms-explained-and-their-relations/)
- 2-sprachigen Text "myscan.png" erkennen und in "myscan.ocr.txt" ablegen:  
`$ tesseract myscan.png myscan.ocr -l eng+deu`
- mit Text-Layer, "pdf" als finale Option genügt  
`$ tesseract eurotext.png  eurotext-eng -l eng pdf`


### Optimierungen

- Optimierung der Scann-Parameter (z.B. Helligkeit, Kontrast) mit [Skanlite](https://wiki.ubuntuusers.de/Skanlite/) (= [SANE](https://wiki.ubuntuusers.de/SANE/) light)
- Optimierung der Scans mit [unpaper](https://wiki.ubuntuusers.de/unpaper/): Flecken entfernen, Geradeziehen, Ränder erkennen, ...
- Optimierung der Scans mit [noteshrink](https://mzucker.github.io/2016/09/20/noteshrink.html): Transformation in anderen Farbraum (RGB --> HSV) und Clusteranalyse zwecks Farbauswahl  
   `$ pip install noteshrink`
- [OCRmyPDF](https://wiki.ubuntuusers.de/OCRmyPDF/) für die Anreicherung eines PDFs um eine Textebene:  
`$ ocrmypdf -l  deu+eng  in.pdf   deu_eng.ocr.out.pdf`  
Optionen: `--rotate-pages  --remove-background --deskew  --clean  --clean-final`
- Auflösung reduzieren, üblich sind 200dpi:  
`$ convert -density 200 ePB.jpg ePB.200.jpg`
- grobe Qualitätsvergleiche mit [hunspell](../Prgs.d/hunspell.md)


## Übungen

Ziel: Lernen, wie man sich aus diversen Komponenten an Hand von Test eine gute Lösung zusammenbaut.

- Besorgen Sie sich bei [https://t1p.de/6wyzh](https://t1p.de/6wyzh)  eine gute/schlechte/alte/zerknitterte Seitenansicht eines  eingescannten Buches.
- Machen Sie einen Screenshot davon mit [Flameshot](../Prgs.d/Screenshot.md).
- Erzeugen Sie daraus an PDF/A mit Textebene.
- Testen Sie die oben genannten Optimierungsvarianten.
- Lässt sich das beste Ergebnis automatisch erkennen?

Tragen Sie die Erkenntnisse in das [OCR-Pad](https://pad.gwdg.de/3DNoEKHHSziBeQL0_a7FVg#) ein.


## siehe auch

- [PDF/A kompakt](https://pdfa.org/wp-content/until2016_uploads/2011/08/PDFA_kompakt_pdfa1b.pdf) - kompakt = 87 Seiten, Digitale Langzeitarchivierung mit PDF

---


<sub>
Autor: Helmut Hayd<br />
Schlagwörter: PDF Software Informationsmanagement <br />
letzte Änderung: 2025-02-03<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
