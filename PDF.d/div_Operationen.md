# Operationen mit PDFs


## Poppler

- [Poppler](https://de.wikipedia.org/wiki/Poppler) ist eine freie PDF rendering library
- wird u.a. genutzt von: [Evince](./Viewer.md) , [TeXstudio](https://www.texstudio.org/) , [Inkscape](https://de.wikipedia.org/wiki/Inkscape)
- [poppler-utils](https://wiki.ubuntuusers.de/poppler-utils/) ist eine Sammlung von Kommandozeilen-Programmen für Operationen an PDFs
- im Repo


## JavaScript?

- Abfrage, ob ein PDF JavaScript enthält:  
`$ pdfinfo javascript-test.pdf | grep JavaScript`
- Testen Sie ein [Formular](https://www.bundesregierung.de/resource/blob/974430/1797446/bd6287f57d296faa8c8b34923f6e8122/2020-10-09-antragsformular-prod-foerd-kurzfilm-2021-data.pdf?download=1) der Bundesregierung auf seinen JavaScript-Gehalt.



## Seiten zählen

- **pdfinfo** liefert Informationen über ein PDF, z.B: JavaScript, #Seiten, Page size, PDF version, ...
- Zahl der Seiten bestimmen:  
`$ pdfinfo xyz.pdf | grep Pages`


## PDFs zusammenhängen

- Seiten zusammenhängen:  
`$ pdfunite [0-9]*.pdf all.pdf`


## Einsatz von Fonts

- Bestimmung der verwendeten Fonts:  
`$ pdffonts  xyz.pdf`
- Oft werden lizensierte Fonts eingesetzt. Diese können [Ärger](https://www.heise.de/news/Neue-Abmahnwelle-Wieder-gehen-Schreiben-wegen-Google-Fonts-raus-7322064.html) bereiten.


---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: PDF Software<br />
letzte Änderung: 2025-02-05<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
