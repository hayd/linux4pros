# PFDs editieren, Formulare ausfüllen

Das Ausfüllen von Formularen ist unter Linux oft ein größeres Problem, da die Formulare meist von Adobe-Tools für Adobe Programme geschrieben wurden. 

Problematische, teure Adobe-Lizenzen, offene Standards, digitale Selbstbestimmung - was ist das?

## inkscape

- [Inkscape](https://de.wikipedia.org/wiki/Inkscape) kann alles in einem ungeschützten PDF verändern. Bisher nur seitenweise, Änderung in Sicht.
- Eine formular-interne Logik wird natürlich ignoriert.


## Firefox

- [Version 119](https://www.soeren-hentzschel.at/firefox/firefox-119-erlaubt-das-einfuegen-von-bildern-in-pdf-dateien/): Der PDF-Betrachter von Firefox kann längst nicht mehr nur zum reinen Betrachten von PDF-Dateien, sondern auch zum Ausfüllen von PDF-Formularen oder zum Ergänzen von Text und Zeichnungen genutzt werden. Firefox 119 bringt zusätzlich die Möglichkeit, Bilder einzufügen.

## PDF24

- [PDF24](https://www.pdf24.org/de/about-us)  bietet kostenlose und einfach zu benutzende PDF-Lösungen für die PDF-Bearbeitung, online und als Software zum Download.
- bei Online-Nutzung DSGVO beachten
- die [Tools](https://tools.pdf24.org/de/)
- [keine Angaben](https://help.pdf24.org/de/fragen/frage/geschaeftsmodell/) zur Finanzierung, sondern: [Geldprobleme haben wir wirklich nicht.](https://help.pdf24.org/de/fragen/frage/wie-kann-ich-an-pdf24-spenden-zahlen/)
- siehe auch [WP](https://de.wikipedia.org/wiki/PDF24_Creator)


## siehe auch

- [Master PDF Editor for Linux](https://code-industry.net/free-pdf-editor/#get) Eigenwerbung: "Edit PDF documents. The best solution for working with PDF files in Linux."
  - kompliziert zu bedienen, stürzt ab
  - [bei UU](https://wiki.ubuntuusers.de/Master_PDF_Editor/)


## Aufgaben
  
- Versuchen Sie eines der Formulare in /tmp/PDF-Spielwiese/PDF-Formulare des tarballs auszufüllen.
- Womit kann man die Formulare am besten ausfüllen: inkscape, Firefox oder PDF24?
  - gut heisst: geringer Arbeitsaufwand, keine seltsamen Fonts oder Schriftgrößen, Inhalt und Aussehen bleiben beim Abspeichern erhalten

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  PDF Software<br />
letzte Änderung: 2024-01-17<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>