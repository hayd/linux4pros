# PDF Viewer

## Es gibt nicht nur Adobe

- [FSFE gegen Adobe-Werbung auf Regierungs-Websites](https://www.heise.de/news/FSFE-gegen-Adobe-Werbung-auf-Regierungs-Websites-1077816.html)
- [Freie PDF-Betrachter](https://fsfe.org/activities/pdfreaders/pdfreaders.de.html)



## MuPDF

- [home](https://www.mupdf.com/), [Code](https://github.com/ArtifexSoftware/mupdf), [UU](https://wiki.ubuntuusers.de/MuPDF/)
- Fokus  auf **Schnelligkeit** und geringem Ressourcenbedarf
- in **C** geschrieben
- **Renderer** für hohe Darstellungsqualität (anti-aliased, fractions of a pixel)
- verwendet eigenen Renderer, nicht die Bibliothek  [Poppler](https://de.wikipedia.org/wiki/Poppler), daher 70% - 200% schneller
- **Epub**-Dokumente unterstützt
- gesteuert wird MuPDF nur mit der **Tastatur** (s.u.), keine Menüs
- kann **nicht drucken**


### Optionen

- `-r 130`  Resolution; default: 72, 130 füllt Hochkant-Monitor
- `-p GeHeim`  Passwort für verschlüsseltes PDF


### Key Bindings

Die Key Bindings sind direkt in das Viewer-Fenster einzugeben.

- `+ -`  vergrößern verkleinern
- `123g`  springt zur Seite 123
- `/wosteht`  Suche nach "wosteht"
- `n N` Weitersuchen vorwärts, rückwärts
- `L R`  Seite drehen
- `w W` Breite anpassen: des Fensters , des PDFs

### siehe auch

- [bei UU](https://wiki.ubuntuusers.de/MuPDF/)



## Evince

- [home](https://wiki.gnome.org/Apps/Evince)
- kommt Viewer von Adobe rel. nahe
- Standard-Dokumentenbetrachter von GNOME
- in C, C++ geschrieben, verwendet [Poppler](https://de.wikipedia.org/wiki/Poppler) (freie Programmbibliothek für Linux zur Anzeige von PDF-Dateien)
- darstellbare Formate:     pdf, ps, djvu, tiff, dvi, Bilder
- unterstützt das Ausfüllen von einfachen PDF-Formularen
- bietet Vorschaubilder


## Viewer und JavaScript

JavaScript kann gefährlich werden. Daher sollte man ein PDF immer zuerst in einem Viewer ohne JavaScript-Unterstützung ansehen. Diese kann man möglicherweise ein-/ausschalten und kann in neueren Version dazukommen.


## Aufgaben

- Test-PDF [javascript-test.pdf](./javascript-test.pdf) herunterladen und mit mupdf, evince, Firefox testen


## siehe auch

- [eine Programmübersicht](https://wiki.ubuntuusers.de/PDF/) - PDF-Dateien anzeigen, kommentieren und Formulare ausfüllen
- [Xournal – Handschriftliche Notizen am Bildschirm unter Linux](https://linux-bibel.at/index.php/2023/09/23/xournal-handschriftliche-notizen-am-bildschirm-unter-linux/)



---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter:  PDF Software<br />
letzte Änderung: 2024-11-20<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
