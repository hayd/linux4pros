# PDF

## Allgemeines

- PDF (Portable Document Format), ein **plattformunabhängiges** Dateiformat, das 1992 von Adobe entwickelt wurde
- Ziel: ein Dateiformat für elektronische Schriftstücke, das unabhängig von Erzeugung und Ausgabe **immer gleich** aussieht
- PDFs können unter Linux zu Problem werden, besonders Formulare. Wenn diese nur mit und für Adobe-Software mit viel interner Logik erstellt wurden, ist Ärger vorprogrammiert.
  - [»Open Source hängt zum Beispiel bei der Verarbeitung von PDFs klar hinterher.«](https://www.heise.de/hintergrund/IT-Leiter-der-Stadt-Schwaebisch-Hall-Unter-Linux-leidet-die-Produktivitaet-6678160.html)


- Ein PDF ist ein **Container**, der neben dem Sichtbaren auch noch enthält:
  - [ausführbaren Code](https://www.heise.de/hintergrund/Analysiert-Alte-Masche-neue-Verpackung-PDFs-als-Angriffsvektor-3722708.html) (JavaScript)
    - z.B: [Tetris](https://th0mas.nl/downloads/pdftris.pdf), [Details](https://www.golem.de/news/per-browser-spielbar-entwickler-portiert-tetris-in-ein-pdf-2501-192340.html)
    - z.B [Doom](https://doompdf.pages.dev/doom.pdf)
  - Trailer Dictionary mit grundlegenden Infos
  - Document Information Dictionary mit ctime, mtime, Titel, Keywords, Author, Erstellungsprogramm, ...
  - Document Catalog: die Seiten
  - [mehr](https://www.oreilly.com/library/view/pdf-explained/9781449321581/ch04.html)
- Die PDF-Referenzdokumentation, die das Potenzial dieses sehr leistungsfähigen und vielseitigen Dateiformats beschreibt, umfasst mehr als 800 (!) Seiten.


## Gefahren

- PDF erlaubt einige zusätzliche Funktionen – insbesondere interaktive Elemente wie Lesezeichen, Kommentare, Formularfelder und deren Programmierung mit **JavaScript**. Das Ausführen von [JavaScript-Codes](https://www.heise.de/hintergrund/Analysiert-Alte-Masche-neue-Verpackung-PDFs-als-Angriffsvektor-3722708.html)  ist möglich.
- Und es zeichnet sich ein eindeutiger Trend ab: PDF-Dokumente als bevorzugte Waffe von Cyberkriminellen.
- tolle Kombi


### Gefahren-Abwehr

- Seit [Version 88](https://www.sueddeutsche.de/wissen/technik-browser-sicherheit-firefox-javascript-in-pdfs-verbieten-dpa.urn-newsml-dpa-com-20090101-210526-99-749401) unterstützt Firefox auch Javascript in PDF-Dokumenten.
  - "about:config" >  "pdfjs.enableScripting" > "false"
- Man nehme einen dummen PDF-Viewer, der kein JavaScript beherrscht.
- Die JavaScript-Verfügbarkeit kann sich bei einem Update (unbemerkt) ändern. Daher prüfen.


## Arbeiten an und mit PDFs

Laden Sie für die folgenden Aufgaben den tarball [PDF-Spielwiese.tar.bzip2](../Materialien.d/PDF-Spielwiese.tar.bzip2) nach /tmp herunter und packen ihn aus:

```bash
cd /tmp
tar -xvjf PDF-Spielwiese.tar.bzip2
```

- [Viewer](./Viewer.md) - ansehen von PDFs
- [PDF-Cutter](./PDF-Cutter.md) - Seiten ausschneiden (und mehr)
- [PDF-Editoren](./PDF-Editoren.md)
- [Text extrahieren](./Text_extrahieren.md)
- [Suchen in PDFs](./suchen.md)
- [diverse Operationen](./div_Operationen.md) an PDFs


## Misc

[Spaßprojekt: Entwicklerin erstellt PDF-Dokument in der Größe der Welt](https://www.golem.de/news/spassprojekt-mann-erstellt-pdf-dokument-in-der-groesse-der-welt-2402-181844.html)



---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: PDF <br />
letzte Änderung: 2025-01-17<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
