# PDF-Cutter

Programme, die aus einem großen PDF einzelne Seiten, ganze Artikel ausschneiden

## qpdf

### Allgemeines

- [home](https://qpdf.sourceforge.io/) , [Doc](https://qpdf.readthedocs.io/en/stable/) , [source](https://github.com/qpdf/qpdf/releases) , [bei WP](https://en.wikipedia.org/wiki/QPDF)
- nur **CLI**
- performs content-preserving transformations on PDF files
- in C++
- als .deb im Repo


### Optionen

- `--empty` : Metadaten (info, outlines, page numbers, etc.) weglassen
- `--pages` : danach kommen page selection flag
- `--`  terminates parsing of page selection flags
- page ranges: 1,3,5-9,15-12


### Beispiele

- PDF in einzelne Seiten zerlegen, die Seiten heissen  seite-01.pdf, seite-02.pdf, usw.  
  `$ qpdf input.pdf --split-pages seite.pdf`
- einzelne Seiten extrahieren ohne Metadaten:  
  `$ qpdf --empty  --pages infile.pdf  1-10,17,19 -- some_pages.pdf`
- Passwort für weiteren Gebrauch entfernen, man muss es aber kennen:  
  `$ qpdf --password=GeHeim --decrypt input.pdf output.pdf`


### siehe auch

- [PDFs zerlegen, zusammenfügen und Passwort entfernen mit qpdf](https://sebstein.hpfsc.de/2019/01/23/pdfs-zerlegen-zusammenfuegen-und-passwort-entfernen-mit-qpdf/)

### Aufgabe

- ein Script, das qpdf vereinfacht:
  
```sh

USAGE='usage :  qpdfpp  in.pdf  PAGES  name of output

extract pages from pdf

Examples:
  qpdfpp  in.pdf 9-14   out
  qpdfpp  in.pdf 9-14,18,20    ganz wichtiger Text

No  " ", no .pdf  necessary for the words of the output name.  
'
```

## PDF Arranger

- [home, source](https://github.com/pdfarranger/pdfarranger) , [bei UU](https://wiki.ubuntuusers.de/PDF_Arranger/)
- in Python mit GTK
- als .deb im Repo
- nur **GUI**
- Gebrauch:
  - PDF öffnen: `$ pdfarranger xyz.pdf`
  - Seiten wie üblich selektieren (Maus + Ctrl/Shift)
  - abspeichern:  `File > Export selection`  
- kann auch: Seiten entfernen, drehen, umsortieren oder beschneiden, merge PDFs
- siehe auch: [PDF Arranger: Merge, Split, Rotate, Crop Or Rearrange PDF Documents](https://www.linuxuprising.com/2018/12/pdfarranger-merge-split-rotate-crop-or.html)


## Aufgabe

- [Linux-Journal-2019-08.pdf](https://github.com/datawookie/linux-journal/blob/master/Linux-Journal-2019-08.pdf) herunterladen und den Artikel "Digging Through the DevOps Arsenal: Introducing Ansible"  ausschneiden mit beiden Programmen. qpdf mit der Option "--empty" verwenden.
- Um wie viel unterscheiden sich die Größen der beiden Artikel-Varianten?

---

<sub>
Autor: Helmut Hayd<br />
Schlagwörter: PDF Software<br />
letzte Änderung: 2024-11-20<br />
Der Text ist unter der Lizenz [Creative-Commons Namensnennung – Weitergabe unter gleichen Bedingungen](https://creativecommons.org/licenses/by-sa/4.0/deed.de) verfügbar.<br />  
![cc-by-sa](../Pics.d/cc-by-sa.120.png)
</sub>
